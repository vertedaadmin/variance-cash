﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Services;
using System.Linq;
using Microsoft.Win32;
using MenuSync;
using System.IO;
using System.Threading;
using System.Xml;
using Bugsnag.Clients;
using System.Reflection;
using System.Net;

namespace VarianceWebService
{
    //Last updated 16/2/2017 - Sarah Phillips

    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class VarianceWebService : System.Web.Services.WebService
    {
        // bugsnag functions added by TS
        // this API is for the solution and really should be held in a config file.
        public Bugsnag.Clients.BaseClient bugsnag = new BaseClient("e971d4bc9ee3f926c53adab068835323");


        public void Primo_SendBugsnag(string issueDescription, Exception ex, string snagType)
        {
            try
            {

                // Bugsnag.Clients.BaseClient bugsnag = new BaseClient(myConfig.SnagKey);

                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

                var metadata = new Bugsnag.Metadata();
                metadata.AddToTab("Additional Info", "Description", issueDescription);


                for (int i = 0; i < localIPs.Length; i++)
                {
                    if (!IPAddress.IsLoopback(localIPs[i]) && localIPs[i].AddressFamily.ToString().IndexOf("V6") == -1)
                    {
                        metadata.AddToTab("Additional Info", "IP Address", localIPs[i].ToString());
                    }
                }

                if (ex != null)
                {
                    metadata.AddToTab("Additional Info", "Error message", ex.Message);
                    metadata.AddToTab("Additional Info", "Full Stack Trace", ex.StackTrace);
                }
                


                bugsnag.Config.SetUser("Manchester City FC", "", "Manchester City FC");
                bugsnag.Config.AppVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

                bugsnag.Notify(new ArgumentException(snagType), metadata);

            }
            catch (Exception jex)
            { }
        }


       

        // end of bugsnag


        #region Logging
        public VarianceWebService()
        {
            String path = HttpContext.Current.Server.MapPath("~/"); //Server.MapPath(".");               

            //doc.Load(path + "\\primo_services\\system_config\\primo_stock_management_config.xml");

            string configPath = path + "\\VarianceCashLog\\";

            //replace default log directory if IG is installed
            //try
            //{
            //    RegistryKey igKey = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("InfoGenesis");
            //    configPath = igKey.GetValue("InfoGenesisPath").ToString() + "\\Log_dir\\";
            //}
            //catch { }

            try
            {
                if (File.Exists(configPath + "VarCash.log"))
                {
                    VertedaDiagnostics.OpenLog(configPath, "VarCash.log");
                }
                else
                {
                    //create a blank file and then open
                    File.Create(configPath + "VarCash.log").Dispose();
                    VertedaDiagnostics.OpenLog(configPath, "VarCash.log");
                }

                VertedaDiagnostics.LogMessage("Variance WebService Reporting Started", "I", false);
            }
            catch { } 
        }

        #endregion

        #region Terminal & Profit Migration
        [WebMethod]
        public VarianceActivityResponse UpdateLocations()
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                VertedaDiagnostics.LogMessage("Terminal & location migration started", "I", false);

                List<ProfitCenterValues> listProfits = ReadProfits();
                VertedaDiagnostics.LogMessage("Terminal & location migration: Profit centers read", "I", false);

                List<LocationValues> listLocations = ReadLocationMaster();
                VertedaDiagnostics.LogMessage("Terminal & location migration: existing locations read read", "I", false);

                List<TerminalValues> listTerminals = ReadTerminals();
                VertedaDiagnostics.LogMessage("Terminal & location migration: terminals read", "I", false);

                DeleteTerminals();
                VertedaDiagnostics.LogMessage("Terminal & location migration: old terminals deleted", "I", false);
                
                InsertTerminals(listTerminals);
                VertedaDiagnostics.LogMessage("Terminal & location migration: new terminals added", "I", false);

                CalculateProfits(listProfits, listLocations);
                VertedaDiagnostics.LogMessage("Terminal & location migration: new locations added", "I", false);

                VAR.status = true;
                VertedaDiagnostics.LogMessage("Terminal & location migration: Profit centers read", "I", false);
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error whilst migrating terminals & locations: " + e.ToString(), "E", false);
            }

            return VAR;
        }

        public static void CalculateProfits(List<ProfitCenterValues> lp, List<LocationValues> ll)
        {
            //for each profit center not in locations
            //add profit to locations
            try
            {
                foreach (ProfitCenterValues pcv in lp)
                {
                    //bool containshead = ll.Any(p => p.Transaction_Data_ID == cr.Transaction_Data_ID);
                    bool exists = ll.Any(p => p.prof_center_id == pcv.prof_center_id);

                    if (!exists)
                    {
                        Location_Manager(pcv.prof_center_id, pcv.prof_center_name, 1);
                    }
                }

                VertedaDiagnostics.LogMessage("Profit centers not in venue_location_master migrated across", "I", false);
                //for each locaiton not in profits
                //delete location

                foreach (LocationValues lv in ll)
                {
                    //bool containshead = ll.Any(p => p.Transaction_Data_ID == cr.Transaction_Data_ID);
                    bool exists = lp.Any(p => p.prof_center_id == lv.prof_center_id);

                    if (!exists)
                    {
                        Location_Manager(lv.prof_center_id, lv.prof_center_name, 2);
                    }
                }
                VertedaDiagnostics.LogMessage("locations in venue_location_master that are no longer profit centers have been deleted", "I", false);
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in CalculateProfits: " + e.ToString(), "E", false);
            }
        }
        
        private static void Location_Manager(int loc_ID, string loc_name, int mode)
        {
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            ConnectionString con_str = GetConnectionString(2);
            conn.ConnectionString = con_str.connection_string;


            if (mode == 1)
            {
                //create - insert
                cmd.CommandText = "SP_venue_location_master_InsertLocation_Plus";
                cmd.Parameters.Add("@location_id", SqlDbType.Int).Value = loc_ID;
                cmd.Parameters.Add("@location_name", SqlDbType.VarChar).Value = loc_name;
                cmd.Parameters.Add("@is_warehouse", SqlDbType.Int).Value = 0;
            }
            else if (mode == 2)
            {
                //delete - delete
                cmd.CommandText = "SP_venue_location_master_DeleteLocation";
                cmd.Parameters.Add("@location_id", SqlDbType.Int).Value = loc_ID;
            }

            cmd.Connection = conn;

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                VertedaDiagnostics.LogMessage("Location_Manager Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VertedaDiagnostics.LogMessage("Error in Location_Manager: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
        
        //read IGPOS440 profit centers
        public static List<ProfitCenterValues> ReadProfits() 
        {
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();


            List<ProfitCenterValues> profit_list = new List<ProfitCenterValues>();

            ConnectionString con_str = GetConnectionString(4);
            conn.ConnectionString = con_str.connection_string;

            try
            {
                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    //cmd.CommandText = "SELECT [profit_center_dim_id], [profit_center_id], ISNULL([profit_center_name], 'NULL') AS [profit_center_name] FROM [ig_dimension].[dbo].[Profit_Center_Dimension] WHERE [eff_date_to] is NULL";// LOAD PROFIT CENTERS
                    cmd.CommandText = "SELECT [profit_center_id] ,ISNULL([profit_center_name], 'NULL') AS [profit_center_name] FROM [it_cfg].[dbo].[Profit_Center_Master]";// LOAD PROFIT CENTERS

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {


                        ProfitCenterValues item = new ProfitCenterValues();                        
                        item.prof_center_id = Convert.ToInt32(rdr["profit_center_id"].ToString());
                        item.prof_center_name = rdr["profit_center_name"].ToString();


                        profit_list.Add(item);
                    }
                    rdr.Close();

                    conn.Close();
                }
                VertedaDiagnostics.LogMessage("ReadProfits Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in ReadProfits: " + e.ToString(), "E", false);
            }
            return profit_list;
        }
        
        //read rts_v4 locations
        public static List<LocationValues> ReadLocationMaster()
        {
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            List<LocationValues> location_list = new List<LocationValues>();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }

                

                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [id],[location_name],ISNULL([is_warehouse], -1) AS [is_warehouse] FROM ["+ con_str.database_name + "].[dbo].[venue_location_master]";// LOAD PROFIT CENTERS

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        LocationValues item = new LocationValues();
                        item.prof_center_id = Convert.ToInt32(rdr["id"].ToString());
                        item.prof_center_name = rdr["location_name"].ToString();
                        item.is_warehouse = Convert.ToInt32(rdr["is_warehouse"].ToString());

                        location_list.Add(item);
                    }
                    rdr.Close();

                    conn.Close();
                }
                VertedaDiagnostics.LogMessage("ReadLocationMaster Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in ReadLocationMaster: " + e.ToString(), "E", false);
            }
            return location_list;
        }
        
        //read IGPOS440 terminals
        public static List<TerminalValues> ReadTerminals()
        {
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            List<TerminalValues> terminal_list = new List<TerminalValues>();

            try
            {
                ConnectionString con_str = GetConnectionString(4);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    
                    cmd.CommandText = "SELECT ISNULL([primary_profit_center_id], -1) AS [primary_profit_center_id],[term_id] ,ISNULL([term_name], 'NULL') AS [term_name]  FROM [it_cfg].[dbo].[Terminal_Master]";

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {


                        TerminalValues item = new TerminalValues();
                        item.terminal_id = Convert.ToInt32(rdr["term_id"].ToString());                        
                        item.prof_center_id = Convert.ToInt32(rdr["primary_profit_center_id"].ToString());                        
                        item.terminal_name = rdr["term_name"].ToString();



                        terminal_list.Add(item);
                    }
                    rdr.Close();

                    conn.Close();
                }
                VertedaDiagnostics.LogMessage("ReadTerminals Completed successfully", "I", false);
            }
            catch(Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in ReadTerminals: " + e.ToString(), "E", false); 
            }
            return terminal_list;
        }
        
        //stored procedure to delete old values 
        public static void DeleteTerminals()
        {
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {

                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "DELETE FROM [dbo].[Venue_Terminal_Master]";

                    SqlDataReader rdr = cmd.ExecuteReader();

                    rdr.Close();

                    conn.Close();
                }
                VertedaDiagnostics.LogMessage("DeleteTerminals Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in DeleteTerminals: " + e.ToString(), "E", false);
            }
        }
        
        //stored procedure to insert new values 
        public static void InsertTerminals(List<TerminalValues> tlist)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.AddRange(new DataColumn[5] { new DataColumn("terminal_id", typeof(int)),
                    new DataColumn("terminal_dim_id", typeof(int)),
                    new DataColumn("profit_center_id", typeof(int)),
                    new DataColumn("profit_center_dim_id", typeof(int)),
                    new DataColumn("terminal_name",typeof(string)) });

                foreach (TerminalValues pcv in tlist)
                {
                    dt.Rows.Add(pcv.terminal_id,
                                pcv.terminal_dim_id,
                                pcv.prof_center_id,
                                pcv.prof_center_dim_id,
                                pcv.terminal_name);
                }
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        ConnectionString con_str = GetConnectionString(2);

                        using (SqlConnection con = new SqlConnection(con_str.connection_string))
                        {
                            using (SqlCommand cmd = new SqlCommand("Insert_Terminals"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@tblTerminals", dt);
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                        VertedaDiagnostics.LogMessage("InsertTerminals Completed successfully", "I", false);
                    }
                    catch (Exception e)
                    {
                        VertedaDiagnostics.LogMessage("Error in InsertTerminals: " + e.ToString(), "E", false);
                    }
                }
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in InsertTerminals: " + e.ToString(), "E", false);
            }
        }


        //read terminals
        public static List<TerminalValues> ReadTerminalsForInputLocations(List<int> profit_center_ids)
        {
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            List<TerminalValues> terminal_list = new List<TerminalValues>();

            string profit_centers = string.Join(",",profit_center_ids);

            try
            {
                ConnectionString con_str = GetConnectionString(4);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;


                    cmd.CommandText = "SELECT ISNULL([primary_profit_center_id], -1) AS [primary_profit_center_id],[term_id] ,ISNULL([term_name], 'NULL') AS [term_name]  FROM [it_cfg].[dbo].[Terminal_Master] WHERE [primary_profit_center_id] IN (" + profit_centers + ")";

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {


                        TerminalValues item = new TerminalValues();
                        item.terminal_id = Convert.ToInt32(rdr["term_id"].ToString());
                        item.prof_center_id = Convert.ToInt32(rdr["primary_profit_center_id"].ToString());
                        item.terminal_name = rdr["term_name"].ToString();



                        terminal_list.Add(item);
                    }
                    rdr.Close();

                    conn.Close();
                }
                VertedaDiagnostics.LogMessage("ReadTerminalsForInputLocations Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in ReadTerminalsForInputLocations: " + e.ToString(), "E", false);
            }
            return terminal_list;
        }
        



        #endregion

        #region Bag composition - COMPLETE 

        [WebMethod]
        public VarianceActivityResponse InsertComposition(string composition_name, List<Denominations> input_denominations)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            decimal total = 0;

            try
            {
                VarianceActivityResponseCompositions ExistingCompositions = Get_Existing_Composition_Name_Matches(composition_name);
                int numberOfExistingCompositions = ExistingCompositions.Compositions.Count();

                if (numberOfExistingCompositions == 0)
                {
                    foreach (Denominations den in input_denominations)
                    {
                        total += (den.denomination_value * den.denomination_qty);
                    }

                    VarianceActivityResponse var1 = CreateComposition(composition_name, total);
                    int id = var1.ID;


                    VarianceActivityResponse var2 = CreateCompositionDetails(id, input_denominations);

                    if (var1.status == false || var2.status == false)
                    {
                        VAR.status = false;
                        VAR.errorText = "Error when deleting composition records";
                        VertedaDiagnostics.LogMessage("Error when deleting composition records", "E", false);
                    }
                    else
                    {
                        VAR.status = true;
                    }


                    VAR.status = true;
                    VAR.ID = id;
                    VertedaDiagnostics.LogMessage("InsertComposition Completed successfully", "I", false);
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "A composition with this name already exists. Id: " + ExistingCompositions.Compositions[0].composition_id + " Name: " + ExistingCompositions.Compositions[0].composition_name;
                    VertedaDiagnostics.LogMessage("A composition with this name already exists. Id: " + ExistingCompositions.Compositions[0].composition_id + " Name: " + ExistingCompositions.Compositions[0].composition_name, "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertComposition: " + e.ToString(), "E", false);

            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse CreateComposition(string composition_name, decimal composition_total)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
            conn.ConnectionString = con_str.connection_string;

            //edit existing record
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Composition_Dim_Insert";
            cmd.Parameters.Add("@total_value", SqlDbType.Money).Value = composition_total;
            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = composition_name;
            cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Connection = conn;
            //return package_id to use in copy

            
                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.ID = Convert.ToInt32(cmd.Parameters["@id"].Value.ToString());
                VAR.status = true;
                VertedaDiagnostics.LogMessage("CreateComposition Completed successfully", "I", false);

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in CreateComposition: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse CreateCompositionDetails(int composition_id, List<Denominations> denominations)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            foreach (Denominations dr in denominations)
            {
                //Using Output parameter
                SqlConnection conn = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                try
                {
                    ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Composition_Denomination_Join_Insert";
                cmd.Parameters.Add("@comp_id", SqlDbType.Int).Value = composition_id;
                cmd.Parameters.Add("@denom_id", SqlDbType.Int).Value = dr.denomination_id;
                cmd.Parameters.Add("@denom_qty", SqlDbType.Int).Value = dr.denomination_qty;
                cmd.Parameters.Add("@unid", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Connection = conn;
                //return package_id to use in copy

                
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    //VAR.ID = Convert.ToInt32(cmd.Parameters["@unid"].Value.ToString()); returning 1 ID is pointless. Could return list of all created unids, but unlikely to be required
                    VAR.status = true;

                }
                catch (Exception ex)
                {
                    VAR.status = false;
                    VAR.errorText = ex.ToString();
                    VertedaDiagnostics.LogMessage("Error in CreateCompositionDetails: " + ex.ToString(), "E", false);
                    

                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                    VertedaDiagnostics.LogMessage("CreateCompositionDetails Completed successfully", "I", false);
                }
            }

            return VAR;
        }



        [WebMethod]
        public VarianceActivityResponse UpdateComposition(int composition_id, List<Denominations> input_denominations, string composition_name)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            decimal total = 0;

            try
            {
                foreach (Denominations den in input_denominations)
                {
                    total += (den.denomination_value * den.denomination_qty);
                }

                VarianceActivityResponse var1 = AmendComposition(composition_id, total, composition_name);
                VarianceActivityResponse var2 = AmendCompositionDetails(composition_id, input_denominations);

                if (var1.status == false || var2.status == false)
                {
                    VAR.status = false;
                    VAR.errorText = "Error when amending composition records";
                }
                else
                {
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("UpdateComposition Completed successfully", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateComposition: " + e.ToString(), "E", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse AmendCompositionDetails(int composition_id, List<Denominations> input_denominations)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            foreach (Denominations dr in input_denominations)
            {
                //Using Output parameter
                SqlConnection conn = new SqlConnection();
                SqlCommand cmd = new SqlCommand();

                try { 

                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Composition_Denomination_Join_Update";
                cmd.Parameters.Add("@comp_id", SqlDbType.Int).Value = composition_id;
                cmd.Parameters.Add("@denom_id", SqlDbType.Int).Value = dr.denomination_id;
                cmd.Parameters.Add("@denom_qty", SqlDbType.Int).Value = dr.denomination_qty;
                cmd.Connection = conn;
                //return package_id to use in copy

                
                
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("AmendCompositionDetails Completed successfully", "I", false);

                }
                catch (Exception ex)
                {
                    VAR.status = false;
                    VAR.errorText = ex.ToString();
                    VertedaDiagnostics.LogMessage("Error in AmendCompositionDetails: " + ex.ToString(), "E", false);
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse AmendComposition(int composition_id, decimal composition_total, string composition_name)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try { 
            ConnectionString con_str = GetConnectionString(2);
            conn.ConnectionString = con_str.connection_string;

            //edit existing record
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Composition_Dim_Update";
            cmd.Parameters.Add("@comp_id", SqlDbType.Int).Value = composition_id;
            cmd.Parameters.Add("@total", SqlDbType.Money).Value = composition_total;
            cmd.Parameters.Add("@compName", SqlDbType.VarChar).Value = composition_name;
            cmd.Connection = conn;
            //return package_id to use in copy

     
                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.status = true;
                VertedaDiagnostics.LogMessage("AmendComposition Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in AmendComposition: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }
                


        [WebMethod]
        public VarianceActivityResponse RemoveComposition(int composition_id)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {

                VarianceActivityResponse var1 = DeleteComposition(composition_id);
                VarianceActivityResponse var2 = DeleteCompositionDetails(composition_id);

                if (var1.status == false || var2.status == false)
                {
                    VAR.status = false;
                    VAR.errorText = "Error when deleting composition records";
                    VertedaDiagnostics.LogMessage("Error when deleting composition records", "E", false);
                }
                else
                {
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("RemoveComposition Completed successfully", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in RemoveComposition: " + e.ToString(), "E", false);
            }

            return VAR;
        }
        
        [WebMethod]
        public VarianceActivityResponse DeleteComposition(int composition_id)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try { 
            ConnectionString con_str = GetConnectionString(2);
            conn.ConnectionString = con_str.connection_string;

            //edit existing record
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Composition_Dim_Delete";
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = composition_id;
            cmd.Connection = conn;
            //return package_id to use in copy

                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.status = true;
                VertedaDiagnostics.LogMessage("DeleteComposition Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in DeleteComposition: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse DeleteCompositionDetails(int composition_id)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try { 
            ConnectionString con_str = GetConnectionString(2);
            conn.ConnectionString = con_str.connection_string;

            //edit existing record
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Composition_Denomination_Join_Delete";
            cmd.Parameters.Add("@comp_id", SqlDbType.Int).Value = composition_id;
            cmd.Connection = conn;
            //return package_id to use in copy

  
                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.status = true;
                VertedaDiagnostics.LogMessage("DeleteCompositionDetails Completed successfully", "I", false);
            }
            catch (Exception ex)
            {               
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in DeleteCompositionDetails: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }


        #endregion 

        #region Bag - location joins - Bag composition - COMPLETE 
        [WebMethod]
        public VarianceActivityResponse DeleteMultipleLocationDefaultCompositions()
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            
            try
            {
                //get list of all locations
                List<LocationValues> locations = ReadLocationMaster();
                List<int> locationIDs = new List<int>();
                foreach (LocationValues lv in locations)
                {
                    locationIDs.Add(lv.prof_center_id);
                }

                //get list of all joins
                VarianceActivityResponseCompositionLocationJoins varclj = Get_Location_Default_Compositions();
                List<LocationAndTerminal> locCompJoins = varclj.CompositionLocationJoins;

                foreach (LocationAndTerminal lcj in locCompJoins)
                {
                    bool isInList = locationIDs.Contains(lcj.location_id);

                    //if lcj.location_id does not exist in profID's > delete lcj.loc_id
                    if (!isInList)
                    {
                        DeleteLocationDefaultCompositions(lcj);
                    }
                }

                VAR.status = true;
                VertedaDiagnostics.LogMessage("DeleteMultipleLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in DeleteMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse InsertMultipleLocationDefaultCompositions(List<LocationAndTerminal> Location_composition_join)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                foreach (LocationAndTerminal lcj in Location_composition_join)
                {
                    InsertLocationDefaultCompositions(lcj);
                }

                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertMultipleLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse UpdateMultipleLocationDefaultCompositions(List<LocationAndTerminal> Location_composition_join)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
                       
            try
            {
                foreach (LocationAndTerminal lcj in Location_composition_join)
                {
                    UpdateLocationDefaultCompositions(lcj);
                }

                VAR.status = true;
                VertedaDiagnostics.LogMessage("UpdateMultipleLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }          

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse DeleteLocationDefaultCompositions(LocationAndTerminal Location_composition_join)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Profit_Composition_Join_Delete";
            cmd.Parameters.Add("@profit_center_id", SqlDbType.Int).Value = Location_composition_join.location_id;
            cmd.Connection = conn;
            //return package_id to use in copy

                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.status = true;
                VertedaDiagnostics.LogMessage("DeleteLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in DeleteLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse InsertLocationDefaultCompositions(LocationAndTerminal Location_composition_join)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Profit_Composition_Join_Insert";
            cmd.Parameters.Add("@profit_center_id", SqlDbType.Int).Value = Location_composition_join.location_id;
            cmd.Parameters.Add("@composition_id", SqlDbType.Int).Value = Location_composition_join.composition_id;
            cmd.Parameters.Add("@unid", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Connection = conn;
            //return package_id to use in copy

            
            
                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.ID = Convert.ToInt32(cmd.Parameters["@unid"].Value.ToString());
                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse UpdateLocationDefaultCompositions(LocationAndTerminal Location_composition_join)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Profit_Composition_Join_Update";
            cmd.Parameters.Add("@profit_center_id", SqlDbType.Int).Value = Location_composition_join.location_id;
            cmd.Parameters.Add("@composition_id", SqlDbType.Int).Value = Location_composition_join.composition_id;
            cmd.Connection = conn;
            //return package_id to use in copy

            
                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.status = true;
                VertedaDiagnostics.LogMessage("UpdateLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse ManageMultipleLocationDefaultCompositions(List<LocationAndTerminal> Location_composition_join)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                //get existing defaults
                VarianceActivityResponseCompositionLocationJoins var1 = Get_Location_Default_Compositions();

                if (var1.status != false)
                {
                    List<LocationAndTerminal> loccompjojns = var1.CompositionLocationJoins;

                    ////delete any existing defaults that weren't passed in ####removed so users can delete manually
                    //foreach (LocationAndTerminal loc in loccompjojns) //defaultCompositions that isn't in Location_composition_join
                    //{
                    //    bool exists = Location_composition_join.Exists(p => p.location_id == loc.location_id);

                    //    if (!exists)
                    //    {
                    //        //delete the record
                    //        DeleteLocationDefaultCompositions(loc);
                    //    }
                    //}

                    //foreach record passed in, insert if new, update if not
                    foreach (LocationAndTerminal loc in Location_composition_join) //defaultCompositions that isn't in Location_composition_join
                    {
                        bool exists = loccompjojns.Exists(p => p.location_id == loc.location_id);

                        if (exists)
                        {
                            //update the record
                            UpdateLocationDefaultCompositions(loc);
                        }
                        else
                        {
                            //insert the record
                            InsertLocationDefaultCompositions(loc);
                        }
                    }


                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("ManageMultipleLocationDefaultCompositions Completed successfully", "I", false);
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Error in ManageMultipleLocationDefaultCompositions. Could not get existing default floats.";
                    VertedaDiagnostics.LogMessage("Error in ManageMultipleLocationDefaultCompositions. Could not get existing default floats.", "E", false);
                }
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in ManageMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }

            return VAR;
        }



        #endregion

        #region Bag creation - COMPLETE, testing done

        //create bags using default compositions for the number of termials per location
        [WebMethod]
        public VarianceActivityResponseBags SetupEventBags(int employeeID, bool DeleteExisting)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            
            try
            {
                VarianceActivityResponseEmployee VAR9 = ValidateEmployee(employeeID);
                if (VAR9.status)
                {

                    int eventid = GetEventID();

                    if (eventid > 0)
                    {
                        VarianceActivityResponse var1 = new VarianceActivityResponse();
                        var1.status = true;


                        if (DeleteExisting)
                        {
                            //if deleteExisting, get rid of existing bags first
                            var1 = DeleteBags();
                        }

                        VarianceActivityResponse var2 = CreateNewBags(VAR9.Employee);
                        VarianceActivityResponseBags var3 = GetExistingBags();

                        if (var1.status != true || var2.status != true || var3.status != true)
                        {
                            VAR.status = false;
                            VAR.errorText = "Error when inserting new bags";
                            VertedaDiagnostics.LogMessage("Error when inserting new bags", "E", false);
                        }
                        else
                        {
                            VAR.status = true;
                            VAR.Bags = var3.Bags;
                            VertedaDiagnostics.LogMessage("SetupEventBags Completed successfully", "I", false);
                        }
                    }
                    else
                    {
                        //could not get eventid from RTS
                        VAR.status = false;
                        VAR.errorText = "could not get current eventid from RTS web service";
                        VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                    }
                }
                else
                {
                    //employee not valid to create bags
                    VAR.status = false;
                    VAR.errorText = "This employee is not valid to create bags" + employeeID;
                    VertedaDiagnostics.LogMessage("This employee is not valid to create bags" + employeeID, "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupEventBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse CreateNewBags(Employees emp)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            List<Bag> bagsToCreate = new List<Bag>();

            //Select all locations in event_location_master and not in event_closed_location_master for current event
            //Create a kiosk bag for each location above
            VarianceActivityResponseLocationsAndTerminal locationsResponce = GetOpenLocations();
            if (locationsResponce.status == false)
            {
                VAR.status = false;
                VAR.errorText = "Could not load open locations";
                VertedaDiagnostics.LogMessage("Could not load open locations", "E", false);
            }
            else
            {
                List<LocationAndTerminal> openLocations = locationsResponce.LocationsTerminals;


                int noCompLocations = 0;
                string noCompName = "";
                bool cantLoad = false;
                try
                {
                    //get all default composition - location joins
                    VarianceActivityResponseCompositionLocationJoins ExistingLocationCompositions = Get_Location_Default_Compositions();
                    if (ExistingLocationCompositions.status != false)
                    {
                        List<LocationAndTerminal> loccompjojns = ExistingLocationCompositions.CompositionLocationJoins;
                        foreach (LocationAndTerminal loc in openLocations)
                        {
                            //see if the loc.location_id exists in the list of location composition joins
                            bool exists = loccompjojns.Exists(p => p.location_id == loc.location_id);

                            if (!exists)
                            {
                                //if it doesnt, make a record
                                noCompName = loc.location_name;
                                noCompLocations += 1;
                                VertedaDiagnostics.LogMessage("CreateNewBags: "+loc.location_id + " " + loc.location_name + "doesnt have a default composition:" + loc.composition_id, "E", false);
                            }
                        }
                    }
                    else
                    {
                        VAR.status = false;
                        VAR.errorText = "Error when determining if open locations have default compositions";
                        VertedaDiagnostics.LogMessage("Error when determining if open locations have default compositions", "E", false);
                        cantLoad = true;
                    }

                }
                catch (Exception e)
                {
                    VAR.status = false;
                    VAR.errorText = e.ToString();
                    cantLoad = true;
                    VertedaDiagnostics.LogMessage("Error in CreateNewBags: " + e.ToString(), "E", false);
                }



                //if any bags without compositions, reject
                if (noCompLocations == 1)
                {
                    VAR.status = false;
                    VAR.errorText = noCompName + "has no default composition. Please add a default composition before creating an event with this location";
                    VertedaDiagnostics.LogMessage(noCompName + "has no default composition. Please add a default composition before creating an event with this location", "I", false);
                }
                else if (noCompLocations > 1)
                {
                    VAR.status = false;
                    VAR.errorText = "Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event";
                    VertedaDiagnostics.LogMessage("Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event", "I", false);
                }
                else
                {
                    if (cantLoad)
                    {
                        VAR.status = false;
                        VAR.errorText = noCompName + "Could not read default compositions for the locations of this event";
                        VertedaDiagnostics.LogMessage( noCompName + "Could not read default compositions for the locations of this event", "I", false);
                    }
                    else
                    {
                        try
                        {
                            ///////////////////
                            int eventid = GetEventID();
                            if (eventid > 0)
                            {

                                foreach (LocationAndTerminal loc in openLocations)
                                {
                                    //create a kiosk bag
                                    Bag kioBag = new Bag
                                    {
                                        bag_type = constants.BT1,
                                        composition_id = loc.composition_id,
                                        created_date = DateTime.Now.ToString("G"),
                                        employee_id = emp.Employee_ID,
                                        event_id = eventid,
                                        label_id = "",
                                        destination_location_id = loc.location_id,
                                        origin_location_id = ReadConfig().CashOfficeID,
                                        bag_function_id = 1

                                    };

                                    bagsToCreate.Add(kioBag);

                                    //select all the open terminals for that location
                                    VarianceActivityResponseLocationsAndTerminal openTerminals = GetOpenTerminals(loc.location_id);
                                    List<LocationAndTerminal> openTerms = openTerminals.LocationsTerminals;

                                    //create a bag for each terminal
                                    foreach (LocationAndTerminal lat in openTerms)
                                    {
                                        //create a terminal bag
                                        Bag terBag = new Bag
                                        {
                                            bag_type = constants.BT2,
                                            composition_id = loc.composition_id,
                                            created_date = DateTime.Now.ToString("G"),
                                            employee_id = emp.Employee_ID,
                                            event_id = eventid,
                                            label_id = "",
                                            destination_location_id = loc.location_id,
                                            origin_location_id = ReadConfig().CashOfficeID,
                                            bag_function_id = 0
                                        };

                                        bagsToCreate.Add(terBag);       
                                    }                                    
                                }
                                //add all bags, then add all joins

                                VarianceActivityResponse VAR10 =  InsertBags(bagsToCreate);

                                //get list of bags
                                VarianceActivityResponseBags EventBagresponse = GetExistingBags();
                                if (EventBagresponse.status != false)
                                {
                                    List<Bag> EventBags = EventBagresponse.Bags;

                                    VarianceActivityResponse VAR11 = InsertKidToTidJoinTable(EventBags);
                                    VarianceActivityResponse VAR12 = InsertInitialActions(emp, EventBags);

                                    VAR.status = true;
                                    VertedaDiagnostics.LogMessage("CreateNewBags Completed successfully", "I", false);
                                }
                                else
                                {
                                    VAR.status = false;
                                    VertedaDiagnostics.LogMessage("CreateNewBags did not complete successfully. Failed to get information of inserted bags ", "I", false);
                                }
                            }
                            else
                            {
                                VAR.status = false;
                                VAR.errorText = "Could not get event ID from web service";
                                VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false); 
                            }
                        }
                        catch (Exception e)
                        {
                            VAR.status = false;
                            VAR.errorText = e.ToString();
                            VertedaDiagnostics.LogMessage("Error in CreateNewBags: " + e.ToString(), "E", false);
                        }
                    }
                }
            }
            return VAR;
        }
        

        //create bags using specified compositions and locations, using the default number of terminals per location
        [WebMethod]
        public VarianceActivityResponseBags SetupEventCustomBags(int employeeID, bool DeleteExisting, List<LocationAndTerminal> openLocations)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            try
            {
                VarianceActivityResponseEmployee VAR9 = ValidateEmployee(employeeID);
                if (VAR9.status)
                {

                    int eventid = GetEventID();

                    if (eventid > 0)
                    {
                        VarianceActivityResponse var1 = new VarianceActivityResponse();
                        var1.status = true;


                        if (DeleteExisting)
                        {
                            //if deleteExisting, get rid of existing bags first
                            var1 = DeleteBags();
                        }

                        VarianceActivityResponse var2 = CreateNewCustomBags(VAR9.Employee, openLocations);
                        VarianceActivityResponseBags var3 = GetExistingBags();

                        if (var1.status != true || var2.status != true || var3.status != true)
                        {
                            VAR.status = false;
                            VAR.errorText = "Error when inserting new bags";
                            VertedaDiagnostics.LogMessage("Error when inserting new bags", "E", false);
                        }
                        else
                        {
                            VAR.status = true;
                            VAR.Bags = var3.Bags;
                            VertedaDiagnostics.LogMessage("SetupEventBags Completed successfully", "I", false);
                        }
                    }
                    else
                    {
                        //could not get eventid from RTS
                        VAR.status = false;
                        VAR.errorText = "could not get current eventid from RTS web service";
                        VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                    }
                }
                else
                {
                    //employee not valid to create bags
                    VAR.status = false;
                    VAR.errorText = "This employee is not valid to create bags" + employeeID;
                    VertedaDiagnostics.LogMessage("This employee is not valid to create bags" + employeeID, "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupEventBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        
        [WebMethod]
        public VarianceActivityResponse CreateNewCustomBags(Employees emp, List<LocationAndTerminal> openLocations) 
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            List<Bag> bagsToCreate = new List<Bag>();



                int noCompLocations = 0;
                string noCompName = "";


                //if any bags without compositions, reject
                foreach (LocationAndTerminal LaT in openLocations)
                {
                    if (LaT.composition_id == -1)
                    {
                        noCompLocations += 1;
                        noCompName = LaT.location_name;
                        VertedaDiagnostics.LogMessage("CreateNewBags: " + LaT.location_id + " " + LaT.location_name + "doesnt have a default composition:" + LaT.composition_id, "E", false);
                    }
                }


                if (noCompLocations == 1)
                {
                    VAR.status = false;
                    VAR.errorText = noCompName + "has no default composition. Please add a default composition before creating an event with this location";
                    VertedaDiagnostics.LogMessage(noCompName + "has no default composition. Please add a default composition before creating an event with this location", "I", false);
                }
                else if (noCompLocations > 1)
                {
                    VAR.status = false;
                    VAR.errorText = "Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event";
                    VertedaDiagnostics.LogMessage("Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event", "I", false);
                }
                else
                {
                        try
                        {
                            ///////////////////
                            int eventid = GetEventID();
                            if (eventid > 0)
                            {
                                foreach (LocationAndTerminal loc in openLocations)
                                {
                                    //create a kiosk bag
                                    Bag kioBag = new Bag
                                    {
                                        bag_type = constants.BT1,
                                        composition_id = loc.composition_id,
                                        created_date = DateTime.Now.ToString("G"),
                                        employee_id = emp.Employee_ID,
                                        event_id = eventid,
                                        label_id = "",
                                        destination_location_id = loc.location_id,
                                        origin_location_id = ReadConfig().CashOfficeID,
                                        bag_function_id = 1
                                    };

                                    bagsToCreate.Add(kioBag);

                                    //select all the open terminals for that location
                                    VarianceActivityResponseLocationsAndTerminal openTerminals = GetOpenTerminals(loc.location_id);
                                    List<LocationAndTerminal> openTerms = openTerminals.LocationsTerminals;

                                    //create a bag for each terminal
                                    foreach (LocationAndTerminal lat in openTerms)
                                    {
                                        //create a terminal bag
                                        Bag terBag = new Bag
                                        {
                                            bag_type = constants.BT2,
                                            composition_id = loc.composition_id,
                                            created_date = DateTime.Now.ToString("G"),
                                            employee_id = emp.Employee_ID,
                                            event_id = eventid,
                                            label_id = "",
                                            destination_location_id = loc.location_id,
                                            origin_location_id = ReadConfig().CashOfficeID,
                                            bag_function_id = 0
                                        };

                                        bagsToCreate.Add(terBag);
                                    }
                                }
                                //add all bags, then add all joins
                                VarianceActivityResponse VAR10 = InsertBags(bagsToCreate);

                                //get list of bags
                                VarianceActivityResponseBags EventBagresponse = GetExistingBags();

                                if (EventBagresponse.status != false)
                                {
                                    List<Bag> EventBags = EventBagresponse.Bags;

                                    VarianceActivityResponse VAR11 = InsertKidToTidJoinTable(EventBags);
                                    VarianceActivityResponse VAR12 = InsertInitialActions(emp, EventBags);

                                    VAR.status = true;
                                    VertedaDiagnostics.LogMessage("CreateNewBags Completed successfully", "I", false);
                                }
                                else
                                {
                                    VAR.status = false;
                                    VertedaDiagnostics.LogMessage("CreateNewBags did not complete successfully. Failed to get information of inserted bags ", "I", false);
                                }
                            }
                            else
                            {
                                VAR.status = false;
                                VAR.errorText = "Could not get event ID from web service";
                                VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false);
                            }
                        }
                        catch (Exception e)
                        {
                            VAR.status = false;
                            VAR.errorText = e.ToString();
                            VertedaDiagnostics.LogMessage("Error in CreateNewBags: " + e.ToString(), "E", false);
                        }
                }
            return VAR;
        }






        //create bags using specified compositions and locations, using the default number of terminals per location for a future event
        [WebMethod]
        public VarianceActivityResponseBags SetupEventCustomBagsForSpecififcEventID(int employeeID, bool DeleteExisting, List<LocationAndTerminal> openLocations, int eventid)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            try
            {
                VarianceActivityResponseEmployee VAR9 = ValidateEmployee(employeeID);
                if (VAR9.status)
                {

                    if (eventid > 0)
                    {
                        VarianceActivityResponse var1 = new VarianceActivityResponse();
                        var1.status = true;


                        if (DeleteExisting)
                        {
                            //if deleteExisting, get rid of existing bags first
                            var1 = DeleteBagsForSpecififcEventID(eventid);
                        }

                        VarianceActivityResponse var2 = CreateNewCustomBagsForSpecififcEventID(VAR9.Employee, openLocations, eventid);
                        VertedaDiagnostics.LogMessage("Bags built. Reading them out to return to user.", "I", false); //aaa
                        VarianceActivityResponseBags var3 = GetExistingBagsForSpecififcEventID(eventid);

                        if (var1.status != true || var2.status != true || var3.status != true)
                        {
                            VAR.status = false;
                            VAR.errorText = "Error when inserting new bags";
                            VertedaDiagnostics.LogMessage("Error when inserting new bags", "E", false);
                        }
                        else
                        {
                            VAR.status = true;
                            VAR.Bags = var3.Bags;
                            VertedaDiagnostics.LogMessage("SetupEventBags Completed successfully", "I", false);
                        }
                    }
                    else
                    {
                        //could not get eventid from RTS
                        VAR.status = false;
                        VAR.errorText = "could not get current eventid from RTS web service";
                        VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                    }
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "This employee is not valid to create bags" + employeeID;
                    VertedaDiagnostics.LogMessage("This employee is not valid to create bags" + employeeID, "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupEventBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse CreateNewCustomBagsForSpecififcEventID(Employees emp, List<LocationAndTerminal> openLocations, int eventid)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            List<Bag> bagsToCreate = new List<Bag>();

            int noCompLocations = 0;
            string noCompName = "";


            //if any bags without compositions, reject
            foreach (LocationAndTerminal LaT in openLocations)
            {
                if (LaT.composition_id < 1)
                {
                    noCompLocations += 1;
                    noCompName = LaT.location_name;
                    VertedaDiagnostics.LogMessage("CreateNewCustomBagsForSpecififcEventID: " + LaT.location_id + " " + LaT.location_name + "doesnt have a default composition:" + LaT.composition_id, "E", false);
                }
            }


            if (noCompLocations == 1)
            {
                VAR.status = false;
                VAR.errorText = noCompName + "has no default composition. Please add a default composition before creating an event with this location";
                VertedaDiagnostics.LogMessage(noCompName + "has no default composition. Please add a default composition before creating an event with this location", "I", false);
            }
            else if (noCompLocations > 1)
            {
                VAR.status = false;
                VAR.errorText = "Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event";
                VertedaDiagnostics.LogMessage("Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event", "I", false);
            }
            else
            {
                try
                {

                    ///////////////////
                    if (eventid > 0)
                    {
                        foreach (LocationAndTerminal loc in openLocations)
                        {
                            //create a kiosk bag
                            Bag kioBag = new Bag
                            {
                                bag_type = constants.BT1,
                                composition_id = loc.composition_id,
                                created_date = DateTime.Now.ToString("G"),
                                employee_id = emp.Employee_ID,
                                event_id = eventid,
                                label_id = "",
                                destination_location_id = loc.location_id,
                                origin_location_id = ReadConfig().CashOfficeID,
                                bag_function_id = 1
                            };

                            bagsToCreate.Add(kioBag);

                            //List<LocationAndTerminal> openTerms = new List<LocationAndTerminal>();
                            int terminal_bags_to_create = 0;

                            //if open_termin_count > 0
                            //use that as count instead. This will happen when this method is  called by cloned event as the previous bag count value will be used
                            if (loc.open_terminal_count > 0)
                            {
                                terminal_bags_to_create = loc.open_terminal_count;
                            }
                            else
                            {
                                //select all the open terminals for that location
                                VarianceActivityResponseLocationsAndTerminal openTerminals = GetOpenTerminalsForSpecififcEventID(loc.location_id, eventid);
                                //openTerms = openTerminals.LocationsTerminals;

                                terminal_bags_to_create = openTerminals.LocationsTerminals.Count();
                            }

                            //create a bag for each terminal
                            for (int i = 0; i < terminal_bags_to_create; i++)
                            {
                                //create a terminal bag
                                Bag terBag = new Bag
                                {
                                    bag_type = constants.BT2, //x
                                    composition_id = loc.composition_id,
                                    created_date = DateTime.Now.ToString("G"), //x
                                    employee_id = emp.Employee_ID,
                                    event_id = eventid,  
                                    label_id = "",  //x
                                    destination_location_id = loc.location_id,
                                    origin_location_id = ReadConfig().CashOfficeID,
                                    bag_function_id = 0  //x
                                };

                                bagsToCreate.Add(terBag);
                            }
                        }
                        //add all bags, then add all joins
                        VertedaDiagnostics.LogMessage("All bags created. Starting Insert", "I", false); //aaa
                        VarianceActivityResponse VAR10 = InsertBags(bagsToCreate);

                        //get all the bag details for the current event
                        VarianceActivityResponseBags var1 = GetExistingBagsForSpecififcEventID(eventid);
                        if (var1.status != false)
                        {
                            List<Bag> eventBags = var1.Bags;

                            VarianceActivityResponse VAR11 = InsertKidToTidJoinTableForSpecififcEventID(eventid);


                            VarianceActivityResponse VAR12 = InsertInitialActionsForSpecififcEventID(emp, eventid, eventBags);

                            VAR.status = true;
                            VertedaDiagnostics.LogMessage("CreateNewCustomBagsForSpecififcEventID Completed successfully", "I", false);
                        }
                        else
                        {
                            VAR.status = false;
                            VertedaDiagnostics.LogMessage("CreateNewCustomBagsForSpecififcEventID did not complete successfully. Failed to get information of inserted bags ", "I", false);
                        }
                    }
                    else
                    {
                        VAR.status = false;
                        VAR.errorText = "CreateNewCustomBagsForSpecififcEventID: invalid input event ID";
                        VertedaDiagnostics.LogMessage("CreateNewCustomBagsForSpecififcEventID:  invalid input event ID", "E", false);
                    }
                }
                catch (Exception e)
                {
                    VAR.status = false;
                    VAR.errorText = e.ToString();
                    VertedaDiagnostics.LogMessage("Error in CreateNewCustomBagsForSpecififcEventID: " + e.ToString(), "E", false);
                }
            }
            return VAR;
        }





        // ### THIS IS THE MAIN METHOD BEING CALLED BY PRIMO ##
        //create bags using specified compositions and locations, using the default number of terminals per location for a future event 
        [WebMethod]
        public VarianceActivityResponseBags SetupEventCustomBagsWithTerminalCountForSpecififcEventID(int employeeID, bool DeleteExisting, List<LocationAndTerminal> openLocations, int eventid)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            try
            {
                VarianceActivityResponseEmployee VAR9 = ValidateEmployee(employeeID);
                if (VAR9.status)
                {

                    if (eventid > 0)
                    {
                        VarianceActivityResponse var1 = new VarianceActivityResponse();
                        var1.status = true;


                        if (DeleteExisting)
                        {
                            //if deleteExisting, get rid of existing bags first
                            var1 = DeleteBagsForSpecififcEventID(eventid);
                        }

                        VarianceActivityResponse var2 = CreateNewCustomBagsWithTerminalCountForSpecififcEventID(VAR9.Employee, openLocations, eventid);
                        VertedaDiagnostics.LogMessage("Bags built. Reading them out to return to user.", "I", false); //aaa
                        VarianceActivityResponseBags var3 = GetExistingBagsForSpecififcEventID(eventid);

                        if (var1.status != true || var2.status != true || var3.status != true)
                        {
                            VAR.status = false;
                            VAR.errorText = "Error when inserting new bags";
                            VertedaDiagnostics.LogMessage("Error when inserting new bags", "E", false);
                        }
                        else
                        {
                            VAR.status = true;
                            VAR.Bags = var3.Bags;
                            VertedaDiagnostics.LogMessage("SetupEventBags Completed successfully", "I", false);
                        }
                    }
                    else
                    {
                        //could not get eventid from RTS
                        VAR.status = false;
                        VAR.errorText = "could not get current eventid from RTS web service";
                        VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                    }
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "This employee is not valid to create bags" + employeeID;
                    VertedaDiagnostics.LogMessage("This employee is not valid to create bags" + employeeID, "E", false);
                }
            }
            catch (Exception e)
            {
                Primo_SendBugsnag("Error setting up custom bags for event", e, "Database");
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupEventBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse CreateNewCustomBagsWithTerminalCountForSpecififcEventID(Employees emp, List<LocationAndTerminal> openLocations, int eventid)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            List<Bag> bagsToCreate = new List<Bag>();

            int noCompLocations = 0;
            string noCompName = "";


            //if any bags without compositions, reject
            foreach (LocationAndTerminal LaT in openLocations)
            {
                if (LaT.composition_id == -1)
                {
                    noCompLocations += 1;
                    noCompName = LaT.location_name;
                    VertedaDiagnostics.LogMessage("CreateNewCustomBagsForSpecififcEventID: " + LaT.location_id + " " + LaT.location_name + "doesnt have a default composition:" + LaT.composition_id, "E", false);
                }
            }


            if (noCompLocations == 1)
            {
                VAR.status = false;
                VAR.errorText = noCompName + "has no default composition. Please add a default composition before creating an event with this location";
                VertedaDiagnostics.LogMessage(noCompName + "has no default composition. Please add a default composition before creating an event with this location", "I", false);
            }
            else if (noCompLocations > 1)
            {
                VAR.status = false;
                VAR.errorText = "Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event";
                VertedaDiagnostics.LogMessage("Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event", "I", false);
            }
            else
            {
                try
                {

                    ///////////////////
                    if (eventid > 0)
                    {
                        foreach (LocationAndTerminal loc in openLocations)
                        {
                            //create a kiosk bag
                            Bag kioBag = new Bag
                            {
                                bag_type = constants.BT1,
                                composition_id = loc.composition_id,
                                created_date = DateTime.Now.ToString("G"),
                                employee_id = emp.Employee_ID,
                                event_id = eventid,
                                label_id = "",
                                destination_location_id = loc.location_id,
                                origin_location_id = ReadConfig().CashOfficeID,
                                bag_function_id = 1
                            };

                            bagsToCreate.Add(kioBag);


                            //create a bag for each terminal
                            for (int i = 0; i < loc.open_terminal_count; i++ )
                            {
                                //create a terminal bag
                                Bag terBag = new Bag
                                {
                                    bag_type = constants.BT2, //x
                                    composition_id = loc.composition_id,
                                    created_date = DateTime.Now.ToString("G"), //x
                                    employee_id = emp.Employee_ID,
                                    event_id = eventid,
                                    label_id = "",  //x
                                    destination_location_id = loc.location_id,
                                    origin_location_id = ReadConfig().CashOfficeID,
                                    bag_function_id = 0  //x
                                };

                                bagsToCreate.Add(terBag);
                            }
                        }
                        //add all bags, then add all joins
                        VertedaDiagnostics.LogMessage("All bags created. Starting Insert", "I", false); //aaa
                        VarianceActivityResponse VAR10 = InsertBags(bagsToCreate);

                        //get all the bag details for the current event
                        VarianceActivityResponseBags var1 = GetExistingBagsForSpecififcEventID(eventid);
                        if (var1.status != false)
                        {
                            List<Bag> eventBags = var1.Bags;

                            VarianceActivityResponse VAR11 = InsertKidToTidJoinTableForSpecififcEventID(eventid);
                            VarianceActivityResponse VAR12 = InsertInitialActionsForSpecififcEventID(emp, eventid, eventBags);

                            VAR.status = true;
                            VertedaDiagnostics.LogMessage("CreateNewCustomBagsForSpecififcEventID Completed successfully", "I", false);
                        }
                        else
                        {
                            VAR.status = false;
                            VertedaDiagnostics.LogMessage("CreateNewCustomBagsForSpecififcEventID did not complete successfully. Failed to get information of inserted bags ", "I", false);
                        }
                    }
                    else
                    {
                        VAR.status = false;
                        VAR.errorText = "CreateNewCustomBagsForSpecififcEventID: invalid input event ID";
                        VertedaDiagnostics.LogMessage("CreateNewCustomBagsForSpecififcEventID:  invalid input event ID", "E", false);
                    }
                }
                catch (Exception e)
                {
                    VAR.status = false;
                    VAR.errorText = e.ToString();
                    VertedaDiagnostics.LogMessage("Error in CreateNewCustomBagsForSpecififcEventID: " + e.ToString(), "E", false);
                }
            }
            return VAR;
        }







        //create bags using specified compositions and locations, using a specified number of terminals per location
        [WebMethod]
        public VarianceActivityResponseBags SetupEventCustomBagsWithTerminalCount(int employeeID, bool DeleteExisting, List<LocationAndTerminal> openLocations)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            try
            {
                VarianceActivityResponseEmployee VAR9 = ValidateEmployee(employeeID);
                if (VAR9.status)
                {

                    int eventid = GetEventID();

                    if (eventid > 0)
                    {
                        VarianceActivityResponse var1 = new VarianceActivityResponse();
                        var1.status = true;


                        if (DeleteExisting)
                        {
                            //if deleteExisting, get rid of existing bags first
                            var1 = DeleteBags();
                        }

                        VarianceActivityResponse var2 = CreateNewCustomBagsWithTerminalCount(VAR9.Employee, openLocations);
                        VarianceActivityResponseBags var3 = GetExistingBags();

                        if (var1.status != true || var2.status != true || var3.status != true)
                        {
                            VAR.status = false;
                            VAR.errorText = "Error when inserting new bags";
                            VertedaDiagnostics.LogMessage("Error when inserting new bags", "E", false);
                        }
                        else
                        {
                            VAR.status = true;
                            VAR.Bags = var3.Bags;
                            VertedaDiagnostics.LogMessage("SetupEventBags Completed successfully", "I", false);
                        }
                    }
                    else
                    {
                        //could not get eventid from RTS
                        VAR.status = false;
                        VAR.errorText = "could not get current eventid from RTS web service";
                        VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                    }
                }
                else
                {
                    //employee not valid to create bags
                    VAR.status = false;
                    VAR.errorText = "This employee is not valid to create bags" + employeeID;
                    VertedaDiagnostics.LogMessage("This employee is not valid to create bags" + employeeID, "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupEventBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse CreateNewCustomBagsWithTerminalCount(Employees emp, List<LocationAndTerminal> openLocations)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            List<Bag> bagsToCreate = new List<Bag>();


            int terminalCount = 0;
            int noCompLocations = 0;
            string noCompName = "";


            //if any bags without compositions, reject
            foreach (LocationAndTerminal LaT in openLocations)
            {
                if (LaT.composition_id == -1)
                {
                    noCompLocations += 1;
                    noCompName = LaT.location_name;
                    VertedaDiagnostics.LogMessage("CreateNewBags: " + LaT.location_id + " " + LaT.location_name + "doesnt have a default composition:" + LaT.composition_id, "E", false);
                }
            }


            if (noCompLocations == 1)
            {
                VAR.status = false;
                VAR.errorText = noCompName + "has no default composition. Please add a default composition before creating an event with this location";
                VertedaDiagnostics.LogMessage(noCompName + "has no default composition. Please add a default composition before creating an event with this location", "I", false);
            }
            else if (noCompLocations > 1)
            {
                VAR.status = false;
                VAR.errorText = "Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event";
                VertedaDiagnostics.LogMessage("Multiple locations are missing default compositions. Please add default compositions to each location before creating bags for this event", "I", false);
            }
            else
            {
                try
                {
                    ///////////////////
                    int eventid = GetEventID();
                    if (eventid > 0)
                    {
                        foreach (LocationAndTerminal loc in openLocations)
                        {
                            //create a kiosk bag
                            Bag kioBag = new Bag
                            {
                                bag_type = constants.BT1,
                                composition_id = loc.composition_id,
                                created_date = DateTime.Now.ToString("G"),
                                employee_id = emp.Employee_ID,
                                event_id = eventid,
                                label_id = "",
                                destination_location_id = loc.location_id,
                                origin_location_id = ReadConfig().CashOfficeID,
                                bag_function_id = 1
                            };

                            bagsToCreate.Add(kioBag);

                            //the number of terminal bags is passed in
                            terminalCount = loc.open_terminal_count;

                            //create a bag for each terminal
                            for (int i = 0; i < terminalCount; i++)
                            {
                                //create a terminal bag
                                Bag terBag = new Bag
                                {
                                    bag_type = constants.BT2,
                                    composition_id = loc.composition_id,
                                    created_date = DateTime.Now.ToString("G"),
                                    employee_id = emp.Employee_ID,
                                    event_id = eventid,
                                    label_id = "",
                                    destination_location_id = loc.location_id,
                                    origin_location_id = ReadConfig().CashOfficeID,
                                    bag_function_id = 0
                                };

                                bagsToCreate.Add(terBag);
                            }
                        }
                        //add all bags, then add all joins
                        VarianceActivityResponse VAR10 = InsertBags(bagsToCreate);

                        //get list of bags
                        VarianceActivityResponseBags EventBagresponse = GetExistingBags();

                        if (EventBagresponse.status != false)
                        {
                            List<Bag> EventBags = EventBagresponse.Bags;

                            VarianceActivityResponse VAR11 = InsertKidToTidJoinTable(EventBags);
                            VarianceActivityResponse VAR12 = InsertInitialActions(emp, EventBags);

                            VAR.status = true;
                            VertedaDiagnostics.LogMessage("CreateNewBags Completed successfully", "I", false);
                        }
                        else
                        {
                            VAR.status = false;
                            VertedaDiagnostics.LogMessage("CreateNewBags did not complete successfully. Failed to get information of inserted bags ", "I", false);
                        }
                    }
                    else
                    {
                        VAR.status = false;
                        VAR.errorText = "Could not get event ID from web service";
                        VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false);
                    }
                }
                catch (Exception e)
                {
                    VAR.status = false;
                    VAR.errorText = e.ToString();
                    VertedaDiagnostics.LogMessage("Error in CreateNewBags: " + e.ToString(), "E", false);
                }
            }
            return VAR;
        }





        [WebMethod]
        public VarianceActivityResponseBags SetupClonedEventBags(int employeeID, bool DeleteExisting, int target_eventid, int new_eventid)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            VertedaDiagnostics.LogMessage("SetupClonedEventBags started. emp id, delete, target event, new event: " + employeeID + " " + DeleteExisting.ToString() + " " + target_eventid + " " + new_eventid, "I", false);

            try
            {
                VarianceActivityResponseEmployee VAR9 = ValidateEmployee(employeeID);
                if (VAR9.status)
                {

                    if (new_eventid > 0)
                    {
                        VarianceActivityResponse var1 = new VarianceActivityResponse();
                        var1.status = true;

                        VarianceActivityResponseLocationsAndTerminal var4 = GetExistingKioskBagLocationsForClonedEvent(target_eventid);

                        if (var4.status != false && var4.LocationsTerminals.Count() > 0)
                        {
                            if (DeleteExisting)
                            {
                                //if deleteExisting, get rid of existing bags first
                                var1 = DeleteBagsForSpecififcEventID(new_eventid);
                            }

                            VarianceActivityResponse var2 = CreateNewCustomBagsForSpecififcEventID(VAR9.Employee, var4.LocationsTerminals, new_eventid);
                            VertedaDiagnostics.LogMessage("Bags built. Reading them out to return to user.", "I", false); //aaa
                            VarianceActivityResponseBags var3 = GetExistingBagsForSpecififcEventID(new_eventid);

                            if (var1.status != true || var2.status != true || var3.status != true)
                            {
                                VAR.status = false;
                                VAR.errorText = "Error when inserting new bags";
                                VertedaDiagnostics.LogMessage("Error when inserting new bags", "E", false);
                            }
                            else
                            {
                                VAR.status = true;
                                VAR.Bags = var3.Bags;
                                VertedaDiagnostics.LogMessage("SetupEventBags Completed successfully", "I", false);
                            }
                        }
                        else
                        {
                            //error - could not find locations of previous event
                            VAR.status = false;
                            VAR.errorText = "SetupClonedEventBags: could not find locations of previous event";
                            VertedaDiagnostics.LogMessage("SetupClonedEventBags: could not find locations of previous event", "E", false);
                        }
                    }
                    else
                    {
                        //could not get eventid from RTS
                        VAR.status = false;
                        VAR.errorText = "could not get current eventid from RTS web service";
                        VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                    }
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "This employee is not valid to create bags" + employeeID;
                    VertedaDiagnostics.LogMessage("This employee is not valid to create bags" + employeeID, "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupEventBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }


       






























        [WebMethod]
        public VarianceActivityResponse DeleteBags()
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();



            try
            {
                int eventid = GetEventID();
                if (eventid > 0)
                {
                    ConnectionString con_str = GetConnectionString(2);

                    using (SqlConnection con = new SqlConnection(con_str.connection_string))
                    {
                        using (SqlCommand cmd = new SqlCommand("SP_Bag_dim_Delete"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@event_id", eventid);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                            VAR.status = true;
                            VertedaDiagnostics.LogMessage("DeleteBags Completed successfully", "I", false);
                        }
                    }
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not get event ID from web service";
                    VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false);  
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in DeleteBags: " + e.ToString(), "E", false);

            }

            return VAR;
        }
          
        [WebMethod]
        public VarianceActivityResponse InsertBags(List<Bag> Bags)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[9] 
                    { new DataColumn("event_id", typeof(int)),
                    new DataColumn("bag_type", typeof(string)),
                    new DataColumn("label_id", typeof(string)),
                    new DataColumn("created_date", typeof(DateTime)),
                    new DataColumn("employee_id",typeof(int)),
                    new DataColumn("origin_location_id", typeof(int)),
                    new DataColumn("destination_location_id", typeof(int)),
                    new DataColumn("composition_id", typeof(int)),
                    new DataColumn("bag_function_id", typeof(int)) });

            foreach (Bag btc in Bags)
            {
                dt.Rows.Add(btc.event_id, btc.bag_type, btc.label_id, btc.created_date, btc.employee_id, ReadConfig().CashOfficeID, btc.destination_location_id, btc.composition_id, btc.bag_function_id);
            }
            if (dt.Rows.Count > 0)
            {
                try
                {
                    ConnectionString con_str = GetConnectionString(2);
                    using (SqlConnection con = new SqlConnection(con_str.connection_string))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert_BAGS"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@tblBags", dt);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("InsertBags Completed successfully", "I", false);
                }
                catch (Exception e)
                {
                    VAR.status = false;
                    VAR.errorText = e.ToString();
                    VertedaDiagnostics.LogMessage("Error in InsertBags: " + e.ToString(), "E", false);
                }
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse InsertKidToTidJoinTable(List<Bag> EventBags)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {

            //create join list
                List<BagJoins> KidTidJoins = new List<BagJoins>();

            //foreach terminal bag in bags
                foreach (Bag X in EventBags)
                {
                    if (X.bag_type.Trim() == constants.BT2)
                    { 
                        Bag item = EventBags.Find(c => (c.destination_location_id == X.destination_location_id) && (c.bag_type.Trim() == constants.BT1));

                        BagJoins newjoin = new BagJoins(X.event_id, item.bag_unid, X.bag_unid);

                        KidTidJoins.Add(newjoin);

                    }
                }

                CreateKidToTidJoinTable(KidTidJoins);

            }
            
            catch(Exception e)
            {
            }
            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse InsertKidToTidJoinTableForSpecififcEventID(int eventid)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                //get list of bags
                VarianceActivityResponseBags EventBagresponse = GetExistingBagsForSpecififcEventID(eventid);
                List<Bag> EventBags = EventBagresponse.Bags;
                //create join list
                List<BagJoins> KidTidJoins = new List<BagJoins>();

                //foreach terminal bag in bags
                foreach (Bag X in EventBags)
                {
                    if (X.bag_type.Trim() == constants.BT2)
                    {
                        Bag item = EventBags.Find(c => (c.destination_location_id == X.destination_location_id) && (c.bag_type.Trim() == constants.BT1));

                        BagJoins newjoin = new BagJoins(X.event_id, item.bag_unid, X.bag_unid);

                        KidTidJoins.Add(newjoin);

                    }
                }

                CreateKidToTidJoinTable(KidTidJoins);

            }

            catch (Exception e)
            {
            }
            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse CreateKidToTidJoinTable(List<BagJoins> KidTidJoins)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3] 
                    { new DataColumn("event_id", typeof(int)),
                    new DataColumn("parent_id", typeof(int)),
                    new DataColumn("child_id", typeof(int)) });

            foreach (BagJoins ktj in KidTidJoins)
            {
                dt.Rows.Add(ktj.Event_ID, ktj.Parent_ID, ktj.Child_ID);
            }
            if (dt.Rows.Count > 0)
            {
                try
                {
                    ConnectionString con_str = GetConnectionString(2);
                   
                    using (SqlConnection con = new SqlConnection(con_str.connection_string))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert_Kid_Tid_Join_Table"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@tblKidTidJoins", dt);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("CreateKidToTidJoinTable Completed successfully", "I", false);
                }
                catch (Exception e)
                {
                    VAR.status = false;
                    VAR.errorText = e.ToString();
                    VertedaDiagnostics.LogMessage("Error in CreateKidToTidJoinTable: " + e.ToString(), "E", false);
                }
            }

            return VAR;
        }
               
        [WebMethod]
        public VarianceActivityResponse InsertKidToTidJoin(int ParentID, int ChildID)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("InsertKidToTidJoin started. parent, child: " + ParentID + " " + ChildID, "I", false);


            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                int eventid = GetEventID(); //GetEventIDForCurrentBusinessDay();

            //edit existing record
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Kid_To_Tid_Insert";
            cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
            cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = ParentID;
            cmd.Parameters.Add("@ChildID", SqlDbType.Int).Value = ChildID;
            cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Connection = conn;
            //return package_id to use in copy

                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.ID = Convert.ToInt32(cmd.Parameters["@id"].Value.ToString());
                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertKidToTidJoin Completed successfully", "I", false);

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertKidToTidJoin: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse InsertKidToTidJoinForSpecificEventID(int ParentID, int ChildID, int eventID)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("InsertKidToTidJoinForSpecificEventID started. parent, child, event: " + ParentID + " " + ChildID + " " + eventID, "I", false);


            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;


                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Kid_To_Tid_Insert";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventID;
                cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = ParentID;
                cmd.Parameters.Add("@ChildID", SqlDbType.Int).Value = ChildID;
                cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Connection = conn;
                //return package_id to use in copy

                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.ID = Convert.ToInt32(cmd.Parameters["@id"].Value.ToString());
                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertKidToTidJoinForSpecificEventID Completed successfully", "I", false);

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertKidToTidJoinForSpecificEventID: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse UpdateBagLabels(List<Bag> bags)
        {
            //update exxisting bags with label ID's
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                int eventid = GetEventID(); // GetEventIDForCurrentBusinessDay();
                if (eventid > 0)
                {
                    ConnectionString con_str = GetConnectionString(2);

                    foreach (Bag bg in bags)
                    {
                        using (SqlConnection con = new SqlConnection(con_str.connection_string))
                        {
                            using (SqlCommand cmd = new SqlCommand("SP_Bag_Dim_Update_Labels"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@eventID", eventid);
                                cmd.Parameters.AddWithValue("@labelID", bg.label_id);
                                cmd.Parameters.AddWithValue("@destinstionLocID", bg.destination_location_id);
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();                                
                            }
                        }
                    }
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("UpdateBagLabels Completed successfully", "I", false);
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not get event ID from web service";
                    VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateBagLabels: " + e.ToString(), "E", false);

            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse UpdateBagLabelsWithUnid(List<unidLabel> bags)
        {
            //update exxisting bags with label ID's
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {

                ConnectionString con_str = GetConnectionString(2);    

                foreach (unidLabel bg in bags)
                    {
                        using (SqlConnection con = new SqlConnection(con_str.connection_string))
                        {
                            using (SqlCommand cmd = new SqlCommand("SP_Bag_Dim_Update_Labels_With_Unid"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@labelID", bg.label_id);
                                cmd.Parameters.AddWithValue("@unid", bg.bag_unid);
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("UpdateBagLabels Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                Primo_SendBugsnag("Error creating bag labels", e, "Database");
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateBagLabels: " + e.ToString(), "E", false);

            }

            return VAR;
        }



        [WebMethod]
        public VarianceActivityResponse UpdateBagLabelsForSpecificEventID(List<Bag> bags, int eventid)
        {
            //update exxisting bags with label ID's
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                if (eventid > 0)
                {
                    ConnectionString con_str = GetConnectionString(2);

                    foreach (Bag bg in bags)
                    {
                        using (SqlConnection con = new SqlConnection(con_str.connection_string))
                        {
                            using (SqlCommand cmd = new SqlCommand("SP_Bag_Dim_Update_Labels"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@event_id", eventid);
                                cmd.Parameters.AddWithValue("@labelID", bg.label_id);
                                cmd.Parameters.AddWithValue("@destinstionLocID", bg.destination_location_id);
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("UpdateBagLabels Completed successfully", "I", false);
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not get event ID from web service";
                    VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateBagLabels: " + e.ToString(), "E", false);

            }

            return VAR;
        }

        





        [WebMethod]
        public VarianceActivityResponseBags SetupNewSingleBag(Bag newBag, List<BagJoins> childBags) //used for creating inbound terminal bags before being placed in kiosk bags
        {

            //requires terminal ID, employee ID, amount, type, actionID, labelID, locationID, function_id

            //update existing bags with label ID's
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            try
            {
                VertedaDiagnostics.LogMessage("SetupNewSingleBag started", "I", false);

                if (newBag.destination_location_id == null || newBag.destination_location_id == 0)
                {
                    //bag is a withdrawl or cash out
                    newBag.destination_location_id = ReadConfig().CashOfficeID;
                }
                if (newBag.origin_location_id == null || newBag.destination_location_id == 0)
                {
                    //bag is a loan
                    newBag.origin_location_id = ReadConfig().CashOfficeID;
                }


                //insert a bag (assign label ID if kiosk)
                //get event ID as part of insert

                VarianceActivityResponse var1 = InsertSingleBag(newBag);

                if (var1.status != false)
                {
                    //this just gets the bag_dim information. Action and label information doesnt yet exist, i.e. no value / bag count / actions / employee names
                    VarianceActivityResponseBags var3 = GetSingleBagInfoWithUnid(var1.ID);
                    
                    Bag insertedBag = var3.Bags[0];

                    //if type == kiosk
                    if (insertedBag.bag_type == constants.BT1)
                    {
                        //if (constants.SiteLicence == 1)
                        //{
                        //    //if man city, label ID = location name
                        //}

                        //insert Kid to Tid joins for each bag in childbags  
                        foreach (BagJoins bj in childBags)
                        {
                            InsertKidToTidJoin(insertedBag.bag_unid, bj.Child_ID);
                        }
                    }
                    else
                    {
                        List<Bag> updLblList = var3.Bags;
                        updLblList[0].label_id = updLblList[0].bag_id;
                        //set the labelid = bag id
                        VarianceActivityResponse var5 = UpdateBagLabels(updLblList);
                    }

                    BagAction initialAction = new BagAction();

                    //revalidate the employee to get employee name
                    VarianceActivityResponseEmployee VAR7 = ValidateEmployee(insertedBag.employee_id);

                    initialAction.Action_ID = newBag.action_id;// 6; //or whatever the initial action for return bags is
                    initialAction.Sending_Employee_ID = insertedBag.employee_id;
                    initialAction.Receiving_Employee_ID = insertedBag.employee_id;
                    initialAction.Terminal_ID = newBag.terminal_id; //terminal ID isnt added as part of bag creation. get it from origianl input
                    initialAction.Location_ID = insertedBag.origin_location_id;
                    initialAction.Bag_ID = insertedBag.bag_id;
                    initialAction.sender_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;
                    initialAction.reciever_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;
                    initialAction.Bag_Unid = var1.ID;

                    if (insertedBag.bag_type == constants.BT1) //kiosk
                    {
                        initialAction.Total_Value = 0;
                        initialAction.Bag_Count = childBags.Count();
                    }
                    else if (insertedBag.bag_type == constants.BT2) //terminal
                    {
                        initialAction.Total_Value = newBag.current_value; //this has to come from original paramater as it isnt part of bag_dim insert
                        initialAction.Bag_Count = 0;
                    }

                    VarianceActivityResponse var2 = SetBagAction(initialAction);

                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Error retrieving inserted bags details in SetupNewSingleBag";
                    VertedaDiagnostics.LogMessage("Error retrieving inserted bags details in SetupNewSingleBag", "E", false);
                }


            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupNewSingleBag: " + e.ToString(), "E", false);

            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponseBags SetupNewAdditionalBag(Bag newBag) //used for creating additional terminal bags when required. composition always 0
        {
            VertedaDiagnostics.LogMessage("SetupNewAdditionalBag started", "I", false);
            //requires terminal ID, employee ID, amount, type, actionID, labelID, locationID, function_id


            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            try
            {
                //create a new empty terminal bag
                Bag item = new Bag();

                if (newBag.destination_location_id == null || newBag.destination_location_id == 0)
                {
                    //bag is a withdrawl or cash out
                    newBag.destination_location_id = ReadConfig().CashOfficeID;
                }
                if (newBag.origin_location_id == null || newBag.destination_location_id == 0)
                {
                    //bag is a loan
                    newBag.origin_location_id = ReadConfig().CashOfficeID;
                }

                VarianceActivityResponse var1 = InsertSingleBag(newBag);

                if (var1.status)
                {
                    //this just gets the bag_dim information. Action and label information doesnt yet exist, i.e. no value / bag count / actions / employee names
                    VertedaDiagnostics.LogMessage("SetupNewAdditionalBag. Bag inserted. Getting details to insert kid/tid join + action: "+var1.ID, "I", false);
                    BagOperators op = new BagOperators();
                    op.bag_unid = var1.ID;
                    VarianceActivityResponseBags var3 = GetBagDetails(op);
                    VertedaDiagnostics.LogMessage("SetupNewAdditionalBag. Get bag details 1 complete", "I", false);
                    if (var3.status)
                    {
                        //assign it to parent if not a bag at action 6
                        if (newBag.action_id != 6)
                        {
                            VertedaDiagnostics.LogMessage("SetupNewAdditionalBag. assigning new bag to parent", "I", false);
                            InsertKidToTidJoinForSpecififcEventID(newBag.parent_id, var3.Bags[0].bag_unid, var3.Bags[0].event_id);
                        }

                        VertedaDiagnostics.LogMessage("SetupNewAdditionalBag. Join inserted. Inserting action", "I", false);
                        //## set up bagaction details OR pass is action as we dont know things like terminal ID
                        BagAction ba = new BagAction();//newBag.bag_actions[0]; //#### THIS BAG ACTION DOES NOT EXIST####
                        ba = newBag.bag_actions[0];
                        ba.Bag_Unid = var3.Bags[0].bag_unid;

                        ////advance its action to correct point
                        if (newBag.action_id < 6 && newBag.action_id > 0) //outbound
                        {

                            //AdvanceBagAction to newBag.action_id starting at 1
                            for (int i = 1; i <= newBag.action_id; i++)
                            {
                                ba.Action_ID = i;
                                SetBagAction(ba); 
                            }
                        }
                        else if (newBag.action_id > 5 && newBag.action_id < 11)
                        {
                            //AdvanceBagAction to newBag.action_id starting at 6
                            for (int i = 6; i <= newBag.action_id; i++)
                            {
                                ba.Action_ID = i;
                                SetBagAction(ba); 
                            }
                        }
                        else
                        {
                            //error - invalid action ID
                            Exception bs_ex = null;
                            Primo_SendBugsnag("Error in SetupNewAdditionalBag: invalid action ID :" + newBag.action_id, bs_ex, "General logic");
                            VAR.status = false;
                            VAR.errorText = "Error in SetupNewAdditionalBag: invalid action ID :" + newBag.action_id;
                            VertedaDiagnostics.LogMessage("Error in SetupNewAdditionalBag: invalid action ID :" + newBag.action_id, "E", false);
                        }
                    }
                    else
                    {
                        //error - couldnt find new bags details in database
                        Exception bs_ex = null;
                        Primo_SendBugsnag("Error in SetupNewAdditionalBag: couldnt find new bags details in database", bs_ex, "Workflow");
                        VAR.status = false;
                        VAR.errorText = "Error in SetupNewAdditionalBag: couldnt find new bags details in database";
                        VertedaDiagnostics.LogMessage("Error in SetupNewAdditionalBag: couldnt find new bags details in database", "E", false);
                    }


                }
                else
                {
                    //error - couldnt create a new bag
                    Exception bs_ex = null;
                    Primo_SendBugsnag("Error in SetupNewAdditionalBag: couldnt create a new bag", bs_ex, "Workflow");
                    VAR.status = false;
                    VAR.errorText = "Error in SetupNewAdditionalBag: couldnt create a new bag";
                    VertedaDiagnostics.LogMessage("Error in SetupNewAdditionalBag: couldnt create a new bag", "E", false);
                }



            }
            catch (Exception e)
            {
                Primo_SendBugsnag("Error in SetupNewAdditionalBag:", e, "Workflow");
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupNewAdditionalBag: " + e.ToString(), "E", false);

            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse InsertSingleBag(Bag SingleBag)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
                       
                try
                {
                    VertedaDiagnostics.LogMessage("InsertSingleBag started with values:" + SingleBag.bag_type + " " +
                        SingleBag.employee_id + " " + SingleBag.origin_location_id + " " + SingleBag.destination_location_id + " " +
                        SingleBag.bag_function_id, "I", false);

                    int eventid = GetEventID(); //GetEventIDForCurrentBusinessDay();
                    if (eventid > 0)
                    {
                    
                    ConnectionString con_str = GetConnectionString(2);
                    using (SqlConnection con = new SqlConnection(con_str.connection_string))
                        {
                            using (SqlCommand cmd = new SqlCommand("SP_Bag_Dim_Insert"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
                                cmd.Parameters.Add("@bag_type", SqlDbType.VarChar).Value = SingleBag.bag_type;
                                if (SingleBag.label_id != null)
                                {
                                    cmd.Parameters.Add("@labelID", SqlDbType.VarChar).Value = SingleBag.label_id;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@labelID", SqlDbType.VarChar).Value = "0";
                                }
                                cmd.Parameters.Add("@employeeID", SqlDbType.Int).Value = SingleBag.employee_id;

                                cmd.Parameters.Add("@origin_location", SqlDbType.Int).Value = SingleBag.origin_location_id;
                                cmd.Parameters.Add("@destination_location", SqlDbType.Int).Value = SingleBag.destination_location_id;

                                if (SingleBag.composition_id == null)
                                {
                                    cmd.Parameters.Add("@composition_ID", SqlDbType.Int).Value = 0; //return bags have no composition
                                }
                                else
                                {
                                    cmd.Parameters.Add("@composition_ID", SqlDbType.Int).Value = SingleBag.composition_id; //return bags have no composition 
                                }
                                cmd.Parameters.Add("@function_ID", SqlDbType.Int).Value = SingleBag.bag_function_id;
                                cmd.Parameters.Add("@unid", SqlDbType.Int).Direction = ParameterDirection.Output;
                                
                                con.Open();
                                cmd.ExecuteNonQuery();
                                VAR.ID = Convert.ToInt32(cmd.Parameters["@unid"].Value.ToString());
                                con.Close();
                            }
                        }
                        VAR.status = true;
                        VertedaDiagnostics.LogMessage("InsertSingleBag Completed successfully. new bag: "+VAR.ID, "I", false);
                    }
                    else
                    {
                        //could not get eventid from RTS
                        VAR.status = false;
                        VAR.errorText = "could not get current eventid from RTS web service";
                        VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                    }
                }
                catch (Exception e)
                {
                    VAR.status = false;
                    VAR.errorText = e.ToString();
                    VertedaDiagnostics.LogMessage("Error in InsertSingleBag: " + e.ToString(), "E", false);
                }

            return VAR;
        }

        



        [WebMethod]
        public VarianceActivityResponseBags SetupNewSingleBagForSpecififcEventID(Bag newBag, List<BagJoins> childBags)
        {
            //requires terminal ID, employee ID, amount, type, actionID, labelID, locationID

            //update existing bags with label ID's
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            try
            {
                //insert a bag (assign label ID if kiosk)
                //get event ID as part of insert

                VarianceActivityResponse var1 = InsertSingleBagForSpecififcEventID(newBag);
                if (var1.status != false)
                {
                    VarianceActivityResponseBags var3 = GetSingleBagInfoWithUnid(var1.ID);


                    if (var3.status && var3.Bags.Count > 0)

                    {
                        Bag insertedBag = var3.Bags[0];

                        //print label 
                        //if type == kiosk
                        if (insertedBag.bag_type == constants.BT1)
                        {
                            //insert Kid to Tid joins for each bag in childbags  
                            foreach (BagJoins bj in childBags)
                            {
                                InsertKidToTidJoinForSpecififcEventID(insertedBag.bag_unid, bj.Child_ID, insertedBag.event_id);
                            }
                        }
                        else
                        {
                            List<Bag> updLblList = var3.Bags;
                            updLblList[0].label_id = updLblList[0].bag_id;
                            //set the labelid = bag id
                            VarianceActivityResponse var5 = UpdateBagLabels(updLblList);
                        }


                        BagAction initialAction = new BagAction();

                        //revalidate the employee to get employee name
                        VarianceActivityResponseEmployee VAR7 = ValidateEmployee(insertedBag.employee_id);

                        initialAction.Action_ID = newBag.action_id; //or whatever the initial action for return bags is
                        initialAction.Sending_Employee_ID = insertedBag.employee_id;
                        initialAction.Receiving_Employee_ID = insertedBag.employee_id;
                        initialAction.Terminal_ID = insertedBag.terminal_id;
                        initialAction.Location_ID = insertedBag.origin_location_id;
                        initialAction.Bag_ID = insertedBag.bag_id;
                        initialAction.sender_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;
                        initialAction.reciever_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;
                        initialAction.Bag_Unid = var1.ID;

                        if (insertedBag.bag_type == constants.BT1) //kiosk
                        {
                            initialAction.Total_Value = 0;
                            initialAction.Bag_Count = childBags.Count();
                        }
                        else if (insertedBag.bag_type == constants.BT2) //terminal
                        {
                            initialAction.Total_Value = insertedBag.current_value;
                            initialAction.Bag_Count = 0;
                        }

                        VarianceActivityResponse var2 = SetBagAction(initialAction);

                        //pass back bag details
                        VAR = var3;
                        VAR.status = true;
                        VertedaDiagnostics.LogMessage("SetupNewSingleBag completed succesfully", "I", false);
                    }
                    else
                    {
                        VAR.status = false;
                        VAR.errorText = "SetupNewSingleBag: error getting details of kiosk bag";
                        VertedaDiagnostics.LogMessage("SetupNewSingleBag: error getting details of kiosk bag", "E", false);
                    }

                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Error retrieving inserted bags details in SetupNewSingleBag";
                    VertedaDiagnostics.LogMessage("Error retrieving inserted bags details in SetupNewSingleBag", "E", false);
                }

            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetupNewSingleBag: " + e.ToString(), "E", false);

            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse InsertSingleBagForSpecififcEventID(Bag SingleBag)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                using (SqlConnection con = new SqlConnection(con_str.connection_string))
                {
                    using (SqlCommand cmd = new SqlCommand("SP_Bag_Dim_Insert"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = SingleBag.event_id;
                        cmd.Parameters.Add("@bag_type", SqlDbType.VarChar).Value = SingleBag.bag_type;
                        if (SingleBag.label_id != null)
                        {
                            cmd.Parameters.Add("@labelID", SqlDbType.VarChar).Value = SingleBag.label_id;
                        }
                        else
                        {
                            cmd.Parameters.Add("@labelID", SqlDbType.VarChar).Value = 0;
                        }
                        cmd.Parameters.Add("@employeeID", SqlDbType.Int).Value = SingleBag.employee_id;

                        cmd.Parameters.Add("@origin_location", SqlDbType.Int).Value = SingleBag.origin_location_id;
                        cmd.Parameters.Add("@destination_location", SqlDbType.Int).Value = SingleBag.destination_location_id;

                        if (SingleBag.composition_id == null)
                        {
                            cmd.Parameters.Add("@composition_ID", SqlDbType.Int).Value = 0; //return bags have no composition
                        }
                        else
                        {
                            cmd.Parameters.Add("@composition_ID", SqlDbType.Int).Value = SingleBag.composition_id; //return bags have no composition 
                        }
                        cmd.Parameters.Add("@function_ID", SqlDbType.Int).Value = SingleBag.bag_function_id;
                        cmd.Parameters.Add("@unid", SqlDbType.Int).Direction = ParameterDirection.Output;

                        con.Open();
                        cmd.ExecuteNonQuery();
                        VAR.ID = Convert.ToInt32(cmd.Parameters["@unid"].Value.ToString());
                        con.Close();

                    }
                }
                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertSingleBag Completed successfully", "I", false);

            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertSingleBag: " + e.ToString(), "E", false);
            }

            return VAR;
        }



        //--------------------------------------------------------------------------------------------------------------------------------------------------







        [WebMethod]
        public VarianceActivityResponse DeleteBagsForSpecififcEventID(int eventid)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();



            try
            {
                if (eventid > 0)
                {
                    ConnectionString con_str = GetConnectionString(2);
                    using (SqlConnection con = new SqlConnection(con_str.connection_string))
                    {
                        using (SqlCommand cmd = new SqlCommand("SP_Bag_dim_Delete"))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@event_id", eventid);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                            VAR.status = true;
                            VertedaDiagnostics.LogMessage("DeleteBags Completed successfully", "I", false);
                        }
                    }
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not get event ID from web service";
                    VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in DeleteBags: " + e.ToString(), "E", false);

            }

            return VAR;
        }

        
        [WebMethod]
        public VarianceActivityResponse InsertKidToTidJoinForSpecififcEventID(int ParentID, int ChildID, int eventid)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("InsertKidToTidJoinForSpecififcEventID started. child / parent:" + ChildID + " " + ParentID, "I", false);


            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Kid_To_Tid_Insert";
                cmd.Parameters.Add("@eventID", SqlDbType.Int).Value = eventid;
                cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = ParentID;
                cmd.Parameters.Add("@ChildID", SqlDbType.Int).Value = ChildID;
                cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Connection = conn;
                //return package_id to use in copy

                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.ID = Convert.ToInt32(cmd.Parameters["@id"].Value.ToString());
                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertKidToTidJoinForSpecififcEventID Completed successfully", "I", false);
               //VertedaDiagnostics.LogMessage("CreateComposition Completed successfully", "I", false);

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertKidToTidJoinForSpecififcEventID: " + ex.ToString(), "E", false);
                //VertedaDiagnostics.LogMessage("Error in CreateComposition: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }


        #endregion 

        #region Get procedures - top 6 done. tested
        //below need try catch error handling

        [WebMethod] //DONE
        public VarianceActivityResponseDenominations Get_Denominations()
        {
            VarianceActivityResponseDenominations VAR = new VarianceActivityResponseDenominations();

            List<Denominations> sqlDenoms = new List<Denominations>();
            //get denomoinations from sql

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [denomination_id],ISNULL([description], 'NULL') AS [description],ISNULL([format], 'NULL') AS [format], ISNULL([value] , -1) AS [value]FROM [variance_cash].[dbo].[Denominations]";// LOAD DENOMINATIONS

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Denominations item = new Denominations();
                        item.denomination_id = Convert.ToInt32(rdr["denomination_id"].ToString());
                        item.denomination_name = rdr["description"].ToString();
                        item.denomination_format = rdr["format"].ToString();
                        item.denomination_value = Math.Round(Convert.ToDecimal(rdr["value"].ToString()), 2);
                                                


                        // if fields queried from database increases, update string array size.
                        //string[] rowValues = new string[4];
                        //for (int i = 0; i < rdr.FieldCount; i++)
                        //{
                        //    if (rdr.IsDBNull(i))
                        //    {
                        //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                        //    }
                        //    else
                        //        rowValues[i] = rdr.GetValue(i).ToString();
                        //}

                        //Denominations item = new Denominations();
                        //item.denomination_id = Convert.ToInt32(rowValues[0]);
                        //item.denomination_name = rowValues[1];
                        //item.denomination_format = rowValues[2];
                        //item.denomination_value = Convert.ToDecimal(rowValues[3]);

                        sqlDenoms.Add(item);
                    }
                    rdr.Close();

                    conn.Close();
                    
                }
                VAR.ListOfDenominations = sqlDenoms;
                VAR.status = true;
                VertedaDiagnostics.LogMessage("Get_Denominations Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                VAR.errorText = e.ToString();
                VAR.status = false;
                VertedaDiagnostics.LogMessage("Error in Get_Denominations: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseCompositions Get_Compositions()
        {
            VarianceActivityResponseCompositions VAR = new VarianceActivityResponseCompositions();


            List<Composition_Details> comp_list = new List<Composition_Details>();
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {

                    }

                if (conn.State == ConnectionState.Open)
                {
                    cmd = new SqlCommand("SELECT [composition_id], ISNULL([total_value], -1) AS [total_value], ISNULL([Name], 'NULL') AS [Name] FROM [variance_cash].[dbo].[Composition_Dim]", conn);
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Composition_Details item = new Composition_Details();
                        item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                        item.composition_value = Math.Round(Convert.ToDecimal(rdr["total_value"].ToString()),2);
                        item.composition_name = rdr["Name"].ToString().Trim();


                        //// if fields queried from database increases, update string array size.
                        //string[] rowValues = new string[3];
                        //for (int i = 0; i < rdr.FieldCount; i++)
                        //{
                        //    if (rdr.IsDBNull(i))
                        //    {
                        //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                        //    }
                        //    else
                        //        rowValues[i] = rdr.GetValue(i).ToString();
                        //}

                        //Composition_Details item = new Composition_Details();
                        //item.composition_id = Convert.ToInt32(rowValues[0]);
                        //item.composition_value = Convert.ToDecimal(rowValues[1]);
                        //item.composition_name = rowValues[2].Trim();

                        comp_list.Add(item);

                    }
                    rdr.Close();
                    conn.Close();
                }

                VAR.Compositions = comp_list;
                VAR.status = true;
                VertedaDiagnostics.LogMessage("Get_Compositions Completed successfully", "I", false);
            }
            catch(Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in Get_Compositions: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        
        [WebMethod] //done
        public VarianceActivityResponseCompositionDetails Get_Composition_Details(int compositionID)
        {
            VarianceActivityResponseCompositionDetails VAR = new VarianceActivityResponseCompositionDetails();

            List<Denominations> composition_details = new List<Denominations>();
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;
                if (conn.State != ConnectionState.Open)
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {

                    }

                if (conn.State == ConnectionState.Open)
                {
                    cmd = new SqlCommand("SELECT [Composition_Denomination_Join].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Composition_Dim].[total_value], -1) AS [total_value],[Composition_Denomination_Join].[denomination_id],ISNULL([denomination_qty], -1) AS [denomination_qty],ISNULL([Denominations].[description], 'NULL') AS [description],ISNULL([Denominations].[format], 'NULL') AS [format], ISNULL([Denominations].[value], -1) AS  [value] FROM [variance_cash].[dbo].[Composition_Denomination_Join] INNER JOIN [variance_cash].[dbo].[Composition_Dim]  ON [Composition_Denomination_Join].[composition_id] = [Composition_Dim].[composition_id]  INNER JOIN [variance_cash].[dbo].[Denominations]  ON [Composition_Denomination_Join].[denomination_id] = [Denominations].[denomination_id] WHERE [Composition_Denomination_Join].[composition_id] = " + compositionID, conn);
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {

                        Denominations item = new Denominations();

                        item.denomination_id = Convert.ToInt32(rdr["denomination_id"].ToString());
                        item.denomination_qty = Convert.ToInt32(rdr["denomination_qty"].ToString());
                        item.denomination_name = rdr["description"].ToString();
                        item.denomination_format = rdr["format"].ToString();
                        item.denomination_value = Math.Round(Convert.ToDecimal(rdr["value"].ToString()), 2);

                        item.denomination_total_value = Math.Round((item.denomination_qty * item.denomination_value), 2);
                        // if fields queried from database increases, update string array size.
                        //string[] rowValues = new string[8];
                        //for (int i = 0; i < rdr.FieldCount; i++)
                        //{
                        //    if (rdr.IsDBNull(i))
                        //    {
                        //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                        //    }
                        //    else
                        //        rowValues[i] = rdr.GetValue(i).ToString();
                        //}

                        //Composition_Details item = new Composition_Details();

                        //item.composition_id = Convert.ToInt32(rowValues[0]);
                        //item.composition_name = rowValues[1];
                        //item.composition_value = Convert.ToDecimal(rowValues[2]);
                        //item.denomination_id = Convert.ToInt32(rowValues[3]);
                        //item.denomination_qty = Convert.ToInt32(rowValues[4]);
                        //item.denomination_description = rowValues[5];
                        //item.denomination_format = rowValues[6];
                        //item.denomination_value = Convert.ToDecimal(rowValues[7]);

                        VAR.CompositionID = Convert.ToInt32(rdr["composition_id"].ToString());
                        VAR.CompositionName = rdr["Name"].ToString();
                        VAR.CompositionValue = Math.Round(Convert.ToDecimal(rdr["total_value"].ToString()), 2);

                        composition_details.Add(item);

                    }
                    rdr.Close();
                    conn.Close();
                }
                VAR.status = true;
                VAR.DenominationDetails = composition_details;
                VertedaDiagnostics.LogMessage("Get_Composition_Details Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                VAR.errorText = e.ToString();
                VAR.status = false;
                VertedaDiagnostics.LogMessage("Error in Get_Composition_Details: " + e.ToString(), "E", false); 
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseCompositionLocationJoins Get_Location_Default_Compositions()
        {
            VarianceActivityResponseCompositionLocationJoins VAR = new VarianceActivityResponseCompositionLocationJoins();

            List<LocationAndTerminal> join_list = new List<LocationAndTerminal>();
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;
                if (conn.State != ConnectionState.Open)
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {

                    }

                if (conn.State == ConnectionState.Open)
                {
                    //SqlCommand cmd = new SqlCommand("SELECT [profit_center_id] ,[Profit_Composition_Join].[composition_id] ,[Composition_Dim].[Name] ,[venue_location_master].[location_name] FROM [variance_cash].[dbo].[Profit_Composition_Join] INNER JOIN [variance_cash].[dbo].[Composition_Dim] ON [Profit_Composition_Join].[composition_id] = [Composition_Dim].[composition_id] INNER JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Profit_Composition_Join].[profit_center_id] = [venue_location_master].[id]", conn);
                    //SqlCommand cmd = new SqlCommand("SELECT [Profit_Composition_Join].[profit_center_id], ISNULL([Profit_Composition_Join].[composition_id], -1) AS [composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name]FROM [variance_cash].[dbo].[Profit_Composition_Join] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Profit_Composition_Join].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Profit_Composition_Join].[profit_center_id] = [venue_location_master].[id]", conn);
                    cmd = new SqlCommand("SELECT [Profit_Composition_Join].[profit_center_id], ISNULL([Profit_Composition_Join].[composition_id], -1) AS [composition_id], ISNULL([Composition_Dim].[Name], 'NULL') AS [Name], ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name], COUNT([Venue_Terminal_Master].[terminal_id]) AS number_of_terminals FROM [variance_cash].[dbo].[Profit_Composition_Join] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Profit_Composition_Join].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Profit_Composition_Join].[profit_center_id] = [venue_location_master].[id] LEFT JOIN [variance_cash].[dbo].[Venue_Terminal_Master] ON [Profit_Composition_Join].[profit_center_id] = [Venue_Terminal_Master].[profit_center_id] GROUP BY [Profit_Composition_Join].[profit_center_id], [venue_location_master].[location_name],[Composition_Dim].[Name], [Profit_Composition_Join].[composition_id]", conn);
                    
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {

                        LocationAndTerminal item = new LocationAndTerminal();
                        item.location_id = Convert.ToInt32(rdr["profit_center_id"].ToString());
                        item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                        item.composition_name = rdr["Name"].ToString();
                        item.location_name = rdr["location_name"].ToString();

                        // if fields queried from database increases, update string array size.
                        //string[] rowValues = new string[4];
                        //for (int i = 0; i < rdr.FieldCount; i++)
                        //{
                        //    if (rdr.IsDBNull(i))
                        //    {
                        //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                        //    }
                        //    else
                        //        rowValues[i] = rdr.GetValue(i).ToString();
                        //}

                        //LocationCompositionJoin item = new LocationCompositionJoin();
                        //item.location_id = Convert.ToInt32(rowValues[0]);
                        //item.composition_id = Convert.ToInt32(rowValues[1]);
                        //item.composition_name = rowValues[2];
                        //item.location_name = rowValues[2];

                        join_list.Add(item);

                    }
                    rdr.Close();
                    conn.Close();
                }
                VAR.status = true;
                VAR.CompositionLocationJoins = join_list;
                VertedaDiagnostics.LogMessage("Get_Location_Default_Composition Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in Get_Location_Default_Composition: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] // done
        public VarianceActivityResponseCompositions Get_Existing_Composition_Name_Matches(string composition_name)
        {
            VarianceActivityResponseCompositions VAR = new VarianceActivityResponseCompositions();
            List<Composition_Details> composition_list = new List<Composition_Details>();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {

                    }

                if (conn.State == ConnectionState.Open)
                {
                    cmd = new SqlCommand("SELECT ISNULL([Name], 'NULL') AS [Name], [composition_id] FROM [variance_cash].[dbo].[Composition_Dim] WHERE [Name] = '" + composition_name + "'", conn);
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Composition_Details item = new Composition_Details();
                        item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                        item.composition_name = rdr["Name"].ToString();

                        //// if fields queried from database increases, update string array size.
                        //string[] rowValues = new string[2];
                        //for (int i = 0; i < rdr.FieldCount; i++)
                        //{
                        //    if (rdr.IsDBNull(i))
                        //    {
                        //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                        //    }
                        //    else
                        //        rowValues[i] = rdr.GetValue(i).ToString();
                        //}

                        //Composition_Details item = new Composition_Details();
                        //item.composition_id = Convert.ToInt32(rowValues[1]);
                        //item.composition_name = rowValues[0];

                        composition_list.Add(item);

                    }
                    rdr.Close();
                    conn.Close();
                }
                VAR.status = true;
                VAR.Compositions = composition_list;
                VertedaDiagnostics.LogMessage("Get_Existing_Composition_Name_Matches Completed successfully", "I", false);
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in Get_Existing_Composition_Name_Matches: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        
        [WebMethod] //done
        public VarianceActivityResponseBags GetExistingBags()
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                int eventid = GetEventID();
                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        //cmd.CommandText = "SELECT [event_id],[bag_type],[bag_id],[profit_center_id],[composition_id]  FROM [variance_cash].[dbo].[Bag_Dim]  WHERE [event_id] = " + eventid;// LOAD DENOMINATIONS
                        //cmd.CommandText = "SELECT [event_id],[event_master].[event_name],[bag_type] ,[bag_id] ,[label_id] ,[created_date] ,[created_by_employee],[profit_center_id] ,[venue_location_master].[location_name],[Bag_Dim].[composition_id] ,[Composition_Dim].[Name] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Bag_Dim].[profit_center_id] = [venue_location_master].[id] WHERE event_id = " + eventid;// LOAD DENOMINATIONS

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id, [bag_dim].[unid],[event_id],ISNULL([total_child_bags], -1) AS [total_child_bags],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin  ON [Bag_Dim].[origin_location_id] = origin.[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination ON [Bag_Dim].[destination_location_id] = destination.[id]LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid] LEFT JOIN [variance_cash].[dbo].[Action_Dim] ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id =" + eventid;

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G"); 
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bags"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());


                            //// if fields queried from database increases, update string array size.
                            //string[] rowValues = new string[13];
                            //for (int i = 0; i < rdr.FieldCount; i++)
                            //{
                            //    if (rdr.IsDBNull(i))
                            //    {
                            //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                            //    }
                            //    else
                            //        rowValues[i] = rdr.GetValue(i).ToString();
                            //}

                            //Bag item = new Bag();
                            //item.event_id = Convert.ToInt32(rowValues[0]);
                            //item.event_name = rowValues[1];
                            //item.bag_type = rowValues[2];
                            //item.bag_id = rowValues[3];
                            //item.label_id = Convert.ToInt32(rowValues[4]);
                            //item.created_date = Convert.ToDateTime(rowValues[5]);
                            //item.employee_id = Convert.ToInt32(rowValues[6]);
                            //item.location_id = Convert.ToInt32(rowValues[7]);
                            //item.location_name = rowValues[8];
                            //item.composition_id = Convert.ToInt32(rowValues[9]);
                            //item.composition_name = rowValues[10];
                            //item.action_id = Convert.ToInt32(rowValues[11]);
                            //item.action_name = rowValues[12];

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetExistingBags query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }
 
                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseBags GetExistingBagsForSpecififcEventID(int eventid)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        //cmd.CommandText = "SELECT [event_id],[bag_type],[bag_id],[profit_center_id],[composition_id]  FROM [variance_cash].[dbo].[Bag_Dim]  WHERE [event_id] = " + eventid;// LOAD DENOMINATIONS
                        //cmd.CommandText = "SELECT [event_id],[event_master].[event_name],[bag_type] ,[bag_id] ,[label_id] ,[created_date] ,[created_by_employee],[profit_center_id] ,[venue_location_master].[location_name],[Bag_Dim].[composition_id] ,[Composition_Dim].[Name] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Bag_Dim].[profit_center_id] = [venue_location_master].[id] WHERE event_id = " + eventid;// LOAD DENOMINATIONS

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id, [bag_dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bags],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin  ON [Bag_Dim].[origin_location_id] = origin.[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination ON [Bag_Dim].[destination_location_id] = destination.[id]LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid] LEFT JOIN [variance_cash].[dbo].[Action_Dim] ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id =" + eventid;

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bags"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());

                            //// if fields queried from database increases, update string array size.
                            //string[] rowValues = new string[13];
                            //for (int i = 0; i < rdr.FieldCount; i++)
                            //{
                            //    if (rdr.IsDBNull(i))
                            //    {
                            //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                            //    }
                            //    else
                            //        rowValues[i] = rdr.GetValue(i).ToString();
                            //}

                            //Bag item = new Bag();
                            //item.event_id = Convert.ToInt32(rowValues[0]);
                            //item.event_name = rowValues[1];
                            //item.bag_type = rowValues[2];
                            //item.bag_id = rowValues[3];
                            //item.label_id = Convert.ToInt32(rowValues[4]);
                            //item.created_date = Convert.ToDateTime(rowValues[5]);
                            //item.employee_id = Convert.ToInt32(rowValues[6]);
                            //item.location_id = Convert.ToInt32(rowValues[7]);
                            //item.location_name = rowValues[8];
                            //item.composition_id = Convert.ToInt32(rowValues[9]);
                            //item.composition_name = rowValues[10];
                            //item.action_id = Convert.ToInt32(rowValues[11]);
                            //item.action_name = rowValues[12];

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetExistingBags query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }

                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        [WebMethod] //done
        public VarianceActivityResponseBags GetExistingKioskBags(int mode) //1 = all, 2 = out, 3 = in
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                int eventid = GetEventID(); //GetEventIDForCurrentBusinessDay();

                string modeParam = "";
                if (mode == 2)
                {
                    modeParam = " AND [Bag_Action_Join].action_id IN (2,3,4)"; 
                }
                else if(mode == 3)
                {
                    modeParam = " AND [Bag_Action_Join].action_id IN (7,8,9)";
                }

                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        //cmd.CommandText = "SELECT [event_id],[bag_type],[bag_id],[profit_center_id],[composition_id]  FROM [variance_cash].[dbo].[Bag_Dim]  WHERE [event_id] = " + eventid;// LOAD DENOMINATIONS
                        //cmd.CommandText = "SELECT [event_id],[event_master].[event_name],[bag_type] ,[bag_id] ,[label_id] ,[created_date] ,[created_by_employee],[profit_center_id] ,[venue_location_master].[location_name],[Bag_Dim].[composition_id] ,[Composition_Dim].[Name] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Bag_Dim].[profit_center_id] = [venue_location_master].[id] WHERE event_id = " + eventid;// LOAD DENOMINATIONS

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id, [bag_dim].[unid],[event_id],ISNULL([total_child_bags], -1) AS [total_child_bags],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin  ON [Bag_Dim].[origin_location_id] = origin.[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination ON [Bag_Dim].[destination_location_id] = destination.[id]LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid] LEFT JOIN [variance_cash].[dbo].[Action_Dim] ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id =" + eventid + "AND [Bag_Action_Join].end_date IS NULL AND bag_type = 'Kiosk'" + modeParam;

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bags"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());

                            //// if fields queried from database increases, update string array size.
                            //string[] rowValues = new string[13];
                            //for (int i = 0; i < rdr.FieldCount; i++)
                            //{
                            //    if (rdr.IsDBNull(i))
                            //    {
                            //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                            //    }
                            //    else
                            //        rowValues[i] = rdr.GetValue(i).ToString();
                            //}

                            //Bag item = new Bag();
                            //item.event_id = Convert.ToInt32(rowValues[0]);
                            //item.event_name = rowValues[1];
                            //item.bag_type = rowValues[2];
                            //item.bag_id = rowValues[3];
                            //item.label_id = Convert.ToInt32(rowValues[4]);
                            //item.created_date = Convert.ToDateTime(rowValues[5]);
                            //item.employee_id = Convert.ToInt32(rowValues[6]);
                            //item.location_id = Convert.ToInt32(rowValues[7]);
                            //item.location_name = rowValues[8];
                            //item.composition_id = Convert.ToInt32(rowValues[9]);
                            //item.composition_name = rowValues[10];
                            //item.action_id = Convert.ToInt32(rowValues[11]);
                            //item.action_name = rowValues[12];

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetExistingBags query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }

                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        
        [WebMethod] //done
        public VarianceActivityResponseBags GetExistingKioskBagsForSpecififcEventID(int eventid, int mode)//1 = all, 2 = out, 3 = in
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                string modeParam = "";
                if (mode == 2)
                {
                    modeParam = " AND [Bag_Action_Join].action_id IN (2,3,4)";
                }
                else if (mode == 3)
                {
                    modeParam = " AND [Bag_Action_Join].action_id IN (7,8,9)";
                }

                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        //cmd.CommandText = "SELECT [event_id],[bag_type],[bag_id],[profit_center_id],[composition_id]  FROM [variance_cash].[dbo].[Bag_Dim]  WHERE [event_id] = " + eventid;// LOAD DENOMINATIONS
                        //cmd.CommandText = "SELECT [event_id],[event_master].[event_name],[bag_type] ,[bag_id] ,[label_id] ,[created_date] ,[created_by_employee],[profit_center_id] ,[venue_location_master].[location_name],[Bag_Dim].[composition_id] ,[Composition_Dim].[Name] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Bag_Dim].[profit_center_id] = [venue_location_master].[id] WHERE event_id = " + eventid;// LOAD DENOMINATIONS

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id, [bag_dim].[unid],[event_id],ISNULL([total_child_bags], -1) AS [total_child_bags],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin  ON [Bag_Dim].[origin_location_id] = origin.[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination ON [Bag_Dim].[destination_location_id] = destination.[id]LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid] LEFT JOIN [variance_cash].[dbo].[Action_Dim] ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id =" + eventid + " AND [Bag_Action_Join].end_date IS NULL AND bag_type = 'Kiosk'" + modeParam;

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bags"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());

                            //// if fields queried from database increases, update string array size.
                            //string[] rowValues = new string[13];
                            //for (int i = 0; i < rdr.FieldCount; i++)
                            //{
                            //    if (rdr.IsDBNull(i))
                            //    {
                            //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                            //    }
                            //    else
                            //        rowValues[i] = rdr.GetValue(i).ToString();
                            //}

                            //Bag item = new Bag();
                            //item.event_id = Convert.ToInt32(rowValues[0]);
                            //item.event_name = rowValues[1];
                            //item.bag_type = rowValues[2];
                            //item.bag_id = rowValues[3];
                            //item.label_id = Convert.ToInt32(rowValues[4]);
                            //item.created_date = Convert.ToDateTime(rowValues[5]);
                            //item.employee_id = Convert.ToInt32(rowValues[6]);
                            //item.location_id = Convert.ToInt32(rowValues[7]);
                            //item.location_name = rowValues[8];
                            //item.composition_id = Convert.ToInt32(rowValues[9]);
                            //item.composition_name = rowValues[10];
                            //item.action_id = Convert.ToInt32(rowValues[11]);
                            //item.action_name = rowValues[12];

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetExistingBags query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }

                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseLocationsAndTerminal GetExistingKioskBagLocationsForClonedEvent(int eventid)
        {
            VarianceActivityResponseLocationsAndTerminal VAR = new VarianceActivityResponseLocationsAndTerminal();
            VertedaDiagnostics.LogMessage("GetExistingKioskBagLocationsForClonedEvent query started.", "I", false);

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }

                    ConfigSettings conf = ReadConfig();



                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;


                        cmd.CommandText = "SELECT [Bag_Dim].[unid] ,[Bag_Dim].[event_id]  ,[bag_type] ,[bag_id]  ,[label_id]  ,[created_date]  ,[created_by_employee]  ,[origin_location_id]  ,[destination_location_id], [venue_location_master].[location_name], [composition_id] ,[bag_function_id] ,(SELECT count([child_id])   FROM [variance_cash].[dbo].[Kid_Tid_Join]  WHERE parent_id = [Bag_Dim].[unid]) as child_bag_count  FROM [variance_cash].[dbo].[Bag_Dim]  INNER JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Bag_Dim].[destination_location_id] = [venue_location_master].[id] WHERE [Bag_Dim].event_id = " + eventid + " AND bag_type = 'Kiosk'  AND origin_location_id = " + conf.CashOfficeID;

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.total_child_bags = Convert.ToInt32(rdr["child_bag_count"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            item.destination_name = rdr["location_name"].ToString();

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    //convert the bags into locations for use in generating new bags
                    List<LocationAndTerminal> LaT = new List<LocationAndTerminal>();
                    foreach (Bag bg in currentBags)
                    {
                        LocationAndTerminal item = new LocationAndTerminal();
                        item.location_id = bg.destination_location_id;
                        item.location_name = bg.destination_name;
                        item.composition_id = bg.composition_id;
                        item.open_terminal_count = bg.total_child_bags;

                        LaT.Add(item);
                    }



                    VAR.status = true;
                    VAR.LocationsTerminals = LaT;
                    VertedaDiagnostics.LogMessage("GetExistingKioskBagLocationsForClonedEvent query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("GetExistingKioskBagLocationsForClonedEvent: No bags exist for the current event", "I", false);
                    }

                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "GetExistingKioskBagsForClonedEvent: could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("GetExistingKioskBagsForClonedEvent: could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingKioskBagsForClonedEvent: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        
        

        
        [WebMethod] //done
        public VarianceActivityResponseBags GetSingleBagInfo(string bagID)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;


                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        //cmd.CommandText = "SELECT [event_id],[bag_type],[bag_id],[profit_center_id],[composition_id]  FROM [variance_cash].[dbo].[Bag_Dim]  WHERE [event_id] = " + eventid;// LOAD DENOMINATIONS
                        //cmd.CommandText = "SELECT [event_id],[event_master].[event_name],[bag_type] ,[bag_id] ,[label_id] ,[created_date] ,[created_by_employee],[profit_center_id] ,[venue_location_master].[location_name],[Bag_Dim].[composition_id] ,[Composition_Dim].[Name] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Bag_Dim].[profit_center_id] = [venue_location_master].[id] WHERE event_id = " + eventid;// LOAD DENOMINATIONS
                        
                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id, [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin  ON [Bag_Dim].[origin_location_id] = origin.[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination ON [Bag_Dim].[destination_location_id] = destination.[id] LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid] LEFT JOIN [variance_cash].[dbo].[Action_Dim] ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE [bag_dim].[bag_id] = '" + bagID + "'  AND ( [Bag_Action_Join].[end_date] IS NULL OR ([Bag_Action_Join].action_id = 10 AND Bag_Dim.bag_type = 'Terminal') OR ([Bag_Action_Join].action_id = 9 AND Bag_Dim.bag_type = 'Kiosk') )";

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();    
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());



                            //// if fields queried from database increases, update string array size.
                            //string[] rowValues = new string[13];
                            //for (int i = 0; i < rdr.FieldCount; i++)
                            //{
                            //    if (rdr.IsDBNull(i))
                            //    {
                            //        rowValues[i] = "-1"; // LOG THAT VALUE IS NULL
                            //    }
                            //    else
                            //        rowValues[i] = rdr.GetValue(i).ToString();
                            //}

                            //Bag item = new Bag();
                            //item.event_id = Convert.ToInt32(rowValues[0]);
                            //item.event_name = rowValues[1];
                            //item.bag_type = rowValues[2];
                            //item.bag_id = rowValues[3];
                            //item.label_id = Convert.ToInt32(rowValues[4]);
                            //item.created_date = Convert.ToDateTime(rowValues[5]);
                            //item.employee_id = Convert.ToInt32(rowValues[6]);
                            //item.location_id = Convert.ToInt32(rowValues[7]);
                            //item.location_name = rowValues[8];
                            //item.composition_id = Convert.ToInt32(rowValues[9]);
                            //item.composition_name = rowValues[10];
                            //item.action_id = Convert.ToInt32(rowValues[11]);
                            //item.action_name = rowValues[12];

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetExistingBags query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }

                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }


            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        
        [WebMethod] //done
        public VarianceActivityResponseBagChildren GetBagChildren(int bagUnid)
        {
            VertedaDiagnostics.LogMessage("GetBagChildren started. bagunid: " +bagUnid, "I", false);

            VarianceActivityResponseBagChildren VAR = new VarianceActivityResponseBagChildren();

            List<BagJoins> currentBagJoins = new List<BagJoins>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT [event_id],[parent_id],[child_id] FROM [variance_cash].[dbo].[Kid_Tid_Join] WHERE [parent_id] = " + bagUnid;


                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            BagJoins item = new BagJoins();
                            item.Event_ID = Convert.ToInt32(rdr["event_id"].ToString());
                            item.Parent_ID = Convert.ToInt32(rdr["parent_id"].ToString());
                            item.Child_ID = Convert.ToInt32(rdr["child_id"].ToString());



                            currentBagJoins.Add(item);
                        }
                        rdr.Close();

                        conn.Close();


                    VAR.status = true;
                    VAR.BagJoins = currentBagJoins;
                    VertedaDiagnostics.LogMessage("GetBagChildren query completed", "I", false);
                }                
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetBagChildren: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseBagChildren GetBagParent(int bagUnid)
        {
            VarianceActivityResponseBagChildren VAR = new VarianceActivityResponseBagChildren();

            List<BagJoins> currentBagJoins = new List<BagJoins>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [event_id],[parent_id],[child_id] FROM [variance_cash].[dbo].[Kid_Tid_Join] WHERE [child_id] = " + bagUnid;


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        BagJoins item = new BagJoins();
                        item.Event_ID = Convert.ToInt32(rdr["event_id"].ToString());
                        item.Parent_ID = Convert.ToInt32(rdr["parent_id"].ToString());
                        item.Child_ID = Convert.ToInt32(rdr["child_id"].ToString());



                        currentBagJoins.Add(item);
                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VAR.BagJoins = currentBagJoins;
                    VertedaDiagnostics.LogMessage("GetBagParent query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetBagParent: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseBagUNID GetLastTerminalBagAmount(int bagUnid)
        {
            VarianceActivityResponseBagUNID VAR = new VarianceActivityResponseBagUNID();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [bag_unid] ,ISNULL([total_value], 0) AS [total_value]	FROM [variance_cash].[dbo].[Bag_Action_Join] WHERE end_date IS NULL AND bag_unid = " +bagUnid;


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        decimal bagAmt = Convert.ToDecimal(rdr["total_value"].ToString());
                        int sqlBagUnid = Convert.ToInt32(rdr["bag_unid"].ToString());

                        VAR.BagAmount = bagAmt;
                        VAR.BagUNID = sqlBagUnid;

                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("GetLastTerminalBagAmount query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetLastTerminalBagAmount: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponseLocationsAndTerminal GetOpenLocations()
        {

            VarianceActivityResponseLocationsAndTerminal VAR = new VarianceActivityResponseLocationsAndTerminal();
            List<LocationAndTerminal> currentLocations = new List<LocationAndTerminal>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                int eventid = GetEventID(); //GetEventIDForCurrentBusinessDay();

                if (eventid > 0)
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT [venue_location_master].[id],ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name],ISNULL([Profit_Composition_Join].[composition_id], -1) AS [composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name]FROM ["+con_str.database_name+"].[dbo].[venue_location_master] LEFT JOIN [variance_cash].[dbo].[Profit_Composition_Join]ON [venue_location_master].[id] = [Profit_Composition_Join].[profit_center_id]LEFT JOIN [variance_cash].[dbo].[Composition_Dim]ON [Profit_Composition_Join].[composition_id] = [Composition_Dim].[composition_id]WHERE NOT EXISTS (SELECT 1 FROM ["+con_str.database_name+"].[dbo].[event_closed_locations_master] WHERE [venue_location_master].[id] = [event_closed_locations_master].[location_id] AND [event_closed_locations_master].[event_id] = " + eventid + ")";


                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            LocationAndTerminal item = new LocationAndTerminal();
                            item.location_id = Convert.ToInt32(rdr["id"].ToString());
                            item.location_name = rdr["location_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();

                            currentLocations.Add(item);
                        }

                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.LocationsTerminals = currentLocations;
                    VertedaDiagnostics.LogMessage("GetOpenLocations Completed successfully", "I", false);
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not get event ID from web service";
                    VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetOpenLocations: " + e.ToString(), "E", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponseLocationsAndTerminal GetOpenLocationsForSpecififcEventID(int eventid)
        {

            VarianceActivityResponseLocationsAndTerminal VAR = new VarianceActivityResponseLocationsAndTerminal();
            List<LocationAndTerminal> currentLocations = new List<LocationAndTerminal>();
            VertedaDiagnostics.LogMessage("GetOpenLocationsForSpecififcEventID started", "I", false);

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (eventid > 0)
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                            VertedaDiagnostics.LogMessage("Error in GetOpenLocationsForSpecififcEventID when connecting to SQL", "E", false);
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT [venue_location_master].[id],ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name],ISNULL([Profit_Composition_Join].[composition_id], -1) AS [composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name]FROM ["+con_str.database_name+"].[dbo].[venue_location_master] LEFT JOIN [variance_cash].[dbo].[Profit_Composition_Join]ON [venue_location_master].[id] = [Profit_Composition_Join].[profit_center_id]LEFT JOIN [variance_cash].[dbo].[Composition_Dim]ON [Profit_Composition_Join].[composition_id] = [Composition_Dim].[composition_id]WHERE NOT EXISTS (SELECT 1 FROM ["+con_str.database_name+"].[dbo].[event_closed_locations_master] WHERE [venue_location_master].[id] = [event_closed_locations_master].[location_id] AND [event_closed_locations_master].[event_id] = " + eventid + ")";


                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            LocationAndTerminal item = new LocationAndTerminal();
                            item.location_id = Convert.ToInt32(rdr["id"].ToString());
                            item.location_name = rdr["location_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();

                            currentLocations.Add(item);
                        }

                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.LocationsTerminals = currentLocations;
                    VertedaDiagnostics.LogMessage("GetOpenLocationsForSpecififcEventID Completed successfully", "I", false);
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not get event ID from web service";
                    VertedaDiagnostics.LogMessage("Could not get event ID from web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetOpenLocationsForSpecififcEventID: " + e.ToString(), "E", false);
            }

            return VAR;
        }
        
        [WebMethod]
        public VarianceActivityResponseLocationsAndTerminal GetOpenTerminals(int location_id)
        {
            VarianceActivityResponseLocationsAndTerminal VAR = new VarianceActivityResponseLocationsAndTerminal();

            List<LocationAndTerminal> openTerminals = new List<LocationAndTerminal>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                int eventid = GetEventID(); //####

                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        //cmd.CommandText = "SELECT [terminal_id] FROM [variance_cash].[dbo].[Venue_Terminal_Master] WHERE NOT EXISTS (SELECT 1 FROM [variance_cash].[dbo].[event_closed_terminal_master] WHERE [Venue_Terminal_Master].[terminal_id] = [event_closed_terminal_master].[terminal_id] AND [event_closed_terminal_master].event_id = " + eventid + ") AND [Venue_Terminal_Master].[profit_center_id] = " + locationid;// LOAD DENOMINATIONS
                        cmd.CommandText = "SELECT [Venue_Terminal_Master].[terminal_id], ISNULL([Venue_Terminal_Master].[terminal_name], 'NULL') AS [terminal_name], [Venue_Terminal_Master].[profit_center_id], ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name]FROM [variance_cash].[dbo].[Venue_Terminal_Master] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]ON [Venue_Terminal_Master].[profit_center_id] = [venue_location_master].[id] WHERE NOT EXISTS (SELECT 1 FROM [variance_cash].[dbo].[event_closed_terminal_master] LEFT JOIN [variance_cash].[dbo].[Venue_Terminal_Master]ON [event_closed_terminal_master].[terminal_id] = [Venue_Terminal_Master].[terminal_id]WHERE [Venue_Terminal_Master].[terminal_id] = [event_closed_terminal_master].[terminal_id] AND [event_closed_terminal_master].event_id = " + eventid + " ) AND [Venue_Terminal_Master].[profit_center_id] = " + location_id;
                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            LocationAndTerminal item = new LocationAndTerminal();
                            item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());
                            item.terminal_name = rdr["terminal_name"].ToString();
                            item.location_id = Convert.ToInt32(rdr["profit_center_id"].ToString());
                            item.location_name = rdr["location_name"].ToString();

                            openTerminals.Add(item);
                        }
                        rdr.Close();

                        conn.Close();

                        VAR.status = true;
                        VAR.LocationsTerminals = openTerminals;
                        VertedaDiagnostics.LogMessage("SelectOpenTerminals Completed successfully", "I", false);
                    }
                }

                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not get eventId from web service when attempting GetOpenTerminals";
                    VertedaDiagnostics.LogMessage("Could not get eventId from web service when attempting GetOpenTerminals", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SelectOpenTerminals: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponseLocationsAndTerminal GetOpenTerminalsForSpecififcEventID(int location_id, int eventid)
        {
            VarianceActivityResponseLocationsAndTerminal VAR = new VarianceActivityResponseLocationsAndTerminal();

            List<LocationAndTerminal> openTerminals = new List<LocationAndTerminal>();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        //cmd.CommandText = "SELECT [terminal_id] FROM [variance_cash].[dbo].[Venue_Terminal_Master] WHERE NOT EXISTS (SELECT 1 FROM [variance_cash].[dbo].[event_closed_terminal_master] WHERE [Venue_Terminal_Master].[terminal_id] = [event_closed_terminal_master].[terminal_id] AND [event_closed_terminal_master].event_id = " + eventid + ") AND [Venue_Terminal_Master].[profit_center_id] = " + locationid;// LOAD DENOMINATIONS
                        cmd.CommandText = "SELECT [Venue_Terminal_Master].[terminal_id], ISNULL([Venue_Terminal_Master].[terminal_name], 'NULL') AS [terminal_name], [Venue_Terminal_Master].[profit_center_id], ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name]FROM [variance_cash].[dbo].[Venue_Terminal_Master] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]ON [Venue_Terminal_Master].[profit_center_id] = [venue_location_master].[id] WHERE NOT EXISTS (SELECT 1 FROM [variance_cash].[dbo].[event_closed_terminal_master] LEFT JOIN [variance_cash].[dbo].[Venue_Terminal_Master]ON [event_closed_terminal_master].[terminal_id] = [Venue_Terminal_Master].[terminal_id]WHERE [Venue_Terminal_Master].[terminal_id] = [event_closed_terminal_master].[terminal_id] AND [event_closed_terminal_master].event_id = " + eventid + " ) AND [Venue_Terminal_Master].[profit_center_id] = " + location_id;
                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            LocationAndTerminal item = new LocationAndTerminal();
                            item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());
                            item.terminal_name = rdr["terminal_name"].ToString();
                            item.location_id = Convert.ToInt32(rdr["profit_center_id"].ToString());
                            item.location_name = rdr["location_name"].ToString();

                            openTerminals.Add(item);
                        }
                        rdr.Close();

                        conn.Close();

                        VAR.status = true;
                        VAR.LocationsTerminals = openTerminals;
                        VertedaDiagnostics.LogMessage("SelectOpenTerminals Completed successfully", "I", false);
                    }
                }
                else
                {
                VAR.status = false;
                VAR.errorText = "Could not get eventId from web service when attempting GetOpenTerminals";
                VertedaDiagnostics.LogMessage("Could not get eventId from web service when attempting GetOpenTerminals", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in SelectOpenTerminals: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        //[WebMethod] //done
        //public VarianceActivityResponseActions GetBagActionHistory(string bagID)
        //{
        //    VarianceActivityResponseActions VAR = new VarianceActivityResponseActions();

        //    List<BagAction> currentBagActions = new List<BagAction>();


        //    SqlConnection conn = new SqlConnection();
        //    SqlCommand cmd = new SqlCommand();

        //    try
        //    {

        //        conn.ConnectionString = GetConnectionString(2);

        //        if (conn.State != ConnectionState.Open)
        //        {
        //            try
        //            {
        //                conn.Open();
        //            }
        //            catch (SqlException e)
        //            {
        //                //error
        //            }
        //        }


        //        if (conn.State == ConnectionState.Open)
        //        {
        //            cmd.Connection = conn;

        //            cmd.CommandText = "SELECT [Bag_Action_Join].[action_id]	  ,ISNULL([Action_Dim].[action], 'NULL') AS [action]      ,[start_date]      ,ISNULL([end_date], '1900-01-01') AS [end_date]      ,[sending_employee]      ,[recieving_employee]      ,ISNULL([terminal_id], -1) AS [terminal_id]      ,ISNULL([location_id], -1) AS [location_id]      ,[bag_id]      ,[total_value]      ,ISNULL([total_child_bags], -1) AS [total_child_bags]  FROM [variance_cash].[dbo].[Bag_Action_Join]  LEFT JOIN [variance_cash].[dbo].[Action_Dim]  ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id]  WHERE [bag_id] = " + bagID + " ORDER BY start_date DESC";


        //            SqlDataReader rdr = cmd.ExecuteReader();

        //            while (rdr.Read())
        //            {
        //                BagAction item = new BagAction();
        //                item.Action_ID = Convert.ToInt32(rdr["action_id"].ToString());
        //                item.Action_Name = rdr["action"].ToString();
        //                item.Start_time = Convert.ToDateTime(rdr["start_date"].ToString());
        //                item.End_time = Convert.ToDateTime(rdr["end_date"].ToString());
        //                item.Sending_Employee_ID = Convert.ToInt32(rdr["sending_employee"].ToString());
        //                item.Receiving_Employee_ID = Convert.ToInt32(rdr["recieving_employee"].ToString());
        //                item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
        //                item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
        //                item.Bag_ID = rdr["bag_id"].ToString();
        //                item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
        //                item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());


        //                currentBagActions.Add(item);
        //            }
        //            rdr.Close();

        //            conn.Close();


        //            VAR.status = true;
        //            VAR.BagAction = currentBagActions;
        //            VertedaDiagnostics.LogMessage("GetBagActionHistory query completed", "I", false);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        VAR.status = false;
        //        VAR.errorText = e.ToString();
        //        VertedaDiagnostics.LogMessage("Error in GetBagActionHistory: " + e.ToString(), "E", false);
        //    }
        //    return VAR;
        //}

        [WebMethod] //done
        public VarianceActivityResponseActions GetBagActionHistoryWithUnid(int unid)
        {
            VarianceActivityResponseActions VAR = new VarianceActivityResponseActions();

            List<BagAction> currentBagActions = new List<BagAction>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [Bag_Dim].[label_id], [Bag_Action_Join].[action_id]	  ,ISNULL([Action_Dim].[action], 'NULL') AS [action], ISNULL([bag_function_id], -1) AS [bag_function_id]         ,[start_date]      ,ISNULL([end_date], '1900-01-01') AS [end_date]      ,[sending_employee]      ,[recieving_employee]      ,ISNULL([terminal_id], -1) AS [terminal_id]      ,ISNULL([location_id], -1) AS [location_id]      ,[bag_dim].[bag_id]      ,[total_value]      ,ISNULL([total_child_bags], -1) AS [total_child_bags] ,ISNULL([sender_name], 'NULL') AS [sender_name]  ,ISNULL([reciever_name], 'NULL') AS [reciever_name]FROM [variance_cash].[dbo].[Bag_Action_Join]  LEFT JOIN [variance_cash].[dbo].[Action_Dim]    ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id]     LEFT JOIN [variance_cash].[dbo].[Bag_Dim] ON [Bag_Action_Join].[bag_unid] = [Bag_Dim].[unid]   WHERE [bag_dim].[unid] = " + unid + " ORDER BY start_date DESC";


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        BagAction item = new BagAction();
                        item.Action_ID = Convert.ToInt32(rdr["action_id"].ToString());
                        item.Action_Name = rdr["action"].ToString();
                        item.Start_time = Convert.ToDateTime(rdr["start_date"].ToString()).ToString("G");
                        item.End_time = Convert.ToDateTime(rdr["end_date"].ToString()).ToString("G");
                        item.Sending_Employee_ID = Convert.ToInt32(rdr["sending_employee"].ToString());
                        item.Receiving_Employee_ID = Convert.ToInt32(rdr["recieving_employee"].ToString());
                        item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                        item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                        item.Bag_ID = rdr["bag_id"].ToString();
                        item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
                        item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());
                        item.sender_name = rdr["sender_name"].ToString();
                        item.reciever_name = rdr["reciever_name"].ToString();
                        item.label_id = rdr["label_id"].ToString();

                        item.function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());

                        currentBagActions.Add(item);
                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VAR.BagActions = currentBagActions;
                    VertedaDiagnostics.LogMessage("GetBagActionHistory query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetBagActionHistory: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        [WebMethod] //done
        public VarianceActivityResponseActions GetFullBagActionHistoryInLocation(int location_id)
        {
            VarianceActivityResponseActions VAR = new VarianceActivityResponseActions();

            List<BagAction> currentBagActions = new List<BagAction>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                int eventID = GetEventID();

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open && eventID != null)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [Bag_Action_Join].[unid] AS bag_action_unid, [Bag_Dim].[unid], ISNULL([bag_function_id], -1) AS [bag_function_id]    ,  [Bag_Dim].[label_id], [Bag_Action_Join].[action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action]  ,[start_date] ,ISNULL([end_date], '1900-01-01') AS [end_date] ,[sending_employee]      ,[recieving_employee]  ,ISNULL([terminal_id], -1) AS [terminal_id] ,ISNULL([terminal_name], 'NULL') AS [terminal_name]  ,ISNULL([location_id], -1) AS [location_id] ,ISNULL([location_name], 'NULL') AS [location_name]  ,[bag_dim].[bag_id]   ,[total_value]      ,ISNULL([total_child_bags], -1) AS [total_child_bags] ,ISNULL([sender_name], 'NULL') AS [sender_name]  ,ISNULL([reciever_name], 'NULL') AS [reciever_name]   FROM [variance_cash].[dbo].[Bag_Action_Join] LEFT JOIN [variance_cash].[dbo].[Action_Dim]       ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id]  LEFT JOIN [variance_cash].[dbo].[Bag_Dim]	   ON [Bag_Action_Join].[bag_unid] = [Bag_Dim].[unid]  WHERE [bag_dim].[origin_location_id] =  " + location_id + " AND [Bag_Dim].[event_id] = " + eventID + "  ORDER BY start_date DESC";

                    
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        BagAction item = new BagAction();
                        item.Action_ID = Convert.ToInt32(rdr["action_id"].ToString());
                        item.Action_Name = rdr["action"].ToString();
                        item.Start_time = Convert.ToDateTime(rdr["start_date"].ToString()).ToString("G");
                        item.End_time = Convert.ToDateTime(rdr["end_date"].ToString()).ToString("G");
                        item.Sending_Employee_ID = Convert.ToInt32(rdr["sending_employee"].ToString());
                        item.Receiving_Employee_ID = Convert.ToInt32(rdr["recieving_employee"].ToString());
                        item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                        item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                        item.Bag_ID = rdr["bag_id"].ToString();
                        item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
                        item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());
                        item.sender_name = rdr["sender_name"].ToString();
                        item.reciever_name = rdr["reciever_name"].ToString();
                        item.label_id = rdr["label_id"].ToString();
                        item.Bag_Action_Unid = Convert.ToInt32(rdr["bag_action_unid"].ToString());
                        item.Bag_Unid = Convert.ToInt32(rdr["unid"].ToString());

                        item.function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());

                        currentBagActions.Add(item);
                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VAR.BagActions = currentBagActions;
                    VertedaDiagnostics.LogMessage("GetFullBagActionHistoryInLocation query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetFullBagActionHistoryInLocation: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseActions GetFullBagActionHistoryInLocationForSpecificEventID(int location_id, int eventID)
        {
            VarianceActivityResponseActions VAR = new VarianceActivityResponseActions();

            List<BagAction> currentBagActions = new List<BagAction>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open && eventID != null)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [Bag_Action_Join].[unid] AS bag_action_unid, [Bag_Dim].[unid], ISNULL([bag_function_id], -1) AS [bag_function_id]   ,  [Bag_Dim].[label_id], [Bag_Action_Join].[action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action]  ,[start_date] ,ISNULL([end_date], '1900-01-01') AS [end_date] ,[sending_employee]      ,[recieving_employee]  ,ISNULL([terminal_id], -1) AS [terminal_id] ,ISNULL([terminal_name], 'NULL') AS [terminal_name]  ,ISNULL([location_id], -1) AS [location_id] ,ISNULL([location_name], 'NULL') AS [location_name]  ,[bag_dim].[bag_id]   ,[total_value]      ,ISNULL([total_child_bags], -1) AS [total_child_bags] ,ISNULL([sender_name], 'NULL') AS [sender_name]  ,ISNULL([reciever_name], 'NULL') AS [reciever_name]   FROM [variance_cash].[dbo].[Bag_Action_Join] LEFT JOIN [variance_cash].[dbo].[Action_Dim]       ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id]  LEFT JOIN [variance_cash].[dbo].[Bag_Dim]	   ON [Bag_Action_Join].[bag_unid] = [Bag_Dim].[unid]  WHERE [bag_dim].[origin_location_id] =  " + location_id + " AND [Bag_Dim].[event_id] = " + eventID + "  ORDER BY start_date DESC";


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        BagAction item = new BagAction();
                        item.Action_ID = Convert.ToInt32(rdr["action_id"].ToString());
                        item.Action_Name = rdr["action"].ToString();
                        item.Start_time = Convert.ToDateTime(rdr["start_date"].ToString()).ToString("G");
                        item.End_time = Convert.ToDateTime(rdr["end_date"].ToString()).ToString("G");
                        item.Sending_Employee_ID = Convert.ToInt32(rdr["sending_employee"].ToString());
                        item.Receiving_Employee_ID = Convert.ToInt32(rdr["recieving_employee"].ToString());
                        item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                        item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                        item.Bag_ID = rdr["bag_id"].ToString();
                        item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
                        item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());
                        item.sender_name = rdr["sender_name"].ToString();
                        item.reciever_name = rdr["reciever_name"].ToString();
                        item.label_id = rdr["label_id"].ToString();
                        item.Bag_Action_Unid = Convert.ToInt32(rdr["bag_action_unid"].ToString());
                        item.Bag_Unid = Convert.ToInt32(rdr["unid"].ToString());

                        item.function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());

                        currentBagActions.Add(item);
                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VAR.BagActions = currentBagActions;
                    VertedaDiagnostics.LogMessage("GetFullBagActionHistoryInLocation query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetFullBagActionHistoryInLocation: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        
        [WebMethod] //done
        public VarianceActivityResponseActions GetTerminalActionsInLocation(int locID)
        {
            VarianceActivityResponseActions VAR = new VarianceActivityResponseActions();

            List<BagAction> currentBagActions = new List<BagAction>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }

                int eventID = GetEventID(); //GetEventIDForCurrentBusinessDay();


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    //cmd.CommandText = "SELECT [Bag_Action_Join].[action_id],[Action_Dim].[action]      ,[start_date]      ,ISNULL([end_date], '1900-01-01') AS [end_date]      ,[sending_employee]      ,[recieving_employee]      ,ISNULL([terminal_id], -1) AS [terminal_id]      ,ISNULL([location_id], -1) AS [location_id]      ,[bag_id]      ,[total_value]      ,ISNULL([total_child_bags], -1) AS [total_child_bags]  FROM [variance_cash].[dbo].[Bag_Action_Join]  LEFT JOIN [variance_cash].[dbo].[Action_Dim]  ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id]  WHERE EXISTS (SELECT [Venue_Terminal_Master].[terminal_id]FROM [variance_cash].[dbo].[Venue_Terminal_Master] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]ON [Venue_Terminal_Master].[profit_center_id] = [venue_location_master].[id] WHERE NOT EXISTS (SELECT 1 FROM [variance_cash].[dbo].[event_closed_terminal_master] LEFT JOIN [variance_cash].[dbo].[Venue_Terminal_Master]ON [event_closed_terminal_master].[terminal_id] = [Venue_Terminal_Master].[terminal_id]WHERE [Venue_Terminal_Master].[terminal_id] = [event_closed_terminal_master].[terminal_id] AND [event_closed_terminal_master].event_id = "+eventID+") AND [Venue_Terminal_Master].[profit_center_id] = " + locID +") AND end_date IS NULL";

                    cmd.CommandText = "SELECT [Bag_Action_Join].[action_id],[Action_Dim].[action]      ,[start_date]      ,ISNULL([end_date], '1900-01-01') AS [end_date]      ,[sending_employee]      ,[recieving_employee]      ,ISNULL([terminal_id], -1) AS [terminal_id]      ,ISNULL([location_id], -1) AS [location_id]      ,[bag_unid]      ,[total_value]      ,ISNULL([total_child_bags], -1) AS [total_child_bags] FROM [variance_cash].[dbo].[Bag_Action_Join] LEFT JOIN [variance_cash].[dbo].[Action_Dim] ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] LEFT JOIN [variance_cash].[dbo].[Bag_Dim] ON  [Bag_Action_Join].[bag_unid] = [Bag_Dim].[unid] WHERE EXISTS (SELECT [Venue_Terminal_Master].[terminal_id] FROM [variance_cash].[dbo].[Venue_Terminal_Master] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Venue_Terminal_Master].[profit_center_id] = [venue_location_master].[id] WHERE NOT EXISTS (SELECT 1 FROM [variance_cash].[dbo].[event_closed_terminal_master] LEFT JOIN [variance_cash].[dbo].[Venue_Terminal_Master] ON [event_closed_terminal_master].[terminal_id] = [Venue_Terminal_Master].[terminal_id] WHERE [Venue_Terminal_Master].[terminal_id] = [event_closed_terminal_master].[terminal_id] AND [event_closed_terminal_master].event_id = "+eventID+") AND [Venue_Terminal_Master].[profit_center_id] =" + locID +") AND end_date IS NULL AND [Bag_Dim].[bag_type] = '" + constants.BT2 + "' AND [Bag_Action_Join].[location_id] = " + locID;


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        BagAction item = new BagAction();
                        item.Action_ID = Convert.ToInt32(rdr["action_id"].ToString());
                        item.Action_Name = rdr["action"].ToString();
                        item.Start_time = Convert.ToDateTime(rdr["start_date"].ToString()).ToString("G");
                        item.End_time = Convert.ToDateTime(rdr["end_date"].ToString()).ToString("G");
                        item.Sending_Employee_ID = Convert.ToInt32(rdr["sending_employee"].ToString());
                        item.Receiving_Employee_ID = Convert.ToInt32(rdr["recieving_employee"].ToString());
                        item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                        item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                        item.Bag_Unid = Convert.ToInt32(rdr["bag_unid"].ToString());
                        item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
                        item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());


                        currentBagActions.Add(item);
                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VAR.BagActions = currentBagActions;
                    VertedaDiagnostics.LogMessage("GetTerminalActionsInLocation query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetTerminalActionsInLocation: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseBags GetSingleBagInfoWithLabel(string labelID)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                int eventid = GetEventID(); //GetEventIDForCurrentBusinessDay();
                if (eventid > 0)
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;


                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id, [Bag_Dim].[bag_type], [Bag_Dim].[unid],[event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin  ON [Bag_Dim].[origin_location_id] = origin.[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination ON [Bag_Dim].[destination_location_id] = destination.[id] LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid] LEFT JOIN [variance_cash].[dbo].[Action_Dim] ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id = " + eventid + " AND [bag_dim].[label_id] = '" + labelID + "'  AND ( [Bag_Action_Join].[end_date] IS NULL OR ([Bag_Action_Join].action_id = 10 AND Bag_Dim.bag_type = 'Terminal') OR ([Bag_Action_Join].action_id = 9 AND Bag_Dim.bag_type = 'Kiosk') )";

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString(); 


                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetExistingBags query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }

                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseBags GetSingleBagInfoWithUnid(int bagUnid)
        {
            VertedaDiagnostics.LogMessage("GetSingleBagInfoWithUnid started. unid: "+bagUnid, "I", false);
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                            VertedaDiagnostics.LogMessage("error in GetSingleBagInfoWithUnid " +e.ToString(), "E", false);
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id, [Bag_Dim].[bag_type], [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NUL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin  ON [Bag_Dim].[origin_location_id] = origin.[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination ON [Bag_Dim].[destination_location_id] = destination.[id] LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid] LEFT JOIN [variance_cash].[dbo].[Action_Dim] ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE [bag_dim].[unid] = " + bagUnid + "  AND ( [Bag_Action_Join].[end_date] IS NULL OR ([Bag_Action_Join].action_id = 10 AND Bag_Dim.bag_type = 'Terminal') OR ([Bag_Action_Join].action_id = 9 AND Bag_Dim.bag_type = 'Kiosk')  OR ([Bag_Action_Join].action_id = 5 AND Bag_Dim.bag_type = 'Terminal') OR ([Bag_Action_Join].action_id = 4 AND Bag_Dim.bag_type = 'Kiosk') )";

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString(); 


                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetExistingBags query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }

                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }



            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        [WebMethod] //done
        public VarianceActivityResponseLocationsAndTerminal GetLocationEventCompositions()
        {
            VarianceActivityResponseLocationsAndTerminal VAR = new VarianceActivityResponseLocationsAndTerminal();

            List<LocationAndTerminal> LaT = new List<LocationAndTerminal>();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;
                int eventID = GetEventID(); //GetEventIDForCurrentBusinessDay();

                if (eventID != null)
                { 

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT [bag_id], [destination_location_id],ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name], [Bag_Dim].[composition_id], ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],[bag_dim].[unid],  ISNULL([Bag_Dim].[label_id], '-1') AS [label_id] FROM [variance_cash].[dbo].[Bag_Dim]   LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]   ON [Bag_Dim].[destination_location_id] = [venue_location_master].[id]    LEFT JOIN [variance_cash].[dbo].[Composition_Dim]  	ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id]  	WHERE [Bag_Dim].[bag_type] = 'Kiosk'  AND [Bag_Dim].[event_id] = " + eventID;


                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            LocationAndTerminal item = new LocationAndTerminal();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.location_name = rdr["location_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();

                            item.label_id = rdr["label_id"].ToString();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());


                            LaT.Add(item);
                        }
                        rdr.Close();

                        conn.Close();


                        VAR.status = true;
                        VAR.LocationsTerminals = LaT;
                        VertedaDiagnostics.LogMessage("GetLocationEventCompositions query completed", "I", false);
                    }
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "could not get current eventID while running GetLocationEventCompositions";
                    VertedaDiagnostics.LogMessage("could not get current eventID while running GetLocationEventCompositions", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetLocationEventCompositions: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseLocationsAndTerminal GetLocationEventCompositionsForSpecificEvent(int eventID)
        {
            VarianceActivityResponseLocationsAndTerminal VAR = new VarianceActivityResponseLocationsAndTerminal();

            List<LocationAndTerminal> LaT = new List<LocationAndTerminal>();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (eventID != null)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT [bag_id], [destination_location_id],ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name], [Bag_Dim].[composition_id], ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],[bag_dim].[unid],  ISNULL([Bag_Dim].[label_id], '-1') AS [label_id] FROM [variance_cash].[dbo].[Bag_Dim]   LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]   ON [Bag_Dim].[destination_location_id] = [venue_location_master].[id]    LEFT JOIN [variance_cash].[dbo].[Composition_Dim]  	ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id]  	WHERE [Bag_Dim].[bag_type] = 'Kiosk'  AND [Bag_Dim].[event_id] = " + eventID;


                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            LocationAndTerminal item = new LocationAndTerminal();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.location_name = rdr["location_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();

                            item.label_id = rdr["label_id"].ToString();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());


                            LaT.Add(item);
                        }
                        rdr.Close();

                        conn.Close();


                        VAR.status = true;
                        VAR.LocationsTerminals = LaT;
                        VertedaDiagnostics.LogMessage("GetLocationEventCompositionsForSpecificEvent query completed", "I", false);
                    }
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "could not get current eventID while running GetLocationEventCompositionsForSpecificEvent";
                    VertedaDiagnostics.LogMessage("could not get current eventID while running GetLocationEventCompositionsForSpecificEvent", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetLocationEventCompositions: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponseBags GetKioskBagUnidFromLocation(int locationID, int mode) // 1 = loan, 4 = top-up
        {
            VertedaDiagnostics.LogMessage("GetKioskBagUnidFromLocation started. location: " +locationID, "I", false);

            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            List<Bag> locationKioskUnid = new List<Bag>();

            string bagFunction = "";

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;
                int eventid = GetEventID(); // GetEventIDForCurrentBusinessDay();
                if (eventid > 0)
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }

                    if (mode == 1)
                    {
                        bagFunction = " AND [Bag_Dim].[bag_function_id] = 1";
                    }
                    else if (mode == 4)
                    {
                        bagFunction = " AND [Bag_Dim].[bag_function_id] IN (2,3,4)";
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;
                        //get bags, order by created date (oldest first)
                        //cmd.CommandText = "SELECT [Bag_Action_Join].[bag_unid] FROM [variance_cash].[dbo].[Bag_Action_Join]  LEFT JOIN [variance_cash].[dbo].[Bag_Dim]  ON [Bag_Action_Join].[bag_unid] = [Bag_Dim].[unid]  WHERE [Bag_Action_Join].[location_id] = " + locationID + "  AND [Bag_Dim].[bag_type] = '" + constants.BT1 + "' AND [Bag_Dim].[event_id] = " + eventid + bagFunction + "  ORDER BY [Bag_Dim].[created_date] ";

                        cmd.CommandText = "SELECT [unid]      ,[event_id]      ,[bag_type]      ,[bag_id]      ,[label_id]      ,[created_date]      ,[created_by_employee]      ,[origin_location_id]      ,[destination_location_id]   ,[composition_id]  ,[bag_function_id]  FROM [variance_cash].[dbo].[Bag_Dim]  WHERE bag_type = '" + constants.BT1 + "'  AND destination_location_id = " + locationID + "  AND event_id = " + eventid + bagFunction + "  ORDER by created_date desc";


                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());

                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = rdr["created_date"].ToString();
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            
                            locationKioskUnid.Add(item);
                        }

                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = locationKioskUnid;
                    VertedaDiagnostics.LogMessage("GetKioskBagUnidFromLocation Completed successfully", "I", false);
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not get event ID from web service in GetKioskBagUnidFromLocation";
                    VertedaDiagnostics.LogMessage("Could not get event ID from web service in GetKioskBagUnidFromLocation", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetKioskBagUnidFromLocation: " + e.ToString(), "E", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponseKioskSummary OutboundKioskBagSummary(int bagunid)
        {

            VarianceActivityResponseKioskSummary VAR = new VarianceActivityResponseKioskSummary();
            try
            {
                int totalChildBagsCashedIn = 0;
                int KioskStartBags = 0;
                int KioskEndBags = 0;
                decimal startingCompositionAmt = 0;

                List<BagAction> CashedInChildren = new List<BagAction>();

                //-------------------------------------------------------------------------
                
                //Gets destination name
                VarianceActivityResponseBags VAR5 = GetSingleBagInfoWithUnid(bagunid);                
                //-------------------------------------------------------------------------

                if (VAR5.status)
                {
                    if (VAR5.Bags[0].bag_type == constants.BT1)
                    {

                        //Gets Action history of Kiosk Bag
                        VarianceActivityResponseActions VAR2 = GetBagActionHistoryWithUnid(bagunid);
                        //-------------------------------------------------------------------------

                        //Foreach action in the bags history, get the starting number of bags and the finishing
                        foreach (BagAction bagAc in VAR2.BagActions)
                        {
                            if (bagAc.Action_ID == 2)
                            {
                                KioskStartBags = bagAc.Bag_Count;
                            }
                            else if (bagAc.Action_ID == 4)
                            {
                                KioskEndBags = bagAc.Bag_Count;
                            }
                        }
                        //-------------------------------------------------------------------------

                        //Get kiosk bags children
                        VarianceActivityResponseBagChildren VAR3 = GetBagChildren(bagunid);

                        //-------------------------------------------------------------------------

                        //For each child
                        foreach (BagJoins bj in VAR3.BagJoins)
                        {
                            //Get the action history of the child
                            VarianceActivityResponseActions VAR4 = GetBagActionHistoryWithUnid(bj.Child_ID);

                            foreach (BagAction ba in VAR4.BagActions)
                            {
                                //get the start value for child
                                if (ba.Action_ID == 2)
                                {
                                    startingCompositionAmt = ba.Total_Value;
                                }

                                //-------------------------------------------------------------------------

                                //get the action for cashed in child
                                if (ba.Action_ID == 5)
                                {
                                    totalChildBagsCashedIn += 1;
                                    CashedInChildren.Add(ba);
                                }
                            }
                        }

                        VAR.status = true;
                        VAR.Properties = VAR5.Bags[0];
                        VAR.KioskHistory = VAR2.BagActions;
                        VAR.CashedInChildren = CashedInChildren;
                        VAR.ChildrenUsed = totalChildBagsCashedIn;
                        VAR.StartingChildren = KioskStartBags;
                        VAR.EndingChildren = KioskEndBags;
                        VAR.CompositionValue = Math.Round(startingCompositionAmt, 2); 
                    }
                    else
                    {
                        //You didnt enter a Kiosk bag unid
                        VAR.status = false;
                        VAR.errorText = "The Bag Unid provided is not a valid Kiosk bag unid.";
                        VertedaDiagnostics.LogMessage("The Bag Unid provided is not a valid Kiosk bag unid.", "E", false);
                    }
                }
                else
                {
                    //Bag unid invalid
                    VAR.status = false;
                    VAR.errorText = "The Bag Unid provided is not a valid  unid.";
                    VertedaDiagnostics.LogMessage("The Bag Unid provided is not a valid  unid.", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in OutboundKioskBagSummary: " + e.ToString(), "E", false);
            }

            return VAR;
        }

        [WebMethod] //shows the most current action of each incoming bag
        public VarianceActivityResponseBags GetInboundBagsFromLocation(int locationID)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;
                int eventid = GetEventID(); // GetEventIDForCurrentBusinessDay();
                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id      ,[Bag_Action_Join].[terminal_id], [Bag_Action_Join].[total_value], [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin   ON [Bag_Dim].[origin_location_id] = origin.[id]    LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination    ON [Bag_Dim].[destination_location_id] = destination.[id] 	LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] 	ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]	 LEFT JOIN [variance_cash].[dbo].[Action_Dim] 	 ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id = " + eventid + "  AND [bag_dim].[origin_location_id] = " + locationID + "  AND  ( [Bag_Action_Join].[end_date] IS NULL OR ([Bag_Action_Join].action_id = 10 AND Bag_Dim.bag_type = 'Terminal') OR ([Bag_Action_Join].action_id = 9 AND Bag_Dim.bag_type = 'Kiosk') ) ORDER BY  terminal_id, bag_id, created_date desc";

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString();

                            item.current_value = Convert.ToDecimal(rdr["total_value"].ToString());
                            item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetInboundBagsFromLocation query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }

                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetInboundBagsFromLocation: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseBags GetInboundBagsFromLocationForSpecificEvent(int locationID, int eventid)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id      ,[Bag_Action_Join].[terminal_id],  [Bag_Action_Join].[total_value], [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin   ON [Bag_Dim].[origin_location_id] = origin.[id]    LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination    ON [Bag_Dim].[destination_location_id] = destination.[id] 	LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] 	ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]	 LEFT JOIN [variance_cash].[dbo].[Action_Dim] 	 ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id = " + eventid + "  AND [bag_dim].[origin_location_id] = " + locationID + "  AND  ( [Bag_Action_Join].[end_date] IS NULL OR ([Bag_Action_Join].action_id = 10 AND Bag_Dim.bag_type = 'Terminal') OR ([Bag_Action_Join].action_id = 9 AND Bag_Dim.bag_type = 'Kiosk') ) ORDER BY  terminal_id, bag_id, created_date desc";

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString();
                            item.current_value = Convert.ToDecimal(rdr["total_value"].ToString());
                            item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());




                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetInboundBagsFromLocationForSpecificEvent query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {
                        //int bagsWithActions = 0;
                        //foreach (Bag b in currentBags)
                        //{
                        //    if (b.action_id != -1)
                        //    {
                        //        bagsWithActions += 1;
                        //    }

                        //}

                        //if (bagsWithActions == 0)
                        //{
                        //    VAR.MessageCode = 2;
                        //    VAR.MessageText = constants.ExistingBagsNoAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
                        //}
                        //else
                        //{
                        //    VAR.MessageCode = 3;
                        //    VAR.MessageText = constants.ExistingBagsWithAction;
                        //    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
                        //}

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetInboundBagsFromLocationForSpecificEvent: " + e.ToString(), "E", false);
            }
            return VAR;
        }



        [WebMethod] //done
        public VarianceActivityResponseBags GetInboundBagsFromAllLocation()
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();

            try
            {
                VarianceActivityResponseLocationsAndTerminal VAR1 = GetOpenLocations();

                if (VAR1.status != false)
                {
                    foreach (LocationAndTerminal LaT in VAR1.LocationsTerminals)
                    {
                        VarianceActivityResponseBags VAR2 = GetInboundBagsFromLocation(LaT.location_id);

                        if (VAR2.status != false && VAR2.Bags.Count() > 0)
                        {
                            currentBags.AddRange(VAR2.Bags);
                        }
                        else
                        {
                            //error - no bags exist for location + LaT.location_id
                            VertedaDiagnostics.LogMessage("GetInboundBagsFromAllLocation.  no bags exist for location" + LaT.location_id, "I", false);
                        }
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetInboundBagsFromAllLocation query completed", "I", false);

                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "GetInboundBagsFromAllLocation: could not find any locations";
                    VertedaDiagnostics.LogMessage("GetInboundBagsFromAllLocation: could not find any locations", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetInboundBagsFromAllLocation: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseBags GetInboundBagsFromAllLocationForSpecificEvent(int eventid)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();

            try
            {
                VarianceActivityResponseLocationsAndTerminal VAR1 = GetOpenLocationsForSpecififcEventID(eventid);

                if (VAR1.status != false)
                {
                    foreach (LocationAndTerminal LaT in VAR1.LocationsTerminals)
                    {
                        VarianceActivityResponseBags VAR2 = GetInboundBagsFromLocationForSpecificEvent(LaT.location_id, eventid);

                        if (VAR2.status != false && VAR2.Bags.Count() > 0)
                        {
                            currentBags.AddRange(VAR2.Bags);
                        }
                        else
                        {
                            //error - no bags exist for location + LaT.location_id
                            VertedaDiagnostics.LogMessage("GetInboundBagsFromAllLocationForSpecificEvent.  no bags exist for location" + LaT.location_id, "I", false);
                        }                             
                    }   

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetInboundBagsFromAllLocationForSpecificEvent query completed", "I", false);
                 
                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "GetInboundBagsFromAllLocationForSpecificEvent: could not find any locations for the specified eventID: "+eventid;
                    VertedaDiagnostics.LogMessage("GetInboundBagsFromAllLocationForSpecificEvent: could not find any locations for the specified eventID: "+eventid, "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetInboundBagsFromAllLocationForSpecificEvent: " + e.ToString(), "E", false);
            }
            return VAR;
        }






        [WebMethod] //done
        public VarianceActivityResponseBags GetInboundBagsStartingValuesFromAllLocation()
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();

            try
            {
                VarianceActivityResponseLocationsAndTerminal VAR1 = GetOpenLocations();

                if (VAR1.status != false)
                {
                    foreach (LocationAndTerminal LaT in VAR1.LocationsTerminals)
                    {
                        VarianceActivityResponseBags VAR2 = GetInboundBagsStartingValuesFromLocation(LaT.location_id);

                        if (VAR2.status != false && VAR2.Bags.Count() > 0)
                        {
                            currentBags.AddRange(VAR2.Bags);
                        }
                        else
                        {
                            //error - no bags exist for location + LaT.location_id
                            VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromAllLocation.  no bags exist for location" + LaT.location_id, "I", false);
                        }
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromAllLocation query completed", "I", false);

                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "GetInboundBagsStartingValuesFromAllLocation: could not find any locations";
                    VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromAllLocation: could not find any locations", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetInboundBagsStartingValuesFromAllLocation: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        [WebMethod] //done
        public VarianceActivityResponseBags GetInboundBagsStartingValuesFromAllLocationForSpecificEvent(int eventid)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();

            try
            {
                VarianceActivityResponseLocationsAndTerminal VAR1 = GetOpenLocationsForSpecififcEventID(eventid);

                if (VAR1.status != false)
                {
                    foreach (LocationAndTerminal LaT in VAR1.LocationsTerminals)
                    {
                        VarianceActivityResponseBags VAR2 = GetInboundBagsStartingValuesFromLocationForSpecificEvent(LaT.location_id, eventid);

                        if (VAR2.status != false && VAR2.Bags.Count() > 0)
                        {
                            currentBags.AddRange(VAR2.Bags);
                        }
                        else
                        {
                            //error - no bags exist for location + LaT.location_id
                            VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromAllLocationForSpecificEvent.  no bags exist for location" + LaT.location_id, "I", false);
                        }
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromAllLocationForSpecificEvent query completed", "I", false);

                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "GetInboundBagsStartingValuesFromAllLocationForSpecificEvent: could not find any locations";
                    VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromAllLocationForSpecificEvent: could not find any locations", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetInboundBagsStartingValuesFromAllLocationForSpecificEvent: " + e.ToString(), "E", false);
            }
            return VAR;
        }





        [WebMethod] //shows the starting values of each incoming bag
        public VarianceActivityResponseBags GetInboundBagsStartingValuesFromLocation(int locationID)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                int eventid = GetEventID(); // GetEventIDForCurrentBusinessDay();
                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id      ,[Bag_Action_Join].[terminal_id], [Bag_Action_Join].[total_value], [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin   ON [Bag_Dim].[origin_location_id] = origin.[id]    LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination    ON [Bag_Dim].[destination_location_id] = destination.[id] 	LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] 	ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]	 LEFT JOIN [variance_cash].[dbo].[Action_Dim] 	 ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id]  WHERE event_id = " + eventid + "   AND [bag_dim].[origin_location_id] = " + locationID + "   AND  (([Bag_Action_Join].action_id = 6 AND Bag_Dim.bag_type = 'Terminal') OR ([Bag_Action_Join].action_id = 7 AND Bag_Dim.bag_type = 'Kiosk') )   ORDER BY  terminal_id,  bag_id, created_date desc";

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString();

                            item.current_value = Convert.ToDecimal(rdr["total_value"].ToString());
                            item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromLocation query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
                    }
                    else
                    {

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetInboundBagsStartingValuesFromLocation: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        [WebMethod] //shows the starting values of each incoming bag
        public VarianceActivityResponseBags GetInboundBagsStartingValuesFromLocationForSpecificEvent(int locationID, int eventid)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (eventid > 0)
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id      ,[Bag_Action_Join].[terminal_id], [Bag_Action_Join].[total_value], [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin   ON [Bag_Dim].[origin_location_id] = origin.[id]    LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination    ON [Bag_Dim].[destination_location_id] = destination.[id] 	LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] 	ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]	 LEFT JOIN [variance_cash].[dbo].[Action_Dim] 	 ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id]  WHERE event_id = " + eventid + "   AND [bag_dim].[origin_location_id] = " + locationID + "   AND  (([Bag_Action_Join].action_id = 6 AND Bag_Dim.bag_type = 'Terminal') OR ([Bag_Action_Join].action_id = 7 AND Bag_Dim.bag_type = 'Kiosk') )   ORDER BY  terminal_id,  bag_id, created_date desc";

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.event_name = rdr["event_name"].ToString();
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.origin_name = rdr["origin_name"].ToString();
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.destination_name = rdr["destination_name"].ToString();
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.composition_name = rdr["Name"].ToString();
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                            item.action_name = rdr["action"].ToString();
                            item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString();

                            item.current_value = Convert.ToDecimal(rdr["total_value"].ToString());
                            item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());

                            currentBags.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.Bags = currentBags;
                    VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromLocationForSpecificEvent query completed", "I", false);


                    if (currentBags.Count == 0)
                    {
                        VAR.MessageCode = 1;
                        VAR.MessageText = constants.NoExistingBags;
                        VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromLocationForSpecificEvent: No bags exist for the current event", "I", false);
                    }
                    else
                    {

                    }


                }
                else
                {
                    //could not get eventid from RTS
                    VAR.status = false;
                    VAR.errorText = "GetInboundBagsStartingValuesFromLocationForSpecificEvent: could not get current eventid from RTS web service";
                    VertedaDiagnostics.LogMessage("GetInboundBagsStartingValuesFromLocationForSpecificEvent: could not get current eventid from RTS web service", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetInboundBagsStartingValuesFromLocationForSpecificEvent: " + e.ToString(), "E", false);
            }
            return VAR;
        }







        [WebMethod] //done
        public VarianceActivityResponseBags GetLoanKioskBagInfoForADestinationLocation(int event_id, int location_id) //for use when finding out what bag should go to a team leader and passing default values
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            VertedaDiagnostics.LogMessage("GetLoanKioskBagInfoForADestinationLocation started: event_id + location_id: "+event_id + " " + location_id, "I", false);

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    //cmd.CommandText = "SELECT [event_id],[bag_type],[bag_id],[profit_center_id],[composition_id]  FROM [variance_cash].[dbo].[Bag_Dim]  WHERE [event_id] = " + eventid;// LOAD DENOMINATIONS
                    //cmd.CommandText = "SELECT [event_id],[event_master].[event_name],[bag_type] ,[bag_id] ,[label_id] ,[created_date] ,[created_by_employee],[profit_center_id] ,[venue_location_master].[location_name],[Bag_Dim].[composition_id] ,[Composition_Dim].[Name] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] ON [Bag_Dim].[profit_center_id] = [venue_location_master].[id] WHERE event_id = " + eventid;// LOAD DENOMINATIONS

                    cmd.CommandText = "SELECT [Bag_Dim].[unid] ,[Bag_Dim].[event_id]  ,[bag_type] ,[bag_id]  ,[label_id]  ,[created_date]  ,[created_by_employee]  ,[origin_location_id]  ,[destination_location_id]  ,[composition_id] ,[bag_function_id] ,(SELECT count([child_id])   FROM [variance_cash].[dbo].[Kid_Tid_Join]  WHERE parent_id = [Bag_Dim].[unid]) as child_bag_count ,[Bag_Action_Join].[action_id] FROM [variance_cash].[dbo].[Bag_Dim] INNER JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]  WHERE [Bag_Dim].event_id = " + event_id + " AND bag_type = 'Kiosk' AND [bag_dim].[bag_function_id] = 1 AND [bag_dim].[destination_location_id] = " + location_id + " AND ([Bag_Action_Join].[end_date] IS NULL OR [Bag_Action_Join].[action_id] > 3)";

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                            Bag item = new Bag();
                            item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                            item.bag_type = rdr["bag_type"].ToString();
                            item.bag_id = rdr["bag_id"].ToString();
                            item.label_id = rdr["label_id"].ToString();
                            item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                            item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                            item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                            item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                            item.total_child_bags = Convert.ToInt32(rdr["child_bag_count"].ToString());
                            item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                            item.action_id = Convert.ToInt32(rdr["action_id"].ToString());

                            VertedaDiagnostics.LogMessage("GetLoanKioskBagInfoForADestinationLocation values: "
                                + item.bag_unid + " "
                                + item.event_id + " "
                                + item.bag_type + " "
                                + item.bag_id + " "
                                + item.label_id + " "
                                + item.created_date + " "
                                + item.employee_id + " "
                                + item.origin_location_id + " "
                                + item.destination_location_id + " "
                                + item.composition_id + " "
                                + item.total_child_bags + " "
                                + item.bag_function_id + " "
                                + item.action_id, "I", false);

                        currentBags.Add(item);
                    }
                    rdr.Close();

                    conn.Close();
                }

                VAR.status = true;
                VAR.Bags = currentBags;
                VertedaDiagnostics.LogMessage("GetLoanKioskBagInfoForADestinationLocation query completed", "I", false);


                if (currentBags.Count == 0)
                {
                    VAR.MessageCode = 1;
                    VAR.MessageText = constants.NoExistingBags;
                    VertedaDiagnostics.LogMessage("GetLoanKioskBagInfoForADestinationLocation: No bags exist for the current event", "I", false);
                }
                else
                {


                }


            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetLoanKioskBagInfoForADestinationLocation: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        




        #endregion 
                
        #region Bag Actions

        [WebMethod]
        public VarianceActivityResponse AdvanceBagActionWithLabelID(BagAction UpdateBagAction) //needs to include bagID, 2 x employeeID, value or count of cash bags. Optional terminal ID / profit ID
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("AdvanceBagActionWithLabelID started. labelID: "+UpdateBagAction.label_id, "I", false);

            try
            {
                VarianceActivityResponseBags var1 = GetSingleBagInfoWithLabel(UpdateBagAction.label_id);
                if (var1.status != false)
                {
                    UpdateBagAction.Bag_ID = var1.Bags[0].bag_id;
                    UpdateBagAction.Bag_Unid = var1.Bags[0].bag_unid;
                    AdvanceBagAction(UpdateBagAction);
                }
                else
                {
                    //an errror occured. Could not find a bag matching that label ID
                    VAR.status = false;
                    VAR.errorText = "Could not find a bag matching that label ID running AdvanceBagActionWithLabelID";
                    VertedaDiagnostics.LogMessage("Could not find a bag matching that label ID running AdvanceBagActionWithLabelID", "E", false);
                }


            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in AdvanceBagActionWithLabelID", "E", false);
            }

            VertedaDiagnostics.LogMessage("AdvanceBagActionWithLabelID finished", "I", false);
            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse AdvanceBulkBagAction(List<BagAction> UpdateBulkBagAction)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("AdvanceBulkBagAction started", "I", false);

            try 
            {
                foreach (BagAction ba in UpdateBulkBagAction)
                {
                    AdvanceBagAction(ba);
                }

                VAR.status = true;
                VertedaDiagnostics.LogMessage("AdvanceBulkBagAction finished succesfully", "I", false);
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = "Error in AdvanceBulkBagAction. Not all bags could be advanced";
                VertedaDiagnostics.LogMessage("Error in AdvanceBulkBagAction. Not all bags could be advanced", "E", false);
            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse AdvanceBagAction(BagAction UpdateBagAction) //needs to include bagID, 2 x employeeID, value or count of cash bags. Optional terminal ID / profit ID
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("AdvanceBagAction started. bagID: " + UpdateBagAction.Bag_ID, "I", false);

            try 
            {
                //get current bag action
                VarianceActivityResponseBags VAR1 = GetSingleBagInfoWithUnid(UpdateBagAction.Bag_Unid);

                if (VAR1.status != false)
                {
                    //if its not 5 (at team leader outbound) or 9 (at safe inbound), add 1
                    if (VAR1.Bags[0].action_id != 5 && VAR1.Bags[0].action_id != 9 && VAR1.Bags[0].action_id != 13)
                    {
                        //call setbagaction with new number
                        int newAction = VAR1.Bags[0].action_id + 1;
                        UpdateBagAction.Action_ID = newAction;

                        SetBagAction(UpdateBagAction);

                        VAR.status = true;
                    }
                    else
                    {
                        //else error
                        //this bag can not be advanced
                        VAR.status = false;
                        VAR.errorText = "Bag " + UpdateBagAction.Bag_Unid + " cannot be advanced further";
                        VertedaDiagnostics.LogMessage("Bag " + UpdateBagAction.Bag_Unid + " cannot be advanced further", "E", false);
                    }                    
                }
                else
                {
                    //could not find the bag specified
                    VAR.status = false;
                    VAR.errorText = "Bag " + UpdateBagAction.Bag_Unid + " could not be found";
                    VertedaDiagnostics.LogMessage("Bag " + UpdateBagAction.Bag_Unid + "  could not be found", "E", false);
                }
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in AdvanceBagAction: " + e.ToString(), "E", false);
            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse AcceptKioskFloatBagIntoKiosk(BagAction UpdateBagAction)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("AcceptKioskFloatBagIntoKiosk started.", "I", false);

            try
            { 
                VarianceActivityResponseEmployee VAR5 = ValidateEmployee(UpdateBagAction.Sending_Employee_ID);
                VarianceActivityResponseEmployee VAR6 = ValidateEmployee(UpdateBagAction.Receiving_Employee_ID);

                if (VAR5.status && VAR6.status)
                {
                    UpdateBagAction.sender_name = VAR5.Employee.Employee_firstname + " " + VAR5.Employee.Employee_surname;
                    UpdateBagAction.reciever_name = VAR6.Employee.Employee_firstname + " " + VAR6.Employee.Employee_surname;

                    //-----------------------------------------------------------------------------------------------------//
                    //                                  Man city code for determining labelID                              //

                    //VarianceActivityResponseBagUNID var7 = GetProbableKioskBagUnid(UpdateBagAction.Location_ID);
                    BagOperators bop = new BagOperators();
                    bop.to_location_id = UpdateBagAction.Location_ID;
                    bop.kiosk_bags_only = true;
                    VarianceActivityResponseBags var7 = GetBagDetails(bop);

                    Bag most_recent_bag = new Bag();

                    if (var7.status != false)
                    {
                        //most recent kiosk bag will be last bag found
                        most_recent_bag = var7.Bags[var7.Bags.Count - 1];

                        UpdateBagAction.Bag_Unid = most_recent_bag.bag_unid;

                        BagAction most_recent_action = most_recent_bag.bag_actions[most_recent_bag.bag_actions.Count - 1];

                        if (most_recent_action.Action_ID < UpdateBagAction.Action_ID)
                        {
                            //close previous action
                            VarianceActivityResponse VAR1 = ClosePreviousAction(UpdateBagAction.Bag_Unid);
                            //advance action

                            int loop_count = most_recent_action.Bag_Count;
                            UpdateBagAction.Bag_Count = loop_count;

                            //int loop_count = var7.Bags[var7.Bags.Count - 1].bag_actions[var7.Bags[var7.Bags.Count - 1].bag_actions.Count - 1].Bag_Count;

                            VarianceActivityResponse VAR3 = MoveBagAndChildren(UpdateBagAction, loop_count);

                            VAR.status = VAR3.status;
        
                        }
                        else
                        {
                            //action has already been performed
                            VAR.status = false;
                            VAR.errorText = "AcceptKioskFloatBagIntoKiosk: The kiosk bag has already been allocated to the team leader" + UpdateBagAction.Bag_Unid;
                            VertedaDiagnostics.LogMessage("AcceptKioskFloatBagIntoKiosk: The kiosk bag has already been allocated to the team leader" + UpdateBagAction.Bag_Unid, "E", false);
                        }
                    }
                    else
                    {
                        VAR.status = false;
                        VAR.errorText = "Error in SetBagAction. Could not get a unid for the kiosk bag being checked into location " + UpdateBagAction.Location_ID;
                        VertedaDiagnostics.LogMessage("Error in SetBagAction. Could not get a unid for the kiosk bag being checked into location " + UpdateBagAction.Location_ID, "E", false);
                    }

                }
                else
                {
                    //employee not valid
                    //an errror occured. Could not find a bag matching that label ID
                    VAR.status = false;
                    VAR.errorText = "One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID;
                    VertedaDiagnostics.LogMessage("One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID, "E", false);
                }
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in AcceptKioskFloatBagIntoKiosk: "+e.ToString(), "E", false);
            }

            return VAR;
        } //i.e. set bag action for man city for action id = 4

        //[WebMethod]
        //public VarianceActivityResponse AcceptKioskFloatBagIntoKioskV2(BagAction UpdateBagAction)
        //{
        //    VarianceActivityResponse VAR = new VarianceActivityResponse();
        //    VertedaDiagnostics.LogMessage("AcceptKioskFloatBagIntoKiosk started.", "I", false);

        //    try

        //    {
        //        VarianceActivityResponseEmployee VAR5 = ValidateEmployee(UpdateBagAction.Sending_Employee_ID);
        //        VarianceActivityResponseEmployee VAR6 = ValidateEmployee(UpdateBagAction.Receiving_Employee_ID);

        //        if (VAR5.status && VAR6.status)
        //        {
        //            UpdateBagAction.sender_name = VAR5.Employee.Employee_firstname + " " + VAR5.Employee.Employee_surname;
        //            UpdateBagAction.reciever_name = VAR6.Employee.Employee_firstname + " " + VAR6.Employee.Employee_surname;

        //            //-----------------------------------------------------------------------------------------------------//
        //            //                                  Man city code for determining labelID                              //

        //            VarianceActivityResponseBagUNID var7 = GetProbableKioskBagUnid(UpdateBagAction.Location_ID);
        //            if (var7.status != false)
        //            {
        //                UpdateBagAction.Bag_Unid = var7.BagUNID;
        //            }
        //            else
        //            {
        //                VAR.status = false;
        //                VAR.errorText = "Error in SetBagAction. Could not get a unid for the kiosk bag being checked into location " + UpdateBagAction.Location_ID;
        //                VertedaDiagnostics.LogMessage("Error in SetBagAction. Could not get a unid for the kiosk bag being checked into location " + UpdateBagAction.Location_ID, "E", false);
        //            }

        //            //close previous action
        //            VarianceActivityResponse VAR1 = ClosePreviousAction(UpdateBagAction.Bag_Unid);
        //            //advance action
        //            VarianceActivityResponse VAR2 = InsertNewAction(UpdateBagAction);

        //            VarianceActivityResponseBags VAR3 = GetSingleBagInfoWithUnid(UpdateBagAction.Bag_Unid);


        //            if (VAR3.status != false)
        //            {
        //                //check if advanced bag was a kiosk bag
        //                if (VAR3.Bags[0].bag_type == constants.BT1)
        //                {
        //                    //if it was, get all child bags from kid to tid                        

        //                    VarianceActivityResponseBagChildren VAR4 = GetBagChildren(UpdateBagAction.Bag_Unid);

        //                    List<BagJoins> childBags = VAR4.BagJoins;

        //                    //and advance them too 
        //                    foreach (BagJoins CB in childBags)
        //                    {
        //                        BagAction newAction = new BagAction();
        //                        newAction.Bag_Unid = CB.Child_ID;
        //                        newAction.Action_ID = UpdateBagAction.Action_ID;
        //                        newAction.Sending_Employee_ID = UpdateBagAction.Sending_Employee_ID;
        //                        newAction.Receiving_Employee_ID = UpdateBagAction.Receiving_Employee_ID;
        //                        newAction.Location_ID = UpdateBagAction.Location_ID;
        //                        newAction.Terminal_ID = UpdateBagAction.Terminal_ID;
        //                        newAction.Total_Value = GetLastTerminalBagAmount(CB.Child_ID).BagAmount;

        //                        SetBagAction(newAction);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                //Could not get the bag type using the unid during UpdateBagAction
        //                VAR.status = false;
        //                VAR.errorText = "Could not get the bag type using the unid during UpdateBagAction";
        //                VertedaDiagnostics.LogMessage("Could not get the bag type using the unid during UpdateBagAction", "E", false);
        //            }

        //        }
        //        else
        //        {
        //            //employee not valid
        //            //an errror occured. Could not find a bag matching that label ID
        //            VAR.status = false;
        //            VAR.errorText = "One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID;
        //            VertedaDiagnostics.LogMessage("One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID, "E", false);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        VertedaDiagnostics.LogMessage("Error in AcceptKioskFloatBagIntoKiosk: " + e.ToString(), "E", false);
        //    }

        //    return VAR;
        //} //i.e. set bag action for man city for action id = 4





        [WebMethod]
        public VarianceActivityResponse SetBulkBagActionForDefaults(List<BagAction> SetBulkBagAction) //advnce multiple bags to a specific action using as much pre-generated information as possible
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("SetBulkBagActionForDefaults started.", "I", false);

            //We need to replace most values in each bag action with the expected defaults.
        //public string Bag_ID 
        //public int Action_ID                  **      specified
        //public string Action_Name 
        //public string Start_time 
        //public string End_time 
        //public int Sending_Employee_ID        **      specified
        //public int Receiving_Employee_ID      **      specified
        //public int Terminal_ID                **      found
        //public int Location_ID                **      found
        //public decimal Total_Value            **      found
        //public int Bag_Count                  **      found

        //Bag_Unid                              **      specified
        //Bag_Action_Unid 
        //label_id 

        //sender_name                           inherited from emp ID
        //reciever_name 


            try
            {
                VarianceActivityResponseActions VAR1 = GetDefaultsForBagActions(SetBulkBagAction); //find the destination location, terminal id, total children and value of the bag
                if (VAR1.status == true)
                {
                    List<BagAction> DefaultedBagActions = SetBulkBagAction;

                    foreach (BagAction dba in DefaultedBagActions)
                    {
                        //find the matching bag to inherit defaults from
                        BagAction Actionitem = VAR1.BagActions.Find(c => (c.Bag_Unid == dba.Bag_Unid));

                        if (Actionitem != null)
                        {
                            dba.Location_ID = Actionitem.Location_ID;
                            dba.Terminal_ID = Actionitem.Terminal_ID;
                            dba.Total_Value = Actionitem.Total_Value;
                            dba.Bag_Count = Actionitem.Bag_Count;
                        }
                        else
                        {
                            //error
                            VAR.status = false;
                            VAR.errorText = "Error in SetBulkBagActionForDefaults. Could not find matching default values for bag: "+dba.Bag_Unid;
                            VertedaDiagnostics.LogMessage("Error in SetBulkBagActionForDefaults. Could not find matching default values for bag: "+dba.Bag_Unid, "E", false);
                        }
                    }

                    foreach (BagAction ba in DefaultedBagActions)
                    {
                        SetBagAction(ba);
                    }
                    VAR.status = true;
                }
                else
                {
                    //error
                    VAR.status = false;
                    VAR.errorText = "Error in SetBulkBagActionForDefaults. Could not find default values";
                    VertedaDiagnostics.LogMessage("Error in SetBulkBagActionForDefaults. Could not find default values", "E", false);
                }

            }
            catch (Exception e)
            {
                Primo_SendBugsnag("Error setting bulk bag action", e, "Database");

                VAR.status = false;
                VAR.errorText = "Error in SetBulkBagActionForDefaults. Not all bags could be advanced";
                VertedaDiagnostics.LogMessage("Error in SetBulkBagActionForDefaults. Not all bags could be advanced", "E", false);
            }

            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseActions GetDefaultsForBagActions(List<BagAction> SetBulkBagAction)
        {
            VertedaDiagnostics.LogMessage("GetDefaultsForBagActions started.", "I", false);
            VarianceActivityResponseActions VAR = new VarianceActivityResponseActions();

            List<BagAction> currentActions = new List<BagAction>();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                ////make string of each bag unid
                //string bag_unids = "(  ";
                //    foreach (BagAction ba in SetBulkBagAction)
                //    {
                //        if (ba.Bag_Unid != null && ba.Bag_Unid != 0)
                //        {
                //            bag_unids += ba.Bag_Unid + ", ";
                //        }
                //        else
                //        {
                //            VertedaDiagnostics.LogMessage("GetDefaultsForBagActions. Invalid bag_Unid", "E", false);
                //        }
                //    }
                //    //trim the last comma off
                //    bag_unids = bag_unids.Substring(0, bag_unids.Length -2);
                //    bag_unids += ")";


                List<string> unids_to_find = new List<string>();
                foreach (BagAction ba in SetBulkBagAction)
                {
                    if (ba.Bag_Unid != null && ba.Bag_Unid != 0)
                    {
                        unids_to_find.Add(ba.Bag_Unid.ToString());
                    }
                    else
                    {
                        VertedaDiagnostics.LogMessage("GetDefaultsForBagActions. Invalid bag_Unid", "E", false);
                    }
                }

                string bag_unids = "(" + string.Join(",",unids_to_find) + ")";

                VertedaDiagnostics.LogMessage("GetDefaultsForBagActions. Inserting actions for following bags: "+bag_unids, "I", false);

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                            VertedaDiagnostics.LogMessage("Error in GetDefaultsForBagActions: " + e.ToString(), "E", false);
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT [Bag_Dim].[unid] ,ISNULL([destination_location_id], 0) AS [destination_location_id] ,[Venue_Terminal_Master].[terminal_id] ,[Bag_Action_Join].[total_child_bags]  ,[Bag_Action_Join].[total_value] FROM [variance_cash].[dbo].[Bag_Dim] INNER JOIN [variance_cash].[dbo].[Bag_Action_Join] ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid] INNER JOIN [variance_cash].[dbo].[Venue_Terminal_Master] ON [Venue_Terminal_Master].[unid] = (SELECT TOP 1 [Venue_Terminal_Master].[unid] FROM [variance_cash].[dbo].[Venue_Terminal_Master] WHERE [Venue_Terminal_Master].[profit_center_id] = [Bag_Dim].[destination_location_id]) WHERE [Bag_Dim].unid IN "+bag_unids+" AND [Bag_Action_Join].[action_id] = 2";

                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            BagAction item = new BagAction();
                            item.Bag_Unid = Convert.ToInt32(rdr["unid"].ToString());
                            item.Location_ID = Convert.ToInt32(rdr["destination_location_id"].ToString());
                            item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                            item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());
                            item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());

                            currentActions.Add(item);
                        }
                        rdr.Close();

                        conn.Close();
                    }

                    VAR.status = true;
                    VAR.BagActions = currentActions;
                    VertedaDiagnostics.LogMessage("GetDefaultsForBagActions query completed", "I", false);
            }
            catch (Exception e)
            {
                Primo_SendBugsnag("Error getting defaults for bag", e, "Database");
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetDefaultsForBagActions: " + e.ToString(), "E", false);
            }

            return VAR;
        }






        [WebMethod]
        public VarianceActivityResponse SetBulkBagAction(List<BagAction> SetBulkBagAction)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("SetBulkBagAction started.", "I", false);

            List<BagAction> final_actions = new List<BagAction>();
                       

            try
            {
                int cash_office_employee = SetBulkBagAction[0].Receiving_Employee_ID; //grab the first employee. They should all be the same.

                ConfigSettings cs1 = new ConfigSettings();

                try
                {
                    cs1 = ReadConfig();
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in SetBulkBagAction. Couldn't read config", "E", false);
                }

                foreach (BagAction ba in SetBulkBagAction)
                {
                    if (ba.Bag_Action_Unid != 0)
                    {
                        //we are doing an update
                        UpdateBagActionValue(ba);
                    }
                    else
                    {
                        //setbulkbagaction is only called in selected places: bulk pushing bags forwars, checking bags into cash office. Need to account for no values for child bag count
                        if (ba.Terminal_ID == -2)
                        {
                            ba.Terminal_ID = cs1.CashOfficeTerminalID;
                        }
                        if (ba.Location_ID == -2)
                        {
                            ba.Location_ID = cs1.CashOfficeID;
                        }
                        if (ba.Bag_Count == -2)
                        {
                            VarianceActivityResponseBagChildren VAR10 = GetBagChildren(ba.Bag_Unid);
                            if (VAR10.status)
                            {
                                ba.Bag_Count = VAR10.BagJoins.Count();
                                VertedaDiagnostics.LogMessage("SetBulkBagAction: replacing bag count to "+VAR10.BagJoins.Count+" on bag "+ba.Bag_Unid, "I", false);
                            }
                        }
                        
                        if (ba.Action_ID == 10)
                        {
                            final_actions.Add(ba);
                        }
                        SetBagAction(ba);
                    }
                }

                VAR.status = true;

                //we do the below after the true statement as the main purpose of the function is to set the bag actions
                //List<BagAction> unique_final_actions = new List<BagAction>();
                List<int> unique_unids = new List<int>();

                foreach (BagAction ba in final_actions)
                {
                    unique_unids.Add(ba.Bag_Unid);
                }

                //>> call a method to get the parent ID's from a list of the child ID's
                //>> filter out duplicates

                if (unique_unids.Count > 0)
                {
                    VarianceActivityResponseBags VAR5 = GetAllChildrenWithSameParent(unique_unids);

                    if (VAR5.status)
                    {

                        List<int> unique_children = (VAR5.Bags.Select(c => c.bag_unid)).ToList();


                        //>> check the current action for each of the children. If all are 10, bag status = 0, else bag status = down.
                        VarianceActivityResponseBags VAR6 = GetBagDetailsWithMultipleBagUnids(unique_children);

                        if (VAR6.status)
                        {
                            //List<Bag> unique_parents = (VAR6.Bags.Find(c => c.parent_id).Distinct()).ToList();
                            List<Bag> unique_parents = (VAR6.Bags.GroupBy(p => p.parent_id).Select(g => g.FirstOrDefault())).ToList();

                            foreach (Bag parent in unique_parents)
                            {
                                int child_count = (VAR6.Bags.Select(c => c.parent_id == parent.bag_unid)).Count();
                                int counted_child_count = (VAR6.Bags.Select(c => (c.parent_id == parent.bag_unid) && c.bag_actions[c.bag_actions.Count() - 1].Action_ID == 10)).Count();

                                //int parent_bag_action_unid = (parent.bag_actions.Find(c => c.Action_ID == 9)).Bag_Action_Unid;
                                int parent_bag_action_unid = parent.bag_actions[parent.bag_actions.Count - 1].Bag_Action_Unid;

                                if (child_count == counted_child_count)
                                {
                                    //status = 0
                                    UpdateBagActionBagCount(parent_bag_action_unid, counted_child_count, cash_office_employee, 0);
                                }
                                else if (child_count > counted_child_count)
                                {
                                    //count is down
                                    UpdateBagActionBagCount(parent_bag_action_unid, counted_child_count, cash_office_employee, 1);
                                }
                                else if (child_count < counted_child_count)
                                {
                                    //count is up !?!?. Should never happen
                                    UpdateBagActionBagCount(parent_bag_action_unid, counted_child_count, cash_office_employee, 2);
                                }
                            }
                        }
                        else
                        {
                            //error - could not get details about child bags when investigating parent kiosk bags
                            VAR.status = false;
                            VAR.errorText = "Could not get details about child bags when investigating parent kiosk bags";
                            VertedaDiagnostics.LogMessage("Could not get details about child bags when investigating parent kiosk bags", "E", false);
                        }
                    }
                    else
                    {
                        //Error: could not find child bags in order to calculate variance of kiosk bags
                        VAR.status = false;
                        VAR.errorText = "Could not find child bags in order to calculate variance of kiosk bags";
                        VertedaDiagnostics.LogMessage("Could not find child bags in order to calculate variance of kiosk bags", "E", false);
                    }
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = "Error in SetBulkBagAction. Not all bags could be advanced";
                VertedaDiagnostics.LogMessage("Error in SetBulkBagAction. Not all bags could be advanced", "E", false);
            }

            return VAR;
        }
        
        [WebMethod]
        public VarianceActivityResponse SetBagActionOLD(BagAction UpdateBagAction)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("SetBagAction started. bag_unid: "+UpdateBagAction.Bag_Unid, "I", false);
            
            try
            {
                VarianceActivityResponseEmployee VAR5 = ValidateEmployee(UpdateBagAction.Sending_Employee_ID);
                VarianceActivityResponseEmployee VAR6 = ValidateEmployee(UpdateBagAction.Receiving_Employee_ID);

                if (VAR5.status && VAR6.status)
                {
                    UpdateBagAction.sender_name = VAR5.Employee.Employee_firstname + " " + VAR5.Employee.Employee_surname;
                    UpdateBagAction.reciever_name = VAR6.Employee.Employee_firstname + " " + VAR6.Employee.Employee_surname; 

                    //close previous action
                    VarianceActivityResponse VAR1 = ClosePreviousAction(UpdateBagAction.Bag_Unid);
                    //advance action
                    UpdateBagAction.status_id = 0; //##status
                    VarianceActivityResponse VAR2 = InsertNewAction(UpdateBagAction);

                    //if the bag we just advanced was advanced to the final stage, close the end time aswell




                    VarianceActivityResponseBags VAR3 = GetSingleBagInfoWithUnid(UpdateBagAction.Bag_Unid);


                    if (VAR3.status != false)
                    {
                        //check if advanced bag was a kiosk bag
                        if (VAR3.Bags[0].bag_type == constants.BT1)
                        {
                            //if it was, get all child bags from kid to tid                        

                            VarianceActivityResponseBagChildren VAR4 = GetBagChildren(UpdateBagAction.Bag_Unid);

                            List<BagJoins> childBags = VAR4.BagJoins;

                            //and advance them too 
                            foreach (BagJoins CB in childBags)
                            {
                                BagAction newAction = new BagAction();
                                newAction.Bag_Unid = CB.Child_ID;
                                newAction.Action_ID = UpdateBagAction.Action_ID;
                                newAction.Sending_Employee_ID = UpdateBagAction.Sending_Employee_ID;
                                newAction.Receiving_Employee_ID = UpdateBagAction.Receiving_Employee_ID;
                                newAction.Location_ID = UpdateBagAction.Location_ID;
                                newAction.Terminal_ID = UpdateBagAction.Terminal_ID;
                                newAction.Total_Value = GetLastTerminalBagAmount(CB.Child_ID).BagAmount;

                                SetBagAction(newAction);
                            }
                        }
                    }
                    else
                    {
                        //Could not get the bag type using the unid during UpdateBagAction
                        VAR.status = false;
                        VAR.errorText = "Could not get the bag type using the unid during UpdateBagAction";
                        VertedaDiagnostics.LogMessage("Could not get the bag type using the unid during UpdateBagAction", "E", false);
                    }

                }
                else
                {
                    //employee not valid
                    //an errror occured. Could not find a bag matching that label ID
                    VAR.status = false;
                    VAR.errorText = "One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID;
                    VertedaDiagnostics.LogMessage("One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID, "E", false);
                }

            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in SetBagAction: "+e.ToString(), "E", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse SetBagAction(BagAction UpdateBagAction)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("SetBagAction started. bag_unid: " + UpdateBagAction.Bag_Unid, "I", false);

            try
            {
                VarianceActivityResponseEmployee VAR5 = ValidateEmployee(UpdateBagAction.Sending_Employee_ID);
                VarianceActivityResponseEmployee VAR6 = ValidateEmployee(UpdateBagAction.Receiving_Employee_ID);

                int action_status = 0; //0 = norm, 1 = count down, 2 = count up, 3 = value down, 4 = value up, 5 = missing, 6 = additional
                int loop_count = 0;

                //before you do a set action, make sure the action doesnt already exist?
                BagOperators pre_bop = new BagOperators();
                pre_bop.bag_unid = UpdateBagAction.Bag_Unid;

                VarianceActivityResponseBags VAR60 = GetBagDetails(pre_bop);                

                if (VAR60.status && VAR60.Bags.Count > 0)
                {      
                    
                    VertedaDiagnostics.LogMessage("point 1", "I", false);

                    if (VAR5.status && VAR6.status)
                    {
                        //get employee details
                        UpdateBagAction.sender_name = VAR5.Employee.Employee_firstname + " " + VAR5.Employee.Employee_surname;
                        UpdateBagAction.reciever_name = VAR6.Employee.Employee_firstname + " " + VAR6.Employee.Employee_surname;

                        if (VAR60.Bags[0].bag_actions.Count > 0) //not an initial action, so proceed accordingly
                        { 
                            if (VAR60.Bags[0].bag_actions[VAR60.Bags[0].bag_actions.Count - 1].Action_ID < UpdateBagAction.Action_ID)
                            {
                                //close previous action
                                VarianceActivityResponseActions VAR1 = new VarianceActivityResponseActions();
                                VAR1.status = false;
                                VAR1 = ClosePreviousActionAndReturnCounts(UpdateBagAction.Bag_Unid, UpdateBagAction.Action_ID - 1);

                                #region variance_calc
                                

                                if (VAR1.status)
                                {
                                    //if the previous value doesnt equal the input value and we are looking at a terminal bag
                                    #region value variance
                                    if ((VAR1.BagActions[0].Total_Value != UpdateBagAction.Total_Value) && (VAR60.Bags[0].bag_type == constants.BT2))
                                    {
                                        //for terminal, doesnt matter if outbound or inbound
                                        VertedaDiagnostics.LogMessage("point 5", "I", false);
                                        if (VAR60.Bags.Count > 0)
                                        {
                                            //if action_id < 6, we're comparing to composition. if > 5 we're comparing to initial value @ action_id = 6
                                            // **** OUTBOUND ****
                                            if (UpdateBagAction.Action_ID < 6)
                                            {
                                                VertedaDiagnostics.LogMessage("point 6", "I", false);
                                                //compare values
                                                if (VAR60.Bags[0].composition_value < UpdateBagAction.Total_Value)
                                                {
                                                    //status = value has gone up
                                                    action_status = 4;
                                                }
                                                else if (VAR60.Bags[0].composition_value > UpdateBagAction.Total_Value)
                                                {
                                                    // value has gone down
                                                    action_status = 3;
                                                }
                                                else
                                                {
                                                    //value is the same or couldnt get composition
                                                    action_status = 0;
                                                }
                                                VertedaDiagnostics.LogMessage("point 7", "I", false);
                                            }
                                            // **** INBOUND ****
                                            else if (UpdateBagAction.Action_ID > 5)
                                            {
                                                VertedaDiagnostics.LogMessage("point 8", "I", false);
                                                BagAction inbound_bag_starting_value = new BagAction();

                                                foreach (BagAction BA in VAR60.Bags[0].bag_actions)
                                                {
                                                    if (BA.Action_ID == 6)
                                                    {
                                                        inbound_bag_starting_value = BA;
                                                    }
                                                }

                                                VertedaDiagnostics.LogMessage("point 9", "I", false);
                                                //compare values
                                                if (inbound_bag_starting_value.Total_Value < UpdateBagAction.Total_Value)
                                                {
                                                    //status = value has gone up
                                                    action_status = 4;
                                                }
                                                else if (inbound_bag_starting_value.Total_Value > UpdateBagAction.Total_Value)
                                                {
                                                    // value has gone down
                                                    action_status = 3;
                                                }
                                                else
                                                {
                                                    //value is the same or couldnt get composition
                                                    action_status = 0;
                                                }
                                                VertedaDiagnostics.LogMessage("point 10", "I", false);
                                            }
                                            else
                                            {
                                                //error action ID unspecified
                                                VertedaDiagnostics.LogMessage("Error in SetBagAction: Action ID as not been specified. ", "E", false);
                                                action_status = 0;
                                            }

                                        }
                                        else
                                        {
                                            //could not get original values. Let them proceed as normal
                                            VertedaDiagnostics.LogMessage("Error in SetBagAction: Could not get the original values for bag: " + pre_bop.bag_unid, "E", false);
                                            action_status = 0;
                                        }
                                    }

                                    #endregion

                                    //if the previous count doesnt equal the input count and we are looking at a kiosk bag
                                    #region count variance
                                    else if ((VAR1.BagActions[0].Bag_Count != UpdateBagAction.Bag_Count) && (VAR60.Bags[0].bag_type == constants.BT1))
                                    {
                                        VertedaDiagnostics.LogMessage("point 11", "I", false);
                                        if ((UpdateBagAction.Bag_Count > VAR1.BagActions[0].Bag_Count) && UpdateBagAction.Action_ID < 6)
                                        {
                                            VertedaDiagnostics.LogMessage("point 12", "I", false);
                                            //count is up out
                                            //make additional bags at end

                                            if (VAR60.Bags.Count > 0)
                                            {
                                                VertedaDiagnostics.LogMessage("point 13", "I", false);
                                                action_status = 2;
                                                loop_count = UpdateBagAction.Bag_Count;
                                                int bags_to_create = UpdateBagAction.Bag_Count - VAR1.BagActions[0].Bag_Count;

                                                for (int i = 0; i < bags_to_create; i++)
                                                {
                                                    Bag item = new Bag();

                                                    item.event_id = VAR60.Bags[0].event_id;
                                                    item.bag_type = constants.BT2;
                                                    item.label_id = "";
                                                    item.created_date = DateTime.Now.ToString();
                                                    item.employee_id = UpdateBagAction.Receiving_Employee_ID; //recieving employee takes ownership of creating the new bags - likely a team leader
                                                    item.origin_location_id = VAR60.Bags[0].origin_location_id;
                                                    item.destination_location_id = VAR60.Bags[0].destination_location_id;
                                                    item.composition_id = 0;
                                                    item.bag_function_id = 12;

                                                    item.parent_id = VAR60.Bags[0].bag_unid; //this will be the bag the new child is assigned to.

                                                    //set up any known action info - must provide the action it needs to be advanced to. 
                                                    BagAction ba_item = new BagAction();


                                                    ba_item.Action_ID = UpdateBagAction.Action_ID; //#### possibly needs to be -1?
                                                    ba_item.Sending_Employee_ID = UpdateBagAction.Sending_Employee_ID;
                                                    ba_item.Receiving_Employee_ID = UpdateBagAction.Receiving_Employee_ID;
                                                    ba_item.Terminal_ID = UpdateBagAction.Terminal_ID;
                                                    ba_item.Location_ID = UpdateBagAction.Location_ID;
                                                    ba_item.Bag_Unid = 0; //we dont know this yet
                                                    ba_item.Total_Value = 0;
                                                    ba_item.Bag_Count = 0;
                                                    ba_item.sender_name = UpdateBagAction.sender_name;
                                                    ba_item.reciever_name = UpdateBagAction.reciever_name;
                                                    ba_item.status_id = 0;


                                                    item.bag_actions.Add(ba_item);

                                                    VarianceActivityResponseBags VAR70 = SetupNewAdditionalBag(item);

                                                    if (VAR70.status == false)
                                                    {
                                                        //if it failed to make a bag, don't try and advance it later.
                                                        loop_count = loop_count - 1;
                                                        VertedaDiagnostics.LogMessage("Failed to create an additional child bag during SetBagAction for parent: " + UpdateBagAction.Bag_Unid, "E", false);
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                //error - could not get parent bag details. could not set up additional terminal bags
                                                action_status = 2;
                                                loop_count = VAR1.BagActions[0].Bag_Count; //set the count to previous as we can't find the parent details
                                            }

                                        }
                                        else if ((UpdateBagAction.Bag_Count < VAR1.BagActions[0].Bag_Count) && UpdateBagAction.Action_ID < 6)
                                        {
                                            //count is down out
                                            //advance fewer bags at end
                                            action_status = 1;
                                            loop_count = UpdateBagAction.Bag_Count;
                                        }
                                        else
                                        {
                                            //count is inbound, dont change it
                                            //loop should advance all bags
                                            //dont proceed
                                            action_status = 0;
                                            loop_count = VAR1.BagActions[0].Bag_Count; //in an ideal world we would set this loop count to the bag count when created (action =7), but for most purposes, previous action value will be the same.
                                        }

                                    }
                                    #endregion  
                                    else
                                    {
                                        //no variance, continue as normal
                                        action_status = 0;
                                        loop_count = UpdateBagAction.Bag_Count;
                                    }


                                    //basic stuff would go here + amended loops
                                    VarianceActivityResponse VAR80 = MoveBagAndChildren(UpdateBagAction, loop_count);
                                    VAR.status = VAR80.status;
                                    VertedaDiagnostics.LogMessage("SetBagAction: MoveBagAndChildren finished for non-initial action. status: " + VAR80.status, "I", false);
                                }
                                else
                                {
                                    //could not close previous action - error
                                    VAR.status = false;
                                    VAR.errorText = "Error in SetBagAction: Could not close previous action";
                                    VertedaDiagnostics.LogMessage("Error in SetBagAction: Could not close previous action", "E", false);
                                }

                                #endregion
                            }
                            else
                            {
                                VAR.status = false;
                                VAR.errorText = "The specified bag has already done the requested action. Bag: " + UpdateBagAction.Bag_Unid + " Action: " + UpdateBagAction.Action_ID;
                                VertedaDiagnostics.LogMessage("The specified bag has already done the requested action. Bag: " + UpdateBagAction.Bag_Unid + " Action: " + UpdateBagAction.Action_ID, "E", false);
                            }
                        }
                        else
                        {
                            //only do basic stuff
                            UpdateBagAction.status_id = action_status;
                            loop_count = UpdateBagAction.Bag_Count;

                            //this bit actually moves the bags + children                            
                            VarianceActivityResponse VAR80 = MoveBagAndChildren(UpdateBagAction, loop_count);
                            VAR.status = VAR80.status;
                            VertedaDiagnostics.LogMessage("SetBagAction: MoveBagAndChildren finished for initial action. status: " + VAR80.status, "I", false);
                        }
                                                     
                    }
                    else
                    {
                        //employee not valid
                        //an errror occured. Could not find a bag matching that label ID
                        Exception bs_ex = null;
                        Primo_SendBugsnag("One of the employees was not valid for this transaction.Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID, bs_ex, "Permission");
                        VAR.status = false;
                        VAR.errorText = "One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID;
                        VertedaDiagnostics.LogMessage("One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID, "E", false);
                    }
                }
                else
                {
                    //error - could not find bag details, or the action you are trying to set is not further along than the current bag action.
                    Exception bs_ex = null;
                    Primo_SendBugsnag("Could not find bag details for bag :" + UpdateBagAction.Bag_Unid, bs_ex, "Workflow");
                    VAR.status = false;
                    VAR.errorText = "Could not find bag details for bag :" + UpdateBagAction.Bag_Unid;
                    VertedaDiagnostics.LogMessage("Could not find bag details for bag :" + UpdateBagAction.Bag_Unid, "E", false);                    
                }

            }
            catch (Exception e)
            {
                Primo_SendBugsnag("Error in SetBagAction: ", e, "Database");
                VertedaDiagnostics.LogMessage("Error in SetBagAction: " + e.ToString(), "E", false);
            }

            return VAR;
        }



        [WebMethod]
        public VarianceActivityResponse MoveBagAndChildren(BagAction UpdateBagAction, int loop_count)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VAR.status = true; //true until error or end of method
            VertedaDiagnostics.LogMessage("MoveBagAndChildren started. bag_unid: " + UpdateBagAction.Bag_Unid, "I", false);

            try
            { 
                //only do basic stuff
                BagOperators pre_bop = new BagOperators();
                pre_bop.bag_unid = UpdateBagAction.Bag_Unid;

                VarianceActivityResponse VAR2 = InsertNewAction(UpdateBagAction);
                VarianceActivityResponseBags VAR3 = GetBagDetails(pre_bop);

                if (VAR3.status != false)
                {
                    //check if advanced bag was a kiosk bag
                    if (VAR3.Bags[0].bag_type == constants.BT1)
                    {
                        //if it was, get all child bags from kid to tid                        

                        BagOperators bop = new BagOperators();
                        bop.parent_id = UpdateBagAction.Bag_Unid;
                        VertedaDiagnostics.LogMessage("MoveBagAndChildren getting child details", "I", false);
                        VarianceActivityResponseBags VAR4 = GetBagDetails(bop); //get all the bags with a parent_id = the bag we just updated
                        VertedaDiagnostics.LogMessage("MoveBagAndChildren got child details: "+VAR4.status + " count: " + VAR4.Bags.Count, "I", false);

                        if (VAR4.status && VAR4.Bags.Count > 0)
                        {
                            List<Bag> childBags = VAR4.Bags;

                            int loop_counter = loop_count;

                            foreach (Bag bg in childBags)
                            {
                                if (loop_counter > 0)
                                {
                                    //and advance them too 
                                    BagAction newAction = new BagAction();
                                    newAction.Bag_Unid = bg.bag_unid;
                                    newAction.Action_ID = UpdateBagAction.Action_ID;
                                    newAction.Sending_Employee_ID = UpdateBagAction.Sending_Employee_ID;
                                    newAction.Receiving_Employee_ID = UpdateBagAction.Receiving_Employee_ID;
                                    newAction.Location_ID = UpdateBagAction.Location_ID;
                                    newAction.Terminal_ID = UpdateBagAction.Terminal_ID;
                                    newAction.Total_Value = bg.bag_actions[bg.bag_actions.Count() - 1].Total_Value; //make value = previous value

                                    SetBagAction(newAction);

                                    loop_counter -= 1;
                                }
                                else
                                {
                                    //set status to missing
                                    BagAction ba = bg.bag_actions[bg.bag_actions.Count() - 1];
                                    ba.status_id = 5; //missing
                                    VarianceActivityResponse VAR7 = UpdateBagActionStatus(ba);
                                    if (VAR7.status != true)
                                    {
                                        //error - could not update status of missing bag: + bag_unid
                                        VAR.status = false;
                                        VAR.errorText = "error - could not update status of missing bag: " + bg.bag_unid;
                                        VertedaDiagnostics.LogMessage("error - could not update status of missing bag: " + bg.bag_unid, "E", false);
                                    }
                                }

                            }

                        }
                        else
                        {
                            VAR.status = false;
                            VAR.errorText = "Could not find child bags of the input kiosk bag: " + UpdateBagAction.Bag_Unid;
                            VertedaDiagnostics.LogMessage("Could not find child bags of the input kiosk bag: " + UpdateBagAction.Bag_Unid, "E", false);
                        }
                    }
                    
                }
                else
                {
                    //Could not get the bag type using the unid during UpdateBagAction
                    VAR.status = false;
                    VAR.errorText = "Could not get the bag type using the unid during UpdateBagAction";
                    VertedaDiagnostics.LogMessage("Could not get the bag type using the unid during UpdateBagAction", "E", false);
                }   
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = "Error in MoveBagAndChildren: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in MoveBagAndChildren: " + e.ToString(), "E", false);
            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse SetBagActionWithLabelID(BagAction UpdateBagAction)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("SetBagActionWithLabelID started. label_id: " + UpdateBagAction.label_id, "I", false);

            try
            {
                VarianceActivityResponseBags VAR5 = GetSingleBagInfoWithLabel(UpdateBagAction.label_id);
                if (VAR5.status != false)
                {
                    UpdateBagAction.Bag_Unid = VAR5.Bags[0].bag_unid;


                    VarianceActivityResponseEmployee VAR6 = ValidateEmployee(UpdateBagAction.Sending_Employee_ID);
                    VarianceActivityResponseEmployee VAR7 = ValidateEmployee(UpdateBagAction.Receiving_Employee_ID);

                    if (VAR6.status && VAR7.status)
                    {
                        UpdateBagAction.sender_name = VAR6.Employee.Employee_firstname + " " + VAR6.Employee.Employee_surname;
                        UpdateBagAction.reciever_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;


                        //close previous action
                        VarianceActivityResponse VAR1 = ClosePreviousAction(UpdateBagAction.Bag_Unid);
                        //advance action
                        VarianceActivityResponse VAR2 = InsertNewAction(UpdateBagAction);

                        VarianceActivityResponseBags VAR3 = GetSingleBagInfoWithUnid(UpdateBagAction.Bag_Unid);


                        if (VAR3.status != false)
                        {
                            //check if advanced bag was a kiosk bag
                            if (VAR3.Bags[0].bag_type == constants.BT1)
                            {
                                //if it was, get all child bags from kid to tid                        

                                VarianceActivityResponseBagChildren VAR4 = GetBagChildren(UpdateBagAction.Bag_Unid);

                                List<BagJoins> childBags = VAR4.BagJoins;

                                //and advance them too 
                                foreach (BagJoins CB in childBags)
                                {
                                    BagAction newAction = new BagAction();
                                    newAction.Bag_Unid = CB.Child_ID;
                                    newAction.Action_ID = UpdateBagAction.Action_ID;
                                    newAction.Sending_Employee_ID = UpdateBagAction.Sending_Employee_ID;
                                    newAction.Receiving_Employee_ID = UpdateBagAction.Receiving_Employee_ID;
                                    newAction.Location_ID = UpdateBagAction.Location_ID;
                                    newAction.Terminal_ID = UpdateBagAction.Terminal_ID;
                                    newAction.Total_Value = GetLastTerminalBagAmount(CB.Child_ID).BagAmount;

                                    SetBagAction(newAction);
                                }
                            }
                        }
                        else
                        {
                            //Could not get the bag type using the unid during UpdateBagAction
                            VAR.status = false;
                            VAR.errorText = "Could not get the bag type using the unid during SetBagActionWithLabelID";
                            VertedaDiagnostics.LogMessage("Could not get the bag type using the unid during SetBagActionWithLabelID", "E", false);
                        }

                    }
                    else
                    {
                        //employee not valid
                        //an errror occured. Could not find a bag matching that label ID
                        VAR.status = false;
                        VAR.errorText = "One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID;
                        VertedaDiagnostics.LogMessage("One of the employees was not valid for this transaction. Please check and try again." + UpdateBagAction.Sending_Employee_ID + " " + UpdateBagAction.Receiving_Employee_ID, "E", false);
                    }
                    
                }
                else
                {
                    //Could not find bag unid using label ID
                    VAR.status = false;
                    VAR.errorText = "Could not find bag unid using label ID";
                    VertedaDiagnostics.LogMessage("Could not find bag unid using label ID", "E", false);
                }

            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in SetBagActionWithLabelID: " + e.ToString(), "E", false);
            }

            return VAR;
        }

        //[WebMethod]
        //public VarianceActivityResponse AdvanceBagActionWithUnid(BagAction UpdateBagAction) //needs to include bagID, 2 x employeeID, value or count of cash bags. Optional terminal ID / profit ID
        //{
        //    VarianceActivityResponse VAR = new VarianceActivityResponse();

        //    try
        //    {
        //        //get current bag action
        //        VarianceActivityResponseBags VAR1 = GetSingleBagInfo(UpdateBagAction.Bag_ID);

        //        if (VAR1.status != false)
        //        {
        //            //if its not 5 (at team leader outbound) or 9 (at safe inbound), add 1
        //            if (VAR1.Bags[0].action_id != 5 && VAR1.Bags[0].action_id != 10 && VAR1.Bags[0].action_id != 13)
        //            {
        //                //call setbagaction with new number
        //                int newAction = VAR1.Bags[0].action_id + 1;
        //                UpdateBagAction.Action_ID = newAction;

        //                SetBagAction(UpdateBagAction);

        //                VAR.status = true;
        //            }
        //            else
        //            {
        //                //else error
        //                //this bag can not be advanced
        //                VAR.status = false;
        //                VAR.errorText = "Bag " + UpdateBagAction.Bag_ID + " cannot be advanced further";
        //                VertedaDiagnostics.LogMessage("Bag " + UpdateBagAction.Bag_ID + " cannot be advanced further", "E", false);
        //            }
        //        }
        //        else
        //        {
        //            //could not find the bag specified
        //            VAR.status = false;
        //            VAR.errorText = "Bag " + UpdateBagAction.Bag_ID + " could not be found";
        //            VertedaDiagnostics.LogMessage("Bag " + UpdateBagAction.Bag_ID + "  could not be found", "E", false);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }

        //    return VAR;
        //}

        //[WebMethod]
        //public VarianceActivityResponse SetBagActionWithUnid(BagAction UpdateBagAction)
        //{
        //    VarianceActivityResponse VAR = new VarianceActivityResponse();

        //    try
        //    {
        //        if (ValidateEmployee(UpdateBagAction.Sending_Employee_ID).status && ValidateEmployee(UpdateBagAction.Receiving_Employee_ID).status)
        //        {
        //            //close previous action
        //            VarianceActivityResponse VAR1 = ClosePreviousAction(UpdateBagAction.Bag_Unid);
        //            //advance action
        //            VarianceActivityResponse VAR2 = InsertNewAction(UpdateBagAction);

        //            //check if advanced bag was a kiosk bag
        //            if (UpdateBagAction.Bag_ID[0] == 'K')
        //            {
        //                //if it was, get all child bags from kid to tid                        

        //                VarianceActivityResponseBagChildren VAR3 = GetBagChildren(UpdateBagAction.Bag_Unid);

        //                List<BagJoins> childBags = VAR3.BagJoins;

        //                //and advance them too 
        //                foreach (BagJoins CB in childBags)
        //                {
        //                    BagAction newAction = new BagAction();
        //                    newAction.Bag_Unid = CB.Child_ID;
        //                    newAction.Action_ID = UpdateBagAction.Action_ID;

        //                    SetBagAction(newAction);
        //                }
        //            }

        //        }
        //        else
        //        {
        //            //employee not valid
        //            //an errror occured. Could not find a bag matching that label ID
        //            VAR.status = false;
        //            VAR.errorText = "One of the employees was not valid for this transaction. Please check and try again.";
        //            VertedaDiagnostics.LogMessage("One of the employees was not valid for this transaction. Please check and try again.", "E", false);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //    }

        //    return VAR;
        //}

        //[WebMethod]
        //public VarianceActivityResponse SetBulkBagActionWithLocationID(int action_id, int employee_id, int terminal_id, int current_location_id, int target_location_id, int total_child_bags)
        //{
        //    VarianceActivityResponse VAR = new VarianceActivityResponse();
        //    VertedaDiagnostics.LogMessage("SetBulkBagActionWithLocationID started. location id: " + target_location_id, "I", false);

        //    try
        //    {
        //        //Close all old actions


        //        //insert all new actions



        //        VAR.status = true;
        //    }
        //    catch (Exception e)
        //    {
        //        VAR.status = false;
        //        VAR.errorText = "Error in SetBulkBagAction. Not all bags could be advanced";
        //        VertedaDiagnostics.LogMessage("Error in SetBulkBagAction. Not all bags could be advanced", "E", false);
        //    }

        //    return VAR;
        //}
        
        
        public VarianceActivityResponse InsertInitialActions(Employees emp, List<Bag> eventBags)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            List<BagAction> bagActions = new List<BagAction>();
            List<unidLabel> initialLabels = new List<unidLabel>();

            VertedaDiagnostics.LogMessage("InsertInitialActions started", "I", false);
            try
            {
                //create bag_action data
                foreach (Bag bg in eventBags)
                {

                    BagAction ba = new BagAction();

                    
                    ba.Receiving_Employee_ID = bg.employee_id;
                    ba.Sending_Employee_ID = bg.employee_id;
                    ba.Terminal_ID = -1;
                    ba.Location_ID = bg.origin_location_id;
                    ba.Bag_ID = bg.bag_id;
                    ba.Bag_Unid = bg.bag_unid;
                    ba.sender_name = emp.Employee_firstname + " " + emp.Employee_surname;
                    ba.reciever_name = emp.Employee_firstname + " " + emp.Employee_surname;

                    if (bg.bag_type == constants.BT1)
                    {
                        //-----------------------------------------
                        //Not related to action code, but this need to run for each terminal bag when created, and this spot already does that \\zzz
                        //-----------------------------------------
                        if (constants.SiteLicence == 1)
                        {
                            unidLabel ul = new unidLabel();
                            ul.bag_unid = bg.bag_unid;
                            ul.label_id = bg.destination_name;
                            initialLabels.Add(ul);
                        }
                        //-----------------------------------------

                        //if bag is kiosk, get total child bags, value = 0
                        VarianceActivityResponseBagChildren var2 = GetBagChildren(bg.bag_unid);
                        List<BagJoins> bj = var2.BagJoins;
                        
                        ba.Action_ID = 2;
                        ba.Bag_Count = bj.Count();
                        ba.Total_Value = 0;

                    }
                    else if (bg.bag_type == constants.BT2)
                    {
                        //-----------------------------------------
                        //Not related to action code, but this need to run for each terminal bag when created, and this spot already does that \\zzz
                        //-----------------------------------------
                        unidLabel ul = new unidLabel();
                        ul.bag_unid = bg.bag_unid;
                        ul.label_id = bg.bag_id;
                        initialLabels.Add(ul);

                        //-----------------------------------------

                        //if bag is terminal, get value of composition: Get_Composition_Details(bg.compositionid)   , child bags = 0
                        VarianceActivityResponseCompositionDetails var3 = Get_Composition_Details(bg.composition_id);
                        
                        ba.Action_ID = 2; //Would normally be 1, but its being created in a kiosk bag, hence 2
                        ba.Bag_Count = 0;
                        ba.Total_Value = var3.CompositionValue;
                    }

                    bagActions.Add(ba);
                }

                //bulk insert it
                VarianceActivityResponse var4 = InsertBulkAction(bagActions);
                VarianceActivityResponse var5 = UpdateBagLabelsWithUnid(initialLabels);

            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in InsertInitialActions", "I", false);
            }
            return VAR;
        }

        public VarianceActivityResponse InsertInitialActionsForSpecififcEventID(Employees emp, int eventid, List<Bag> eventBags)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            List<BagAction> bagActions = new List<BagAction>();
            List<unidLabel> initialLabels = new List<unidLabel>();

            VertedaDiagnostics.LogMessage("InsertInitialActionsForSpecififcEventID started. eventid: "+eventid, "I", false);
            try
            {
                ConfigSettings cs1 = new ConfigSettings();
                try
                {
                    cs1 = ReadConfig();
                    VertedaDiagnostics.LogMessage("InsertInitialActionsForSpecififcEventID: got config", "I", false);

                }
                catch (Exception cex)
                {
                    Primo_SendBugsnag("InsertInitialActionsForSpecififcEventID: failed to get config", cex, "File i/o");
                    VertedaDiagnostics.LogMessage("InsertInitialActionsForSpecififcEventID: failed to get config", "I", false);
                }


                //create bag_action data
                foreach (Bag bg in eventBags)
                {

                    BagAction ba = new BagAction();

                    
                    ba.Receiving_Employee_ID = bg.employee_id;
                    ba.Sending_Employee_ID = bg.employee_id;
                    ba.Terminal_ID = cs1.CashOfficeTerminalID;
                    ba.Location_ID = cs1.CashOfficeID;
                    ba.Bag_ID = bg.bag_id;
                    ba.Bag_Unid = bg.bag_unid;
                    ba.sender_name = emp.Employee_firstname + " " + emp.Employee_surname;
                    ba.reciever_name = emp.Employee_firstname + " " + emp.Employee_surname;

                    ba.location_name = cs1.CashOfficeName;
                    ba.terminal_name = cs1.CashOfficeTerminalName;

                    if (bg.bag_type == constants.BT1)
                    {
                        //-----------------------------------------
                        //Not related to action code, but this need to run for each terminal bag when created, and this spot already does that \\zzz
                        //-----------------------------------------
                        //if (constants.SiteLicence == 1)
                        //{
                        unidLabel ul = new unidLabel();
                        ul.bag_unid = bg.bag_unid;
                        ul.label_id = bg.destination_name;
                        initialLabels.Add(ul);
                        //}
                        //-----------------------------------------
                        ba.Action_ID = 2;                        
                        ba.Total_Value = 0;                        

                        //if bag is kiosk, get total child bags, value = 0
                        VarianceActivityResponseBagChildren var2 = GetBagChildren(bg.bag_unid);
                        if (var2.status)
                        {
                            List<BagJoins> bj = var2.BagJoins;
                            ba.Bag_Count = bj.Count;
                        }
                        else
                        {
                            ba.Bag_Count = 0;
                            //error - couldnt find children for bag: 
                            VertedaDiagnostics.LogMessage("Error in InsertInitialActionsForSpecififcEventID: couldnt find child bags for: "+ba.Bag_Unid, "E", false);
                        }
                        bagActions.Add(ba);
                    }
                    else if (bg.bag_type == constants.BT2)
                    {
                        //-----------------------------------------
                        //Not related to action code, but this need to run for each terminal bag when created, and this spot already does that \\zzz
                        //-----------------------------------------
                        unidLabel ul = new unidLabel();
                        ul.bag_unid = bg.bag_unid;
                        ul.label_id = bg.bag_id;
                        initialLabels.Add(ul);

                        //-----------------------------------------

                        //if bag is terminal, get value of composition: Get_Composition_Details(bg.compositionid)   , child bags = 0
                        VarianceActivityResponseCompositionDetails var3 = Get_Composition_Details(bg.composition_id);

                         ba.Action_ID = 1; 
                         ba.Bag_Count = 0;
                         ba.Total_Value = var3.CompositionValue;

                         bagActions.Add(ba); //action 1


                         BagAction ba_two = new BagAction();
                        ba_two.Receiving_Employee_ID = bg.employee_id;
                        ba_two.Sending_Employee_ID = bg.employee_id;
                        ba_two.Terminal_ID = cs1.CashOfficeTerminalID;
                        ba_two.Location_ID = cs1.CashOfficeID;
                        ba_two.Bag_ID = bg.bag_id;
                        ba_two.Bag_Unid = bg.bag_unid;
                        ba_two.sender_name = emp.Employee_firstname + " " + emp.Employee_surname;
                        ba_two.reciever_name = emp.Employee_firstname + " " + emp.Employee_surname;

                        ba_two.location_name = cs1.CashOfficeName;
                        ba_two.terminal_name = cs1.CashOfficeTerminalName;


                        ba_two.Action_ID = 2;
                        ba_two.Bag_Count = 0;
                        ba_two.Total_Value = var3.CompositionValue;

                        bagActions.Add(ba_two); //action 2
                    }
                    
                }

                //bulk insert it
                VarianceActivityResponse var4 = InsertBulkAction(bagActions);
                VarianceActivityResponse var5 = UpdateBagLabelsWithUnid(initialLabels);

            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in InsertInitialActionsForSpecififcEventID "+e.ToString(), "E", false);
            }
            return VAR;
        }


        //[WebMethod]
        //public VarianceActivityResponse ClosePreviousAction(string BagID) 
        //{
        //    //[SP_Bag_Action_Join_Set_End_Time]

        //    VarianceActivityResponse VAR = new VarianceActivityResponse();

        //        //Using Output parameter
        //        SqlConnection conn = new SqlConnection(GetConnectionString(2));
        //        SqlCommand cmd = new SqlCommand();

        //        //edit existing record
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "SP_Bag_Action_Join_Set_End_Time";
        //        cmd.Parameters.Add("@BagID", SqlDbType.Int).Value = BagID;

        //        cmd.Connection = conn;
        //        //return package_id to use in copy

        //        try
        //        {
        //            conn.Open();
        //            cmd.ExecuteNonQuery();                    
        //            VAR.status = true;

        //        }
        //        catch (Exception ex)
        //        {
        //            VAR.status = false;
        //            VAR.errorText = ex.ToString();
        //            VertedaDiagnostics.LogMessage("Error in ClosePreviousAction: " + ex.ToString(), "E", false);
                    
        //        }
        //        finally
        //        {
        //            conn.Close();
        //            conn.Dispose();
        //            VertedaDiagnostics.LogMessage("ClosePreviousAction Completed successfully", "I", false);
        //        }

        //    return VAR;
        //}

        [WebMethod]
        public VarianceActivityResponse ClosePreviousAction(int BagUnid)
        {
            VertedaDiagnostics.LogMessage("ClosePreviousAction started. bagunid: "+BagUnid, "I", false);
            //[SP_Bag_Action_Join_Set_End_Time]

            VarianceActivityResponse VAR = new VarianceActivityResponse();
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            //edit existing record
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Bag_Action_Join_Set_End_Time_With_Unid";
            cmd.Parameters.Add("@unid", SqlDbType.Int).Value = BagUnid;

            cmd.Connection = conn;
            //return package_id to use in copy


            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    VAR.status = true;
                }

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in ClosePreviousActionWithUnid: " + ex.ToString(), "E", false);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
                VertedaDiagnostics.LogMessage("ClosePreviousAction Completed successfully", "I", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponseActions ClosePreviousActionAndReturnCounts(int BagUnid, int action_id)
        {
            VertedaDiagnostics.LogMessage("ClosePreviousActionAndReturnCounts started. bagunid: " + BagUnid, "I", false);
            //[SP_Bag_Action_Join_Set_End_Time]

            VarianceActivityResponseActions VAR = new VarianceActivityResponseActions();
            List<BagAction> BAs = new List<BagAction>();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            //edit existing record
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Bag_Action_Join_Set_End_Time_With_Unid_And_Return_Counts";
            cmd.Parameters.Add("@unid", SqlDbType.Int).Value = BagUnid;
            cmd.Parameters.Add("@action_id", SqlDbType.Int).Value = action_id;

            //cmd.Parameters.Add("@bag_unid", SqlDbType.Int).Direction = ParameterDirection.Output;
            //cmd.Parameters.Add("@total_child_bags", SqlDbType.Int).Direction = ParameterDirection.Output;
            //cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;

            cmd.Connection = conn;
            //return package_id to use in copy



            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                conn.Open();
                //cmd.ExecuteNonQuery();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    BagAction item = new BagAction();

                    item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());
                    item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
                    item.Bag_Unid = Convert.ToInt32(rdr["bag_unid"].ToString()); 

                    BAs.Add(item);

                }
                rdr.Close();


                VAR.status = true;
                VAR.BagActions = BAs;

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in ClosePreviousActionAndReturnCounts: " + ex.ToString(), "E", false);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
                VertedaDiagnostics.LogMessage("ClosePreviousActionAndReturnCounts Completed successfully", "I", false);
            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse InsertNewAction(BagAction UpdateBagAction) 
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("InsertNewAction started. bag_unid: " + UpdateBagAction.Bag_Unid, "I", false);

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;

                if (con_str.database_instance == 2)
                {
                    cmd.CommandText = "SP_Bag_Action_Join_Insert_hospitality";
                }
                else //always default to 1
                {
                    cmd.CommandText = "SP_Bag_Action_Join_Insert_public";
                }
                

            cmd.Parameters.Add("@actionid", SqlDbType.Int).Value = UpdateBagAction.Action_ID;
            cmd.Parameters.Add("@send_employeeid", SqlDbType.Int).Value = UpdateBagAction.Sending_Employee_ID;
            cmd.Parameters.Add("@rec_employeeid", SqlDbType.Int).Value = UpdateBagAction.Receiving_Employee_ID;
            cmd.Parameters.Add("@terminalid", SqlDbType.Int).Value = UpdateBagAction.Terminal_ID;
            cmd.Parameters.Add("@locationid", SqlDbType.Int).Value = UpdateBagAction.Location_ID;
            cmd.Parameters.Add("@bag_unid", SqlDbType.VarChar).Value = UpdateBagAction.Bag_Unid;
            cmd.Parameters.Add("@totalvalue", SqlDbType.Decimal).Value = UpdateBagAction.Total_Value;
            cmd.Parameters.Add("@bagcount", SqlDbType.Int).Value = UpdateBagAction.Bag_Count;
            cmd.Parameters.Add("@sender_name", SqlDbType.VarChar).Value = UpdateBagAction.sender_name;
            cmd.Parameters.Add("@reciever_name", SqlDbType.VarChar).Value = UpdateBagAction.reciever_name;
            cmd.Parameters.Add("@status", SqlDbType.Int).Value = UpdateBagAction.status_id; 
            cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;

            cmd.Connection = conn;
            //return package_id to use in copy

            
                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.ID = Convert.ToInt32(cmd.Parameters["@id"].Value.ToString());
                VAR.status = true;

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertNewAction: " + ex.ToString(), "E", false);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
                VertedaDiagnostics.LogMessage("InsertNewAction Completed successfully", "I", false);
            }

            return VAR;
        }

        //[WebMethod]
        //public VarianceActivityResponse InsertNewActionWithUnid(BagAction UpdateBagAction)
        //{
        //    VarianceActivityResponse VAR = new VarianceActivityResponse();

        //    //Using Output parameter
        //    SqlConnection conn = new SqlConnection(GetConnectionString(2));
        //    SqlCommand cmd = new SqlCommand();
        //    try
        //    {

        //        //edit existing record
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "SP_Bag_Action_Join_Insert";
        //        cmd.Parameters.Add("@actionid", SqlDbType.Int).Value = UpdateBagAction.Action_ID;
        //        cmd.Parameters.Add("@send_employeeid", SqlDbType.Int).Value = UpdateBagAction.Sending_Employee_ID;
        //        cmd.Parameters.Add("@rec_employeeid", SqlDbType.Int).Value = UpdateBagAction.Receiving_Employee_ID;
        //        cmd.Parameters.Add("@terminalid", SqlDbType.Int).Value = UpdateBagAction.Terminal_ID;
        //        cmd.Parameters.Add("@locationid", SqlDbType.Int).Value = UpdateBagAction.Location_ID;
        //        cmd.Parameters.Add("@bagid", SqlDbType.VarChar).Value = UpdateBagAction.Bag_ID;
        //        cmd.Parameters.Add("@totalvalue", SqlDbType.Decimal).Value = UpdateBagAction.Total_Value;
        //        cmd.Parameters.Add("@bagcount", SqlDbType.Int).Value = UpdateBagAction.Bag_Count;
        //        cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;

        //        cmd.Connection = conn;
        //        //return package_id to use in copy


        //        conn.Open();
        //        cmd.ExecuteNonQuery();
        //        VAR.ID = Convert.ToInt32(cmd.Parameters["@id"].Value.ToString());
        //        VAR.status = true;

        //    }
        //    catch (Exception ex)
        //    {
        //        VAR.status = false;
        //        VAR.errorText = ex.ToString();
        //        VertedaDiagnostics.LogMessage("Error in InsertNewAction: " + ex.ToString(), "E", false);

        //    }
        //    finally
        //    {
        //        conn.Close();
        //        conn.Dispose();
        //        VertedaDiagnostics.LogMessage("InsertNewAction Completed successfully", "I", false);
        //    }

        //    return VAR;
        //}
        

        [WebMethod]
        public VarianceActivityResponse InsertBulkAction(List<BagAction> UpdateBagAction) 
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("InsertBulkAction started", "I", false);

            DataTable dt = new DataTable();
            try
            {
                dt.Columns.AddRange(new DataColumn[12] { 
                    new DataColumn("action_id", typeof(int)),
                    new DataColumn("sending_employee", typeof(int)),
                    new DataColumn("recieving_employee", typeof(int)),
                    new DataColumn("terminal_id", typeof(int)),
                    new DataColumn("terminal_name", typeof(string)),
                    new DataColumn("location_id", typeof(int)),
                    new DataColumn("location_name", typeof(string)),
                    new DataColumn("bag_unid",typeof(int)),
                    new DataColumn("total_value", typeof(decimal)),
                    new DataColumn("total_child_bags", typeof(int)), 
                    new DataColumn("sender_name", typeof(string)),
                    new DataColumn("reciever_name", typeof(string))                    });

                foreach (BagAction pcv in UpdateBagAction)
                {
                    dt.Rows.Add(pcv.Action_ID,
                        pcv.Sending_Employee_ID,
                        pcv.Receiving_Employee_ID,
                        pcv.Terminal_ID,
                        pcv.terminal_name,
                        pcv.Location_ID,
                        pcv.location_name,
                        pcv.Bag_Unid,
                        pcv.Total_Value,
                        pcv.Bag_Count,
                        pcv.sender_name,
                        pcv.reciever_name); //##
                }
                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        ConnectionString con_str = GetConnectionString(2);

                        using (SqlConnection con = new SqlConnection(con_str.connection_string))
                        {
                            using (SqlCommand cmd = new SqlCommand("SP_Bag_Action_Join_Bulk_Insert"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Connection = con;
                                cmd.Parameters.AddWithValue("@tblActions", dt);
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();                                                             
                            }
                        }
                        VAR.status = true;   
                        VertedaDiagnostics.LogMessage("InsertTerminals Completed successfully", "I", false);
                    }
                    catch (Exception e)
                    {
                        VAR.status = false;
                        VAR.errorText = "Error when inserting initial bag actions";
                        VertedaDiagnostics.LogMessage("Error in InsertBulkAction: " + e.ToString(), "E", false);
                    }
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = "Error when inserting initial bag actions";
                VertedaDiagnostics.LogMessage("Error in InsertBulkAction: " + e.ToString(), "E", false);
            }

            return VAR;

        }
                
    


        [WebMethod]
        public VarianceActivityResponseBagUNID GetTerminalBagUNIDfromKioskUNID(int bag_unid)
        {

            VertedaDiagnostics.LogMessage("GetTerminalBagUNIDfromKioskUNID started. bag unid: " + bag_unid, "I", false);
            VarianceActivityResponseBagUNID VAR = new VarianceActivityResponseBagUNID();

            int availableBagUNID = 0;

            try
            {
                ////get bag info of kiosk bag
                VarianceActivityResponseBags var1 = GetSingleBagInfoWithUnid(bag_unid);
                //string kioskBagID = var1.Bags[0].bag_id;

                //use kiosk bag ID to get children
                VarianceActivityResponseBagChildren var2 = GetBagChildren(bag_unid);
                List<BagJoins> childrenBagIDs = var2.BagJoins;

                //get the actions of all bags in the same location as kiosk bag
                VarianceActivityResponseActions var3 = GetTerminalActionsInLocation(var1.Bags[0].destination_location_id);
                List<BagAction> locationActions = var3.BagActions;

                //for each child
                foreach (BagJoins bj in childrenBagIDs)
                {
                    //if its action exists in the list of returned actions
                    foreach (BagAction ba in locationActions)
                    {
                        if (bj.Child_ID == ba.Bag_Unid)
                        {
                            //if the action is 'not yet checked in as loan'
                            if (ba.Action_ID != 5)
                            {
                                //we have a winner!
                                availableBagUNID = ba.Bag_Unid;
                            }
                        }
                    }
                }

                if (availableBagUNID != 0)
                {
                            //advance this bag - need other information
                            //AdvanceBagAction(availableBagID);
                    //send back the bag ID
                    VAR.status = true;
                    VAR.BagUNID = availableBagUNID;
                }
                else
                {
                    //return error message that there are no available bags left to be checked in.
                    VAR.status = false;
                    VAR.errorText = "There are no available terminal bags left to be checked in";
                }
            }
            catch(Exception e)
            {
                VAR.status = false;
                VAR.errorText = "GetTerminalBagUNIDfromKioskUNID could not complete. Error: " + e.ToString();

            }
            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse UpdateBagActionValue(BagAction bag_action_to_amend)
        {
            //[SP_Bag_Action_Join_Set_End_Time]

            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("UpdateBagActionValue started. bag_action_unid: " + bag_action_to_amend.Bag_Action_Unid, "I", false);

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            //edit existing record
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Bag_Action_Join_Update_Bag_Value";
            cmd.Parameters.Add("@bag_action_unid", SqlDbType.Int).Value = bag_action_to_amend.Bag_Action_Unid;
            cmd.Parameters.Add("@bag_value", SqlDbType.Int).Value = bag_action_to_amend.Total_Value;

            cmd.Connection = conn;
            //return package_id to use in copy


                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //back up record first
                VarianceActivityResponse var1 = WriteToBagActionAdjustmentHistory(bag_action_to_amend.Bag_Action_Unid, bag_action_to_amend.Receiving_Employee_ID);

                if (var1.status == true)
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    VAR.status = true;
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not write to bag action join history. Action value not ammended";
                    VertedaDiagnostics.LogMessage("Could not write to bag action join history. Action value not ammended", "E", false); 
                }

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateBagActionValue: " + ex.ToString(), "E", false);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
                VertedaDiagnostics.LogMessage("UpdateBagActionValue Completed successfully", "I", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse UpdateBagActionStatus(BagAction bag_action_to_amend)
        {
            //[SP_Bag_Action_Join_Set_End_Time]

            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("UpdateBagActionStatus started. bag_action_unid: " + bag_action_to_amend.Bag_Action_Unid, "I", false);

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            //edit existing record
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Bag_Action_Join_Update_Bag_Status";
                cmd.Parameters.Add("@bag_action_unid", SqlDbType.Int).Value = bag_action_to_amend.Bag_Action_Unid;
                cmd.Parameters.Add("@bag_status", SqlDbType.Int).Value = bag_action_to_amend.Total_Value;

                cmd.Connection = conn;
                //return package_id to use in copy


                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //back up record first
                VarianceActivityResponse var1 = WriteToBagActionAdjustmentHistory(bag_action_to_amend.Bag_Action_Unid, bag_action_to_amend.Receiving_Employee_ID);

                if (var1.status == true)
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    VAR.status = true;
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "UpdateBagActionStatus: Could not write to bag action join history. Action value not ammended";
                    VertedaDiagnostics.LogMessage("UpdateBagActionStatus: Could not write to bag action join history. Action value not ammended", "E", false);
                }

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateBagActionStatus: " + ex.ToString(), "E", false);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
                VertedaDiagnostics.LogMessage("UpdateBagActionStatus Completed successfully", "I", false);
            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse UpdateMultipleBagActionValues(List<BagAction> update_values)
        {

            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("UpdateMultipleBagActionValues started.", "I", false);

            try
            {
                foreach (BagAction uv in update_values)
                {
                    UpdateBagActionValue(uv);
                }

                VAR.status = true;
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateMultipleBagActionValues: " + ex.ToString(), "E", false);

            }

            return VAR;
        }




        [WebMethod]
        public VarianceActivityResponse UpdateBagActionBagCount(int bag_action_unid, int bag_count, int employee_id, int status_id)
        {
            //[SP_Bag_Action_Join_Set_End_Time]

            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("UpdateBagActionBagCount started. bag_action_unid: " + bag_action_unid, "I", false);

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Bag_Action_Join_Update_Bag_Count";
            cmd.Parameters.Add("@bag_action_unid", SqlDbType.Int).Value = bag_action_unid;
            cmd.Parameters.Add("@child_bag_count", SqlDbType.Int).Value = bag_count;
            cmd.Parameters.Add("@status", SqlDbType.Int).Value = status_id;

            cmd.Connection = conn;
            //return package_id to use in copy

                //back up record first
                VarianceActivityResponse var1 = WriteToBagActionAdjustmentHistory(bag_action_unid, employee_id);

                if (var1.status == true)
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    VAR.status = true;
                }
                else
                {
                    VAR.status = false;
                    VAR.errorText = "Could not write to bag action join history. Action value not ammended";
                    VertedaDiagnostics.LogMessage("Could not write to bag action join history. Action value not ammended", "E", false);
                }

            }
            catch (Exception ex)
            {
                Primo_SendBugsnag("Error updating bag action count", ex, "Database");
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in UpdateBagActionBagCount: " + ex.ToString(), "E", false);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
                VertedaDiagnostics.LogMessage("UpdateBagActionBagCount Completed successfully", "I", false);
            }

            return VAR;
        }

        [WebMethod]
        public VarianceActivityResponse WriteToBagActionAdjustmentHistory(int bag_action_unid, int employee_id) //copy the current bag_action_join record before amending it
        {

            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("WriteToBagActionAdjustmentHistory started. bag_action_unid: " + bag_action_unid, "I", false);


            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_Bag_Action_Join_Adjustment_History_Insert";
            cmd.Parameters.Add("@bag_action_unid", SqlDbType.Int).Value = bag_action_unid;
            cmd.Parameters.Add("@employee_id", SqlDbType.Int).Value = employee_id;

            cmd.Connection = conn;
            //return package_id to use in copy

                conn.Open();
                cmd.ExecuteNonQuery();
                VAR.status = true;

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in WriteToBagActionAdjustmentHistory: " + ex.ToString(), "E", false);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
                VertedaDiagnostics.LogMessage("WriteToBagActionAdjustmentHistory Completed successfully", "I", false);
            }

            return VAR;
        }



        [WebMethod]
        public VarianceActivityResponse SetBagActionToDefaults(int location_id, int terminal_id, int employee_id) //sets a kiosk bags action to team leader, and works out what bag it should be
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("SetBagActionToDefaults started. bag_action_unid.", "I", false);

            try
            {
                //get event ID
                 int eventid = GetEventID();

                 if (eventid > 0)
                 {

                     //find the bag that is expected. function id = 1, event_id = ???, destination_id = location_id
                     VarianceActivityResponseBags VAR1 = GetLoanKioskBagInfoForADestinationLocation(eventid, location_id);

                     if (VAR1.status != false && VAR1.Bags.Count() == 1)
                     {
                         //check the current action of that bag
                         if (VAR1.Bags[0].action_id == 3)
                         {
                             //make new aciton and call setbagaction
                             BagAction BA = new BagAction();

                             BA.Action_ID = 4;
                             BA.Sending_Employee_ID = employee_id;
                             BA.Receiving_Employee_ID = employee_id;
                             BA.Terminal_ID = terminal_id;
                             BA.Location_ID = location_id;
                             BA.Bag_Unid = VAR1.Bags[0].bag_unid;
                             BA.Total_Value = 0;
                             BA.Bag_Count = VAR1.Bags[0].total_child_bags;

                             VarianceActivityResponse VAR2 = SetBagAction(BA);

                             VAR.status = true;

                         }
                         else
                         {
                             //error - this bag is not currently with security
                             VAR.status = false;
                             VAR.errorText = "Error in SetBagActionToDefaults: this bag is not currently with security. current action: " + VAR1.Bags[0].action_id;
                             VertedaDiagnostics.LogMessage("Error in SetBagActionToDefaults: this bag is not currently with security. current action: " + VAR1.Bags[0].action_id, "E", false);
                         }

                     }
                     else
                     {
                         //error - failed to find a kiosk bag to check into team leader or found more than 1 bag
                         //could also be because the bag has multiple actions without end dates
                         VAR.status = false;
                         VAR.errorText = "Error in SetBagActionToDefaults: failed to find a kiosk bag to check into team leader or found more than 1 bag was found.";
                         VertedaDiagnostics.LogMessage("Error in SetBagActionToDefaults: failed to find a kiosk bag to check into team leader or found more than 1 bag was found.", "E", false);
                     }
                 }
                 else
                 {
                     //error - no event ID
                     VAR.status = false;
                     VAR.errorText = "Error in SetBagActionToDefaults: Couldn't find current event id.";
                     VertedaDiagnostics.LogMessage("Error in SetBagActionToDefaults: Couldn't find current event id", "E", false);
                 }
            }
            catch (Exception e)
            {
                //error
                VAR.status = false;
                VAR.errorText = "Error in SetBagActionToDefaults: "+e.ToString();
                VertedaDiagnostics.LogMessage("Error in SetBagActionToDefaults: " + e.ToString(), "E", false);
            }
            return VAR;
        }




        #endregion

        #region Terminal procedures

        //[WebMethod]
        //public VarianceActivityResponsePrintInfo ActivityOnSingleBag(int mode, int terminal_id, int employee_id, int location_id) //mode 1 = loan, 2 = withdraw, 3 = cash out, 4= top up
        //{
        //    VarianceActivityResponsePrintInfo VAR = new VarianceActivityResponsePrintInfo();
        //    VertedaDiagnostics.LogMessage("ActivityOnSingleBag started " + mode + " " + terminal_id + " " + employee_id + " " + location_id, "I", false);

        //    try
        //    {
        //        //validate the employee
        //        VarianceActivityResponseEmployee VAR7 = ValidateEmployee(employee_id);

        //        if (VAR7.status)
        //        {
        //                VertedaDiagnostics.LogMessage("ActivityOnSingleBag: employee valid", "I", false);
        //                decimal lastChangeAmt = 0;
        //                int loopCount = 0;

        //                while (true)
        //                {
        //                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: Loop started. Loopcount: "+loopCount, "I", false);

        //                    //calculate the value of the last withdraw or cash out on [Sum_Shift_BP_PC_MP_CE_backup]
        //                    VarianceActivityResponsShiftData var1 = CalculateShiftChange(terminal_id, employee_id, location_id); //termID, empID, locID

        //                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: Calculate shift change finished. Status: " + var1.status.ToString() + " Shift data count: " + var1.Shift_Data.Count.ToString(), "I", false);
        //                    if (var1.status != false && var1.Shift_Data.Count != 0)
        //                    {
        //                        //calculate the amount needed from the data
        //                        List<Sum_Shift_Data> ShiftAmounts = var1.Shift_Data;

        //                        if (mode == 1 || mode == 4) //loan
        //                        {
        //                            if (ShiftAmounts.Count() == 1)
        //                            {
        //                                lastChangeAmt = ShiftAmounts[0].loan_amt;
        //                            }
        //                            else
        //                            {
        //                                //if  the amount has been reset for whatever reason (employee switch or terminal reset), use the lower value
        //                                if (ShiftAmounts[0].loan_amt < ShiftAmounts[1].loan_amt)
        //                                {
        //                                    lastChangeAmt = ShiftAmounts[0].loan_amt;
        //                                }
        //                                else
        //                                {
        //                                    decimal currentAmt = ShiftAmounts[0].loan_amt;
        //                                    decimal prevAmt = ShiftAmounts[1].loan_amt;

        //                                    lastChangeAmt = currentAmt - prevAmt;
        //                                }
        //                            }
        //                        }
        //                        else if (mode == 2) //withdrawl
        //                        {
        //                            if (ShiftAmounts.Count() == 1)
        //                            {
        //                                lastChangeAmt = ShiftAmounts[0].withdrawal_amt;
        //                            }
        //                            else
        //                            {
        //                                //if 0 is < 1, somehow its been reset, set amount = 0.

        //                                //else
        //                                if (ShiftAmounts[0].withdrawal_amt < ShiftAmounts[1].withdrawal_amt)
        //                                {
        //                                    lastChangeAmt = ShiftAmounts[0].withdrawal_amt;
        //                                }
        //                                else
        //                                {
        //                                    decimal currentAmt = ShiftAmounts[0].withdrawal_amt;
        //                                    decimal prevAmt = ShiftAmounts[1].withdrawal_amt;

        //                                    lastChangeAmt = currentAmt - prevAmt;
        //                                }
        //                            }

        //                        }
        //                        else if (mode == 3) //cash out
        //                        {
        //                            //convert values to dim IDs. Then get cash amount from GetCashDropAmount()

        //                            int businessDimID = GetCurrentBusinessPeriodID();
        //                            int terminalDimID = GetTermDimID(terminal_id);
        //                            int employeeDimID = GetEmpDimID(employee_id);
        //                            int locationDimID = GetLocDimID(location_id);

        //                            VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 3, getting dim values. business period, terminal, employee, location: " + businessDimID + " " + terminalDimID + " " + employeeDimID + " " + locationDimID, "E", false);

        //                            decimal cashOutAmount = GetCashDropAmount(terminalDimID, employeeDimID, locationDimID, businessDimID).CashDrops[0].Amount;

        //                            //terminal  = VariancePrimo_GetTerminalFromID_today(int terminal_id)
        //                            //profit_centre = VariancePrimo_GetProfitCentreFromID_today(int pc_id)
        //                            //employee = VariancePrimo_GetEmployeeFromID_today(int emp_id)

        //                            VertedaDiagnostics.LogMessage("ActivityOnSingleBag: Cash out amount = "+ cashOutAmount, "I", false);

        //                            if (cashOutAmount != 0)
        //                                lastChangeAmt = cashOutAmount;
        //                            else
        //                            {
        //                                //error
        //                                VAR.status = false;
        //                                VAR.errorText = "Could not get a value for the last cash out during ActivityOnSingleBag";
        //                                VertedaDiagnostics.LogMessage("Could not get a value for the last cash out during ActivityOnSingleBag", "E", false);
        //                            }

        //                        }
        //                        else
        //                        {
        //                            //Mode not found, please check and try again.                                    
        //                            VAR.status = false;
        //                            VAR.errorText = "Incorrect mode when using ActivityOnSingleBag";
        //                            VertedaDiagnostics.LogMessage("Incorrect mode when using ActivityOnSingleBag", "E", false);
        //                            break;
        //                        }




        //                        if (lastChangeAmt > 0)
        //                        {
        //                            VertedaDiagnostics.LogMessage("ActivityOnSingleBag. Last change amount: " + lastChangeAmt, "I", false);

        //                            if (mode == 2 || mode == 3)
        //                            {
        //                                //great!
        //                                //create a new bag using above amount   && create the initial action 

        //                                Bag newBag = new Bag();
        //                                newBag.terminal_id = terminal_id;
        //                                newBag.employee_id = employee_id;
        //                                newBag.current_value = lastChangeAmt;
        //                                newBag.bag_type = constants.BT2;
        //                                newBag.action_id = 6;
        //                                newBag.label_id = "";
        //                                newBag.origin_location_id = location_id;
        //                                if (mode == 2)//due to changes, this now includes the type of return. 10 = skim, 11= reset
        //                                {
        //                                    newBag.bag_function_id = 10; 
        //                                }
        //                                else
        //                                {
        //                                    newBag.bag_function_id = 11; 
        //                                }

        //                                newBag.destination_location_id = ReadConfig().CashOfficeID;

        //                                List<BagJoins> noJoinsForTerminalBag = new List<BagJoins>();

        //                                VarianceActivityResponseBags var2 = SetupNewSingleBag(newBag, noJoinsForTerminalBag);             //requires terminal ID, employee ID, amount, type, actionID, labelID, locationID

        //                                VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 2/3. Setup new bag status: "+var2.status.ToString(), "I", false);

        //                                //collate print information for the above bag  and return it
        //                                if (var2.status != false)
        //                                {
        //                                    //bag has been created.
        //                                    //print information no longer needed.
        //                                    //VarianceActivityResponsePrintInfo var3 = GetPrintInformation(var2.Bags[0].bag_unid);
        //                                }
        //                                else
        //                                {
        //                                    //error
        //                                    VAR.status = false;
        //                                    VAR.errorText = "Could not print the bag during ActivityOnSingleBag. Could not setup the bag";
        //                                    VertedaDiagnostics.LogMessage("Could not print the bag during ActivityOnSingleBag. Could not setup the bag", "E", false);

        //                                }
        //                            }
        //                            else if (mode == 1 || mode == 4)
        //                            {
        //                                //do clever stuff
        //                                //Gets the kiosk unid
        //                                VarianceActivityResponseBags VAR4 = GetKioskBagUnidFromLocation(location_id, mode);
        //                                VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. kiosk bag unid from location: " + VAR4.status.ToString(), "I", false);

        //                                if (VAR4.status && VAR4.Bags.Count() > 0)
        //                                {
        //                                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. kiosk bag unid from location = " + VAR4.Bags[0].bag_unid.ToString(), "I", false);
        //                                    VarianceActivityResponseBagUNID VAR5 = new VarianceActivityResponseBagUNID();
        //                                    VAR5.status = false;

        //                                    for (int x = 0; x < VAR4.Bags.Count(); x++)
        //                                    {
        //                                        //get the first available terminal unid
        //                                        VAR5 = GetTerminalBagUNIDfromKioskUNID(VAR4.Bags[0].bag_unid); //xxx
        //                                        if (VAR5.status)
        //                                        {
        //                                            //if a kiosk bag with a usable terminal bag is found, exit loop

        //                                            break;
        //                                        }
        //                                    }

        //                                    if (VAR5.status)
        //                                    {
        //                                        VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. terminal bag from kiosk bag in location found. Bag unid = " + VAR5.BagUNID.ToString() , "I", false);
        //                                        //advance that bag's action and close previous
        //                                        BagAction ba = new BagAction();

        //                                        ba.Action_ID = 5;
        //                                        ba.Sending_Employee_ID = employee_id;
        //                                        ba.Receiving_Employee_ID = employee_id;
        //                                        ba.Terminal_ID = terminal_id;
        //                                        ba.Location_ID = location_id;
        //                                        ba.Bag_Unid = VAR5.BagUNID;
        //                                        ba.Total_Value = lastChangeAmt;
        //                                        ba.Bag_Count = 0;
        //                                        ba.sender_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;
        //                                        ba.reciever_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;

        //                                        VarianceActivityResponse VAR6 = SetBagAction(ba);
        //                                        VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. set bag action on bag unid " + VAR5.BagUNID + " " + VAR6.status.ToString(), "I", false);
        //                                    }
        //                                    else
        //                                    {
        //                                        //error when finding an available terminal bag in location. None were available to be advanced
        //                                        VAR.status = false;
        //                                        VAR.errorText = "error when finding an available terminal bag in location. None were available to be advanced. Could not allocate a bag to terminal " + terminal_id + " when performing action " + mode;
        //                                        VertedaDiagnostics.LogMessage("error when finding an available terminal bag in location. None were available to be . Could not allocate a bag to terminal " + terminal_id + " when performing action " + mode, "E", false);
        //                                    }


        //                                }
        //                                else
        //                                {
        //                                    //error when getting Kiosk bag unid using the location ID in ActivityOnSingleBag
        //                                    VAR.status = false;
        //                                    VAR.errorText = "error when getting Kiosk bag unid using the location ID in ActivityOnSingleBag, or no bags existed";
        //                                    VertedaDiagnostics.LogMessage("error when getting Kiosk bag unid using the location ID in ActivityOnSingleBag, or no bags existed", "E", false);
        //                                }
        //                            }

        //                            break;
        //                        }
        //                        else
        //                        {
        //                            //code didnt find the last withdrawl. Try again?
        //                            if (loopCount < 10)
        //                            {
        //                                VertedaDiagnostics.LogMessage("ActivityOnSingleBag. Last change amount was 0 or could not be found. trying again", "I", false);
        //                                Thread.Sleep(60000); //1 minute
        //                                loopCount += 1;
        //                                VertedaDiagnostics.LogMessage("ActivityOnSingleBag: loopcount +1 = " + loopCount, "I", false);
        //                            }
        //                            else
        //                            {
        //                                VAR.status = false;
        //                                VAR.errorText = "Could not find an updated amount in ActivityOnSingleBag after 10 attempts for terminal "+ terminal_id + " mode " + mode;
        //                                VertedaDiagnostics.LogMessage("Could not find an updated amount in ActivityOnSingleBag after 10 attempts  for terminal "+ terminal_id + " mode " + mode, "E", false);
        //                                break;
        //                            }

        //                        }
        //                    }
        //                    else
        //                    {
        //                        //could not find withdrawl data
        //                        VAR.status = false;
        //                        VAR.errorText = "could not find CalculateShiftChange data";
        //                        VertedaDiagnostics.LogMessage("could not find CalculateShiftChange data", "E", false);
        //                        break; //get out of loop
        //                    }
        //                }
        //                VertedaDiagnostics.LogMessage("ActivityOnSingleBag: while loop ended", "I", false);    

        //        }
        //        else
        //        {
        //            VAR.status = false;
        //            VAR.errorText = "This employee is not valid to insert bags" + employee_id;
        //            VertedaDiagnostics.LogMessage("This employee is not valid to insert bags" + employee_id, "E", false);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        VertedaDiagnostics.LogMessage("ActivityOnSingleBag error: " + e.ToString(), "E", false);
        //    }

        //    return VAR;
        //}

        [WebMethod]
        public VarianceActivityResponsePrintInfo ActivityOnSingleBag(int mode, int terminal_id, int employee_id, int location_id) //mode 1 = loan, 2 = withdraw, 3 = cash out, 4= top up, 5=cash drop withdrawl
        {
            VarianceActivityResponsePrintInfo VAR = new VarianceActivityResponsePrintInfo();
            VertedaDiagnostics.LogMessage("ActivityOnSingleBag started " + mode + " " + terminal_id + " " + employee_id + " " + location_id, "I", false);

            try
            {
                //validate the employee
                VarianceActivityResponseEmployee VAR7 = ValidateEmployee(employee_id);

                if (VAR7.status)
                {
                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: employee valid", "I", false);
                    decimal lastChangeAmt = 0;
                    int loopCount = 0;

                    while (true)
                    {
                        VertedaDiagnostics.LogMessage("ActivityOnSingleBag: Loop started. Loopcount: " + loopCount, "I", false);

                        //calculate the value of the last withdraw or cash out on [Sum_Shift_BP_PC_MP_CE_backup]
                        VarianceActivityResponsShiftData var1 = CalculateShiftChange(terminal_id, employee_id, location_id); //termID, empID, locID

                        VertedaDiagnostics.LogMessage("ActivityOnSingleBag: Calculate shift change finished. Status: " + var1.status.ToString() + " Shift data count: " + var1.Shift_Data.Count.ToString(), "I", false);
                        if (var1.status != false) 
                        {
                            //if var1 fails, break. if it just returns nothing, try again
                            if (var1.Shift_Data.Count > 0)
                            {
                                //calculate the amount needed from the data
                                List<Sum_Shift_Data> ShiftAmounts = var1.Shift_Data;

                                if (mode == 1 || mode == 4) //loan
                                {
                                    if (ShiftAmounts.Count() == 1)
                                    {
                                        lastChangeAmt = ShiftAmounts[0].loan_amt;
                                    }
                                    else
                                    {
                                        //if  the amount has been reset for whatever reason (employee switch or terminal reset), use the lower value
                                        if (ShiftAmounts[0].loan_amt < ShiftAmounts[1].loan_amt)
                                        {
                                            lastChangeAmt = ShiftAmounts[0].loan_amt;
                                        }
                                        else
                                        {
                                            decimal currentAmt = ShiftAmounts[0].loan_amt;
                                            decimal prevAmt = ShiftAmounts[1].loan_amt;

                                            lastChangeAmt = currentAmt - prevAmt;
                                        }
                                    }
                                }
                                else if (mode == 2 || mode == 5) //withdrawl
                                {
                                    if (ShiftAmounts.Count() == 1)
                                    {
                                        lastChangeAmt = ShiftAmounts[0].withdrawal_amt;
                                    }
                                    else
                                    {
                                        //if 0 is < 1, somehow its been reset, set amount = 0.

                                        //else
                                        if (ShiftAmounts[0].withdrawal_amt < ShiftAmounts[1].withdrawal_amt)
                                        {
                                            lastChangeAmt = ShiftAmounts[0].withdrawal_amt;
                                        }
                                        else
                                        {
                                            decimal currentAmt = ShiftAmounts[0].withdrawal_amt;
                                            decimal prevAmt = ShiftAmounts[1].withdrawal_amt;

                                            lastChangeAmt = currentAmt - prevAmt;
                                        }
                                    }

                                }
                                else if (mode == 3) //cash out
                                {
                                    //convert values to dim IDs. Then get cash amount from GetCashDropAmount()

                                    int businessDimID = GetCurrentBusinessPeriodID();
                                    int terminalDimID = GetTermDimID(terminal_id);
                                    int employeeDimID = GetEmpDimID(employee_id);
                                    int locationDimID = GetLocDimID(location_id);

                                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 3, getting dim values. business period, terminal, employee, location: " + businessDimID + " " + terminalDimID + " " + employeeDimID + " " + locationDimID, "E", false);

                                    decimal cashOutAmount = GetCashDropAmount(terminalDimID, employeeDimID, locationDimID, businessDimID).CashDrops[0].Amount;

                                    //terminal  = VariancePrimo_GetTerminalFromID_today(int terminal_id)
                                    //profit_centre = VariancePrimo_GetProfitCentreFromID_today(int pc_id)
                                    //employee = VariancePrimo_GetEmployeeFromID_today(int emp_id)

                                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: Cash out amount = " + cashOutAmount, "I", false);

                                    if (cashOutAmount != 0)
                                        lastChangeAmt = cashOutAmount;
                                    else
                                    {
                                        //error
                                        VAR.status = false;
                                        VAR.errorText = "Could not get a value for the last cash out during ActivityOnSingleBag";
                                        VertedaDiagnostics.LogMessage("Could not get a value for the last cash out during ActivityOnSingleBag", "E", false);
                                    }

                                }
                                else
                                {
                                    //Mode not found, please check and try again.                                    
                                    VAR.status = false;
                                    VAR.errorText = "Incorrect mode when using ActivityOnSingleBag";
                                    VertedaDiagnostics.LogMessage("Incorrect mode when using ActivityOnSingleBag", "E", false);
                                    break;
                                }




                                if (lastChangeAmt > 0)
                                {
                                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag. Last change amount: " + lastChangeAmt, "I", false);

                                    if (mode == 2 || mode == 3 || mode == 5)
                                    {
                                        //great!
                                        //create a new bag using above amount   && create the initial action 

                                        Bag newBag = new Bag();
                                        newBag.terminal_id = terminal_id;
                                        newBag.employee_id = employee_id;
                                        newBag.current_value = lastChangeAmt;
                                        newBag.bag_type = constants.BT2;
                                        newBag.action_id = 6;
                                        newBag.label_id = "";
                                        newBag.origin_location_id = location_id;
                                        newBag.composition_id = 0;
                                        if (mode == 2)//due to changes, this now includes the type of return. 10 = skim, 11= reset
                                        {
                                            newBag.bag_function_id = 10;
                                        }
                                        else if (mode == 5)
                                        {
                                            newBag.bag_function_id = 11;
                                        }
                                        else
                                        {
                                            newBag.bag_function_id = 11;
                                        }

                                        newBag.destination_location_id = ReadConfig().CashOfficeID;

                                        List<BagJoins> noJoinsForTerminalBag = new List<BagJoins>();

                                        //set up action here for initial action
                                        BagAction ba = new BagAction();
                                        ba.Action_ID = 6;
                                        ba.Sending_Employee_ID = VAR7.Employee.Employee_ID;
                                        ba.sender_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;
                                        ba.Receiving_Employee_ID = VAR7.Employee.Employee_ID;
                                        ba.reciever_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;
                                        ba.Terminal_ID = terminal_id;
                                        ba.Location_ID = location_id;
                                        //ba.Bag_Unid = not known yet
                                        ba.Total_Value = lastChangeAmt;
                                        ba.Bag_Count = 0;
                                        ba.status_id = 0;

                                        newBag.bag_actions.Add(ba);

                                        //VarianceActivityResponseBags var2 = SetupNewSingleBag(newBag, noJoinsForTerminalBag);             //requires terminal ID, employee ID, amount, type, actionID, labelID, locationID
                                        VarianceActivityResponseBags var2 = SetupNewAdditionalBag(newBag);


                                        VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 2/3/5. Setup new bag status: " + var2.status.ToString(), "I", false);

                                        //collate print information for the above bag  and return it
                                        if (var2.status != false)
                                        {
                                            //bag has been created.
                                            //print information no longer needed.
                                            //VarianceActivityResponsePrintInfo var3 = GetPrintInformation(var2.Bags[0].bag_unid);
                                        }
                                        else
                                        {
                                            //error
                                            Exception bs_ex = null;
                                            Primo_SendBugsnag("Could not setup the bag during ActivityOnSingleBag", bs_ex, "Workflow");
                                            VAR.status = false;
                                            VAR.errorText = "Could not setup the bag during ActivityOnSingleBag.";
                                            VertedaDiagnostics.LogMessage("Could not setup the bag during ActivityOnSingleBag.", "E", false);

                                        }
                                    }
                                    else if (mode == 1 || mode == 4)
                                    {
                                        //do clever stuff
                                        //Gets the kiosk unid
                                        VarianceActivityResponseBags VAR4 = GetKioskBagUnidFromLocation(location_id, mode);
                                        VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. kiosk bag unid from location: " + VAR4.status.ToString(), "I", false);

                                        if (VAR4.status && VAR4.Bags.Count() > 0)
                                        {
                                            VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. kiosk bag unid from location = " + VAR4.Bags[0].bag_unid.ToString(), "I", false);
                                            VarianceActivityResponseBagUNID VAR5 = new VarianceActivityResponseBagUNID();
                                            VAR5.status = false;

                                            for (int x = 0; x < VAR4.Bags.Count(); x++)
                                            {
                                                //get the first available terminal unid
                                                VAR5 = GetTerminalBagUNIDfromKioskUNID(VAR4.Bags[0].bag_unid); //xxx
                                                if (VAR5.status)
                                                {
                                                    //if a kiosk bag with a usable terminal bag is found, exit loop
                                                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. found a terminal bag to assign float to: "+VAR5.BagUNID, "I", false);

                                                    break;
                                                }
                                            }

                                            VarianceActivityResponse VAR10 = new VarianceActivityResponse();
                                            if (VAR5.status != true)
                                            {
                                                ////try making a new bag instead
                                                //VarianceActivityResponseBags VAR15 = GetKioskBagUnidFromLocation(location_id, 1);
                                                //if (VAR15.status)
                                                //{
                                                    VAR10 = CreateTerminalBagForAdditionalFloat(VAR4.Bags[0], employee_id);
                                                //}
                                            }

                                            
                                            if (VAR5.status || VAR10.status) //if there is an existing bag, or one was made
                                            {
                                                VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. terminal bag from kiosk bag in location found. Bag unid = " + VAR5.BagUNID.ToString(), "I", false);
                                                //advance that bag's action and close previous
                                                BagAction ba = new BagAction();

                                               
                                                ba.Action_ID = 5;
                                                ba.Sending_Employee_ID = employee_id;
                                                ba.Receiving_Employee_ID = employee_id;
                                                ba.Terminal_ID = terminal_id;
                                                ba.Location_ID = location_id;
                                                ba.Bag_Unid = VAR5.BagUNID;
                                                ba.Total_Value = lastChangeAmt;
                                                ba.Bag_Count = 0;
                                                ba.sender_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;
                                                ba.reciever_name = VAR7.Employee.Employee_firstname + " " + VAR7.Employee.Employee_surname;

                                                VarianceActivityResponse VAR6 = SetBagAction(ba);
                                                VertedaDiagnostics.LogMessage("ActivityOnSingleBag: mode 1/4. set bag action on bag unid " + VAR5.BagUNID + " " + VAR6.status.ToString(), "I", false);
                                            }
                                            else
                                            {
                                                //error when finding an available terminal bag in location. None were available to be advanced
                                                Exception bs_ex = null;
                                                Primo_SendBugsnag("error when finding an available terminal bag in location. None were available to be advanced, and a new bag could not be made. Could not allocate a bag to terminal " + terminal_id + " when performing action " + mode, bs_ex, "Workflow");
                                                VAR.status = false;
                                                VAR.errorText = "error when finding an available terminal bag in location. None were available to be advanced, and a new bag could not be made. Could not allocate a bag to terminal " + terminal_id + " when performing action " + mode;
                                                VertedaDiagnostics.LogMessage("error when finding an available terminal bag in location. None were available to be advanced, and a new bag could not be made. Could not allocate a bag to terminal " + terminal_id + " when performing action " + mode, "E", false);
                                            }


                                        }
                                        else
                                        {
                                            //error when getting Kiosk bag unid using the location ID in ActivityOnSingleBag
                                            Exception bs_ex = null;
                                            Primo_SendBugsnag("Error when getting Kiosk bag unid using the location ID in ActivityOnSingleBag, or no bags existed", bs_ex, "Workflow");
                                            VAR.status = false;
                                            VAR.errorText = "error when getting Kiosk bag unid using the location ID in ActivityOnSingleBag, or no bags existed";
                                            VertedaDiagnostics.LogMessage("error when getting Kiosk bag unid using the location ID in ActivityOnSingleBag, or no bags existed", "E", false);
                                        }
                                    }

                                    break;
                                }
                                else
                                {
                                    //there was a record, but the last changed amount was 0. try again
                                    if (loopCount < 10)
                                    {
                                        VertedaDiagnostics.LogMessage("ActivityOnSingleBag. Last change amount was 0 or could not be found. trying again", "I", false);
                                        Thread.Sleep(60000); //1 minute
                                        loopCount += 1;
                                        VertedaDiagnostics.LogMessage("ActivityOnSingleBag: loopcount +1 = " + loopCount, "I", false);
                                    }
                                    else
                                    {
                                        WriteToErrorLog(0, mode, terminal_id, location_id, employee_id, "Could not find an updated amount in ActivityOnSingleBag after 10 attempts");
                                        Exception bs_ex = null;
                                        Primo_SendBugsnag("Could not find an updated amount in ActivityOnSingleBag after 10 attempts for terminal " + terminal_id + " mode " + mode, bs_ex, "Workflow");
                                        VAR.status = false;
                                        VAR.errorText = "Could not find an updated amount in ActivityOnSingleBag after 10 attempts for terminal " + terminal_id + " mode " + mode;
                                        VertedaDiagnostics.LogMessage("Could not find an updated amount in ActivityOnSingleBag after 10 attempts  for terminal " + terminal_id + " mode " + mode, "E", false);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                //the query was true, but no records were found.  try again
                                if (loopCount < 10)
                                {
                                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag. Last change amount was 0 or could not be found. trying again", "I", false);
                                    Thread.Sleep(60000); //1 minute
                                    loopCount += 1;
                                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: loopcount +1 = " + loopCount, "I", false);
                                }
                                else
                                {
                                    WriteToErrorLog(0, mode, terminal_id, location_id, employee_id, "Could not find an updated amount in ActivityOnSingleBag after 10 attempts");
                                    Exception bs_ex = null;
                                    Primo_SendBugsnag("Could not find an updated amount in ActivityOnSingleBag after 10 attempts for terminal " + terminal_id + " mode " + mode, bs_ex, "Workflow");
                                    VAR.status = false;
                                    VAR.errorText = "Could not find an updated amount in ActivityOnSingleBag after 10 attempts for terminal " + terminal_id + " mode " + mode;
                                    VertedaDiagnostics.LogMessage("Could not find an updated amount in ActivityOnSingleBag after 10 attempts  for terminal " + terminal_id + " mode " + mode, "E", false);
                                    break;
                                }

                            }
                        }
                        else
                        {
                            //could not find withdrawl data
                            Exception bs_ex = null;
                            Primo_SendBugsnag("Could not find CalculateShiftChange data", bs_ex, "Workflow");
                            VAR.status = false;
                            VAR.errorText = "could not find CalculateShiftChange data";
                            VertedaDiagnostics.LogMessage("could not find CalculateShiftChange data", "E", false);
                            break; //get out of loop
                        }
                    }
                    VertedaDiagnostics.LogMessage("ActivityOnSingleBag: while loop ended", "I", false);

                }
                else
                {
                    Exception bs_ex = null;
                    Primo_SendBugsnag("This employee is not valid to insert bags" + employee_id, bs_ex, "Permission");
                    VAR.status = false;
                    VAR.errorText = "This employee is not valid to insert bags" + employee_id;
                    VertedaDiagnostics.LogMessage("This employee is not valid to insert bags" + employee_id, "E", false);
                }
            }
            catch (Exception e)
            {
                Primo_SendBugsnag("ActivityOnSingleBag error:", e, "Workflow");
                VertedaDiagnostics.LogMessage("ActivityOnSingleBag error: " + e.ToString(), "E", false);
            }

            return VAR;
        }




        [WebMethod] //done
        public VarianceActivityResponsShiftData CalculateShiftChange(int terminalID, int employeeID, int locationID)
        {
            VertedaDiagnostics.LogMessage("CalculateShiftChange started. termID, empID, locID: " + terminalID + " " + employeeID + " " + locationID, "I", false);

            VarianceActivityResponsShiftData VAR = new VarianceActivityResponsShiftData();

            List<Sum_Shift_Data> withdrawalData = new List<Sum_Shift_Data>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;
                int terminalDimID = GetTermDimID(terminalID);
                int employeeDimID = GetEmpDimID(employeeID);
                int locationDimID = GetLocDimID(locationID);

                //get current business period id
                int businessDimID = GetCurrentBusinessPeriodID();

                VertedaDiagnostics.LogMessage("CalculateShiftChange: values after conversion to Dim IDs:", "I", false);
                VertedaDiagnostics.LogMessage("CalculateShiftChange: Terminal, employee, location: " + terminalDimID + " " + employeeDimID + " " + locationDimID, "I", false);

                if (terminalDimID == -1 || employeeDimID == -1 || locationDimID == -1 || terminalDimID == 0 || employeeDimID == 0 || locationDimID == 0 || businessDimID == -1)
                {
                    VAR.status = false;
                    VertedaDiagnostics.LogMessage("Error in CalculateShiftChange: Could not convert id to dim id", "E", false);
                }
                else
                {

                    if (conn.State != ConnectionState.Open)
                    {
                        try
                        {
                            conn.Open();
                        }
                        catch (SqlException e)
                        {
                            //error
                            Primo_SendBugsnag("CalculateShiftChange: SQL Connection error", e, "Connection");
                            VertedaDiagnostics.LogMessage("CalculateShiftChange: SQL Connection error", "E", false);
                        }
                    }


                    if (conn.State == ConnectionState.Open)
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT [cashier_emp_dim_id] ,ISNULL([loan_amount], 0) AS [loan_amount] ,ISNULL([withdrawal_amount], 0) AS [withdrawal_amount]      ,ISNULL([cash_drop_amount], 0) AS [cash_drop_amount]      ,ISNULL([terminal_dim_id], 0) AS [terminal_dim_id]      ,ISNULL([created_at], '1/1/1970') AS [created_at]  FROM [ig_business].[dbo].[Sum_Shift_BP_PC_MP_CE_backup]  where terminal_dim_id = " + terminalDimID + " AND profit_center_dim_id = " + locationDimID + " AND posted_business_period_dim_id = " + businessDimID + "  ORDER BY created_at DESC";


                        SqlDataReader rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            Sum_Shift_Data item = new Sum_Shift_Data();
                            item.employee_id = Convert.ToInt32(rdr["cashier_emp_dim_id"].ToString());
                            item.loan_amt = Convert.ToDecimal(rdr["loan_amount"].ToString());
                            item.withdrawal_amt = Convert.ToDecimal(rdr["withdrawal_amount"].ToString());
                            item.cash_drop_amt = Convert.ToDecimal(rdr["cash_drop_amount"].ToString());
                            item.terminal_id = Convert.ToInt32(rdr["terminal_dim_id"].ToString());
                            item.created_date = Convert.ToDateTime(rdr["created_at"].ToString()).ToString("G");

                            withdrawalData.Add(item);

                        }
                        rdr.Close();

                        conn.Close();


                        VAR.status = true;
                        VAR.Shift_Data = withdrawalData;
                        VertedaDiagnostics.LogMessage("CalculateShiftChange query completed", "I", false);
                    }
                }
            }
            catch (Exception e)
            {
                Primo_SendBugsnag("CalculateShiftChange: SQL Connection error", e, "Connection");
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in CalculateShiftChange: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        
        [WebMethod] //done - this method requires emp ID and is the old way of getting cash outs (when they are fired from terminals)
        public VarianceActivityResponseCashDrop GetCashDropAmount(int terminalID, int employeeID, int locationID, int businessPeriodID) //these need dim values
        {
            VertedaDiagnostics.LogMessage("GetCashDropAmount: terminal dim id, employee dim id, location dim id, business period dim id: " + terminalID + " " + employeeID + " " + locationID + " " + businessPeriodID, "I", false);

            VarianceActivityResponseCashDrop VAR = new VarianceActivityResponseCashDrop();
            CashDrop cashDropAmount = new CashDrop();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [cash_drop_amount] FROM [ig_business].[dbo].[Shift_Reset_Tender_Detail]   WHERE transaction_data_id = (SELECT top 1 [transaction_data_id] FROM [ig_business].[dbo].[Shift_Reset_Detail] WHERE profit_center_dim_id = " + locationID + "AND cashier_emp_dim_id = " + employeeID + "AND terminal_dim_id = " + terminalID + "AND actual_business_period_dim_id = " + businessPeriodID + " ORDER BY shift_id desc)";


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        decimal item = new decimal();
                        item = Convert.ToDecimal(rdr["cash_drop_amount"].ToString());

                        cashDropAmount.Amount = item;                                               
                    }
                    rdr.Close();

                    conn.Close();

                    VAR.CashDrops.Add(cashDropAmount);
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("GetCashDropAmount query completed. amount: " + cashDropAmount.Amount, "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = "Error in GetCashDropAmount: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetCashDropAmount: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done - this is the new method for cash drops. To be fired by a team leader. Also sums the amount of each tender where tender_dim_ID comes from the config settings
        public VarianceActivityResponseCashDrop GetCashDropAmountAndCollateTotal(int terminalID, int locationID, int businessPeriodID) //these need dim values
        {
            VertedaDiagnostics.LogMessage("GetCashDropAmountAndCollateTotal: terminal dim id, location dim id, business period dim id: " + terminalID + " " + locationID + " " + businessPeriodID, "I", false);

            VarianceActivityResponseCashDrop VAR = new VarianceActivityResponseCashDrop();
            CashDrop cashDropAmount = new CashDrop();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                //Get the value of the cash tender_dim_id. Almost always 1
                ConfigSettings cs1 = new ConfigSettings();
                int TenderDimID = 0;
                try
                {
                    cs1 = ReadConfig();
                    TenderDimID = cs1.CashTenderID;
                    VertedaDiagnostics.LogMessage("GetCashDropAmountAndCollateTotal. Cash tender from congfig: "+TenderDimID, "I", false);
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in GetCashDropAmountAndCollateTotal. Couldn't read config", "E", false);
                }

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    //cmd.CommandText = "SELECT [cash_drop_amount] FROM [ig_business].[dbo].[Shift_Reset_Tender_Detail]   WHERE transaction_data_id = (SELECT top 1 [transaction_data_id] FROM [ig_business].[dbo].[Shift_Reset_Detail] WHERE profit_center_dim_id = " + locationID + "AND cashier_emp_dim_id = " + employeeID + "AND terminal_dim_id = " + terminalID + "AND actual_business_period_dim_id = " + businessPeriodID + " ORDER BY shift_id desc)";
                    cmd.CommandText = "SELECT [cash_drop_amount], ISNULL([cashier_emp_dim_id], 0) AS [cashier_emp_dim_id] FROM [ig_business].[dbo].[Shift_Reset_Tender_Detail]  INNER JOIN [ig_business].[dbo].[Shift_Reset_Detail] ON [Shift_Reset_Tender_Detail].[transaction_data_id] = [Shift_Reset_Detail].[transaction_data_id] WHERE [Shift_Reset_Tender_Detail].transaction_data_id = (SELECT [Shift_Reset_Detail].[transaction_data_id] FROM [ig_business].[dbo].[Shift_Reset_Detail] WHERE profit_center_dim_id = locationID AND terminal_dim_id = terminalID  AND actual_business_period_dim_id = businessPeriodID) AND tender_dim_id = "+TenderDimID;


                    SqlDataReader rdr = cmd.ExecuteReader();

                    List<decimal> allCashDrops = new List<decimal>();
                    int employeeID = 0;

                    while (rdr.Read())
                    {
                        decimal item = new decimal();
                        item = Convert.ToDecimal(rdr["cash_drop_amount"].ToString());

                        allCashDrops.Add(item);
                        employeeID = Convert.ToInt32(rdr["cashier_emp_dim_id"].ToString()); //it will only take the last employee to do a cash drop. the last one is most likely the one who actually cashed up
                    }
                    rdr.Close();

                    conn.Close();

                    decimal sumTotalCashDrop = 0;

                    foreach (decimal x in allCashDrops)
                    {
                        sumTotalCashDrop += x;
                    }


                    cashDropAmount.TerminalID = terminalID;
                    cashDropAmount.LocationID = locationID;
                    cashDropAmount.Amount = sumTotalCashDrop;
                    cashDropAmount.EmployeeID = employeeID;

                    VAR.CashDrops.Add(cashDropAmount);

                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("GetCashDropAmount query completed. amount: " + cashDropAmount.Amount, "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = "Error in GetCashDropAmount: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetCashDropAmount: " + e.ToString(), "E", false);
            }
            return VAR;
        }



        //for use at end of day. Gets all outstanding terminal bags, puts them in kiosk bags and advances them to office. 
        // #### may need to include function to make cash drop bags if functionality is changed.
        [WebMethod]
        public VarianceActivityResponse CloseOutstandingInboundBags(int event_id, int employee_id)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("CloseOutstandingInboundBags started. event_id, employee_id: "+ event_id + " " + employee_id, "I", false);

            try
            {
                //get list of profit centers
                VarianceActivityResponseLocationsAndTerminal VAR1 = GetOpenLocationsForSpecififcEventID(event_id);


                if (VAR1.status == true)
                {
                    //get inbound bags for each location                
                    foreach (LocationAndTerminal LaT in VAR1.LocationsTerminals)
                    {
                        VarianceActivityResponseBags VAR2 = GetInboundBagsFromLocationForSpecificEvent(LaT.location_id, event_id);


                        if (VAR2.status == true && VAR2.Bags.Count() > 0)
                        {
                            //filter out just the ones that need placing in kiosk bags, i.e. action id 6

                            List<Bag> terminalBagsToAssign = new List<Bag>();
                            List<BagJoins> terminalBagsJoinsToAssign = new List<BagJoins>();

                            foreach (Bag bg in VAR2.Bags)
                            {
                                BagAction instPushAction = new BagAction();

                                instPushAction.Action_ID = 8;
                                instPushAction.Sending_Employee_ID = employee_id;
                                instPushAction.Receiving_Employee_ID = employee_id;
                                instPushAction.Terminal_ID = bg.terminal_id;
                                instPushAction.Location_ID = bg.origin_location_id;
                                instPushAction.Bag_ID = bg.bag_id;
                                instPushAction.Bag_Unid = bg.bag_unid;

                                if (bg.action_id == 6)
                                {
                                    terminalBagsToAssign.Add(bg);

                                    BagJoins bgJoin = new BagJoins();
                                    bgJoin.Event_ID = event_id;
                                    bgJoin.Child_ID = bg.bag_unid;
                                    terminalBagsJoinsToAssign.Add(bgJoin);
                                }
                                else if (bg.action_id == 7 && bg.bag_type == constants.BT1)
                                {
                                    SetBagAction(instPushAction);

                                    instPushAction.Action_ID = 9;
                                    SetBagAction(instPushAction);
                                }
                                else if (bg.action_id == 8 && bg.bag_type == constants.BT1)
                                {
                                    instPushAction.Action_ID = 9;
                                    SetBagAction(instPushAction);
                                }
                            }

                            if (terminalBagsToAssign.Count > 0)
                            {
                                //create kiosk bags for those locations
                                //requires terminal ID, employee ID, amount, type, actionID, labelID, locationID, function_id
                                Bag item = new Bag();

                                item.terminal_id = VAR2.Bags[0].terminal_id; // just assume the kiosk bag was made at the same location as the first terminal bag
                                item.employee_id = employee_id;
                                item.current_value = 0; //kiosk bags have no value
                                item.total_child_bags = terminalBagsJoinsToAssign.Count();
                                item.bag_type = constants.BT1; //shift reset. Although it will collect skim bags too
                                item.action_id = 7;
                                item.label_id = "";
                                item.origin_location_id = LaT.location_id;
                                item.destination_location_id = constants.CashOfficeID;
                                item.bag_function_id = 8; //shift reset bag, although it will probably contain skims too.
                                item.event_id = event_id;

                                VarianceActivityResponseBags VAR3 = SetupNewSingleBagForSpecififcEventID(item, terminalBagsJoinsToAssign);


                                //bags and associated children should now by at 7.
                                if (VAR3.status == true)
                                {
                                    //push the created kiosk bag to action 9
                                    BagAction instPushAction = new BagAction();

                                    instPushAction.Action_ID = 8;
                                    instPushAction.Sending_Employee_ID = employee_id;
                                    instPushAction.Receiving_Employee_ID = employee_id;
                                    instPushAction.Terminal_ID = VAR3.Bags[0].terminal_id;
                                    instPushAction.Location_ID = VAR3.Bags[0].origin_location_id;
                                    instPushAction.Bag_ID = VAR3.Bags[0].bag_id;
                                    instPushAction.Bag_Unid = VAR3.Bags[0].bag_unid;

                                    SetBagAction(instPushAction);

                                    instPushAction.Action_ID = 9;
                                    SetBagAction(instPushAction);
                                }


                                VertedaDiagnostics.LogMessage("CloseOutstandingInboundBags: Open bags found for location" + LaT.location_id +" Kiosk bag created", "I", false); 
                            }
                            else
                            {                                
                                VertedaDiagnostics.LogMessage("CloseOutstandingInboundBags: No open bags found for location" + LaT.location_id, "I", false); 
                            }
                        }
                        else
                        {
                            //couldnt find open terminals for this location                            
                            VertedaDiagnostics.LogMessage("Error in CloseOutstandingInboundBags. Could not find open terminal bags for location "+LaT.location_id, "E", false);
                        }
                    }
                }
                else
                {
                    //error could not find locations
                    VAR.status = false;
                    VAR.errorText = "Error in CloseOutstandingInboundBags. Could not find open locations";
                    VertedaDiagnostics.LogMessage("Error in CloseOutstandingInboundBags. Could not find open locations ", "E", false);
                }

            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in CloseOutstandingInboundBags: "+e.ToString(), "E", false);
            }


            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse AdvanceBagsInLocation(int event_id, int employee_id, int location_id, int mode) //mode 1 = skim, mode 2 = cash drop
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();
            VertedaDiagnostics.LogMessage("AdvanceBagsInLocation started. event_id, employee_id: " + event_id + " " + employee_id, "I", false);

            try
            {
                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                BagOperators op = new BagOperators();
                op.event_id = event_id;
                op.from_location_id = location_id;
                op.up_to_action = 6;
                VarianceActivityResponseBags VAR2 = GetBagDetails(op); //GetInboundBagsFromLocationForSpecificEvent(location_id, event_id);
                //function_id 10 = skim, 11 = reset


                if (VAR2.status == true && VAR2.Bags.Count() > 0)
                {
                    //filter out just the ones that need placing in kiosk bags, i.e. action id 6

                    List<Bag> terminalBagsToAssign = new List<Bag>();
                    List<BagJoins> terminalBagsJoinsToAssign = new List<BagJoins>();

                    foreach (Bag bg in VAR2.Bags)
                    {
                        if ((bg.bag_function_id == 10 && mode == 1) || (bg.bag_function_id == 11 && mode == 2))
                        {
                            BagAction instPushAction = new BagAction();

                            terminalBagsToAssign.Add(bg);

                            BagJoins bgJoin = new BagJoins();
                            bgJoin.Event_ID = event_id;
                            bgJoin.Child_ID = bg.bag_unid;
                            terminalBagsJoinsToAssign.Add(bgJoin);

                        }                    
                    }

                    if (terminalBagsToAssign.Count > 0)
                    {
                        //create kiosk bags for those locations
                        //requires terminal ID, employee ID, amount, type, actionID, labelID, locationID, function_id
                        Bag item = new Bag();

                        item.terminal_id = VAR2.Bags[0].terminal_id; // just assume the kiosk bag was made at the same location as the first terminal bag
                        item.employee_id = employee_id;
                        item.current_value = 0; //kiosk bags have no value
                        item.total_child_bags = terminalBagsJoinsToAssign.Count();
                        item.bag_type = constants.BT1; //shift reset. Although it will collect skim bags too
                        item.action_id = 7;
                        item.label_id = "";
                        item.origin_location_id = location_id;
                        item.destination_location_id = constants.CashOfficeID;
                        if (mode == 1)
                        {
                            item.bag_function_id = 5; //skims
                        }
                        else
                        {
                            item.bag_function_id = 8; //shift reset
                        }
                        item.event_id = event_id;

                        VarianceActivityResponseBags VAR3 = SetupNewSingleBagForSpecififcEventID(item, terminalBagsJoinsToAssign);


                        //bags and associated children should now by at 7.
                        if (VAR3.status == true)
                        {
                            //push the created kiosk bag to action 9
                            BagAction instPushAction = new BagAction();

                            instPushAction.Action_ID = 8;
                            instPushAction.Sending_Employee_ID = employee_id;
                            instPushAction.Receiving_Employee_ID = employee_id;
                            instPushAction.Terminal_ID = VAR3.Bags[0].terminal_id;
                            instPushAction.Location_ID = VAR3.Bags[0].origin_location_id;
                            instPushAction.Bag_ID = VAR3.Bags[0].bag_id;
                            instPushAction.Bag_Unid = VAR3.Bags[0].bag_unid;

                            SetBagAction(instPushAction);

                            //instPushAction.Action_ID = 9;
                            //SetBagAction(instPushAction);
                        }


                        VertedaDiagnostics.LogMessage("AdvanceBagsInLocation: Open bags found for location" + location_id + " Kiosk bag created", "I", false);
                    }
                    else
                    {
                        VertedaDiagnostics.LogMessage("AdvanceBagsInLocation: No open bags found for location" + location_id, "I", false);
                    }
                }
                else
                {
                    //couldnt find open terminals for this location                            
                    VertedaDiagnostics.LogMessage("Error in AdvanceBagsInLocation. Could not find open terminal bags for location " + location_id, "E", false);
                }
            }

            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in AdvanceBagsInLocation: " + e.ToString(), "E", false);
            }


            return VAR;
        }







        //Method to identify terminals within a location where a cash drop is available
        [WebMethod] 
        public VarianceActivityResponseCashDrop GetTerminalsWithAvailableCashDrops(int locationID, int businessPeriodID, int eventID) 
        {
            VertedaDiagnostics.LogMessage("GetTerminalsWithAvailableCashDrops: location dim id, business period dim id: " + locationID + " " + businessPeriodID, "I", false);

            VarianceActivityResponseCashDrop VAR = new VarianceActivityResponseCashDrop();

            //get terminals in input location.
            try
            {
                VarianceActivityResponseLocationsAndTerminal VAR1 = GetOpenTerminalsForSpecififcEventID(locationID, eventID);

                if (VAR1.status != false)
                {
                    //for each terminal, call GetCashDropAmountAndCollateTotal
                    foreach (LocationAndTerminal LaT in VAR1.LocationsTerminals)
                    {
                        CashDrop item = new CashDrop();
                        VarianceActivityResponseCashDrop VAR2 = GetCashDropAmountAndCollateTotal(LaT.terminal_id, locationID, businessPeriodID);
                        if (VAR2.status != false && VAR2.CashDrops.Count > 1)
                        {
                            //add the cash drop to the final return
                            VAR.CashDrops.Add(VAR2.CashDrops[0]);
                        }
                        else
                        { 
                            //error - couldnt get a response cash drop for that terminal
                            VAR.errorText = "Error in GetTerminalsWithAvailableCashDrops: couldnt get a response cash drop for that terminal: "+ LaT.terminal_id;
                            VertedaDiagnostics.LogMessage("Error in GetTerminalsWithAvailableCashDrops: couldnt get a response cash drop for that terminal: "+ LaT.terminal_id, "E", false);
                        }

                    }

                    //return list of terminals + available amount + employee
                    VAR.status = true;
                }
                else
                {
                    //error - could not find terminals for that location
                    VAR.errorText = "Error in GetTerminalsWithAvailableCashDrops: could not find terminals for that location";
                    VertedaDiagnostics.LogMessage("Error in GetTerminalsWithAvailableCashDrops: could not find terminals for that location", "E", false);
                    VAR.status = false;
                }
            }
            catch (Exception e)
            {
                //error
                VAR.errorText = "Error in GetTerminalsWithAvailableCashDrops: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetTerminalsWithAvailableCashDrops: " + e.ToString(), "E", false);
                VAR.status = false;
            }
            return VAR;
        }


        [WebMethod] //done, but possibly needs to return the ids of bags made?
        public VarianceActivityResponse CreateCashDropBagsForSelectedTerminals(List<CashDrop> potentialBags)
        {
            VertedaDiagnostics.LogMessage("CreateCashDropBagsForSelectedTerminals started", "I", false);

            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                //make a bag for each input terminal 
                foreach (CashDrop potential_bag in potentialBags)
                {
                    Bag newBag = new Bag();
                    newBag.terminal_id = potential_bag.TerminalID;
                    newBag.employee_id = potential_bag.EmployeeID;
                    newBag.current_value = potential_bag.Amount;
                    newBag.bag_type = constants.BT2;
                    newBag.action_id = 6;
                    newBag.label_id = "";
                    newBag.origin_location_id = potential_bag.LocationID;
                    newBag.bag_function_id = 11; //this method should only be used for cash drops.
                    newBag.destination_location_id = ReadConfig().CashOfficeID;

                    List<BagJoins> noJoinsForTerminalBag = new List<BagJoins>();

                    VarianceActivityResponseBags var2 = SetupNewSingleBag(newBag, noJoinsForTerminalBag);             //requires terminal ID, employee ID, amount, type, actionID, labelID, locationID

                    if (var2.status != false)
                    {
                        VertedaDiagnostics.LogMessage("CreateCashDropBagsForSelectedTerminals: bag made for terminal id, amount: " + potential_bag.TerminalID + " " + potential_bag.Amount, "I", false);
                    }
                    else
                    {
                        VertedaDiagnostics.LogMessage("Error in CreateCashDropBagsForSelectedTerminals: could not make bag for terminal id, amount: " + potential_bag.TerminalID + " " + potential_bag.Amount, "E", false);
                    }
                }

                VAR.status = true;
 
            }
            catch (Exception e)
            {
                //error
                VAR.errorText = "Error in CreateCashDropBagsForSelectedTerminals: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in CreateCashDropBagsForSelectedTerminals: " + e.ToString(), "E", false);
                VAR.status = false;
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseActions GetReturnChildBagsReadyToBePlacedInKioskBags(int location_id, int event_id)
        {
            VertedaDiagnostics.LogMessage("GetReturnChildBagsReadyToBePlacedInKioskBags started. location id + event id: "+location_id + " " + event_id, "I", false);
            VarianceActivityResponseActions VAR = new VarianceActivityResponseActions();

            List<BagAction> currentBagActions = new List<BagAction>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                int eventID = GetEventID(); //GetEventIDForCurrentBusinessDay();

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open && eventID != null)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [Bag_Action_Join].[unid] AS bag_action_unid, [Bag_Dim].[unid],  [Bag_Dim].[label_id], [Bag_Action_Join].[action_id]	  ,ISNULL([Action_Dim].[action], 'NULL') AS [action]      ,[start_date]      ,ISNULL([end_date], '1900-01-01') AS [end_date]      ,[sending_employee]      ,[recieving_employee]       ,ISNULL([terminal_id], -1) AS [terminal_id]      ,ISNULL([location_id], -1) AS [location_id]      ,[bag_dim].[bag_id]        ,[total_value]      ,ISNULL([total_child_bags], -1) AS [total_child_bags] ,ISNULL([sender_name], 'NULL') AS [sender_name]     ,ISNULL([reciever_name], 'NULL') AS [reciever_name] , [Bag_Dim].[bag_function_id], [Function_Dim].[bag_function]  FROM [variance_cash].[dbo].[Bag_Action_Join]     LEFT JOIN [variance_cash].[dbo].[Action_Dim]       ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id]        LEFT JOIN [variance_cash].[dbo].[Bag_Dim]	   ON [Bag_Action_Join].[bag_unid] = [Bag_Dim].[unid]		LEFT JOIN [variance_cash].[dbo].[Function_Dim] ON [Bag_Dim].[bag_function_id] = [Function_Dim].[bag_function_id]   WHERE [Bag_Action_Join].[location_id] = " + location_id + " AND [Bag_Dim].[event_id] = " + eventID + " AND action_id = 6 AND end_date IS NULL";


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        BagAction item = new BagAction();
                        item.Action_ID = Convert.ToInt32(rdr["action_id"].ToString());
                        item.Action_Name = rdr["action"].ToString();
                        item.Start_time = Convert.ToDateTime(rdr["start_date"].ToString()).ToString("G");
                        item.End_time = Convert.ToDateTime(rdr["end_date"].ToString()).ToString("G");
                        item.Sending_Employee_ID = Convert.ToInt32(rdr["sending_employee"].ToString());
                        item.Receiving_Employee_ID = Convert.ToInt32(rdr["recieving_employee"].ToString());
                        item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                        item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                        item.Bag_ID = rdr["bag_id"].ToString();
                        item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
                        item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());
                        item.sender_name = rdr["sender_name"].ToString();
                        item.reciever_name = rdr["reciever_name"].ToString();
                        item.label_id = rdr["label_id"].ToString();
                        item.Bag_Action_Unid = Convert.ToInt32(rdr["bag_action_unid"].ToString());
                        item.Bag_Unid = Convert.ToInt32(rdr["unid"].ToString());

                        item.function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                        item.function_name = rdr["bag_function"].ToString();

                        currentBagActions.Add(item);
                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VAR.BagActions = currentBagActions;
                    VertedaDiagnostics.LogMessage("GetReturnChildBagsReadyToBePlacedInKioskBags query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetReturnChildBagsReadyToBePlacedInKioskBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }



        [WebMethod]
        public VarianceActivityResponse CreateReturnKioskBagForSelectedChildren(List<BagAction> childBags, int returnType, int employeeID, int terminaID, int locationID) //return type is skim or cash out
        {
            VertedaDiagnostics.LogMessage("CreateReturnKioskBagForSelectedChildren started", "I", false);

            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                int eventID = GetEventID();

                if (eventID != null && eventID != 0 && childBags.Count > 0)
                {
                    //create the kiosk bag and assign the children
                    Bag item = new Bag();

                    item.terminal_id = terminaID; // just assume the kiosk bag was made at the same location as the first terminal bag
                    item.employee_id = employeeID;
                    item.current_value = 0; //kiosk bags have no value
                    item.total_child_bags = childBags.Count();
                    item.bag_type = constants.BT1; //shift reset. Although it will collect skim bags too
                    item.action_id = 7;
                    item.label_id = "";
                    item.origin_location_id = locationID;
                    item.destination_location_id = constants.CashOfficeID;
                    item.bag_function_id = returnType; //shift reset bag, although it will probably contain skims too.
                    item.event_id = eventID;

                    List<BagJoins> lbj = new List<BagJoins>();
                    //convert bag actions to bag joins
                    foreach (BagAction ba in childBags)
                    {
                        BagJoins bjitem = new BagJoins();
                        bjitem.Event_ID = eventID;
                        bjitem.Child_ID = ba.Bag_Unid;
                        //bjitem.Parent_ID = 0; //the parent hasn't been created yet, so leave null

                        lbj.Add(bjitem);

                    }

                    VarianceActivityResponseBags VAR3 = SetupNewSingleBagForSpecififcEventID(item, lbj);
                    if (VAR3.status != false)
                    {
                        VertedaDiagnostics.LogMessage("CreateReturnKioskBagForSelectedChildren completed sucesfully ", "E", false);
                        VAR.status = true;
                    }
                    else
                    {
                        VAR.errorText = "Error in CreateReturnKioskBagForSelectedChildren. Could not set up new kiosk bag";
                        VertedaDiagnostics.LogMessage("Error in CreateReturnKioskBagForSelectedChildren. Could not set up new kiosk bag", "E", false);
                        VAR.status = false;
                    }
                }
                else
                {
                    VAR.errorText = "Error in CreateReturnKioskBagForSelectedChildren. Could not get current event id or no child bags were selected";
                    VertedaDiagnostics.LogMessage("Error in CreateReturnKioskBagForSelectedChildren. Could not get current event id or no child bags were selected", "E", false);
                    VAR.status = false;
                }
            }
            catch (Exception e)
            {
                //error
                VAR.errorText = "Error in CreateReturnKioskBagForSelectedChildren: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in CreateReturnKioskBagForSelectedChildren: " + e.ToString(), "E", false);
                VAR.status = false;
            }
            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponseTerminalOverview GetLocationOverview(int location_id, int event_id, int business_period_id) //gets all cash drops ready to be made, all bags made from the terminal, andwhere those bags currently are
        { // maybe get business period from a webservice?
            VertedaDiagnostics.LogMessage("GetLocationOverview started. location id + event id: " + location_id + " " + event_id, "I", false);
            VarianceActivityResponseTerminalOverview VAR = new VarianceActivityResponseTerminalOverview();

            //new class TerminalOverview

            try
            {
                //work out all terminals in location
                VarianceActivityResponseLocationsAndTerminal VAR1 = GetOpenTerminalsForSpecififcEventID(location_id, event_id);

                if (VAR1.status != false && VAR1.LocationsTerminals.Count() > 0)
                {
                    //call GetTerminalsWithAvailableCashDrops for cashouts of each terminal
                    VarianceActivityResponseCashDrop VAR2 = GetTerminalsWithAvailableCashDrops(location_id, business_period_id, event_id);
                    
                    //call a new method to get all bags for action 6 from location
                    VarianceActivityResponseBags VAR3 = GetInboundBagsFromLocationForSpecificEvent(location_id, event_id);

                    //need to match up 
                    foreach (CashDrop CD in VAR2.CashDrops)
                    {
                        TerminalOverview item = new TerminalOverview();
                        item.TerminalID = CD.TerminalID;
                        if (CD.Amount != null && CD.Amount > 0)
                        {
                            item.CashDropAvailable = true;
                            item.CashDropAmount = CD.Amount;
                        }
                        else
                        {
                            item.CashDropAvailable = false;
                            item.CashDropAmount = 0;
                        }


                        foreach (Bag bg in VAR3.Bags)
                        {
                            if (bg.terminal_id == CD.TerminalID)
                            {
                                item.AvailableBags.Add(bg);
                            }                        
                        }

                        VAR.TerminalResults.Add(item);
                    }

                    VAR.status = true;
                }
                else
                {
                    //error - could not find open terminals in location x
                    VAR.status = false;
                    VAR.errorText = "Error in GetLocationOverview. Could not find open terminals in location " + location_id;
                    VertedaDiagnostics.LogMessage("Error in GetLocationOverview. Could not find open terminals in location " + location_id, "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetLocationOverview: " + e.ToString(), "E", false);
            }
            return VAR;
        }






        [WebMethod]
        public VarianceActivityResponseIncomingSummary IncomingTerminalBagSummary(int location_id) //gets all terminals from a location, and gives overview of recieved vs expected amounts
        {
            VertedaDiagnostics.LogMessage("IncomingTerminalBagSummary started. location id: " + location_id, "I", false);
            VarianceActivityResponseIncomingSummary VAR = new VarianceActivityResponseIncomingSummary();

            List<IncomingTerminalSummary> terminal_summary = new List<IncomingTerminalSummary>();

            try
            {
                //get all terminals from location
                VarianceActivityResponseActions VAR1 = GetFullBagActionHistoryInLocation(location_id);
                VarianceActivityResponseLocationsAndTerminal VAR2 = GetOpenTerminals(location_id);

                if (VAR1.status != false && VAR1.BagActions.Count() > 0)
                {
                    if (VAR2.status != false && VAR2.LocationsTerminals.Count() > 0)
                    {
                        //sort out the data
                        foreach (LocationAndTerminal LaT in VAR2.LocationsTerminals)
                        {
                            //Find any bags from that terminal and build summary
                            IncomingTerminalSummary item = new IncomingTerminalSummary();
                            item.Terminal_ID = LaT.terminal_id;
                            item.Terminal_Name = LaT.terminal_name;
                            item.Incoming_Bags = new List<IncomingTerminalBagSummary>();

                            List<BagAction> BagsFromThisTerminal = new List<BagAction>(); //this code finds each bag created in the current terminal
                            foreach (BagAction ba in VAR1.BagActions)
                            {
                                if (ba.Terminal_ID == LaT.terminal_id && ba.Action_ID == 6)
                                {
                                    //BagsFromThisTerminal.Add(ba);     
                                    IncomingTerminalBagSummary bagItem = new IncomingTerminalBagSummary();

                                    bagItem.Bag_Unid = ba.Bag_Unid;
                                    bagItem.Bag_Function_ID = ba.function_id;
                                    bagItem.Bag_System_Value = ba.Total_Value;
                                    bagItem.Bag_ID = ba.Bag_ID;

                                    //bag_action_id = current action
                                    //bag_action_unid
                                    bagItem.Bag_Action_ID = ba.Action_ID;
                                    bagItem.Bag_Action_Unid = ba.Bag_Action_Unid;
                                    bagItem.Bag_Counted_Value = 0;

                                    bagItem.Created_Time = ba.Start_time;

                                    foreach (BagAction ba2 in VAR1.BagActions)
                                    {
                                        if (ba2.Action_ID > bagItem.Bag_Action_ID && ba2.Bag_Unid == bagItem.Bag_Unid)
                                        {
                                            //update stuff
                                            bagItem.Bag_Action_ID = ba2.Action_ID;
                                            bagItem.Bag_Action_Unid = ba2.Bag_Action_Unid;

                                            if (ba2.Action_ID == 10)
                                            {
                                                bagItem.Bag_Counted_Value = ba2.Total_Value;
                                            }
                                        }                                        
                                    }

                                    item.Incoming_Bags.Add(bagItem);                    
                                }
                            }

                            terminal_summary.Add(item);
                        }

                        VAR.Terminal_Summary = terminal_summary;
                        VAR.status = true;
                    }
                    else
                    {
                        //error no terminals found
                        VAR.status = false;
                        VAR.errorText = "Error in IncomingTerminalBagSummary started. No terminals found";
                        VertedaDiagnostics.LogMessage("Error in IncomingTerminalBagSummary started. No terminals found", "E", false);
                    }
                }
                else
                {
                    //error no records found
                    VAR.status = false;
                    VAR.errorText = "Error in IncomingTerminalBagSummary started. No BagActions found";
                    VertedaDiagnostics.LogMessage("Error in IncomingTerminalBagSummary started. No BagActions found", "E", false);
                }
                
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in IncomingTerminalBagSummary: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        
        [WebMethod]
        public VarianceActivityResponseIncomingSummary IncomingTerminalBagSummaryForSpecificEvent(int location_id, int event_id) //gets all terminals from a location, and gives overview of recieved vs expected amounts
        {
            VertedaDiagnostics.LogMessage("IncomingTerminalBagSummaryForSpecificEvent started. location id: " + location_id, "I", false);
            VarianceActivityResponseIncomingSummary VAR = new VarianceActivityResponseIncomingSummary();

            List<IncomingTerminalSummary> terminal_summary = new List<IncomingTerminalSummary>();

            try
            {
                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                //get all terminals from location
                VarianceActivityResponseActions VAR1 = GetFullBagActionHistoryInLocationForSpecificEventID(location_id, event_id);
                VarianceActivityResponseLocationsAndTerminal VAR2 = GetOpenTerminalsForSpecififcEventID(location_id, event_id);

                if (VAR1.status != false && VAR1.BagActions.Count() > 0)
                {
                    if (VAR2.status != false && VAR2.LocationsTerminals.Count() > 0)
                    {
                        //sort out the data
                        foreach (LocationAndTerminal LaT in VAR2.LocationsTerminals)
                        {
                            //Find any bags from that terminal and build summary
                            IncomingTerminalSummary item = new IncomingTerminalSummary();
                            item.Terminal_ID = LaT.terminal_id;
                            item.Terminal_Name = LaT.terminal_name;
                            item.Incoming_Bags = new List<IncomingTerminalBagSummary>();

                            List<BagAction> BagsFromThisTerminal = new List<BagAction>(); //this code finds each bag created in the current terminal
                            foreach (BagAction ba in VAR1.BagActions)
                            {
                                if (ba.Terminal_ID == LaT.terminal_id && ba.Action_ID == 6)
                                {
                                    //BagsFromThisTerminal.Add(ba);     
                                    IncomingTerminalBagSummary bagItem = new IncomingTerminalBagSummary();

                                    bagItem.Bag_Unid = ba.Bag_Unid;
                                    bagItem.Bag_Function_ID = ba.function_id;
                                    bagItem.Bag_System_Value = ba.Total_Value;
                                    bagItem.Bag_ID = ba.Bag_ID;

                                    //bag_action_id = current action
                                    //bag_action_unid
                                    bagItem.Bag_Action_ID = ba.Action_ID;
                                    bagItem.Bag_Action_Unid = ba.Bag_Action_Unid;
                                    bagItem.Bag_Counted_Value = 0;

                                    foreach (BagAction ba2 in VAR1.BagActions)
                                    {
                                        if (ba2.Action_ID > bagItem.Bag_Action_ID && ba2.Bag_Unid == bagItem.Bag_Unid)
                                        {
                                            //update stuff
                                            bagItem.Bag_Action_ID = ba2.Action_ID;
                                            bagItem.Bag_Action_Unid = ba2.Bag_Action_Unid;

                                            if (ba2.Action_ID == 10)
                                            {
                                                bagItem.Bag_Counted_Value = ba2.Total_Value;
                                            }
                                        }
                                    }

                                    item.Incoming_Bags.Add(bagItem);
                                }
                            }

                            terminal_summary.Add(item);
                        }

                        VAR.Terminal_Summary = terminal_summary;
                        VAR.status = true;
                    }
                    else
                    {
                        //error no terminals found
                        VAR.status = false;
                        VAR.errorText = "Error in IncomingTerminalBagSummaryForSpecificEvent started. No terminals found";
                        VertedaDiagnostics.LogMessage("Error in IncomingTerminalBagSummaryForSpecificEvent started. No terminals found", "E", false);
                    }
                }
                else
                {
                    //error no records found
                    VAR.status = false;
                    VAR.errorText = "Error in IncomingTerminalBagSummaryForSpecificEvent started. No BagActions found. No incoming bags found in location: " + location_id;
                    VertedaDiagnostics.LogMessage("Error in IncomingTerminalBagSummaryForSpecificEvent started. No BagActions found. No incoming bags found in location: " + location_id, "E", false);
                }

            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in IncomingTerminalBagSummaryForSpecificEvent: " + e.ToString(), "E", false);
            }
            return VAR;
        }



        [WebMethod] 
        public VarianceActivityResponse CreateTerminalBagForAdditionalFloat(Bag BA, int employee_id)
        {
            VertedaDiagnostics.LogMessage("CreateTerminalBagForAdditionalFloat: ", "I", false);

            VarianceActivityResponse VAR = new VarianceActivityResponse(); 
  
            try
            {

                VarianceActivityResponseEmployee VAR8 = ValidateEmployee(employee_id);

                if (VAR8.status != false)
                {

                    //get the info of a kiosk bag that has gone to that location 
                    

                        //create the bag
                        Bag newBag = new Bag();
                        List<BagJoins> childBags = new List<BagJoins>();

                        VarianceActivityResponseCompositionDetails VAR7 = Get_Composition_Details(BA.composition_id);

                        if (VAR7.status != false)
                        {

                            //set values for new bag
                            newBag.action_id = 1;
                            newBag.event_id = BA.event_id;
                            newBag.bag_type = constants.BT2;
                            newBag.label_id = "0";
                            newBag.employee_id = employee_id;
                            newBag.origin_location_id = BA.origin_location_id;
                            newBag.destination_location_id = BA.destination_location_id;
                            newBag.composition_id = BA.composition_id;
                            newBag.bag_function_id = 12;
                            newBag.current_value = VAR7.CompositionValue;

                            VarianceActivityResponseBags VAR2 = SetupNewSingleBag(newBag, childBags);


                            if (VAR2.status != false)
                            {
                                //assign it to kiosk bag - insert kid to tid join
                                VarianceActivityResponse VAR3 = InsertKidToTidJoin(BA.bag_unid, VAR2.Bags[0].bag_unid);

                                if (VAR3.status != false)
                                {
                                    BagAction ba = new BagAction();

                                    ba.Receiving_Employee_ID = employee_id;
                                    ba.Sending_Employee_ID = employee_id;
                                    ba.Terminal_ID = 0;
                                    ba.Location_ID = VAR2.Bags[0].origin_location_id;
                                    ba.Bag_ID = VAR2.Bags[0].bag_id;
                                    ba.Bag_Unid = VAR2.Bags[0].bag_unid;
                                    ba.sender_name = VAR8.Employee.Employee_firstname + " " + VAR8.Employee.Employee_surname;
                                    ba.reciever_name = VAR8.Employee.Employee_firstname + " " + VAR8.Employee.Employee_surname;
                                    ba.Action_ID = 2;
                                    ba.Bag_Count = 0;
                                    ba.Total_Value = VAR7.CompositionValue;


                                    //advance it the same position as kiosk bag

                                    for (int i = 2; i <= BA.action_id; i++)
                                    {
                                        ba.Action_ID = i;

                                        if (i == 4)
                                        {
                                            ba.Location_ID = VAR2.Bags[0].destination_location_id;
                                        }
                                        VarianceActivityResponse VAR4 = SetBagAction(ba);
                                    }

                                }
                                else
                                {
                                    //error - could not assign new bag to parent
                                    VAR.status = false;
                                    VAR.errorText = "CreateTerminalBagForAdditionalFloat: could not assign new bag to parent";
                                    VertedaDiagnostics.LogMessage("CreateTerminalBagForAdditionalFloat: could not assign new bag to parent", "E", false);
                                }

                            }
                            else
                            {
                                //error - could not create new bag
                                VAR.status = false;
                                VAR.errorText = "CreateTerminalBagForAdditionalFloat: could not create new bag";
                                VertedaDiagnostics.LogMessage("CreateTerminalBagForAdditionalFloat: could not create new bag", "E", false);
                            }
                        }
                        else
                        {
                            //error - could not get composition ID
                            VAR.status = false;
                            VAR.errorText = "CreateTerminalBagForAdditionalFloat: could not get composition ID";
                            VertedaDiagnostics.LogMessage("CreateTerminalBagForAdditionalFloat: could not get composition ID", "E", false);
                        }
                }
                else
                {
                    //error - invalid employee
                    VAR.status = false;
                    VAR.errorText = "CreateTerminalBagForAdditionalFloat: invalid employee";
                    VertedaDiagnostics.LogMessage("CreateTerminalBagForAdditionalFloat: invalid employee", "E", false);
                }

            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = "Error in CreateTerminalBagForAdditionalFloat: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in CreateTerminalBagForAdditionalFloat: " + e.ToString(), "E", false);
            }
            return VAR;
        }







        //The below 3 methods are the new way of creating kiosk bags to go back to cash office. They grab any available bags, put them in a kiosk bag, and assign to action 7.

        [WebMethod]
        public VarianceActivityResponseBags ComposeKioskBagForLocation(int event_id, int location_id, int function_id, int employee_id, string location_name, int terminal_id) //10 = skim, 11 = drop
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            List<Bag> currentBags = new List<Bag>();

            try
            {
                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                if (function_id != 10 || function_id != 11)
                {
                    VarianceActivityResponseBags var1 = GetAllAvailableReturnBags(event_id, location_id, function_id);

                    if (var1.status)
                    {
                        if (var1.Bags.Count > 0)
                        {
                            List<BagAction> child_actions = new List<BagAction>();
                            foreach (Bag bg in var1.Bags)
                            {
                                //each child bag should only have one action (action id =6, end_date = null) so just add first action from each bag
                                child_actions.Add(bg.bag_actions[0]);
                            }


                            int kiosk_function = 0;
                            if (function_id == 10)
                            {
                                kiosk_function = 5;
                            }
                            else
                            {
                                kiosk_function = 8;
                            }
                            
                            VarianceActivityResponse var2 = CreateReturnKioskBagForSelectedChildrenForSpecificEvent(child_actions, kiosk_function, employee_id, terminal_id, location_id, event_id, location_name);

                            VAR.status = var2.status;

                        }
                        else
                        {
                            //error - could not find any bags for that location
                            VAR.status = false;
                            VAR.errorText = "Error in ComposeKioskBagForLocation: could not find any bags for that location: " +location_id;
                            VertedaDiagnostics.LogMessage("Error in ComposeKioskBagForLocation: could not find any bags for that location" + location_id, "E", false);
                        }
                    }
                    else
                    {
                        //error - could not find any bags for that location
                        VAR.status = false;
                        VAR.errorText = "Error in ComposeKioskBagForLocation: could not find any bags for that location: " +location_id;
                        VertedaDiagnostics.LogMessage("Error in ComposeKioskBagForLocation: could not find any bags for that location" +location_id, "E", false);
                    }
                }
                else
                {
                    //specified function_id is not a valid return bag
                    VAR.status = false;
                    VAR.errorText = "Error in ComposeKioskBagForLocation: function_id is not valid for return bags: " + function_id;
                    VertedaDiagnostics.LogMessage("Error in ComposeKioskBagForLocation: function_id is not valid for return bags: " + function_id, "E", false);
                }


            }
            catch (Exception ex)
            {
                Primo_SendBugsnag("Error in ComposeKioskBagForLocation", ex, "General logic");
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in ComposeKioskBagForLocation: " + ex.ToString(), "E", false);
            }

            return VAR;
        }

        
        [WebMethod] //show all additional bags made - get all bags with function ID 12
        public VarianceActivityResponseBags GetAllAvailableReturnBags(int event_id, int location_id, int function_id) //10 = skim, 11 = drop
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            List<Bag> currentBags = new List<Bag>();

            try
            {
                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                if (function_id != 10 || function_id != 11)
                {
                    BagOperators bop = new BagOperators();
                    bop.event_id = event_id;
                    bop.from_location_id = location_id;
                    bop.function_id = function_id;
                    bop.up_to_action = 6;
                    VarianceActivityResponseBags var1 = GetBagDetails(bop);

                    if (var1.status)
                    {
                        VAR.Bags = var1.Bags;
                        VAR.status = true;
                    }
                    else
                    {
                        VAR.status = false;
                        VAR.errorText = "Error in GetAllAvailableReturnBags: failed to find return bags for location: " + location_id;
                        VertedaDiagnostics.LogMessage("Error in GetAllAvailableReturnBags: failed to find return bags for location: " + location_id, "E", false);
                    }
                }
                else
                {
                    //specified function_id is not a valid return bag
                    VAR.status = false; 
                    VAR.errorText = "Error in GetAllAvailableReturnBags: function_id is not valid for return bags: " + function_id;
                    VertedaDiagnostics.LogMessage("Error in GetAllAvailableReturnBags: function_id is not valid for return bags: " + function_id, "E", false);
                }


            }
            catch (Exception ex)
            {
                Primo_SendBugsnag("Error in GetAllAvailableReturnBags", ex, "General logic");
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in GetAllAvailableReturnBags: " + ex.ToString(), "E", false);
            }

            return VAR;
        }
                

        [WebMethod]
        public VarianceActivityResponse CreateReturnKioskBagForSelectedChildrenForSpecificEvent(List<BagAction> childBags, int returnType, int employeeID, int terminaID, int locationID, int eventID, string location_name) //return type is skim or cash out
        {
            VertedaDiagnostics.LogMessage("CreateReturnKioskBagForSelectedChildrenForSpecificEvent started", "I", false);

            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                ConfigSettings cs = ReadConfig();

                //create the kiosk bag and assign the children
                Bag item = new Bag();

                item.terminal_id = terminaID; // just assume the kiosk bag was made at the same location as the first terminal bag
                item.employee_id = employeeID;
                item.current_value = 0; //kiosk bags have no value
                item.total_child_bags = childBags.Count();
                item.bag_type = constants.BT1; //shift reset. Although it will collect skim bags too
                item.action_id = 7;
                item.label_id = location_name;
                item.origin_location_id = locationID;
                item.destination_location_id = cs.CashOfficeID;
                item.bag_function_id = returnType; //shift reset bag, although it will probably contain skims too.
                item.event_id = eventID;

                List<BagJoins> lbj = new List<BagJoins>();
                //convert bag actions to bag joins
                foreach (BagAction ba in childBags)
                {
                    BagJoins bjitem = new BagJoins();
                    bjitem.Event_ID = eventID;
                    bjitem.Child_ID = ba.Bag_Unid;
                    //bjitem.Parent_ID = 0; //the parent hasn't been created yet, so leave null

                    lbj.Add(bjitem);

                }

                VarianceActivityResponseBags VAR3 = SetupNewSingleBagForSpecififcEventID(item, lbj);
                if (VAR3.status != false)
                {
                    //once we've inserted the new kiosk bag and moved it + children to action 7, move everything to 8 and then 9
                    //************************************************************************************************************

                    Bag new_kiosk_bag = VAR3.Bags[0];
                    BagAction instPushAction = new BagAction();

                    instPushAction.Action_ID = 8;
                    instPushAction.Sending_Employee_ID = new_kiosk_bag.bag_actions[0].Sending_Employee_ID;
                    instPushAction.Receiving_Employee_ID = new_kiosk_bag.bag_actions[0].Receiving_Employee_ID;
                    instPushAction.Terminal_ID = new_kiosk_bag.bag_actions[0].Terminal_ID;
                    instPushAction.Location_ID = new_kiosk_bag.bag_actions[0].Location_ID;
                    instPushAction.Bag_ID = new_kiosk_bag.bag_id;
                    instPushAction.Bag_Unid = new_kiosk_bag.bag_actions[0].Bag_Unid;

                    SetBagAction(instPushAction);

                    instPushAction.Action_ID = 9;
                    instPushAction.Terminal_ID = cs.CashOfficeTerminalID;
                    instPushAction.Location_ID = cs.CashOfficeID;

                    SetBagAction(instPushAction);

                    //************************************************************************************************************
                    VertedaDiagnostics.LogMessage("CreateReturnKioskBagForSelectedChildrenForSpecificEvent completed sucesfully ", "E", false);
                    VAR.status = true;
                }
                else
                {
                    VAR.errorText = "Error in CreateReturnKioskBagForSelectedChildrenForSpecificEvent. Could not set up new kiosk bag";
                    VertedaDiagnostics.LogMessage("Error in CreateReturnKioskBagForSelectedChildrenForSpecificEvent. Could not set up new kiosk bag", "E", false);
                    VAR.status = false;
                }
            }
            catch (Exception e)
            {
                //error
                VAR.errorText = "Error in CreateReturnKioskBagForSelectedChildrenForSpecificEvent: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in CreateReturnKioskBagForSelectedChildrenForSpecificEvent: " + e.ToString(), "E", false);
                VAR.status = false;
            }
            return VAR;
        }



        #endregion


        #region Reporting

        //method to show all locations where there is a variance between what is expected vs what was actually recieved


        [WebMethod]
        public VarianceActivityResponseBagComparisons GetVarianceOfIncomingBags(int event_id)
        {
            VarianceActivityResponseBagComparisons VAR = new VarianceActivityResponseBagComparisons();

            List<BagComparisonPair> bags_to_compare = new List<BagComparisonPair>();
            List<BagComparisonPair> bags_with_unequal_amounts = new List<BagComparisonPair>();
            try
            {
                if(event_id < 1)
                {
                    event_id = GetEventID();
                }


                //get all final stages of bags > 
                VarianceActivityResponseBags var1 = GetInboundBagsFromAllLocationForSpecificEvent(event_id);
                // get all starting stages of inbound bags > 
                VarianceActivityResponseBags var2 = GetInboundBagsStartingValuesFromAllLocationForSpecificEvent(event_id);

                if (var1.status != false && var2.status != false)
                {
                //for each final stage bag, pair it up with initial bag
                    foreach (Bag final_bg in var1.Bags)
                    {
                        foreach (Bag start_bg in var2.Bags)
                        {
                            if (final_bg.bag_unid == start_bg.bag_unid)
                            {
                                BagComparisonPair item = new BagComparisonPair();
                                item.bag_pair.Add(start_bg);
                                item.bag_pair.Add(final_bg);
                                bags_to_compare.Add(item);

                                //for each final bag, work out if any have a value difference
                                if(final_bg.current_value != start_bg.current_value)
                                {
                                    item.amount_difference = start_bg.current_value - final_bg.current_value;
                                    bags_with_unequal_amounts.Add(item);
                                }

                                //for each final bag, work out if any have a count difference
                                if (final_bg.total_child_bags != start_bg.total_child_bags)
                                {
                                    item.count_difference = start_bg.total_child_bags - final_bg.total_child_bags;
                                    bags_with_unequal_amounts.Add(item);
                                }
                            }
                        } 
                    }
                    VAR.status = true;
                    VAR.Bag_Comparisons = bags_with_unequal_amounts;
                }
                else
                {
                    //error - couldn't find values for incoming bags
                    VAR.status = false;
                    VAR.errorText = "Error in GetCashVarianceOfIncomingBags: couldn't find values for incoming bags";
                    VertedaDiagnostics.LogMessage("Error in GetCashVarianceOfIncomingBags: couldn't find values for incoming bags", "E", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetCashVarianceOfIncomingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        
        
        [WebMethod]
        public VarianceActivityResponseBags GetIncompleteIncomingBagLocations(int event_id)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> IncompleteBags = new List<Bag>();
                        
            try
            {
                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                //get the last status of each incoming bag
                VarianceActivityResponseBags VAR1 = GetInboundBagsFromAllLocationForSpecificEvent(event_id); 

                //filter the results and find any that are still at action 6-9 for terminal or 6-8 for kiosk
                if (VAR1.status != false)
                {
                    foreach (Bag bg in VAR1.Bags)
                    {
                        if (bg.action_id < 10 && bg.bag_type == constants.BT2)
                        {
                            //terminal bag isnt closed off
                            Bag item = new Bag();
                            //item.origin_location_id = bg.origin_location_id;
                            //item.origin_name = bg.origin_name;
                            //item.bag_unid = bg.bag_unid;
                            //item.action_id = bg.action_id;
                            //item.created_date = bg.created_date;
                            item = bg;
                            IncompleteBags.Add(item);

                        }
                        else if (bg.action_id < 9 && bg.bag_type == constants.BT1)
                        { 
                            //kiosk bag isnt closed off
                            Bag item = new Bag();
                            item = bg;
                            IncompleteBags.Add(item);
                        }
                    }

                    //return list of origin ID + origin name + bag ID + current status + last action time.
                    VAR.Bags = IncompleteBags;
                    VAR.status = true;

                }
                else
                {
                    //error
                    VAR.status = false;
                    VAR.errorText = "Error in GetIncompleteIncomingBagLocations: Could not get inbound bags";
                    VertedaDiagnostics.LogMessage("Error in GetIncompleteIncomingBagLocations: Could not get inbound bags", "E", false);
                }                
               
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetIncompleteIncomingBagLocations: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        

        [WebMethod]
        public VarianceActivityResponse WriteToErrorLog(int event_id, int activity_mode, int terminal_id, int location_id, int employee_id, string message)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                //edit existing record
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Variance_Error_Log_Insert";

                cmd.Parameters.Add("@activity_mode_id", SqlDbType.Int).Value = activity_mode;
                cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = event_id;
                cmd.Parameters.Add("@terminal_id", SqlDbType.Int).Value = terminal_id;
                cmd.Parameters.Add("@location_id", SqlDbType.Int).Value = location_id;
                cmd.Parameters.Add("@employee_id", SqlDbType.Int).Value = employee_id;

                cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = message;

                cmd.Connection = conn;
                //return package_id to use in copy

                try
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    VAR.status = true;
                    VertedaDiagnostics.LogMessage("WriteToErrorLog Completed successfully", "I", false);

                }
                catch (Exception ex)
                {
                    VAR.status = false;
                    VAR.errorText = ex.ToString();
                    VertedaDiagnostics.LogMessage("Error in WriteToErrorLog: " + ex.ToString(), "E", false);
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in WriteToErrorLog: " + e.ToString(), "E", false);
            }

            return VAR;
        }
        


        [WebMethod] //show all additional bags made - get all bags with function ID 12
        public VarianceActivityResponseBags GetAllAdditionalBagsMadeDuringEvent(int event_id)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            List<Bag> currentBags = new List<Bag>();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                cmd.Connection = conn;
            //return package_id to use in copy

                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                conn.Open();
                cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id      ,[Bag_Action_Join].[terminal_id], [Bag_Action_Join].[total_value], [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin   ON [Bag_Dim].[origin_location_id] = origin.[id]    LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination    ON [Bag_Dim].[destination_location_id] = destination.[id] 	LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] 	ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]	 LEFT JOIN [variance_cash].[dbo].[Action_Dim] 	 ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id = " + event_id + " AND [bag_dim].[bag_function_id] = 10 AND ([Bag_Action_Join].[end_date] is null OR [Bag_Action_Join].action_id IN (5,10))";
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Bag item = new Bag();
                    item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                    item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                    item.event_name = rdr["event_name"].ToString();
                    item.bag_type = rdr["bag_type"].ToString();
                    item.bag_id = rdr["bag_id"].ToString();
                    item.label_id = rdr["label_id"].ToString();
                    item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                    item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                    item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                    item.origin_name = rdr["origin_name"].ToString();
                    item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                    item.destination_name = rdr["destination_name"].ToString();
                    item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                    item.composition_name = rdr["Name"].ToString();
                    item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                    item.action_name = rdr["action"].ToString();
                    item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                    item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                    item.bag_type = rdr["bag_type"].ToString();

                    item.current_value = Convert.ToDecimal(rdr["total_value"].ToString());
                    item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());

                    currentBags.Add(item);
                }
                rdr.Close();

                VAR.status = true;
                VAR.Bags = currentBags;
                VertedaDiagnostics.LogMessage("GetAllAdditionalBagsMadeDuringEvent Completed successfully", "I", false);

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in GetAllAdditionalBagsMadeDuringEvent: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }

        [WebMethod] //unused outbound bags - get all bags where action ID = 4 & end_date = null & type = terminal
        public VarianceActivityResponseBags GetAllUnusedOutboundBags(int event_id)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            List<Bag> currentBags = new List<Bag>();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                cmd.Connection = conn;
            //return package_id to use in copy

                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                conn.Open();
                cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id      ,[Bag_Action_Join].[terminal_id], [Bag_Action_Join].[total_value], [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin   ON [Bag_Dim].[origin_location_id] = origin.[id]    LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination    ON [Bag_Dim].[destination_location_id] = destination.[id] 	LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] 	ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]	 LEFT JOIN [variance_cash].[dbo].[Action_Dim] 	 ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id = " + event_id + " and [Bag_Action_Join].[end_date] is null AND [Bag_Action_Join].action_id = 4 and bag_type = '"+constants.BT2+"' ORDER BY  created_date desc";
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Bag item = new Bag();
                    item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                    item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                    item.event_name = rdr["event_name"].ToString();
                    item.bag_type = rdr["bag_type"].ToString();
                    item.bag_id = rdr["bag_id"].ToString();
                    item.label_id = rdr["label_id"].ToString();
                    item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                    item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                    item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                    item.origin_name = rdr["origin_name"].ToString();
                    item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                    item.destination_name = rdr["destination_name"].ToString();
                    item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                    item.composition_name = rdr["Name"].ToString();
                    item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                    item.action_name = rdr["action"].ToString();
                    item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                    item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                    item.bag_type = rdr["bag_type"].ToString();

                    item.current_value = Convert.ToDecimal(rdr["total_value"].ToString());
                    item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());

                    currentBags.Add(item);
                }
                rdr.Close();

                VAR.status = true;
                VAR.Bags = currentBags;
                VertedaDiagnostics.LogMessage("GetAllUnusedOutboundBags Completed successfully", "I", false);

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in GetAllUnusedOutboundBags: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }
                
        [WebMethod] //show all bags where composition value != value at action 5
        public VarianceActivityResponseBags GetAllOuboundBagsWithCashVariance(int event_id)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            List<Bag> currentBags = new List<Bag>();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;
                cmd.Connection = conn;
            //return package_id to use in copy

                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                conn.Open();
                cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id      ,[Bag_Action_Join].[terminal_id],  [Bag_Action_Join].[total_value] as final_value, [Composition_Dim].[total_value] as starting_value, [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin   ON [Bag_Dim].[origin_location_id] = origin.[id]    LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination    ON [Bag_Dim].[destination_location_id] = destination.[id] 	LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] 	ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]	 LEFT JOIN [variance_cash].[dbo].[Action_Dim] 	 ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id = " + event_id + " AND [Bag_Action_Join].action_id = 5 and bag_type = '" + constants.BT2 + "' AND [Composition_Dim].[total_value] != [Bag_Action_Join].[total_value]";

                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Bag item = new Bag();
                    item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                    item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                    item.event_name = rdr["event_name"].ToString();
                    item.bag_type = rdr["bag_type"].ToString();
                    item.bag_id = rdr["bag_id"].ToString();
                    item.label_id = rdr["label_id"].ToString();
                    item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                    item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                    item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                    item.origin_name = rdr["origin_name"].ToString();
                    item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                    item.destination_name = rdr["destination_name"].ToString();
                    item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                    item.composition_name = rdr["Name"].ToString();
                    item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                    item.action_name = rdr["action"].ToString();
                    item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                    item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                    item.bag_type = rdr["bag_type"].ToString();

                    item.current_value = Convert.ToDecimal(rdr["final_value"].ToString());
                    item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());

                    item.composition_value = Convert.ToDecimal(rdr["starting_value"].ToString());
                    currentBags.Add(item);
                }
                rdr.Close();

                VAR.status = true;
                VAR.Bags = currentBags;
                VertedaDiagnostics.LogMessage("GetAllOuboundBagsWithCashVariance Completed successfully", "I", false);

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in GetAllOuboundBagsWithCashVariance: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }
        
        [WebMethod] //show all bags where bag count in action 2 != bag count in 4 #### UNFINISHED
        public VarianceActivityResponseBags GetAllOuboundBagsWithBagVariance(int event_id)
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();
            List<Bag> currentBags = new List<Bag>();

            //Using Output parameter
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                cmd.Connection = conn;
            //return package_id to use in copy

                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                conn.Open();
                cmd.CommandText = "SELECT ISNULL([bag_dim].[bag_function_id], -1) AS bag_function_id      ,[Bag_Action_Join].[terminal_id],  [Bag_Action_Join].[total_value] as final_value, [Composition_Dim].[total_value] as starting_value, [Bag_Dim].[unid], [event_id],ISNULL([total_child_bags], -1) AS [total_child_bag],ISNULL([event_master].[event_name], 'NULL') AS [event_name],ISNULL([bag_type], 'NULL') AS [bag_type],[Bag_Dim].[bag_id],ISNULL([label_id],  -1) AS [label_id],[created_date],ISNULL([created_by_employee], -1) AS [created_by_employee],[Bag_Dim].origin_location_id,ISNULL(origin.[location_name], 'NULL') AS [origin_name],[Bag_Dim].destination_location_id,ISNULL(destination.[location_name], 'NULL') AS [destination_name],[Bag_Dim].[composition_id],ISNULL([Composition_Dim].[Name], 'NULL') AS [Name],ISNULL([Action_Dim].[action_id], -1) AS [action_id],ISNULL([Action_Dim].[action], 'NULL') AS [action] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id] LEFT JOIN [variance_cash].[dbo].[Composition_Dim] ON [Bag_Dim].[composition_id] = [Composition_Dim].[composition_id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin   ON [Bag_Dim].[origin_location_id] = origin.[id]    LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination    ON [Bag_Dim].[destination_location_id] = destination.[id] 	LEFT JOIN [variance_cash].[dbo].[Bag_Action_Join] 	ON [Bag_Dim].[unid] = [Bag_Action_Join].[bag_unid]	 LEFT JOIN [variance_cash].[dbo].[Action_Dim] 	 ON [Bag_Action_Join].[action_id] = [Action_Dim].[action_id] WHERE event_id = " + event_id + " AND [Bag_Action_Join].action_id = 5 and bag_type = '" + constants.BT2 + "' AND [Composition_Dim].[total_value] != [Bag_Action_Join].[total_value]";

                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Bag item = new Bag();
                    item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                    item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                    item.event_name = rdr["event_name"].ToString();
                    item.bag_type = rdr["bag_type"].ToString();
                    item.bag_id = rdr["bag_id"].ToString();
                    item.label_id = rdr["label_id"].ToString();
                    item.created_date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G");
                    item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                    item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                    item.origin_name = rdr["origin_name"].ToString();
                    item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                    item.destination_name = rdr["destination_name"].ToString();
                    item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                    item.composition_name = rdr["Name"].ToString();
                    item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
                    item.action_name = rdr["action"].ToString();
                    item.total_child_bags = Convert.ToInt32(rdr["total_child_bag"].ToString());
                    item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                    item.bag_type = rdr["bag_type"].ToString();

                    item.current_value = Convert.ToDecimal(rdr["final_value"].ToString());
                    item.terminal_id = Convert.ToInt32(rdr["terminal_id"].ToString());

                    item.composition_value = Convert.ToDecimal(rdr["starting_value"].ToString());
                    currentBags.Add(item);
                }
                rdr.Close();

                VAR.status = true;
                VAR.Bags = currentBags;
                VertedaDiagnostics.LogMessage("GetAllOuboundBagsWithCashVariance Completed successfully", "I", false);

            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in GetAllOuboundBagsWithCashVariance: " + ex.ToString(), "E", false);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return VAR;
        }

        //show all entries in Variance_Error_log
        [WebMethod] //if location_id > 0, narrow results by location
        public VarianceActivityResponseErrorLog GetErrorLog(int event_id, int location_id)
        {
            VertedaDiagnostics.LogMessage("GetErrorLog started.", "I", false);

            VarianceActivityResponseErrorLog VAR = new VarianceActivityResponseErrorLog();

            List<ErrorLog> error_logs = new List<ErrorLog>();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                string location_parameter = "";
                if (location_id > 0)
                {
                    location_parameter = " AND [location_id] = " + location_id;
                }

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT [event_id] ,[activity_mode_id] ,[terminal_id]  ,[location_id] ,[employee_id] ,[created_at] ,[message] FROM [variance_cash].[dbo].[Variance_Error_Log]  WHERE event_id = " + event_id + location_parameter;


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        ErrorLog item = new ErrorLog();
                        item.Event_ID = Convert.ToInt32(rdr["event_id"].ToString());

                        item.Activity_Mode_ID = Convert.ToInt32(rdr["activity_mode_id"].ToString());
                        item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                        item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                        item.Employee_ID = Convert.ToInt32(rdr["employee_id"].ToString());
                        item.Created_At = Convert.ToDateTime(rdr["created_at"].ToString());
                        item.Message = rdr["message"].ToString();

                        error_logs.Add(item);
                    }
                    rdr.Close();

                    conn.Close();

                    VAR.status = true;
                    VAR.ErrorLogs = error_logs;
                    VertedaDiagnostics.LogMessage("GetErrorLog query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetErrorLog: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        

        [WebMethod]
        public VarianceActivityResponseVariance GetLocationReport(int event_id, int location_id)
        {
            VarianceActivityResponseVariance VAR = new VarianceActivityResponseVariance();
            location_variance loc_var = new location_variance();

            try
            {
                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                
                BagOperators op = new BagOperators();
                op.event_id = event_id;
                op.to_location_id = location_id;

                //get all kiosk bags going to location
                VarianceActivityResponseBags VAR2 = GetBagDetails(op);

                if (VAR2.status)
                {
                    foreach (Bag bg in VAR2.Bags)
                    {
                        if (bg.bag_type == constants.BT1)
                        {
                            decimal starting_value = 0;
                            decimal end_value = 0;

                            foreach (Bag bgc in VAR2.Bags)
                            {
                                if (bgc.parent_id == bg.bag_unid)
                                {
                                    if ((bgc.bag_actions[bgc.bag_actions.Count - 1].status_id == 6))
                                    {
                                        //additional bag. we will use its value as normal, but bag count is +1
                                        loc_var.location_count_variance_outbound += 1;
                                    }

                                        //grab the starting and end values.    //if status = 5, end value is 0      //add flag is bag isnt at end
                                        starting_value += bgc.bag_actions[0].Total_Value;

                                    // ignore bags that are missing (status = 5)
                                    if (bgc.bag_actions[bgc.bag_actions.Count - 1].Action_ID == 5)
                                    {
                                        //we have a final value
                                        end_value += bgc.bag_actions[bgc.bag_actions.Count - 1].Total_Value;
                                    }
                                    else if ((bgc.bag_actions[bgc.bag_actions.Count - 1].status_id == 5))
                                    {
                                        //we have a non final value that has been declared missing. no value added
                                        //end_value += 0;
                                        loc_var.location_count_variance_outbound -= 1;
                                    }
                                    else
                                    {
                                        //we have a non final value that is still active
                                        end_value += bgc.bag_actions[bgc.bag_actions.Count - 1].Total_Value;
                                        loc_var.all_bags_complete = false;
                                    }
                                }
                            }
                            //add the start / end values for this kiosk bag to the location total
                            loc_var.location_amount_variance_outbound += (end_value - starting_value);
                        }
                    }
                }
                else
                {
                    //error - could not find outgoing bags for location + location_id
                    VAR.status = false;
                    VAR.errorText = "GetLocationReport: Could not find outgoing bags for location "+ location_id;
                    VertedaDiagnostics.LogMessage("GetLocationReport: Could not find outgoing bags for location "+ location_id, "E", false);
                }


                op.to_location_id = 0;
                op.from_location_id = location_id;

                VarianceActivityResponseBags VAR3 = GetBagDetails(op);

                //get all kiosk bags comnig from location
                if (VAR3.status)
                {
                    foreach (Bag bg in VAR3.Bags)
                    {
                        if (bg.bag_type == constants.BT1)
                        {
                            decimal starting_value = 0;
                            decimal end_value = 0;

                            foreach (Bag bgc in VAR3.Bags)
                            {
                                if (bgc.parent_id == bg.bag_unid)
                                {
                                    if ((bgc.bag_actions[bgc.bag_actions.Count - 1].status_id == 6))
                                    {
                                        //additional bag. we will use its value as normal, but bag count is +1
                                        loc_var.location_count_variance_inbound += 1;
                                    }

                                    //grab the starting and end values.    //if status = 5, end value is 0      //add flag is bag isnt at end
                                    starting_value += bgc.bag_actions[0].Total_Value;
                                    // ignore bags that are missing (status = 5)
                                    if (bgc.bag_actions[bgc.bag_actions.Count - 1].Action_ID == 10)
                                    {
                                        //we have a final value
                                        end_value += bgc.bag_actions[bgc.bag_actions.Count - 1].Total_Value;
                                    }
                                    else if ((bgc.bag_actions[bgc.bag_actions.Count - 1].status_id == 5))
                                    {
                                        //we have a non final value that has been declared missing. no value added
                                        //end_value += 0;
                                        loc_var.location_count_variance_inbound -= 1;
                                    }
                                    else
                                    {
                                        //we have a non final value that is still active
                                        end_value += bgc.bag_actions[bgc.bag_actions.Count - 1].Total_Value;
                                        loc_var.all_bags_complete = false;
                                    }
                                }
                            }
                            //add the start / end values for this kiosk bag to the location total
                            loc_var.location_amount_variance_inbound += (end_value - starting_value);
                        }
                    }
                }
                else
                {
                    //error - could not find inbound bags for location + location_id
                    VAR.status = false;
                    VAR.errorText = "GetLocationReport: Could not find inbound bags for location " + location_id;
                    VertedaDiagnostics.LogMessage("GetLocationReport: Could not find inbound bags for location " + location_id, "E", false);
                }
                loc_var.location_amount_variance = loc_var.location_amount_variance_inbound + loc_var.location_amount_variance_outbound;
                loc_var.location_count_variance = loc_var.location_count_variance_inbound + loc_var.location_count_variance_outbound;
                VAR.Venue_Total_Variance.venue_locations.Add(loc_var);
            }
            catch (Exception e)
            {
                //error - could not find inbound bags for location + location_id
                VAR.status = false;
                VAR.errorText = "Error in GetLocationReport: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetLocationReport: " + e.ToString(), "E", false);
            }

           
            return VAR;
        }
        [WebMethod]
        public VarianceActivityResponseVariance GetVenueReport(int event_id)
        {
            VarianceActivityResponseVariance VAR = new VarianceActivityResponseVariance();
            try
            {
                if (event_id < 1)
                {
                    event_id = GetEventID();
                }

                VarianceActivityResponseLocationsAndTerminal VAR1 = GetOpenLocations();

                if (VAR1.status && VAR1.LocationsTerminals.Count > 0)
                {
                    foreach (LocationAndTerminal LaT in VAR1.LocationsTerminals)
                    {
                        VarianceActivityResponseVariance VAR3 = GetLocationReport(event_id, LaT.location_id);

                        if (VAR3.status)
                        {
                            VAR.Venue_Total_Variance.venue_locations.Add(VAR3.Venue_Total_Variance.venue_locations[0]);

                            VAR.Venue_Total_Variance.venue_amount_variance += VAR3.Venue_Total_Variance.venue_locations[0].location_amount_variance;
                            VAR.Venue_Total_Variance.venue_count_variance += VAR3.Venue_Total_Variance.venue_locations[0].location_count_variance;
                        }
                        else
                        {
                            //Error in GetVenueReport: couldn't find variance details for location: +LaT.location_id
                            VAR.status = false;
                            VAR.errorText = "Error in GetVenueReport: couldn't find variance details for location: " + LaT.location_id;
                            VertedaDiagnostics.LogMessage("Error in GetVenueReport: couldn't find variance details for location: " + LaT.location_id, "E", false);
                        }
                    }                      
                }
                else
                {
                    //Error in GetVenueReport: couldn't find any locations for the specified event
                    VAR.status = false;
                    VAR.errorText = "Error in GetVenueReport: couldn't find any locations for the specified event";
                    VertedaDiagnostics.LogMessage("Error in GetVenueReport: couldn't find any locations for the specified event", "E", false);
                }
            }
            catch (Exception e)
            {
                //error - could not find inbound bags for location + location_id
                VAR.status = false;
                VAR.errorText = "Error in GetLocationReport: " + e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetLocationReport: " + e.ToString(), "E", false);
            }


            return VAR;
        }










        #endregion 


        #region Get Producers V2.0

        [WebMethod] //done
        public VarianceActivityResponseBags GetBagDetails(BagOperators operators)
        {
            VertedaDiagnostics.LogMessage("GetBagDetails started.", "I", false);
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();


            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                cmd.CommandType = CommandType.StoredProcedure;

                if (con_str.database_instance == 2)
                {
                    cmd.CommandText = "SP_Get_Bag_Details_hospitality";
                }
                else //always default to 1
                {
                    cmd.CommandText = "SP_Get_Bag_Details_public";
                }

                //if operators dont exist, dont add them.

                if (operators.from_all_events == false)
                {
                    if (operators.event_id > 0)
                    {
                        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = operators.event_id;
                    }
                    else
                    {
                        int event_id = GetEventID();
                        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = event_id;
                    }
                }
                else
                {
                    cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = DBNull.Value; 
                }

                if (operators.bag_unid > 0)
                {
                    cmd.Parameters.Add("@bag_unid", SqlDbType.Int).Value = operators.bag_unid;
                }
                else
                {
                    cmd.Parameters.Add("@bag_unid", SqlDbType.Int).Value = DBNull.Value;
                }


                if (operators.parent_id > 0)
                {
                    cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = operators.parent_id;
                }
                else
                {
                    cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = DBNull.Value;
                }

                if (operators.function_id > 0) 
                {
                    cmd.Parameters.Add("@function_id", SqlDbType.Int).Value = operators.function_id;
                }
                else
                {
                    cmd.Parameters.Add("@function_id", SqlDbType.Int).Value = DBNull.Value;
                }

                //if (operators.bag_label.Length >  0)
                if (operators.bag_label != "" && operators.bag_label != "0")
                    {
                    cmd.Parameters.Add("@label_id", SqlDbType.VarChar).Value = operators.bag_label;
                }
                else
                {
                    cmd.Parameters.Add("@label_id", SqlDbType.VarChar).Value = DBNull.Value; //DBNull.Value;
                }



                if (operators.outbound_bags_only == true)
                {
                    ConfigSettings cfg = ReadConfig();
                    cmd.Parameters.Add("@origin_location_id", SqlDbType.Int).Value = cfg.CashOfficeID;
                }
                else if (operators.from_location_id > 0)
                {
                    cmd.Parameters.Add("@origin_location_id", SqlDbType.Int).Value = operators.from_location_id;
                }
                else
                {
                    cmd.Parameters.Add("@origin_location_id", SqlDbType.Int).Value = DBNull.Value;
                }


                if (operators.inbound_bags_only == true)
                {
                    ConfigSettings cfg = ReadConfig();
                    cmd.Parameters.Add("@destination_location_id", SqlDbType.Int).Value = cfg.CashOfficeID;
                }
                else if (operators.to_location_id > 0)
                {
                    cmd.Parameters.Add("@destination_location_id", SqlDbType.Int).Value = operators.to_location_id;
                }
                else
                {
                    cmd.Parameters.Add("@destination_location_id", SqlDbType.Int).Value = DBNull.Value;
                }

                if (operators.created_by_employee > 0)
                {
                    cmd.Parameters.Add("@created_by_employee", SqlDbType.Int).Value = operators.created_by_employee;
                }
                else
                {
                    cmd.Parameters.Add("@created_by_employee", SqlDbType.Int).Value = DBNull.Value;
                }


                if (operators.kiosk_bags_only == true || (operators.with_bag_count_variance == true && operators.with_cash_variance == false)) //must allow for getting both cash + bag variance
                {
                    cmd.Parameters.Add("@bag_type", SqlDbType.VarChar).Value = constants.BT1;
                }
                else if (operators.terminal_bags_only == true || (operators.with_bag_count_variance == false && operators.with_cash_variance == true))
                {
                    cmd.Parameters.Add("@bag_type", SqlDbType.VarChar).Value = constants.BT2;
                }
                else
                {
                    cmd.Parameters.Add("@bag_type", SqlDbType.VarChar).Value = DBNull.Value; // DBNull.Value;
                }
                

                if (operators.from_terminal_id > 0 && !(operators.action_id > 0)) //if you want bags from terminal, action_id must be set to 6
                {
                    cmd.Parameters.Add("@terminal_id", SqlDbType.Int).Value = operators.from_terminal_id;
                    cmd.Parameters.Add("@action_id", SqlDbType.Int).Value = 6;
                }
                else if (operators.action_id > 0)
                {
                    cmd.Parameters.Add("@action_id", SqlDbType.Int).Value = operators.action_id;
                    cmd.Parameters.Add("@terminal_id", SqlDbType.Int).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@action_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@terminal_id", SqlDbType.Int).Value = DBNull.Value;
                }

                if (operators.up_to_action > 0)
                {
                    cmd.Parameters.Add("@up_to_action", SqlDbType.Int).Value = operators.up_to_action;
                }
                else
                {
                    cmd.Parameters.Add("@up_to_action", SqlDbType.Int).Value = DBNull.Value;
                }
                //    cmd.CommandText = "SP_venue_location_master_InsertLocation_Plus";
                //    cmd.Parameters.Add("@location_id", SqlDbType.Int).Value = loc_ID;
                //    cmd.Parameters.Add("@location_name", SqlDbType.VarChar).Value = loc_name;
                //    cmd.Parameters.Add("@is_warehouse", SqlDbType.Int).Value = 0;

                VertedaDiagnostics.LogMessage("GetBagDetails pre-sql call.", "I", false);
                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                        VertedaDiagnostics.LogMessage("error in GetBagDetails " + e.ToString(), "E", false);
                    }
                }
                VertedaDiagnostics.LogMessage("GetBagDetails before connection open.", "I", false);
                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;
                    VertedaDiagnostics.LogMessage("GetBagDetails after connection open.", "I", false);
                    SqlDataReader rdr = cmd.ExecuteReader();
                    VertedaDiagnostics.LogMessage("GetBagDetails after reader.", "I", false);
                    while (rdr.Read())
                    {
                        VertedaDiagnostics.LogMessage("GetBagDetails start of loop.", "I", false);
                        Bag b_item = new Bag();
                        BagAction a_item = new BagAction();

                        //make a bag
                        //make a bag action
                        //add the bag to main list if it doesnt exist already
                        //add the action to the bag

                        b_item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                        b_item.event_name = rdr["event_name"].ToString();
                        b_item.bag_id = rdr["bag_id"].ToString();
                        b_item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                        b_item.label_id = rdr["label_id"].ToString();
                        b_item.bag_type = rdr["bag_type"].ToString();
                        b_item.created_date = rdr["created_date"].ToString();
                        b_item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                        b_item.employee_name = rdr["emp_first_name"].ToString() + " " + rdr["emp_last_name"].ToString();
                        b_item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                        b_item.bag_function_name = rdr["bag_function"].ToString();
                        b_item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                        b_item.origin_name = rdr["origin_name"].ToString();
                        b_item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                        b_item.destination_name = rdr["destination_name"].ToString();
                        b_item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                        b_item.composition_name = rdr["composition_name"].ToString();
                        b_item.composition_value = Convert.ToDecimal(rdr["composition_value"].ToString());

                        b_item.parent_id = Convert.ToInt32(rdr["parent_id"].ToString());

                        a_item.Bag_Unid = Convert.ToInt32(rdr["unid"].ToString());

                        a_item.Bag_Action_Unid = Convert.ToInt32(rdr["bag_action_unid"].ToString());
                        a_item.Action_ID = Convert.ToInt32(rdr["action_id"].ToString());
                        a_item.Action_Name = rdr["action_name"].ToString();
                        a_item.Start_time = rdr["start_date"].ToString();
                        a_item.End_time = rdr["end_date"].ToString();
                        a_item.Sending_Employee_ID = Convert.ToInt32(rdr["sending_employee"].ToString());
                        a_item.sender_name = rdr["sender_name"].ToString();
                        a_item.Receiving_Employee_ID = Convert.ToInt32(rdr["recieving_employee"].ToString());
                        a_item.reciever_name = rdr["reciever_name"].ToString();
                        a_item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                        a_item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                        a_item.terminal_name = rdr["terminal_name"].ToString();
                        a_item.location_name = rdr["location_name"].ToString();
                        a_item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
                        a_item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());




                        //if bag doesnt exist in list, add it + action
                        //else add the action to the existing bag
                        int index = currentBags.FindIndex(a => a.bag_unid == a_item.Bag_Unid); //-1 if not found

                        if (index == -1)
                        {
                            if (a_item.Bag_Action_Unid != -1) //if the action is null, don't add it
                            {
                                b_item.bag_actions.Add(a_item);
                            }

                            currentBags.Add(b_item);
                        }
                        else if (index > -1)
                        {
                            if (a_item.Bag_Action_Unid != -1) //if the action is null, don't add it
                            {
                                currentBags[index].bag_actions.Add(a_item);
                            }
 
                        }
                        VertedaDiagnostics.LogMessage("GetBagDetails end of loop.", "I", false);
                    }
                    rdr.Close();

                    conn.Close();
                }

                VAR.status = true;
                VertedaDiagnostics.LogMessage("GetBagDetails query completed", "I", false);


                //calculate variance
                ConfigSettings cnfg = ReadConfig();

                VertedaDiagnostics.LogMessage("GetBagDetails after config .", "I", false);

                List<Bag> bags_with_cash_variance = new List<Bag>();
                List<Bag> bags_with_count_variance = new List<Bag>();

                foreach (Bag bg in currentBags) //because all records are order by date, and we only care about start vs end value
                {
                    if (bg.bag_actions.Count > 0)
                    {
                        int index_count = bg.bag_actions.Count - 1;

                        bg.bag_count_variance = bg.bag_actions[0].Bag_Count - bg.bag_actions[index_count].Bag_Count;
                        if (bg.bag_count_variance != 0) //can be positive or negative
                        {  bags_with_count_variance.Add(bg); }

                        bg.bag_value_variance = bg.bag_actions[0].Total_Value - bg.bag_actions[index_count].Total_Value;
                        if (bg.bag_value_variance != 0) //can be positive or negative
                        { bags_with_cash_variance.Add(bg); }
                    }
                }

                VertedaDiagnostics.LogMessage("GetBagDetails after config loop 1.", "I", false);


                if (operators.with_bag_count_variance == true && operators.with_cash_variance == true)
                {
                    VAR.Bags = bags_with_count_variance;
                    VAR.Bags.AddRange(bags_with_cash_variance);
                }
                else if (operators.with_bag_count_variance == true)
                {
                    VAR.Bags = bags_with_count_variance;
                }
                else if (operators.with_cash_variance == true)
                {
                    VAR.Bags = bags_with_cash_variance;
                }
                else
                {
                    VAR.Bags = currentBags;
                }


                VertedaDiagnostics.LogMessage("GetBagDetails query end.", "I", false);
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
            }
            return VAR;
        }

        [WebMethod] //done
        public VarianceActivityResponseBags GetBagDetailsWithMultipleBagUnids(List<int> bag_unids)
        {
            VertedaDiagnostics.LogMessage("GetBagDetailsWithMultipleBagUnids started.", "I", false);
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> currentBags = new List<Bag>();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                cmd.CommandType = CommandType.StoredProcedure;

                if (con_str.database_instance == 2)
                {
                    cmd.CommandText = "SP_Get_Bag_Details_With_Multiple_Bag_Unids_hospitality";
                }
                else //always default to 1
                {
                    cmd.CommandText = "SP_Get_Bag_Details_With_Multiple_Bag_Unids_public";
                }

                

                //string joined_bag_unids = string.Join(",", bag_unids);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[1]
                        { new DataColumn("IntItem", typeof(int))  });

                foreach (int i in bag_unids)
                {
                    dt.Rows.Add(i);
                }
                cmd.Parameters.AddWithValue("@bag_unids", dt);
                //cmd.Parameters.Add("@bag_unid", SqlDbType.VarChar).Value = joined_bag_unids;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                        VertedaDiagnostics.LogMessage("error in GetBagDetailsWithMultipleBagUnids " + e.ToString(), "E", false);
                    }
                }

                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Bag b_item = new Bag();
                        BagAction a_item = new BagAction();

                        //make a bag
                        //make a bag action
                        //add the bag to main list if it doesnt exist already
                        //add the action to the bag

                        b_item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                        b_item.event_name = rdr["event_name"].ToString();
                        b_item.bag_id = rdr["bag_id"].ToString();
                        b_item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());
                        b_item.label_id = rdr["label_id"].ToString();
                        b_item.bag_type = rdr["bag_type"].ToString();
                        b_item.created_date = rdr["created_date"].ToString();
                        b_item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
                        b_item.employee_name = rdr["emp_first_name"].ToString() + " " + rdr["emp_last_name"].ToString();
                        b_item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());
                        b_item.bag_function_name = rdr["bag_function"].ToString();
                        b_item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
                        b_item.origin_name = rdr["origin_name"].ToString();
                        b_item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
                        b_item.destination_name = rdr["destination_name"].ToString();
                        b_item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
                        b_item.composition_name = rdr["composition_name"].ToString();
                        b_item.composition_value = Convert.ToDecimal(rdr["composition_value"].ToString());

                        b_item.parent_id = Convert.ToInt32(rdr["parent_id"].ToString());

                        a_item.Bag_Unid = Convert.ToInt32(rdr["unid"].ToString());

                        a_item.Bag_Action_Unid = Convert.ToInt32(rdr["bag_action_unid"].ToString());
                        a_item.Action_ID = Convert.ToInt32(rdr["action_id"].ToString());
                        a_item.Action_Name = rdr["action_name"].ToString();
                        a_item.Start_time = rdr["start_date"].ToString();
                        a_item.End_time = rdr["end_date"].ToString();
                        a_item.Sending_Employee_ID = Convert.ToInt32(rdr["sending_employee"].ToString());
                        a_item.sender_name = rdr["sender_name"].ToString();
                        a_item.Receiving_Employee_ID = Convert.ToInt32(rdr["recieving_employee"].ToString());
                        a_item.reciever_name = rdr["reciever_name"].ToString();
                        a_item.Terminal_ID = Convert.ToInt32(rdr["terminal_id"].ToString());
                        a_item.Location_ID = Convert.ToInt32(rdr["location_id"].ToString());
                        a_item.terminal_name = rdr["terminal_name"].ToString();
                        a_item.location_name = rdr["location_name"].ToString();
                        a_item.Total_Value = Convert.ToDecimal(rdr["total_value"].ToString());
                        a_item.Bag_Count = Convert.ToInt32(rdr["total_child_bags"].ToString());




                        //if bag doesnt exist in list, add it + action
                        //else add the action to the existing bag
                        int index = currentBags.FindIndex(a => a.bag_unid == a_item.Bag_Unid); //-1 if not found

                        if (index == -1)
                        {
                            b_item.bag_actions.Add(a_item);
                            currentBags.Add(b_item);
                        }
                        else if (index > -1)
                        {
                            currentBags[index].bag_actions.Add(a_item);

                        }
                    }
                    rdr.Close();

                    conn.Close();
                }

                VAR.status = true;
                VertedaDiagnostics.LogMessage("GetBagDetailsWithMultipleBagUnids query completed", "I", false);


                //calculate variance
                ConfigSettings cnfg = ReadConfig();

                List<Bag> bags_with_cash_variance = new List<Bag>();
                List<Bag> bags_with_count_variance = new List<Bag>();

                foreach (Bag bg in currentBags) //because all records are order by date, and we only care about start vs end value
                {
                    if (bg.bag_actions.Count > 0)
                    {
                        int index_count = bg.bag_actions.Count - 1;

                        bg.bag_count_variance = bg.bag_actions[0].Bag_Count - bg.bag_actions[index_count].Bag_Count;
                        if (bg.bag_count_variance != 0) //can be positive or negative
                        { bags_with_count_variance.Add(bg); }

                        bg.bag_value_variance = bg.bag_actions[0].Total_Value - bg.bag_actions[index_count].Total_Value;
                        if (bg.bag_value_variance != 0) //can be positive or negative
                        { bags_with_cash_variance.Add(bg); }
                    }
                }

                VAR.Bags = currentBags;

            }
            catch (Exception e)
            {
                Primo_SendBugsnag("Error getting bag details with multiple unids", e, "Database");
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetBagDetailsWithMultipleBagUnids: " + e.ToString(), "E", false);
            }
            return VAR;
        }
        
        [WebMethod] //done
        public VarianceActivityResponseBags GetAllChildrenWithSameParent(List<int> bagUnids)
        {
            VertedaDiagnostics.LogMessage("GetAllChildrenWithSameParent started", "I", false);

            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            List<Bag> child_bags = new List<Bag>();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(2);
                conn.ConnectionString = con_str.connection_string;

                string bag_unids_string = string.Join(",", bagUnids);

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                        
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    cmd.CommandText = "SELECT  [parent_id]  ,[child_id] FROM[variance_cash].[dbo].[Kid_Tid_Join]  where parent_id in (select distinct parent_id FROM[variance_cash].[dbo].[Kid_Tid_Join] where child_id in (" + bag_unids_string + "))";


                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Bag item = new Bag();
                        item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
                        item.parent_id = Convert.ToInt32(rdr["parent_id"].ToString());
                        item.bag_unid = Convert.ToInt32(rdr["child_id"].ToString());



                        child_bags.Add(item);
                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VAR.Bags = child_bags;
                    VertedaDiagnostics.LogMessage("GetAllChildrenWithSameParent query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetAllChildrenWithSameParent: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        //GetBagDetailsWithMultipleUnids


        //[WebMethod] //done
        //public VarianceActivityResponseBags GetBagDetailsTest(int event_id, int destination, string type)
        //{
        //    VertedaDiagnostics.LogMessage("GetBagDetails started.", "I", false);
        //    VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

        //    List<Bag> currentBags = new List<Bag>();


        //    SqlConnection conn = new SqlConnection();
        //    SqlCommand cmd = new SqlCommand();

        //    try
        //    {
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.CommandText = "SP_Get_Bag_Details_TEST";

        //        //if operators dont exist, dont add them.

        //        if (event_id > 0)
        //        {
        //            cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = event_id;
        //        }
        //        else
        //        {
        //            event_id = GetEventID();
        //            cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = event_id;
        //        }

        //        if (destination > 0)
        //        {
        //            ConfigSettings cfg = ReadConfig();
        //            cmd.Parameters.Add("@destination", SqlDbType.Int).Value = destination;
        //        }
        //        else
        //        {
        //            cmd.Parameters.Add("@destination", SqlDbType.Int).Value = DBNull.Value;
        //        }

        //        if (type.Length > 0) 
        //        {
        //            cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = constants.BT1;
        //        }
        //        else
        //        {
        //            cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = DBNull.Value; // DBNull.Value;
        //        }



        //        conn.ConnectionString = GetConnectionString(2);

        //        if (conn.State != ConnectionState.Open)
        //        {
        //            try
        //            {
        //                conn.Open();
        //            }
        //            catch (SqlException e)
        //            {
        //                //error
        //                VertedaDiagnostics.LogMessage("error in GetBagDetailsTEST " + e.ToString(), "E", false);
        //            }
        //        }

        //        if (conn.State == ConnectionState.Open)
        //        {
        //            cmd.Connection = conn;

        //            SqlDataReader rdr = cmd.ExecuteReader();

        //            while (rdr.Read())
        //            {
        //                Bag b_item = new Bag();
        //                BagAction a_item = new BagAction();

        //                //make a bag
        //                //make a bag action
        //                //add the bag to main list if it doesnt exist already
        //                //add the action to the bag
        //                b_item.bag_unid = Convert.ToInt32(rdr["unid"].ToString());                        
        //                b_item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
        //                b_item.bag_type = rdr["bag_type"].ToString();
        //                b_item.bag_id = rdr["bag_id"].ToString();
        //                b_item.label_id = rdr["label_id"].ToString();
        //                b_item.created_date = rdr["created_date"].ToString();
        //                b_item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
        //                b_item.origin_location_id = Convert.ToInt32(rdr["origin_location_id"].ToString());
        //                b_item.destination_location_id = Convert.ToInt32(rdr["destination_location_id"].ToString());
        //                b_item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
        //                b_item.bag_function_id = Convert.ToInt32(rdr["bag_function_id"].ToString());


        //                currentBags.Add(b_item);

        //            }
        //            rdr.Close();

        //            conn.Close();
        //        }

        //        VAR.status = true;
        //        VertedaDiagnostics.LogMessage("GetBagDetails query completed", "I", false);


        //            VAR.Bags = currentBags;
        //    }
        //    catch (Exception e)
        //    {
        //        VAR.status = false;
        //        VAR.errorText = e.ToString();
        //        VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
        //    }
        //    return VAR;
        //}


        #endregion


        #region Printing
        //event name    - event ID from bag
        //date          - from bag
        //bag ID        - from bag
        //Emp ID        - from bag
        //Location name - loc ID from bag

        [WebMethod]
        public VarianceActivityResponsePrintInfo GetPrintInformation(int bagUnid)
        {
            VertedaDiagnostics.LogMessage("GetPrintInformationWithBagID started", "I", false);

            VarianceActivityResponsePrintInfo VAR = new VarianceActivityResponsePrintInfo();

            PrintInfo pi = new PrintInfo();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                ConnectionString con_str = GetConnectionString(3);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }


                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    //cmd.CommandText = "SELECT [bag_id],[created_date],[created_by_employee], ISNULL([venue_location_master].[location_name], 'NULL') AS [location_name]	  ,ISNULL([event_master].[event_name], 'NULL') AS [event_name]  FROM [variance_cash].[dbo].[Bag_Dim]  LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  ON [Bag_Dim].[destination_location_id] = [venue_location_master].[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master]  ON [Bag_Dim].[event_id] = [event_master].[id] WHERE  [bag_id] = '" + bagID + "'";
                    //cmd.CommandText = "SELECT [bag_id], [created_date],  [created_by_employee],  ISNULL(origin.[location_name], 'NULL') AS [origin_name], ISNULL(destination.[location_name], 'NULL') AS [destination_name],  ISNULL([event_master].[event_name], 'NULL') AS [event_name]    FROM [variance_cash].[dbo].[Bag_Dim]   LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination  ON [Bag_Dim].[destination_location_id] = destination.[id]   LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin    ON [Bag_Dim].[origin_location_id] = origin.[id]   LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master]   ON [Bag_Dim].[event_id] = [event_master].[id]   WHERE  [bag_id] = '" + bagID + "'";
                    cmd.CommandText = "SELECT [bag_id], [created_date], [created_by_employee], ISNULL([emp_Master].emp_first_name, 'NULL') AS emp_first_name,ISNULL([emp_Master].emp_last_name, 'NULL') AS emp_last_name,ISNULL(origin.[location_name], 'NULL') AS [origin_name], ISNULL(destination.[location_name], 'NULL') AS [destination_name], ISNULL([event_master].[event_name], 'NULL') AS [event_name] FROM [variance_cash].[dbo].[Bag_Dim] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master]  as destination ON [Bag_Dim].[destination_location_id] = destination.[id] LEFT JOIN ["+con_str.database_name+"].[dbo].[venue_location_master] as origin ON [Bag_Dim].[origin_location_id] = origin.[id]  LEFT JOIN ["+con_str.database_name+"].[dbo].[event_master] ON [Bag_Dim].[event_id] = [event_master].[id]  LEFT JOIN [it_cfg].[dbo].[Emp_Master] ON [Bag_Dim].[created_by_employee] = [Emp_Master].[emp_id] WHERE  [unid] = '" + bagUnid;

                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        pi.Bag_ID = rdr["bag_id"].ToString();
                        pi.Date = Convert.ToDateTime(rdr["created_date"].ToString()).ToString("G"); 
                        pi.Emp_ID = Convert.ToInt32(rdr["created_by_employee"].ToString());
                        pi.Origin_Name = rdr["origin_name"].ToString();
                        pi.Destination_Name = rdr["destination_name"].ToString(); 
                        pi.Event_Name = rdr["event_name"].ToString();

                        pi.Employee_name = rdr["emp_first_name"].ToString() + " " + rdr["emp_last_name"].ToString();
                       
                    }
                    rdr.Close();

                    conn.Close();


                    VAR.status = true;
                    VAR.PrintInformation = pi;
                    VertedaDiagnostics.LogMessage("GetPrintInformationWithBagID query completed", "I", false);
                }
            }
            catch (Exception e)
            {
                VAR.status = false;
                VAR.errorText = e.ToString();
                VertedaDiagnostics.LogMessage("Error in GetPrintInformationWithBagID: " + e.ToString(), "E", false);
            }
            return VAR;
        }


        //[WebMethod] //done
        //public VarianceActivityResponseBags GetPrintInformationWithBagID(string bagID)
        //{
        //    VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

        //    List<Bag> currentBags = new List<Bag>();


        //    SqlConnection conn = new SqlConnection();
        //    SqlCommand cmd = new SqlCommand();

        //    try
        //    {
        //        int eventid = GetEventID();
        //        if (eventid > 0)
        //        {

        //            conn.ConnectionString = GetConnectionString(2);

        //            if (conn.State != ConnectionState.Open)
        //            {
        //                try
        //                {
        //                    conn.Open();
        //                }
        //                catch (SqlException e)
        //                {
        //                    //error
        //                }
        //            }


        //            if (conn.State == ConnectionState.Open)
        //            {
        //                cmd.Connection = conn;

        


        //                SqlDataReader rdr = cmd.ExecuteReader();

        //                while (rdr.Read())
        //                {
        //                    Bag item = new Bag();
        //                    item.event_id = Convert.ToInt32(rdr["event_id"].ToString());
        //                    item.event_name = rdr["event_name"].ToString();
        //                    item.bag_type = rdr["bag_type"].ToString();
        //                    item.bag_id = rdr["bag_id"].ToString();
        //                    item.label_id = Convert.ToInt32(rdr["label_id"].ToString());
        //                    item.created_date = Convert.ToDateTime(rdr["created_date"].ToString());
        //                    item.employee_id = Convert.ToInt32(rdr["created_by_employee"].ToString());
        //                    item.location_id = Convert.ToInt32(rdr["profit_center_id"].ToString());
        //                    item.location_name = rdr["location_name"].ToString();
        //                    item.composition_id = Convert.ToInt32(rdr["composition_id"].ToString());
        //                    item.composition_name = rdr["Name"].ToString();
        //                    item.action_id = Convert.ToInt32(rdr["action_id"].ToString());
        //                    item.action_name = rdr["action"].ToString();
        //                    item.total_child_bags = Convert.ToInt32(rdr["total_child_bags"].ToString());


        //                    currentBags.Add(item);
        //                }
        //                rdr.Close();

        //                conn.Close();
        //            }

        //            VAR.status = true;
        //            VAR.Bags = currentBags;
        //            VertedaDiagnostics.LogMessage("GetExistingBags query completed", "I", false);


        //            if (currentBags.Count == 0)
        //            {
        //                VAR.MessageCode = 1;
        //                VAR.MessageText = constants.NoExistingBags;
        //                VertedaDiagnostics.LogMessage("No bags exist for the current event", "I", false);
        //            }
        //            else
        //            {
        //                int bagsWithActions = 0;
        //                foreach (Bag b in currentBags)
        //                {
        //                    if (b.action_id != -1)
        //                    {
        //                        bagsWithActions += 1;
        //                    }

        //                }

        //                if (bagsWithActions == 0)
        //                {
        //                    VAR.MessageCode = 2;
        //                    VAR.MessageText = constants.ExistingBagsNoAction;
        //                    VertedaDiagnostics.LogMessage("Bags exist for the current event, but don't have actions", "I", false);
        //                }
        //                else
        //                {
        //                    VAR.MessageCode = 3;
        //                    VAR.MessageText = constants.ExistingBagsWithAction;
        //                    VertedaDiagnostics.LogMessage("Bags exist for the current event, and have actions", "I", false);
        //                }

        //            }


        //        }
        //        else
        //        {
        //            //could not get eventid from RTS
        //            VAR.status = false;
        //            VAR.errorText = "could not get current eventid from RTS web service";
        //            VertedaDiagnostics.LogMessage("could not get current eventid from RTS web service", "E", false);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        VAR.status = false;
        //        VAR.errorText = e.ToString();
        //        VertedaDiagnostics.LogMessage("Error in GetExistingBags: " + e.ToString(), "E", false);
        //    }
        //    return VAR;
        //}



        #endregion

        #region utility: SQL connection strings and RTS calls
        //public static string GetConnectionString(int target)
        //{
        //    string connStr;
        //    //// get mysql connection details from config file. 

        //    // get mysql connection details from config file. 
        //    if (target == 1)
        //    {
        //        connStr = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
        //                         "172.27.28.69\\IGPOS440",
        //                         "sa",
        //                         "Pa$$word1",
        //                         "ig_dimension");
        //    }
        //    else if (target == 2)
        //    {
        //        connStr = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
        //                         "172.27.28.69\\Demonormandy",
        //                         "sa",
        //                         "Pa$$word1",
        //                         "variance_cash");

        //    }
        //    else if (target == 3)
        //    {
        //        connStr = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
        //                         "172.27.28.69\\Demonormandy",
        //                         "sa",
        //                         "Pa$$word1",
        //                         "verteda_primo_variance");

        //    }
        //    else if (target == 4)
        //    {
        //        connStr = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
        //                         "172.27.28.69\\IGPOS440",
        //                         "sa",
        //                         "Pa$$word1",
        //                         "it_cfg");
        //    }
        //    else
        //    {
        //        //add log / handling here
        //        return "error";
        //    }

        //    return connStr;
        //}

        public static ConnectionString GetConnectionString(int target)
        {
            ConnectionString VAR = new ConnectionString();

            ConfigSettings cs1 = new ConfigSettings();

            try
            {
                cs1 = ReadConfig();
            }
            catch (Exception ex)
            {
                VertedaDiagnostics.LogMessage("Error in GetConnectionString. Couldn't read config", "E", false);
            }

            if (cs1.ServerIP != null)
            {
                
                //// get mysql connection details from config file. 

                // get mysql connection details from config file. 
                if (target == 1)
                {
                    VAR.connection_string = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
                                     cs1.ServerIP, //"172.27.28.29\\yww",
                                     cs1.ServerUsername,
                                     cs1.ServerPassword,
                                     "ig_dimension");

                    VAR.database_name = "ig_dimension";
                }
                else if (target == 2)
                {
                    VAR.connection_string = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
                                     cs1.ServerIP, //"172.27.28.29\\yww",
                                     cs1.ServerUsername,
                                     cs1.ServerPassword,
                                     "variance_cash");

                    VAR.database_name = "variance_cash";
                }
                else if (target == 3)
                {
                    if (cs1.PrimoInstance == 2)
                    {
                        VAR.connection_string = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
                                         cs1.ServerIP, //"172.27.28.29\\yww",
                                         cs1.ServerUsername,
                                         cs1.ServerPassword,
                                         "verteda_primo_variance_hospitality");

                        VAR.database_name = "verteda_primo_variance_hospitality";
                    }
                    else //always default to 1
                    {
                        VAR.connection_string = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
                        cs1.ServerIP, //"172.27.28.29\\yww",
                        cs1.ServerUsername,
                        cs1.ServerPassword,
                        "verteda_primo_variance_public");

                        VAR.database_name = "verteda_primo_variance_public";
                    }

                }
                else if (target == 4)
                {
                    VAR.connection_string = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
                                     cs1.ServerIP, //"172.27.28.29\\yww",
                                     cs1.ServerUsername,
                                     cs1.ServerPassword,
                                     "it_cfg");

                    VAR.database_name = "it_cfg";
                }
                else
                {
                    VertedaDiagnostics.LogMessage("Error in GetConnectionString. Invalid target", "I", false);
                    return VAR;
                }

                VAR.database_instance = cs1.PrimoInstance;
            }
            return VAR;
        }

        private static ConfigSettings ReadConfig()
        {
            //VertedaDiagnostics.LogMessage("ReadConfig started", "I", false);

            ConfigSettings myResponse = new ConfigSettings();

            try
            {

                String path = HttpContext.Current.Server.MapPath("~/"); //Server.MapPath(".");               


                //string path = "C:\\Variance";

                XmlDocument doc = new XmlDocument();
                //doc.Load(path + "\\primo_services\\system_config\\primo_stock_management_config.xml");
                doc.Load(path + "\\VarianceCashConfig\\VarianceConfig.xml");

                

                XmlNodeList nodeList = doc.SelectNodes("//CashOfficeID");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.CashOfficeID = Convert.ToInt32(node.InnerText);
                }

                nodeList = doc.SelectNodes("//ServerIP");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.ServerIP = node.InnerText;
                }

                nodeList = doc.SelectNodes("//ServerUsername");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.ServerUsername = node.InnerText;
                }

                nodeList = doc.SelectNodes("//ServerPassword");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.ServerPassword = node.InnerText;
                }

                nodeList = doc.SelectNodes("//ValidEmployees");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.ValidEmployees = node.InnerText;
                }
                nodeList = doc.SelectNodes("//WebServiceAddress");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.WebServiceAddress = node.InnerText;
                }
                nodeList = doc.SelectNodes("//CashTenderID");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.CashTenderID = Convert.ToInt32(node.InnerText);
                }
                nodeList = doc.SelectNodes("//CashOfficeTerminalID");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.CashOfficeTerminalID = Convert.ToInt32(node.InnerText);
                }
                nodeList = doc.SelectNodes("//PrimoInstance");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.PrimoInstance = Convert.ToInt32(node.InnerText);
                }
                nodeList = doc.SelectNodes("//CashOfficeName");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.CashOfficeName = node.InnerText;
                }
                nodeList = doc.SelectNodes("//CashOfficeTerminalName");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.CashOfficeTerminalName = node.InnerText;
                }


            }
            catch (Exception yex)
            {
                VertedaDiagnostics.LogMessage("Error in ReadConfig. Couldn't read config", "E", false);
            }
            return myResponse;
        }


        public static PrimoRTS.RTSSectionDetail[] GetAllSections()
        {
            PrimoRTS.RTSSectionDetail[] SectionLocations = null;
            VertedaDiagnostics.LogMessage("GetAllSections started", "I", false);

            try
            {
                ConfigSettings cs1 = new ConfigSettings();

                try
                {
                    cs1 = ReadConfig();
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in GetEmpDimID. Couldn't read config", "E", false);
                }


                PrimoRTS.Service1 RTSservice = new PrimoRTS.Service1();

                RTSservice.Url = cs1.WebServiceAddress;
                

                PrimoRTS.RTSSectionDetail[] sqlSectionLocations = RTSservice.GetAllLocationsBySection();
                PrimoRTS.RTSGetSectionListResponse Sections = RTSservice.GetSectionsList();

                SectionLocations = sqlSectionLocations;
                VertedaDiagnostics.LogMessage("GetAllSections finished.", "I", false);
            }
            catch(Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in GetAllSections: " + e.ToString(), "E", false);
                
            }

            return SectionLocations;
        }

        public static int GetEventID()
        {
            VertedaDiagnostics.LogMessage("GetEventID started", "I", false);
            int EventID = -1;

            try
            {
                ConfigSettings cs1 = new ConfigSettings();

                try
                {
                    cs1 = ReadConfig();
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in GetEmpDimID. Couldn't read config", "E", false);
                }


                PrimoRTS.Service1 RTSservice = new PrimoRTS.Service1();

                RTSservice.Url = cs1.WebServiceAddress;

                PrimoRTS.RTSEvent RTSEvent = RTSservice.GetCurrentEvent();

                EventID = RTSEvent.EventID;
                VertedaDiagnostics.LogMessage("GetEventID finished. Event ID: "+EventID, "I", false);
            }
            catch(Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in GetEventID: " + e.ToString(), "E", false);
            }

            return EventID;
        }

        public static int GetEventIDForCurrentBusinessDay()
        {
            VertedaDiagnostics.LogMessage("GetEventIDForCurrentBusinessDay started", "I", false);
            int EventID = -1;

            try
            {
                ConfigSettings cs1 = new ConfigSettings();

                try
                {
                    cs1 = ReadConfig();
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in GetEmpDimID. Couldn't read config", "E", false);
                }


                PrimoRTS.Service1 RTSservice = new PrimoRTS.Service1();

                RTSservice.Url = cs1.WebServiceAddress;

                PrimoRTS.RTSEvent RTSEvent = RTSservice.GetEventForCurrentBusinessDay();

                EventID = RTSEvent.EventID;
                VertedaDiagnostics.LogMessage("GetEventIDForCurrentBusinessDay finished. EventID: "+EventID, "I", false);
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in GetEventIDForCurrentBusinessDay: " + e.ToString(), "E", false);
            }

            return EventID;
        }


        public static int GetEmpDimID(int employee_id)
        {
            int EmpID = -1;
            VertedaDiagnostics.LogMessage("GetEmpDimID started. input employee : " + employee_id, "I", false);

            try
            {
                ConfigSettings cs1 = new ConfigSettings();

                try
                {
                    cs1 = ReadConfig();
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in GetEmpDimID. Couldn't read config", "E", false);
                }


                PrimoRTS.Service1 RTSservice = new PrimoRTS.Service1();

                RTSservice.Url = cs1.WebServiceAddress;

                EmpID = RTSservice.VariancePrimo_Convert_EmpIDToEmp_Dim_Id_today(employee_id);
                VertedaDiagnostics.LogMessage("GetEmpDimID finished. output employee: " + EmpID, "I", false);

            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in GetEmpDimID" + employee_id + " " + EmpID, "E", false);
            }

            return EmpID;
        }
        
        public static int GetLocDimID(int location_id)
        {
            int LocID = -1;
            VertedaDiagnostics.LogMessage("GetLocDimID started. input location : " + location_id, "I", false);
            try
            {
                ConfigSettings cs1 = new ConfigSettings();

                try
                {
                    cs1 = ReadConfig();
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in GetLocDimID. Couldn't read config", "E", false);
                }

                PrimoRTS.Service1 RTSservice = new PrimoRTS.Service1();

                RTSservice.Url = cs1.WebServiceAddress;

                LocID = RTSservice.VariancePrimo_Convert_ProfitCenterIDToProfit_Center_Dim_Id_today(location_id);
                VertedaDiagnostics.LogMessage("GetLocDimID finished. output location : " + LocID, "I", false);
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in GetLocDimID" + location_id + " " + LocID, "E", false);
            }

            return LocID;
        }
        
        public static int GetTermDimID(int terminal_id)
        {
            int termID = -1;
            VertedaDiagnostics.LogMessage("GetTermDimID started. input terminal : " + terminal_id, "I", false);
            try
            {
                ConfigSettings cs1 = new ConfigSettings();

                try
                {
                    cs1 = ReadConfig();
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in GetTermDimID. Couldn't read config", "E", false);
                }

                PrimoRTS.Service1 RTSservice = new PrimoRTS.Service1();

                RTSservice.Url = cs1.WebServiceAddress;


                termID = RTSservice.VariancePrimo_Convert_TerminalIDToTerminal_Dim_Id_today(terminal_id);
                VertedaDiagnostics.LogMessage("GetTermDimID finished. output terminal : " + termID, "I", false);

            }
            catch (Exception e)
            {
                
                VertedaDiagnostics.LogMessage("Error in GetTermDimID" + terminal_id + " " + termID, "E", false);
                VertedaDiagnostics.LogMessage(e.Message,"E",false);
                VertedaDiagnostics.LogMessage(e.StackTrace, "E", false);
            }

            return termID;
        }
        
        public static int GetCurrentBusinessPeriodID()
        {
            int businessPeriodID = -1;
            VertedaDiagnostics.LogMessage("GetTermDimID started", "I", false);
            try
            {
                ConfigSettings cs1 = new ConfigSettings();

                try
                {
                    cs1 = ReadConfig();
                }
                catch (Exception ex)
                {
                    VertedaDiagnostics.LogMessage("Error in GetCurrentBusinessPeriodID. Couldn't read config", "E", false);
                }

                PrimoRTS.Service1 RTSservice = new PrimoRTS.Service1();

                RTSservice.Url = cs1.WebServiceAddress;

                businessPeriodID = Convert.ToInt32(RTSservice.VariancePrimo_GetCurrentBusinessDayDIMID());
                VertedaDiagnostics.LogMessage("GetTermDimID finished. business period: "+businessPeriodID, "I", false);

            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in GetCurrentBusinessPeriodID" + businessPeriodID , "E", false);
            }

            return businessPeriodID;
        }


        public bool VerifyJobCode(Employees emp)
        {
            VertedaDiagnostics.LogMessage("VerifyJobCode started. employee: "+emp.Employee_ID, "I", false);
            try
            {
                List<string> jobcodes = new List<string>();
                //String[] csv = File.ReadAllLines("C:\\Variance\\ValidJobCodes.csv");

                //foreach (string csvrow in csv)
                //{
                //    var fields = csvrow.Split(','); // csv delimiter
                //    jobcodes.AddRange(fields.ToList());
                //}

                ConfigSettings cs1 = new ConfigSettings();

                cs1 = ReadConfig();
                var fields = cs1.ValidEmployees.Split(',');
                jobcodes.AddRange(fields.ToList());
                VertedaDiagnostics.LogMessage("VerifyJobCode. job codes from config: " + cs1.ValidEmployees, "I", false);



                bool exists = jobcodes.Contains(emp.Job_Code_ID.ToString());

                if (exists)
                {
                    VertedaDiagnostics.LogMessage("VerifyJobCode. employee valid: " + emp.Employee_ID, "I", false);
                    return true;
                }
                else
                {
                    VertedaDiagnostics.LogMessage("VerifyJobCode. employee invalid: " + emp.Employee_ID, "E", false);
                    return false;
                }
            }
            catch (Exception e)
            {
                VertedaDiagnostics.LogMessage("Error in verifyJobCode. : " + e.ToString(), "E", false);
                return false;
            }

            return false;
        }

        [WebMethod]
        public VarianceActivityResponseEmployee ValidateEmployee(int employeeID)
        {
            VertedaDiagnostics.LogMessage("ValidateEmployee started " +  employeeID , "I", false);
            VarianceActivityResponseEmployee VAR = new VarianceActivityResponseEmployee();

            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            List<Employees> all_employee_records = new List<Employees>();

            try
            {
                ConnectionString con_str = GetConnectionString(1);
                conn.ConnectionString = con_str.connection_string;

                if (conn.State != ConnectionState.Open)
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (SqlException e)
                    {
                        //error
                    }
                }

                if (conn.State == ConnectionState.Open)
                {
                    cmd.Connection = conn;

                    //cmd.CommandText = "SELECT [emp_id], ISNULL ([emp_name], 'NULL') AS [emp_name], ISNULL ([primary_job_code_id],  -1) AS [primary_job_code_id], ISNULL ([primary_job_code_name],  'NULL') AS [primary_job_code_name] FROM [ig_dimension].[dbo].[Employee_Dimension]";// LOAD DENOMINATIONS
                    cmd.CommandText = "SELECT [Emp_Master].[emp_first_name], emp_last_name,[Emp_Jobcode_Join].[emp_id], ISNULL([jobcode_id], -1) AS [jobcode_id] FROM [it_cfg].[dbo].[Emp_Jobcode_Join]   LEFT JOIN [it_cfg].[dbo].[Emp_Master] ON [Emp_Jobcode_Join].[emp_id] = [Emp_Master].[emp_id] WHERE [Emp_Jobcode_Join].[emp_id] = " + employeeID;


                    SqlDataReader rdr = cmd.ExecuteReader();
                    
                    while (rdr.Read())
                    {
                        Employees EMP = new Employees();

                        EMP.Employee_ID = Convert.ToInt32(rdr["emp_id"].ToString()); //emp_dim_id
                        EMP.Job_Code_ID = Convert.ToInt32(rdr["jobcode_id"].ToString());
                        EMP.Employee_firstname = rdr["emp_first_name"].ToString();
                        EMP.Employee_surname = rdr["emp_last_name"].ToString();

                        all_employee_records.Add(EMP);
                    }
                    rdr.Close();

                    conn.Close();
                }

                VAR.status = false; //false until a valid record found

                foreach (Employees empl in all_employee_records)
                {
                    bool valid = VerifyJobCode(empl);

                    if (valid)
                    {
                        VAR.status = true;
                        VertedaDiagnostics.LogMessage("EmployeeID :" + employeeID + "Auth Status: " + VAR.status.ToString(), "I", false);
                        VAR.Employee = empl;
                        break;
                    }
                }

            }
            catch (Exception e)
            {
                VAR.errorText = e.ToString();
                VAR.status = false;
                VertedaDiagnostics.LogMessage("EmployeeID :" + employeeID + "Error in ValidateEmployee: " + e.ToString(), "E", false);
            }

            VertedaDiagnostics.LogMessage("ValidateEmployee finished " + employeeID + " " + VAR.status.ToString() , "I", false);
            return VAR;
        }
                   


        #endregion

        #region unused methods


        //[WebMethod]
        //public VarianceActivityResponse AdvanceTerminalBagActionWithLabelID(BagAction UpdateBagAction, string labelID) //needs to include bagID, actionID, 2 x employeeID, value. Optional terminal ID / profit ID
        //{
        //    VarianceActivityResponse VAR = new VarianceActivityResponse();

        //    try
        //    {
        //        //get a terminal bag ID if possible
        //        VarianceActivityResponseBagID VAR2 = GetTerminalBagIDfromKioskLabelID(labelID);

        //        if (VAR2.status != false)
        //        {
        //            //set bagID as returned value
        //            UpdateBagAction.Bag_ID = VAR2.BagID;

        //            //get current bag action
        //            VarianceActivityResponseBags VAR1 = GetSingleBagInfo(UpdateBagAction.Bag_ID);

        //            if (VAR1.status != false)
        //            {
        //                //if its not 5 (at team leader outbound) or 9 (at safe inbound), add 1
        //                if (VAR1.Bags[0].action_id != 4 && VAR1.Bags[0].action_id != 9 && VAR1.Bags[0].action_id != 10)
        //                {
        //                    //call setbagaction with new number
        //                    int newAction = VAR1.Bags[0].action_id + 1;
        //                    UpdateBagAction.Action_ID = newAction;

        //                    SetBagAction(UpdateBagAction);

        //                    VAR.status = true;
        //                }
        //                else
        //                {
        //                    //else error
        //                    //this bag can not be advanced
        //                    VAR.status = false;
        //                    VAR.errorText = "Bag " + UpdateBagAction.Bag_ID + " cannot be advanced further";
        //                    VertedaDiagnostics.LogMessage("Bag " + UpdateBagAction.Bag_ID + " cannot be advanced further", "E", false);
        //                }
        //            }
        //            else
        //            {
        //                //could not find the bag specified
        //                VAR.status = false;
        //                VAR.errorText = "Bag " + UpdateBagAction.Bag_ID + " could not be found";
        //                VertedaDiagnostics.LogMessage("Bag " + UpdateBagAction.Bag_ID + "  could not be found", "E", false);
        //            }
        //        }
        //        else
        //        {
        //            VAR.status = false;
        //            VAR.errorText = VAR2.errorText;
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }

        //    return VAR;
        //}
        

        //get bag actions unlikely to be required as information now comes from GetExistingBags

        //[WebMethod]
        //public VarianceActivityResponseBag GetBagActions()
        //{
        //    VarianceActivityResponseBag VAR = new VarianceActivityResponseBag();

        //    List<Bag> lcb = new List<Bag>();

        //    SqlConnection conn = new SqlConnection();
        //    SqlCommand cmd = new SqlCommand();

        //    try
        //    {
        //        int eventid = GetEventID();
        //        if (eventid > 0)
        //        {
        //            conn.ConnectionString = GetConnectionString(2);

        //            if (conn.State != ConnectionState.Open)
        //            {
        //                try
        //                {
        //                    conn.Open();
        //                }
        //                catch (SqlException e)
        //                {
        //                    //error
        //                }
        //            }

        //            if (conn.State == ConnectionState.Open)
        //            {
        //                cmd.Connection = conn;

        

        //                SqlDataReader rdr = cmd.ExecuteReader();

        //                while (rdr.Read())
        //                {
        //                    // if fields queried from database increases, update string array size.
        //                    string[] rowValues = new string[2];
        //                    for (int i = 0; i < rdr.FieldCount; i++)
        //                    {
        //                        if (rdr.IsDBNull(i))
        //                        {
        //                            rowValues[i] = "NULL"; // LOG THAT VALUE IS NULL
        //                        }
        //                        else
        //                            rowValues[i] = rdr.GetValue(i).ToString();
        //                    }

        //                    Bag item = new Bag();
        //                    item.bag_id = rowValues[0];
        //                    item.event_id = Convert.ToInt32(rowValues[1]);

        //                    lcb.Add(item);
        //                }
        //                rdr.Close();

        //                conn.Close();

        //                VAR.status = true;
        //                VAR.Bags = lcb;
        //            }
        //        }
        //        else
        //        {
        //            //could not get eventid from RTS
        //            VAR.status = false;
        //            VAR.errorText = "could not get current eventid from RTS web service";
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        VAR.status = false;
        //        VAR.errorText = e.ToString();
        //    }

        //    return VAR;
        //}








        //[WebMethod]
        //public List<Denominations> LoadExistingBag(int compID)
        //{
        //    List<Denominations> denom_list = new List<Denominations>();
        //    SqlConnection conn = new SqlConnection(GetConnectionString(2));

        //    if (conn.State != ConnectionState.Open)
        //        try
        //        {
        //            conn.Open();
        //        }
        //        catch (SqlException e)
        //        {

        //        }

        //    if (conn.State == ConnectionState.Open)
        //    {
        //        SqlCommand cmd = new SqlCommand("SELECT [denomination_id],[denomination_qty]  FROM [variance_cash].[dbo].[Composition_Denomination_Join]", conn);
        //        SqlDataReader rdr = cmd.ExecuteReader();

        //        while (rdr.Read())
        //        {
        //            // if fields queried from database increases, update string array size.
        //            int[] rowValues = new int[2];
        //            for (int i = 0; i < rdr.FieldCount; i++)
        //            {
        //                if (rdr.IsDBNull(i))
        //                {
        //                    rowValues[i] = -1; // LOG THAT VALUE IS NULL
        //                }
        //                else
        //                    rowValues[i] = Convert.ToInt32(rdr.GetValue(i).ToString());
        //            }

        //            Denominations item = new Denominations();
        //            item.denomination_id = rowValues[0];
        //            item.denomination_qty = rowValues[1];

        //            denom_list.Add(item);

        //        }
        //        rdr.Close();
        //        conn.Close();
        //    }
        //    return denom_list;
        //}


        //[WebMethod] redundant now that GetExistingBags returns actions
        //public VarianceActivityResponse DoesEventExist(int eventid, int employeeID)
        //{
        //    VarianceActivityResponse VAR = new VarianceActivityResponse();

        //    List<Bag> currentBags = GetExistingBags(eventid);

        //    try
        //    {
        //        if (currentBags.Count == 0)
        //        {
        //            //no records exist, create new.
        //            CreateNewBags(eventid, employeeID);
        //            VAR.status = true;

        //            //MessageBox.Show("Bags have been created for event " + eventid);
        //        }
        //        else
        //        {
        //            //return whether the existing bags have acitons against them
        //                VAR.status = false;
        //                VAR.ID = CheckBagActions(eventid).Count;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        VAR.status = false;
        //        VAR.errorText = e.ToString();
        //    }
        //    return VAR;
        //}

        //[WebMethod] redundant too
        //public VarianceActivityResponse DeleteBagsandInsertNew(int eventid, int employeeID)
        //{
        //    VarianceActivityResponse VAR = new VarianceActivityResponse();

        //    try
        //    {
        //        //delete current rows, then recall method to make new ones
        //        DeleteBags(eventid);
        //        DoesEventExist(eventid, employeeID);
        //        VAR.status = true;
        //    }
        //    catch (Exception e)
        //    {
        //        VAR.status = false;
        //        VAR.errorText = e.ToString();
        //    }
        //    return VAR;
        //}
          

        #endregion

        #region test methods

        [WebMethod]
        public VarianceActivityResponse TESTSetBulkBagActionForDefaults()
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                List<BagAction> lba = new List<BagAction>();
                BagAction ba = new BagAction();
                ba.Location_ID = 1;
                ba.Terminal_ID = 1;
                ba.Sending_Employee_ID = 1;
                ba.Receiving_Employee_ID = 1;
                ba.Bag_Unid = 15818;

                ba.label_id = "K15818";
                ba.Action_ID = 3;

                lba.Add(ba);

                SetBulkBagActionForDefaults(lba);


                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertMultipleLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }

            return VAR;
        }
        


        [WebMethod]
        public VarianceActivityResponse TESTAdvanceBagActionWithLabelID()
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                BagAction ba = new BagAction();
                ba.Location_ID = 3;
                ba.Terminal_ID = 28;
                ba.Sending_Employee_ID = 1;
                ba.Receiving_Employee_ID = 1;
                ba.Bag_Count = 1;

                ba.label_id = "K7250";

                AdvanceBagActionWithLabelID(ba);


                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertMultipleLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }

            return VAR;
        }


        [WebMethod]
        public VarianceActivityResponse TESTInsertMultipleLocationDefaultCompositions()
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                List<LocationAndTerminal> LocList = new List<LocationAndTerminal>();

                LocationAndTerminal l1 = new LocationAndTerminal();
                l1.composition_id = 1;
                l1.location_id = 1;
                LocList.Add(l1);

                LocationAndTerminal l2 = new LocationAndTerminal();
                l2.composition_id = 2;
                l2.location_id = 2;
                LocList.Add(l2);

                LocationAndTerminal l3 = new LocationAndTerminal();
                l3.composition_id = 3;
                l3.location_id = 3;
                LocList.Add(l3);

                InsertMultipleLocationDefaultCompositions(LocList);

                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertMultipleLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }

            return VAR;
        }

                [WebMethod]
        public VarianceActivityResponse TESTManageMultipleLocationDefaultCompositions()
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                List<LocationAndTerminal> LocList = new List<LocationAndTerminal>();

                LocationAndTerminal l4 = new LocationAndTerminal();
                l4.composition_id = 4;
                l4.location_id = 4;
                LocList.Add(l4);

                LocationAndTerminal l2 = new LocationAndTerminal();
                l2.composition_id = 5;
                l2.location_id = 2;
                LocList.Add(l2);

                LocationAndTerminal l3 = new LocationAndTerminal();
                l3.composition_id = 3;
                l3.location_id = 3;
                LocList.Add(l3);

                ManageMultipleLocationDefaultCompositions(LocList);

                VAR.status = true;
                VertedaDiagnostics.LogMessage("TESTManageMultipleLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in TESTManageMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }

            return VAR;
        }

       [WebMethod]
       public VarianceActivityResponse TESTsetupbagsforspecificevent()
                {
                    VarianceActivityResponse VAR = new VarianceActivityResponse();

                    try
                    {
                        //VarianceActivityResponseCompositionLocationJoins var1 = Get_Location_Default_Compositions();

                        List<LocationAndTerminal> laList = new List<LocationAndTerminal>();

                        LocationAndTerminal la1 = new LocationAndTerminal();

                        la1.location_id = 111;
                        la1.location_name = "Naboo";
                        la1.composition_id = 1;
                        la1.composition_name = "1";
                        la1.terminal_id = 1;
                        la1.terminal_name = "Naboo 1";
                        la1.bag_id = "K100001";

                        laList.Add(la1);

                        LocationAndTerminal la2 = new LocationAndTerminal();

                        la2.location_id = 113;
                        la2.location_name = "Tatooine";
                        la2.composition_id = 1;
                        la2.composition_name = "1";
                        la2.terminal_id = 1;
                        la2.terminal_name = "Tatooine 1";
                        la2.bag_id = "K100002";

                        laList.Add(la2);

                        LocationAndTerminal la3 = new LocationAndTerminal();

                        la3.location_id = 110;
                        la3.location_name = "Hoth";
                        la3.composition_id = 1;
                        la3.composition_name = "1";
                        la3.terminal_id = 1;
                        la3.terminal_name = "Hoth 1";
                        la3.bag_id = "K100003";

                        laList.Add(la3);


                        VarianceActivityResponseBags var2 = SetupEventCustomBagsForSpecififcEventID(1, false, laList, 555);
                        
                    }
                    catch (Exception ex)
                    {
                        VAR.status = false;
                        VAR.errorText = ex.ToString();
                        VertedaDiagnostics.LogMessage("Error in TESTsetupbagsforspecificevent: " + ex.ToString(), "E", false);
                    }

                    return VAR;
                }

       [WebMethod]
       public VarianceActivityResponse TESTSetBagActionWithLabelIdLabelID()
       {
           VarianceActivityResponse VAR = new VarianceActivityResponse();

           try
           {
               BagAction ba = new BagAction();
               ba.Location_ID = 66;
               ba.Terminal_ID = 3055;
               ba.Sending_Employee_ID = 1;
               ba.Receiving_Employee_ID = 1;
               ba.Bag_Count = 5;
               ba.label_id = "K9811";
               ba.Action_ID = 3;

               SetBagActionWithLabelID(ba);
               
               VAR.status = true;
               VertedaDiagnostics.LogMessage("InsertMultipleLocationDefaultCompositions Completed successfully", "I", false);
           }
           catch (Exception ex)
           {
               VAR.status = false;
               VAR.errorText = ex.ToString();
               VertedaDiagnostics.LogMessage("Error in InsertMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
           }

           return VAR;
       }

       [WebMethod]
       public VarianceActivityResponse TESTActivityOnSingleBag(int mode, int terminal_id, int location_id)
       {
           VarianceActivityResponse VAR = new VarianceActivityResponse();

           try
           {
               //ActivityOnSingleBag(mode, 3055, 2162, 66);
               ActivityOnSingleBag(mode, terminal_id, 1, location_id);

               VAR.status = true;
               VertedaDiagnostics.LogMessage("TESTActivityOnSingleBag Completed successfully", "I", false);
           }
           catch (Exception ex)
           {
               VAR.status = false;
               VAR.errorText = ex.ToString();
               VertedaDiagnostics.LogMessage("Error in TESTActivityOnSingleBag: " + ex.ToString(), "E", false);
           }

           return VAR;
       }

       [WebMethod]
       public VarianceActivityResponse TESTSetBagAction(int actionid)
       {
           VarianceActivityResponse VAR = new VarianceActivityResponse();

           try
           {
               //cmd.Parameters.Add("@actionid", SqlDbType.Int).Value = UpdateBagAction.Action_ID;
               //cmd.Parameters.Add("@send_employeeid", SqlDbType.Int).Value = UpdateBagAction.Sending_Employee_ID;
               //cmd.Parameters.Add("@rec_employeeid", SqlDbType.Int).Value = UpdateBagAction.Receiving_Employee_ID;
               //cmd.Parameters.Add("@terminalid", SqlDbType.Int).Value = UpdateBagAction.Terminal_ID;
               //cmd.Parameters.Add("@locationid", SqlDbType.Int).Value = UpdateBagAction.Location_ID;
               //cmd.Parameters.Add("@bag_unid", SqlDbType.VarChar).Value = UpdateBagAction.Bag_Unid;
               //cmd.Parameters.Add("@totalvalue", SqlDbType.Decimal).Value = UpdateBagAction.Total_Value;
               //cmd.Parameters.Add("@bagcount", SqlDbType.Int).Value = UpdateBagAction.Bag_Count;
               //cmd.Parameters.Add("@sender_name", SqlDbType.VarChar).Value = UpdateBagAction.sender_name;
               //cmd.Parameters.Add("@reciever_name", SqlDbType.VarChar).Value = UpdateBagAction.reciever_name;

               BagAction ba = new BagAction();
               ba.Location_ID = 1;
               ba.Terminal_ID = 9998;
               ba.Sending_Employee_ID = 1;
               ba.Receiving_Employee_ID = 1;
               ba.Bag_Count = 5;
               ba.label_id = "";
               ba.Action_ID = actionid;
               ba.Bag_Unid = 19813;
               ba.Total_Value = 0;
               ba.sender_name = "Anomitra Dey";
               ba.reciever_name = "Anomitra Dey";               

               SetBagAction(ba);
               //SetBagAction(ba2);

               VAR.status = true;
               VertedaDiagnostics.LogMessage("InsertMultipleLocationDefaultCompositions Completed successfully", "I", false);
           }
           catch (Exception ex)
           {
               VAR.status = false;
               VAR.errorText = ex.ToString();
               VertedaDiagnostics.LogMessage("Error in InsertMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
           }

           return VAR;
       }

        [WebMethod]
        public VarianceActivityResponse TESTAcceptKioskFloatBagIntoKiosk(int actionid)
        {
            VarianceActivityResponse VAR = new VarianceActivityResponse();

            try
            {
                BagAction ba = new BagAction();
                ba.Location_ID = 83;
                ba.Terminal_ID = 4304;
                ba.Sending_Employee_ID = 1;
                ba.Receiving_Employee_ID = 1;
                ba.Action_ID = 4;
                ba.Total_Value = 0;

                ba.Bag_Unid = 0;
                ba.Total_Value = 0;
                ba.Bag_Count = 0;
                ba.status_id = 0;

                AcceptKioskFloatBagIntoKiosk(ba);
                //SetBagAction(ba2);

                VAR.status = true;
                VertedaDiagnostics.LogMessage("InsertMultipleLocationDefaultCompositions Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in InsertMultipleLocationDefaultCompositions: " + ex.ToString(), "E", false);
            }

            return VAR;
        }


        [WebMethod]
       public VarianceActivityResponseBags TESTGetBagDetails(int bag_unid, string bag_label, int from_location_id, int from_terminal_id, int event_id, int created_by_employee, int action_id, bool with_cash_variance, bool with_bag_count_variance, bool terminal_bags_only, bool kiosk_bags_only, bool outbound_bags_only, bool inbound_bags_only, bool from_all_events, int parent_id, int at_action)
       {
           VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

           try
           {
               BagOperators op = new BagOperators();

               op.bag_unid = bag_unid; 
               op.bag_label = bag_label;
               op.from_location_id = from_location_id;
               op.from_terminal_id = from_terminal_id;
               op.event_id = event_id;
               op.created_by_employee = created_by_employee;
               op.action_id = action_id;
               op.with_cash_variance = with_cash_variance;
               op.with_bag_count_variance = with_bag_count_variance;
               op.terminal_bags_only = terminal_bags_only;
               op.kiosk_bags_only = kiosk_bags_only;
               op.outbound_bags_only = outbound_bags_only;
               op.inbound_bags_only = inbound_bags_only;
               op.from_all_events = from_all_events;
                op.parent_id = parent_id;
                op.up_to_action = at_action;

               VarianceActivityResponseBags var1 = GetBagDetails(op);
 
               VAR.status = true;
               VAR.Bags = var1.Bags;
               VertedaDiagnostics.LogMessage("TESTGetBagDetails Completed successfully", "I", false);
           }
           catch (Exception ex)
           {
               VAR.status = false;
               VAR.errorText = ex.ToString();
               VertedaDiagnostics.LogMessage("Error in TESTGetBagDetails: " + ex.ToString(), "E", false);
           }

           return VAR;
       }


        [WebMethod]
        public VarianceActivityResponseBags TESTGetBagDetailsWithMultipleBagUnids()
        {
            VarianceActivityResponseBags VAR = new VarianceActivityResponseBags();

            try
            {
                List<int> op = new List<int>();

                op.Add(17047);
                op.Add(17048);
                op.Add(17049);

                VarianceActivityResponseBags var1 = GetBagDetailsWithMultipleBagUnids(op);

                VAR.status = true;
                VAR.Bags = var1.Bags;
                VertedaDiagnostics.LogMessage("TESTGetBagDetailsWithMultipleBagUnids Completed successfully", "I", false);
            }
            catch (Exception ex)
            {
                VAR.status = false;
                VAR.errorText = ex.ToString();
                VertedaDiagnostics.LogMessage("Error in TESTGetBagDetailsWithMultipleBagUnids: " + ex.ToString(), "E", false);
            }

            return VAR;
        }



        [WebMethod]
        public int Test_GetEventID()
        {
            int eventID = 0;

            eventID = GetEventID();

            return eventID;
        }


        #endregion


        #region Man City Paradox Wrappers

        //assign labelID = location name

        //Determine the unid of bag thats arrived

        [WebMethod] //done
       public VarianceActivityResponseBagUNID GetProbableKioskBagUnid(int destination_id) //works out what the most likely kiosk bag unid being checked in is.
       {
           VarianceActivityResponseBagUNID VAR = new VarianceActivityResponseBagUNID();


           SqlConnection conn = new SqlConnection();
           SqlCommand cmd = new SqlCommand();

           try
           {
               int eventid = GetEventID(); // GetEventIDForCurrentBusinessDay();
               if (eventid > 0)
               {
                    ConnectionString con_str = GetConnectionString(2);
                    conn.ConnectionString = con_str.connection_string;

                    int sqlBagUnid = -1;

                   if (conn.State != ConnectionState.Open)
                   {
                       try
                       {
                           conn.Open();
                       }
                       catch (SqlException e)
                       {
                           //error
                       }
                   }


                   if (conn.State == ConnectionState.Open)
                   {
                       cmd.Connection = conn;

                       //order by desc. Most recent bags will be at top, oldest at bottom. the Last unid read out and returned will be the oldest, i.e. most likely to be delivered. 
                       cmd.CommandText = "SELECT [Bag_Action_Join].[bag_unid] FROM [variance_cash].[dbo].[Bag_Action_Join] LEFT JOIN [variance_cash].[dbo].[Bag_Dim]  ON [Bag_Action_Join].[bag_unid] = [Bag_Dim].[unid]  WHERE destination_location_id = " + destination_id + " AND event_id = " + eventid + " AND [Bag_Action_Join].[action_id] = 3  AND [bag_function_id] IN (1,2,3,4)  ORDER BY created_date DESC";


                       SqlDataReader rdr = cmd.ExecuteReader();

                       while (rdr.Read())
                       {
                           sqlBagUnid = Convert.ToInt32(rdr["bag_unid"].ToString());
                       }
                       rdr.Close();

                       conn.Close();


                       VAR.status = true;
                       VAR.BagUNID = sqlBagUnid;
                       VertedaDiagnostics.LogMessage("GetProbabaleKioskBagUnid query completed", "I", false);
                   }
               }
               else
               {
                   VAR.status = false;
                   VertedaDiagnostics.LogMessage("Error in GetProbabaleKioskBagUnid. Could not get eventid ", "E", false);
               }
           }
           catch (Exception e)
           {
               VAR.status = false;
               VAR.errorText = e.ToString();
               VertedaDiagnostics.LogMessage("Error in GetProbabaleKioskBagUnid: " + e.ToString(), "E", false);
           }
           return VAR;
       }
                
        //Determine the labelID of the bag thats arrived


       // //get list of potential returning kiosk bags
       //[WebMethod] //done
       //public VarianceActivityResponseSectionDetails GetSectionDetailsAndIncomingBags(int destination_id) //works out what the most likely kiosk bag unid being checked in is.
       //{
       //    VarianceActivityResponseSectionDetails VAR = new VarianceActivityResponseSectionDetails();
       //    try
       //    {
       //        //call a method to get sections, and the locations in each section
       //        PrimoRTS.RTSSectionDetail[] Sections = GetAllSections();


       //        VarianceActivityResponseBags var1 = GetExistingKioskBags(3); //get all inbound kiosk bags

       //        //for each section
       //         //for each location in that section, grab the bags that belong to you using locationID
       //         //if no bags to grab, insert a blank row





       //        //for each section
       //        //check each location in that section to find any kiosk bags available to be checked in

       //    }
       //    catch (Exception e)
       //    {
       //        VAR.status = false;
       //        VAR.errorText = e.ToString();
       //        VertedaDiagnostics.LogMessage("Error in GetSectionDetailsAndIncomingBags: " + e.ToString(), "E", false);
       //    }
       //    return VAR;
       //}
              
            


        #endregion

    }

    #region Classes
    public class VarianceActivityResponse
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _id = 0;
        public int ID { get { return _id; } set { _id = value; } }
    }

    public class VarianceActivityResponseErrorLog
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _id = 0;
        public int ID { get { return _id; } set { _id = value; } }

        private List<ErrorLog> _errorlogs = new List<ErrorLog>();
        public List<ErrorLog> ErrorLogs { get { return _errorlogs; } set { _errorlogs = value; } }
    }


    public class VarianceActivityResponseCashDrop
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _id = 0;
        public int ID { get { return _id; } set { _id = value; } }

        private List<CashDrop> _cashdrops = new List<CashDrop>();
        public List<CashDrop> CashDrops { get { return _cashdrops; } set { _cashdrops = value; } }
    }

    public class VarianceActivityResponseBagUNID
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _id = 0;
        public int ID { get { return _id; } set { _id = value; } }

        private int _bagunid = -1;
        public int BagUNID { get { return _bagunid; } set { _bagunid = value; } }

        private decimal _bagamt = -1;
        public decimal BagAmount { get { return Math.Round(_bagamt, 2); } set { _bagamt = value; } }
    }

    public class VarianceActivityResponseSectionDetails
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _id = 0;
        public int ID { get { return _id; } set { _id = value; } }

        private List<Section> _sectiondetails = new List<Section>();
        public List<Section> SectionDetails { get { return _sectiondetails; } set { _sectiondetails = value; } }

    }

    public class VarianceActivityResponseDenominations
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<Denominations> _listofdenominations = new List<Denominations>();
        public List<Denominations> ListOfDenominations { get { return _listofdenominations; } set { _listofdenominations = value; } }
    }

    public class VarianceActivityResponseCompositions
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _compositionID =-1;
        public int CompositionID { get { return _compositionID; } set { _compositionID = value; } }

        private string _compositionName = String.Empty;
        public string CompositionName { get { return _compositionName; } set { _compositionName = value; } }

        private decimal _compositionValue = 0;
        public decimal CompositionValue { get { return Math.Round(_compositionValue, 2); } set { _compositionValue = value; } }

        private List<Composition_Details> _compositions = new List<Composition_Details>();
        public List<Composition_Details> Compositions { get { return _compositions; } set { _compositions = value; } }
    }

    public class VarianceActivityResponseCompositionDetails
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _compositionID = -1;
        public int CompositionID { get { return _compositionID; } set { _compositionID = value; } }

        private string _compositionName = String.Empty;
        public string CompositionName { get { return _compositionName; } set { _compositionName = value; } }

        private decimal _compositionValue = 0;
        public decimal CompositionValue { get { return Math.Round(_compositionValue, 2); } set { _compositionValue = value; } }

        private List<Denominations> _denominationDetails = new List<Denominations>();
        public List<Denominations> DenominationDetails { get { return _denominationDetails; } set { _denominationDetails = value; } }
    }


    public class VarianceActivityResponseCompositionLocationJoins
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<LocationAndTerminal> _compositionLocationJoins = new List<LocationAndTerminal>();
        public List<LocationAndTerminal> CompositionLocationJoins { get { return _compositionLocationJoins; } set { _compositionLocationJoins = value; } }
    }

    public class VarianceActivityResponseBags
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<Bag> _bags = new List<Bag>();
        public List<Bag> Bags { get { return _bags; } set { _bags = value; } }

        private int _messageCode = 0;
        public int MessageCode { get { return _messageCode; } set { _messageCode = value; } }

        private string _messageText = "";
        public string MessageText { get { return _messageText; } set { _messageText = value; } }
    }

    public class VarianceActivityResponseBagComparisons
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<BagComparisonPair> _bag_comparisons = new List<BagComparisonPair>();
        public List<BagComparisonPair> Bag_Comparisons { get { return _bag_comparisons; } set { _bag_comparisons = value; } }

        private int _messageCode = 0;
        public int MessageCode { get { return _messageCode; } set { _messageCode = value; } }

        private string _messageText = "";
        public string MessageText { get { return _messageText; } set { _messageText = value; } }
    }

    public class VarianceActivityResponseKioskSummary
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _messageCode = 0;
        public int MessageCode { get { return _messageCode; } set { _messageCode = value; } }

        private string _messageText = "";
        public string MessageText { get { return _messageText; } set { _messageText = value; } }

        //KioskProperties
        private Bag _properties = new Bag();
        public Bag Properties { get { return _properties; } set { _properties = value; } }

        //List<KioskHistory>
        private List<BagAction> _kioskhistory = new List<BagAction>();
        public List<BagAction> KioskHistory { get { return _kioskhistory; } set { _kioskhistory = value; } }
        
        //List<CashedInChildren>
        private List<BagAction> _cashedinchildren = new List<BagAction>();
        public List<BagAction> CashedInChildren { get { return _cashedinchildren; } set { _cashedinchildren = value; } }

        //int totalChildBagsCashedIn 
        private int _childrenused = 0;
        public int ChildrenUsed { get { return _childrenused; } set { _childrenused = value; } }

        //int KioskStartBags
        private int _startingchildren = 0;
        public int StartingChildren { get { return _startingchildren; } set { _startingchildren = value; } }

        //int KioskEndBags 
        private int _endingchildren = 0;
        public int EndingChildren { get { return _endingchildren; } set { _endingchildren = value; } }

        //decimal starting value
        private decimal _compositionvalue = 0;
        public decimal CompositionValue { get { return Math.Round(_compositionvalue, 2); } set { _compositionvalue = value; } }

    }


    public class VarianceActivityResponseEmployee
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private Employees _employee = new Employees();
        public Employees Employee { get { return _employee; } set { _employee = value; } }

        private int _messageCode = 0;
        public int MessageCode { get { return _messageCode; } set { _messageCode = value; } }

        private string _messageText = "";
        public string MessageText { get { return _messageText; } set { _messageText = value; } }
    }


    public class VarianceActivityResponsePrintInfo
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private PrintInfo _printInformation = new PrintInfo();
        public PrintInfo PrintInformation { get { return _printInformation; } set { _printInformation = value; } }

        private int _messageCode = 0;
        public int MessageCode { get { return _messageCode; } set { _messageCode = value; } }

        private string _messageText = "";
        public string MessageText { get { return _messageText; } set { _messageText = value; } }
    }

    public class VarianceActivityResponseBagChildren
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<BagJoins> _bagJoins = new List<BagJoins>();
        public List<BagJoins> BagJoins { get { return _bagJoins; } set { _bagJoins = value; } }

        private int _messageCode = 0;
        public int MessageCode { get { return _messageCode; } set { _messageCode = value; } }

        private string _messageText = null;
        public string MessageText { get { return _messageText; } set { _messageText = value; } }
    }

    public class VarianceActivityResponsShiftData
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<Sum_Shift_Data> _shift_data = null;
        public List<Sum_Shift_Data> Shift_Data { get { return _shift_data; } set { _shift_data = value; } }

        private int _messageCode = 0;
        public int MessageCode { get { return _messageCode; } set { _messageCode = value; } }

        private string _messageText = null;
        public string MessageText { get { return _messageText; } set { _messageText = value; } }
    }


    public class VarianceActivityResponseLocationsAndTerminal
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<LocationAndTerminal> _locationsTerminals = null;
        public List<LocationAndTerminal> LocationsTerminals { get { return _locationsTerminals; } set { _locationsTerminals = value; } }

        private int _messageCode = 0;
        public int MessageCode { get { return _messageCode; } set { _messageCode = value; } }

        private string _messageText = null;
        public string MessageText { get { return _messageText; } set { _messageText = value; } }

    }

    public class VarianceActivityResponseTerminals
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<LocationAndTerminal> _locations = null;
        public List<LocationAndTerminal> Locations { get { return _locations; } set { _locations = value; } }

    }

    public class VarianceActivityResponseTerminalOverview
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<TerminalOverview> _terminalresults = null;
        public List<TerminalOverview> TerminalResults { get { return _terminalresults; } set { _terminalresults = value; } }

    }

    public class TerminalOverview
    {
        private int _terminalid = 0;
        public int TerminalID { get { return _terminalid; } set { _terminalid = value; } }

        private bool _cashdropavailable = false;
        public bool CashDropAvailable { get { return _cashdropavailable; } set { _cashdropavailable = value; } }

        private decimal _cashdropamount = 0;
        public decimal CashDropAmount { get { return _cashdropamount; } set { _cashdropamount = value; } }

        private List<Bag> _availablebags = new List<Bag>();
        public List<Bag> AvailableBags { get { return _availablebags; } set { _availablebags = value; } }

    }

    public class VarianceActivityResponseActions
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<BagAction> _bagaction = new List<BagAction>();
        public List<BagAction> BagActions { get { return _bagaction; } set { _bagaction = value; } }

    }

    public class ErrorLog
    {
        private int _unid = 0;
        public int Unid { get { return _unid; } set { _unid = value; } }

        private int _event_id = 0;
        public int Event_ID { get { return _event_id; } set { _event_id = value; } }

        private int _activity_mode_id = 0;
        public int Activity_Mode_ID { get { return _activity_mode_id; } set { _activity_mode_id = value; } }

        private int _terminal_id = 0;
        public int Terminal_ID { get { return _terminal_id; } set { _terminal_id = value; } }

        private int _locationid = 0;
        public int Location_ID { get { return _locationid; } set { _locationid = value; } }

        private int _employee_id = 0;
        public int Employee_ID { get { return _employee_id; } set { _employee_id = value; } }

        private DateTime _created_at = DateTime.MinValue;
        public DateTime Created_At { get { return _created_at; } set { _created_at = value; } }

        private string _message = String.Empty;
        public string Message { get { return _message; } set { _message = value; } } 
    }


    public class BagOperators
    {
        private int _bag_unid = 0;
        public int bag_unid { get { return _bag_unid; } set { _bag_unid = value; } }

        private string _bag_label = "";
        public string bag_label { get { return _bag_label; } set { _bag_label = value; } }

        private int _from_location_id = 0;
        public int from_location_id { get { return _from_location_id; } set { _from_location_id = value; } }

        private int _function_id = 0;
        public int function_id { get { return _function_id; } set { _function_id = value; } }

        private int _to_location_id = 0;
        public int to_location_id { get { return _to_location_id; } set { _to_location_id = value; } }

        private int _from_terminal_id = 0;
        public int from_terminal_id { get { return _from_terminal_id; } set { _from_terminal_id = value; } }

        private int _event_id = 0;
        public int event_id { get { return _event_id; } set { _event_id = value; } }

        private int _created_by_employee = 0;
        public int created_by_employee { get { return _created_by_employee; } set { _created_by_employee = value; } }

        private int _action_id = 0;
        public int action_id { get { return _action_id; } set { _action_id = value; } }

        private int _parent_id = 0;
        public int parent_id { get { return _parent_id; } set { _parent_id = value; } }

        private bool _with_cash_variance = false;
        public bool with_cash_variance { get { return _with_cash_variance; } set { _with_cash_variance = value; } }

        private bool _with_bag_count_variance = false;
        public bool with_bag_count_variance { get { return _with_bag_count_variance; } set { _with_bag_count_variance = value; } }

        private bool _terminal_bags_only = false;
        public bool terminal_bags_only { get { return _terminal_bags_only; } set { _terminal_bags_only = value; } }

        private bool _kiosk_bags_only = false;
        public bool kiosk_bags_only { get { return _kiosk_bags_only; } set { _kiosk_bags_only = value; } }

        private bool _outbound_bags_only = false;
        public bool outbound_bags_only { get { return _outbound_bags_only; } set { _outbound_bags_only = value; } }

        private bool _inbound_bags_only = false;
        public bool inbound_bags_only { get { return _inbound_bags_only; } set { _inbound_bags_only = value; } }

        private bool _from_all_events = false;
        public bool from_all_events { get { return _from_all_events; } set { _from_all_events = value; } }

        private int _up_to_action = 0;
        public int up_to_action { get { return _up_to_action; } set { _up_to_action = value; } }
    }









    public class CashDrop
    {
        private int _locationid = 0;
        public int LocationID { get { return _locationid; } set { _locationid = value; } }

        private int _terminalid = 0;
        public int TerminalID { get { return _terminalid; } set { _terminalid = value; } }

        private int _employeeid = 0;
        public int EmployeeID { get { return _employeeid; } set { _employeeid = value; } }

        private decimal _amount = 0;
        public decimal Amount { get { return Math.Round(_amount, 2); } set { _amount = value; } } 
    }


    public class UpdateBagActionValue
    {
        private int _bag_action_unid = 0;
        public int Bag_Action_Unid { get { return _bag_action_unid; } set { _bag_action_unid = value; } }

        private int _employee_id = 0;
        public int Employee_ID { get { return _employee_id; } set { _employee_id = value; } }

        private decimal _bag_value = 0;
        public decimal Bag_Value { get { return _bag_value; } set { _bag_value = value; } }
    }

    public class VarianceActivityResponseIncomingSummary
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private List<IncomingTerminalSummary> _terminal_summary = null;
        public List<IncomingTerminalSummary> Terminal_Summary { get { return _terminal_summary; } set { _terminal_summary = value; } }

    }


    public class IncomingTerminalSummary
    {
        private int _terminal_id = 0;
        public int Terminal_ID { get { return _terminal_id; } set { _terminal_id = value; } }

        private string _terminal_name = null;
        public string Terminal_Name { get { return _terminal_name; } set { _terminal_name = value; } }

        private List<IncomingTerminalBagSummary> _incoming_bags = null;
        public List<IncomingTerminalBagSummary> Incoming_Bags { get { return _incoming_bags; } set { _incoming_bags = value; } }
    }

    public class IncomingTerminalBagSummary
    {
        private int _bag_action_id = 0;
        public int Bag_Action_ID { get { return _bag_action_id; } set { _bag_action_id = value; } }

        private int _bag_function_id = 0;
        public int Bag_Function_ID { get { return _bag_function_id; } set { _bag_function_id = value; } }

        private int _bag_unid = 0;
        public int Bag_Unid { get { return _bag_unid; } set { _bag_unid = value; } }

        private string _bag_id = null;
        public string Bag_ID { get { return _bag_id; } set { _bag_id = value; } }

        private decimal _bag_system_value = 0;
        public decimal Bag_System_Value { get { return _bag_system_value; } set { _bag_system_value = value; } }

        private decimal _bag_counted_value = 0;
        public decimal Bag_Counted_Value { get { return _bag_counted_value; } set { _bag_counted_value = value; } }

        private int _bag_action_unid = 0;
        public int Bag_Action_Unid { get { return _bag_action_unid; } set { _bag_action_unid = value; } }

        private string _created_time = null;
        public string Created_Time { get { return _bag_id; } set { _bag_id = value; } }
    }



    public class ProfitCenterValues
    {
        public int prof_center_dim_id { get; set; }
        public int prof_center_id { get; set; }
        public string prof_center_name { get; set; }
    }

    public class LocationValues
    {
        public int prof_center_id { get; set; }
        public string prof_center_name { get; set; }
        public int is_warehouse { get; set; }
    }

    public class BagJoins
    {
        public int Event_ID { get; set; }
        public int Parent_ID { get; set; }
        public int Child_ID { get; set; }

        public BagJoins()
        {
        }

        public BagJoins(int event_id, int parent_ID, int child_ID)        
        {
            Event_ID = event_id;
            Parent_ID = parent_ID;
            Child_ID = child_ID;
        }
    }

    public class TerminalValues
    {
        public int terminal_id { get; set; }
        public int terminal_dim_id { get; set; }
        public int prof_center_id { get; set; }
        public int prof_center_dim_id { get; set; }
        public string terminal_name { get; set; }
    }

    public class BagAction
    {
        public string Bag_ID { get; set; }
        public int Action_ID { get; set; }
        public string Action_Name { get; set; }
        public string Start_time { get; set; }
        public string End_time { get; set; }
        public int Sending_Employee_ID { get; set; }
        public int Receiving_Employee_ID { get; set; }
        public int Terminal_ID { get; set; }
        public int Location_ID { get; set; }

        private decimal _total_value = 0;
        public decimal Total_Value { get { return Math.Round(_total_value, 2); } set { _total_value = value; } }

        public int Bag_Count { get; set; }

        public int Bag_Unid { get; set; }
        public int Bag_Action_Unid { get; set; }
        public string label_id { get; set; }

        public string sender_name { get; set; }
        public string reciever_name { get; set; }

        public int function_id { get; set; }
        public string function_name { get; set; }

        public string terminal_name { get; set; }
        public string location_name { get; set; }

        public int status_id = -1; //set it to -1 by default as there may be code the use bag actions without setting status_id
        public string status_name { get; set; }
    }

    public class PrintInfo
    {
        public string Event_Name { get; set; }
        public string Date { get; set; }
        public string Bag_ID { get; set; }
        public int Emp_ID { get; set; }
        public string Employee_name { get; set; }
        public int Terminal_ID { get; set; }
        public int Location_ID { get; set; }
        public string Origin_Name { get; set; }
        public string Destination_Name { get; set; }
    }

    public class unidLabel
    {
        public string label_id { get; set; }
        public int bag_unid { get; set; }
    }

    public class BagComparisonPair
    {
        public List<Bag> bag_pair { get; set; }
        public decimal amount_difference { get; set; }
        public int count_difference { get; set; }
    }

    public class Bag
    {
        public int event_id { get; set; }
        public string bag_type { get; set; }
        public string bag_id { get; set; }
        public int origin_location_id { get; set; }
        public int destination_location_id { get; set; }
        public int composition_id { get; set; }

        public string label_id { get; set; }
        public string created_date { get; set; }
        public string end_date { get; set; }
        public int employee_id { get; set; }

        public string event_name { get; set; }
        public string composition_name { get; set; }
        public string origin_name { get; set; }
        public string destination_name { get; set; }

        private decimal _composition_value = 0;
        public decimal composition_value { get { return Math.Round(_composition_value, 2); } set { _composition_value = value; } }

        public int bag_unid { get; set; }
        public int bag_function_id { get; set; }

        private decimal _bag_value_variance = 0;
        public decimal bag_value_variance { get { return Math.Round(_bag_value_variance, 2); } set { _bag_value_variance = value; } }

        public int bag_count_variance { get; set; }

        public string employee_name { get; set; }
        public string bag_function_name { get; set; }

        public int parent_id { get; set; }

        private int _pos_action_unid = 0;
        public int pos_action_unid { get { return _pos_action_unid; } set { _pos_action_unid = value; } }



        public List<BagAction> bag_actions { get; set; }



        //below values to be consolidated into List<BagAction>
        public int action_id { get; set; }
        public string action_name { get; set; }

        public int status_id { get; set; }
        public string status_name { get; set; }

        private decimal _current_value = 0;
        public decimal current_value { get { return Math.Round(_current_value, 2); } set { _current_value = value; } }

        public int total_child_bags { get; set; }
        public int terminal_id { get; set; }

        public int profit_centre_dim_id { get; set; }
        public int terminal_dim_id { get; set; }

        //private decimal _starting_bag_value = 0;
        //public decimal starting_bag_value { get { return Math.Round(_starting_bag_value, 2); } set { _starting_bag_value = value; } }

        //public int starting_bag_count { get; set; }

        public Bag()
        {
            bag_actions = new List<BagAction>();
        }
    }

    public class LocationAndTerminal
    {
        public int location_id { get; set; }
        public string location_name { get; set; }
        public int composition_id { get; set; }
        public string composition_name { get; set; }
        public int terminal_id { get; set; }
        public string terminal_name { get; set; }

        public string bag_id { get; set; }
        public string label_id { get; set; }

        public int bag_unid { get; set; }

        public int open_terminal_count { get; set; }
    }

    public class Denominations
    {
        public int denomination_id { get; set; }
        public string denomination_name { get; set; }
        public string denomination_format { get; set; }

        private decimal _denomination_value = 0;
        public decimal denomination_value { get { return Math.Round(_denomination_value, 2); } set { _denomination_value = value; } }

        public int denomination_qty { get; set; }
        private decimal _denomination_total_value = 0;
        public decimal denomination_total_value { get { return Math.Round(_denomination_total_value, 2); } set { _denomination_total_value = value; } }
    }

    public class Composition_Details
    {
        public int composition_id { get; set; }
        public string composition_name { get; set; }

        private decimal _composition_value = 0;
        public decimal composition_value { get { return Math.Round(_composition_value, 2); } set { _composition_value = value; } }
        
        public int denomination_id { get; set; }
        public int denomination_qty { get; set; }
        public string denomination_description { get; set; }
        public string denomination_format { get; set; }

        private decimal _denomination_value = 0;
        public decimal denomination_value { get { return Math.Round(_denomination_value, 2); } set { _denomination_value = value; } }

        private decimal _denomination_total_value = 0;
        public decimal denomination_total_value { get { return Math.Round(_denomination_total_value, 2); } set { _denomination_total_value = value; } }
    }

    public class Employees
    {
        public int Employee_ID { get; set; }
        public string Employee_firstname { get; set; }
        public string Employee_surname { get; set; }
        public int Job_Code_ID { get; set; }
        public string Job_Code_Name { get; set; }        
    }

    public class Sum_Shift_Data
    {

        private decimal _loan_amt = 0;
        public decimal loan_amt { get { return Math.Round(_loan_amt, 2); } set { _loan_amt = value; } }
        private decimal _withdrawal_amt = 0;
        public decimal withdrawal_amt { get { return Math.Round(_withdrawal_amt, 2); } set { _withdrawal_amt = value; } }
        private decimal _cash_drop_amt = 0;
        public decimal cash_drop_amt { get { return Math.Round(_cash_drop_amt, 2); } set { _cash_drop_amt = value; } }

        public int terminal_id { get; set; }        
        public string created_date { get; set; }        
        public int employee_id { get; set; }
    }

    public class Section
    {
        public string section_name { get; set; }
        public int section_id { get; set; }
        List<int> section_locations { get; set; }
        List<Bag> section_Kiosk_Bags { get; set; }
    }




    public class constants
    {
        public const string BT1 = "Kiosk";
        public const string BT2 = "Terminal";
        public const string BT3 = "Return";

        public const int CashOfficeID = 161;

        public const string NoExistingBags = "There are no bags for the current event. Do you wish to create a new set of default bags?";
        public const string ExistingBagsNoAction = "Bags have already been created for the current event, but currently have no actions against them. Do you wish to delete these bags and create a new set of default bags?";
        public const string ExistingBagsWithAction = "Bags have already been created for the current event, and already have actions against them. Unable to delete";

        public const int SiteLicence = 1; //1 = man city 

    }


    public class ConfigSettings
    {

        private int _cashOfficeID = 0;
        public int CashOfficeID { get { return _cashOfficeID; } set { _cashOfficeID = value; } }

        private int _cashOfficeTerminalID = 0;
        public int CashOfficeTerminalID { get { return _cashOfficeTerminalID; } set { _cashOfficeTerminalID = value; } }

        private string _serverIP = "";
        public string ServerIP { get { return _serverIP; } set { _serverIP = value; } }

        private string _serverUsername = "";
        public string ServerUsername { get { return _serverUsername; } set { _serverUsername = value; } }

        private string _serverPassword = "";
        public string ServerPassword { get { return _serverPassword; } set { _serverPassword = value; } }

        private string _validEmployees = "";
        public string ValidEmployees { get { return _validEmployees; } set { _validEmployees = value; } }

        private string _webServiceAddress = "";
        public string WebServiceAddress { get { return _webServiceAddress; } set { _webServiceAddress = value; } }

        private int _cashTenderID = 0;
        public int CashTenderID { get { return _cashTenderID; } set { _cashTenderID = value; } }

        private int _primoInstance = 0;
        public int PrimoInstance { get { return _primoInstance; } set { _primoInstance = value; } }

        private string _cashOfficeName = "";
        public string CashOfficeName { get { return _cashOfficeName; } set { _cashOfficeName = value; } }

        private string _cashOfficeTerminalName = "";
        public string CashOfficeTerminalName { get { return _cashOfficeTerminalName; } set { _cashOfficeTerminalName = value; } }
    }

    public class ConnectionString
    {
        public string connection_string { get; set; }
        public string database_name { get; set; }
        public int database_instance { get; set; }
    }

    public class terminal_variance
    {
        public decimal terminal_variance_value_outbound { get; set; }
        public decimal terminal_variance_value_inbound { get; set; }
        public decimal terminal_variance_value { get; set; }
        public bool all_bags_complete { get; set; }
       // public List<Bag> terminal_bags { get; set; }
    }

    public class location_variance
    {
        public decimal location_amount_variance_outbound = 0;
        public int location_count_variance_outbound = 0;
        public decimal location_amount_variance_inbound = 0;
        public int location_count_variance_inbound = 0;
        public decimal location_amount_variance = 0;
        public int location_count_variance = 0;
        public bool all_bags_complete = true;
        public List<terminal_variance> location_terminals { get; set; }
    }

    public class venue_variance
    {
        public decimal venue_amount_variance { get; set; }
        public bool all_bags_complete { get; set; }
        public int venue_count_variance { get; set; }
        public List<location_variance> venue_locations { get; set; }
    }

    public class VarianceActivityResponseVariance
    {
        private bool _status = true;
        public bool status { get { return _status; } set { _status = value; } }

        private string _errorText = String.Empty;
        public string errorText { get { return _errorText; } set { _errorText = value; } }

        private int _id = 0;
        public int ID { get { return _id; } set { _id = value; } }

        private venue_variance _venue_total_variance;

        public venue_variance Venue_Total_Variance { get { return _venue_total_variance; } set { _venue_total_variance = value; } }
    }

    //public class LocationCompositionJoin
    //{
    //    public int location_id { get; set; }
    //    public string location_name { get; set; }
    //    public int composition_id { get; set; }
    //    public string composition_name { get; set; }
    //}

    #endregion

}