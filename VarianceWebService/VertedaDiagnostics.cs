﻿using System;
using System.Collections.Generic;
using System.Text;
using Agilysys_Code_Library.Logging;
using System.Net.Sockets;
using System.Net.Mail;
using System.Xml;
using System.Net;
using System.IO;
using Bugsnag.Clients;

namespace MenuSync
{

    class Settings
    {
        private String mailToAddress="";
        private String mailFromAddress="";
        private String password="";
        private String siteName="";

        public String SiteName
        {
            get { return siteName; }
            set { siteName = value; }
        }

        public String Password
        {
            get { return password; }
            set { password = value; }
        }

        public String MailFromAddress
        {
            get { return mailFromAddress; }
            set { mailFromAddress = value; }
        }

        public String MailToAddress
        {
            get { return mailToAddress; }
            set { mailToAddress = value; } 
        }
    }
   
    class VertedaDiagnostics
    {
        public String logFileName = "";
        public static Settings GetStaticSettings()
        {
            Settings myResponse = new Settings();

            try
            {

                string path = "C:\\tsm sync";

                XmlDocument doc = new XmlDocument();
                doc.Load(path + "\\diagnostics_config.xml");


                XmlNodeList nodeList = doc.SelectNodes("//site_name");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.SiteName = node.InnerText;
                }

                nodeList = doc.SelectNodes("//mail_from_address");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.MailFromAddress = node.InnerText;
                }

                nodeList = doc.SelectNodes("//mail_to_address");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.MailToAddress = node.InnerText;
                }

                nodeList = doc.SelectNodes("//key");
                foreach (XmlNode node in nodeList)
                {
                    myResponse.Password = node.InnerText;
                }
            }
            catch (Exception yex)
            {
                
            }

            return myResponse;
        }


        public static void ErrorMailer( String strMessage)
        {
            LogMessage("Sending error report email","I",true);
            Settings mySettings = GetStaticSettings();
            try
            {
                var fromAddress = new MailAddress(mySettings.MailFromAddress, "Verteda Error Reporting");
                var toAddress = new MailAddress(mySettings.MailToAddress, "To Name");
                string fromPassword = mySettings.Password;
                string subject = "V-E-R: " + mySettings.SiteName + " (" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name+ ")";
                string body = strMessage;

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                MailMessage message = new MailMessage(fromAddress, toAddress);
                message.Subject = subject;
                message.Body = body;

               
                try
                {
                    message.Attachments.Add(new Attachment("screenshot.png"));
                }
                catch (Exception jex)
                {
                }
                smtp.Send(message);


            }
            catch (Exception ex)
            {
                
            }
            LogMessage("Sent error report email", "I", true);
            
        }

        public static void VertedaErrorHandler(String errorDetails, bool sendReport, bool notifyUser)
        {
            LogMessage("-------------ERROR:" + errorDetails, "E", true);
            LogMessage("STARTED ERROR HANDLING", "I", true);

            String mailBody = "";

            mailBody = "Application Error Report" + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Application Name:" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Appication Version:" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Terminal IP Address:";

            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    mailBody = mailBody + ip.ToString() + ", ";
                }
            }

            mailBody = mailBody + Environment.NewLine;

            mailBody = mailBody + "Error occured:" + DateTime.Now.ToShortDateString() + "  " + DateTime.Now.ToShortTimeString() + Environment.NewLine;
            

            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Error details:" + Environment.NewLine;
            mailBody = mailBody + errorDetails;

            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "END OF MESSAGE" + Environment.NewLine;

            if (sendReport)
            {
                ErrorMailer(mailBody);
            }
            

            if (notifyUser)
            {
              
            }
            LogMessage("ENDED ERROR HANDLING", "I", true);

        }

       
        public static void VertedaExceptionHandler( Exception ex, bool sendReport, bool notifyUser, bool shutdown)
        {
            LogMessage("-------------ERROR:" + ex.Message, "E", true);
            LogMessage(ex.StackTrace, "E", true);
            LogMessage("STARTED ERROR HANDLING", "I", true);

           

            String mailBody = "";

            mailBody = "Application Exception Report" + Environment.NewLine+Environment.NewLine;
            mailBody = mailBody + "Application Name:" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Appication Version:" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Terminal IP Address:";

            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    mailBody = mailBody+ ip.ToString()+", ";
                }
            }

            mailBody = mailBody + Environment.NewLine;

            mailBody = mailBody + "Exception occured:" + DateTime.Now.ToShortDateString() + "  " + DateTime.Now.ToShortTimeString()+ Environment.NewLine;
            


            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Exception details:" + Environment.NewLine;
            mailBody = mailBody + ex.Message;

            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Exception Stack Trace:" + Environment.NewLine;
            mailBody = mailBody + ex.StackTrace;

            
            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Exception Source:" + Environment.NewLine;
            mailBody = mailBody + ex.Source;

            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "END OF MESSAGE" + Environment.NewLine;

            if (sendReport)
            {
                ErrorMailer(mailBody);
            }
            
            if (shutdown)
            {
              
            }
            else
            {
                if (notifyUser)
                {
                   
                }
            }
            LogMessage("ENDED ERROR HANDLING", "I", true);

        }


        public static void VertedaUHExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception ex = (Exception)args.ExceptionObject;
            LogMessage("-------------ERROR:" + ex.Message, "E", true);
            LogMessage(ex.StackTrace, "E", true);

            LogMessage("STARTED ERROR HANDLING", "I", true);


           
            
            String mailBody = "";

            mailBody = "Application Unhandled Exception Report" + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Application Name:" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Appication Version:" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Terminal IP Address:";

            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    mailBody = mailBody + ip.ToString() + ", ";
                }
            }

            mailBody = mailBody + Environment.NewLine;

            mailBody = mailBody + "Exception occured:" + DateTime.Now.ToShortDateString() + "  " + DateTime.Now.ToShortTimeString() + Environment.NewLine;



            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Exception details:" + Environment.NewLine;
            mailBody = mailBody + ex.Message;

            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Exception Stack Trace:" + Environment.NewLine;
            mailBody = mailBody + ex.StackTrace;


            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "Exception Source:" + Environment.NewLine;
            mailBody = mailBody + ex.Source;

            mailBody = mailBody + Environment.NewLine + Environment.NewLine;
            mailBody = mailBody + "END OF MESSAGE" + Environment.NewLine;

       
            ErrorMailer(mailBody);
            LogMessage("ENDED ERROR HANDLING", "I", true);


         

        }

        public static event EventHandler MessageFired;
        private static String newMessage;
        public static String NewMessage
        {
            get { return newMessage; }
            set
            {
                newMessage = value;
                MessageFired(newMessage, new EventArgs());
            }
        }

        /// <summary>
        /// Opens a log file
        /// </summary>
        /// <param name="logLocation">The location of the log file</param>
        /// <param name="logFileName">The file name for the log file</param>
        public static void OpenLog(string logLocation, string logFileName)
        {
            TextFileLogging.open_logfile(logLocation, logFileName);
           
            
        }

        /// <summary>
        /// Closes the currently open log file
        /// </summary>
        public static void CloseLog()
        {
            TextFileLogging.close_logfile();
        }

        /// <summary>
        /// Logs a message to the text file log.  Also will fire the event handler if Broadcast = true 
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="messsageType">The type of message "I" = info "E" = error</param>
        /// <param name="Broadcast">Whether to update the string with event handler attached</param>
        public static void LogMessage(String message, String messsageType, Boolean Broadcast)
        {
            try
            {
                TextFileLogging.write_to_log(message, messsageType);

                if (Broadcast == true)
                    NewMessage = DateTime.Now.TimeOfDay.ToString() + " : " + message;
            }
            catch (Exception)
            {
                //don't worry!
            }

        }

    }
}
