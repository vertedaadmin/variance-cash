﻿USE [variance_cash]
GO

/****** Object:  Table [dbo].[Bag_Dim]    Script Date: 22/06/2016 12:11:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Bag_Dim](
	[unid] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[event_id] [int] NOT NULL,
	[bag_type] [nchar](10) NOT NULL,
	[bag_id] as LEFT([bag_type], 1) + CAST([unid] as varchar(16)),
	[label_id] [nchar](20) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by_employee] [int] NOT NULL,
	[profit_center_id] [int] NOT NULL,
	[composition_id] [int] NOT NULL
) ON [PRIMARY]

GO




SELECT [venue_location_master].[id]
      ,[venue_location_master].[location_name]
  FROM [verteda_rts_v4].[dbo].[venue_location_master]
  WHERE NOT EXISTS (
  SELECT 1 
  FROM [verteda_rts_v4].[dbo].[event_closed_locations_master]
  WHERE [venue_location_master].[id] = [event_closed_locations_master].[location_id]
  AND [event_closed_locations_master].[event_id] = 104)




